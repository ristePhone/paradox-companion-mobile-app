package com.paradoxplaza.pdxcompanion;
import com.paradoxplaza.pdxcompanion.BuildConfig;


import com.babisoft.ReactNativeLocalization.ReactNativeLocalizationPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.brentvatne.react.ReactVideoPackage;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.imagepicker.ImagePickerPackage; // <-- add this import
import com.RNFetchBlob.RNFetchBlobPackage;         
import java.util.Arrays;
import java.util.List;

import com.reactnativenavigation.NavigationApplication;

public class MainApplication extends NavigationApplication {

  @Override
  public boolean isDebug() {
    if (BuildConfig.DEBUG) {
      return true;
    }

    return false;
  }

  protected List<ReactPackage> getPackages() {
    return Arrays.<ReactPackage>asList(
            new MainReactPackage(),
            new RNFetchBlobPackage(),
            new ImagePickerPackage(),
            new ReactNativeLocalizationPackage(),
            new VectorIconsPackage(),
            new ReactVideoPackage(),
            new RNDeviceInfo(),
            new CustomConfigurationPackage()
    );
  }

  @Override
  public List<ReactPackage> createAdditionalReactPackages() {
    return getPackages();
  }


  @Override
  public String getJSMainModuleName() {
    return "index";
  }
}