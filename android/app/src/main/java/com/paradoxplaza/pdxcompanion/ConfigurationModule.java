package com.paradoxplaza.pdxcompanion;

import com.paradoxplaza.pdxcompanion.BuildConfig;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.util.Map;
import java.util.HashMap;

public class ConfigurationModule extends ReactContextBaseJavaModule {

    @Override
    public String getName() {
        return "ConfigurationModule";
    }

    public ConfigurationModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public Map<String, Object> getConstants() {
        final Map<String, Object> constants = new HashMap<>();
        String productFlavor = BuildConfig.FLAVOR;
        constants.put("environment", productFlavor);
        return constants;
    }
}
