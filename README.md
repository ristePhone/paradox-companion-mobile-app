
# Techical documentation

Documentation for developers involved in the React Native Application for Paradox Interactive

For project setup, refer to [Project Setup](Documentation/Project_setup.md).

##  Content

- [React Native Libraries](#React\ Native\ Libraries)
- [IOS](#IOS)
    - [Instaling](#Instaling)
- [Android](#Android)
    - [Create release build](#Create\ release\ build)
- [React Native](#React\ Native)
    - [App structure](#App\ structure)
    - [Creating new feature](#Creating\ new\ feature)
    - [Register new screen](#Register\ new\ screen)
    - [Update Configuration](#Update\ Configuration) 
- [Troubleshooting](#Troubleshooting)
- [TO DO](#TO\ DO)

## React Native Libraries

| Library | Link | Version | Used for | Helper links |
| :--- | :---: | :---: | :--- | -- |
| `glamorous-native` |https://github.com/robinpowered/glamorous-native | 1.1.1 | Styling the components. Mostly used for styling the UI | 
|`ReactNativeLocalization`|https://github.com/stefalda/ReactNativeLocalization | 1.x | String localization. |
|`react-native-splash-screen`|https://github.com/crazycodeboy/react-native-splash-screen|3.x| Control over the SplashScreen, moslty duration, if we want to finish some task before we close it|
|`react-native-navigation`| https://github.com/wix/react-native-navigation | 1.x | *To be implemented. Will replace react-navigation*|
|`react-native-mail`|https://github.com/chirag04/react-native-mail| 3.0.6 | Native Feedback flow|
|`react-native-video`| https://github.com/react-native-community/react-native-video | x.x|Playing videos |
|`react-redux`| https://redux.js.org/basics/usage-with-react| x | Redux framework integration for application state |  
|`react-native-vector-icons`|https://github.com/oblador/react-native-vector-icons| 4.6.0| Navigation, TabBar icons, ect.|
|`generator-rn-toolbox`| https://github.com/bamlab/generator-rn-toolbox/blob/master/generators/assets/README.md https://github.com/bamlab/generator-rn-toolbox#features | x | Generation *splash_screen* and other *assets*| https://medium.com/@pqkluan/how-to-implement-splash-screen-in-react-native-navigation-ee2184a1a96 |
|`react-native-elements`|https://github.com/react-native-training/react-native-elements| 1.0 | Beatifull custom UI elements for React Native

## IOS
---

## Instaling

### Install Homebrew packate manager

Paste the following line at a Terminal prompt.

    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

Install jq

    brew install jq

Go to your project `<path to project>`

*Important*: **Install node_modules**. Do this everytime after you clone repo, because node_modules are added to `.gitIgnore` file

    npm install




# Android

## Create release build

First, create `.mainBundle` file using following command. Make sure you are in the `<project folder>`.

    react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/res

- *Note:* This is a hack for now, should be changed in the near future...

Then, go to Android Studio and click on **Build -> Generate Signed APK**

Then, import the `.keystore` file with the credentials. Complete the process.

# React Native

## Development Guidelines

---

### Update Configuration

Modify `config.json` file located in you `<project path>`.

After you modify, you must rebuild the app, so the effects should take place, and you can see the update in the `config` property of the `redux store`.

### App structure

    | app 
    -- | actions
    -- | components
            | login
            | sigup
            | news ect..
    -- | config
    -- | model
    -- | reducers
    -- | services
            | authentication.js        // Paradox authentication
            | social-authentication.js // Here we implement Facebook, Twitter, ect. 
    -- | resources
    -- | utils // to be removed
    | App.js
    | screens.js

Folders based on Redux flow: **actions**, **reducers**




### Creating new feature

For each feature create folder with `feature_name` (for example `login/`)

Inside you have 3 files: `container.js`, `screen,js`, and `styles.js`

| Component | Used for|
|--|--|
|`container.js` (Smart Component) | Contains the Redux connection to the screen generated from screen.js. Responsible for handling UI actions and getting data for Redux Store |
|`screen.js` (Dumb Component)| The UI for the Screen itself. Responsible for handling the UI |
|`styles.js`| Styling of the screen component using glamorous |

### Register new screen

Import and setup new screen to `screens.js` file, located in `app/` folder.

If you have the implementation like the previus step, you import the `container.js` from the feature folder.


## Why do we use this

 - Better code clarity and distributing 

## Tips

Install every package with --save command, so it can be added to `package.json` file

    npm install --save <dependency_name>

Reset node server

    npm start --reset--cashe

When you want to remove react native dependency from `package.json`, do the following

    // 1. in terminal
    react-native unlink <dependency_name>

    // 2. remove dependency for package.json

    // 3. Reload modules
    npm install
    // or
    yarn


# Troubleshooting

## Wix navigation

 - When adding custom fonts to your app, follow this link: https://wix.github.io/react-native-navigation/#/styling-the-navigator?id=custom-fonts

### Android

Known issues
- If custom fonts are not added, it shows white splash screen on Android
### IOS

---

# TO DO

- Android configuration with scripts
    - Curently we have to change manual the dev/stage/prod in the `config.json` file
