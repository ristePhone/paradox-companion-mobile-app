export default class NewsItem {
    constructor(props) {
        this.id = (props.id) ? props.id : ""
        this.headline = (props.headline) ? props.headline : ""
        this.bodyText = (props.bodyText) ? props.bodyText : ""
        this.publishDate = (props.publishDate) ? props.publishDate : null
        this.headerImage =  props.headerImage ?  props.headerImage : null
    }
}