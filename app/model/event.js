export default class Event {
    constructor(props) {
        this.eventHeadline = (props.eventHeadline) ? props.eventHeadline : ""
        this.eventDescription = (props.eventDescription) ? props.eventDescription : ""
        this.eventStart = (props.eventStart) ? props.eventStart : null
        this.eventFinish = (props.eventFinish) ? props.eventFinish : null
        this.eventLocation = (props.eventLocationFree) ?  props.eventLocationFree  : ""
        this.imageUrl = (props.imageUrl) ? props.imageUrl : ""
        this.eventsTags = (props.eventTags) ? props.eventTags : []
    }
}