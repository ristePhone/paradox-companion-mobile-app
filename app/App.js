import { registerScreens } from '../app/screens'
import Routes from './config/routes'
import { autoLogin, cleanUpCredentials } from './services/authentication'
import Colors from './config/colors'
import { YellowBox, Platform, AsyncStorage } from 'react-native'
import { Navigation } from 'react-native-navigation'
import { Provider, } from 'react-redux'
import { configurationSetup } from './config/config'
import { store } from './reducers/index'
import {
  UPDATE_CONFIGURATION,
  UPDATE_ROOT_CONTROLER,
  UPDATE_CONTENTFUL_NEWS,
  UPDATE_CONTENTFUL_FEEDBACK,
  UPDATE_USER_LOGGED_IN_STATUS,
  UPDATE_CONTENTFUL_WIKI,
  UPDATE_APPLICATION_ASSETS,
  UPDATE_HOME_SCREEN,
} from './actions/action-types'

// App icons
import Entypo from 'react-native-vector-icons/Entypo'

// Localization
import { strings } from './config/localization'

/**
 *  Generate navigation style, common for the application
 *  @return {Promise} - Returns the style of the application
 */
const appStyle = () => {
  return {
    animationType: 'fade',
    appStyle: {
      orientation: 'portrait',
      hideBackButtonTitle: true,
      navBarBackgroundColor: Colors.DARK_GRAY_COLOR,
      navBarButtonColor: 'white',
      screenBackgroundColor: Colors.BACKGROUND_COLOR,
      navBarTextColor: 'white',
      tabBarBackgroundColor: Colors.DARK_GRAY_COLOR,  // Must for Android
      tabBarSelectedButtonColor: Colors.ORANGE_COLOR,  // Must for Android
      tabBarButtonColor: 'white',  // Must for Android,
      animationType: 'fade',
      //Only Android
      navBarTitleTextCentered: true,
      forceTitlesDisplay: true,
    }
  }
}

/**
 *  Async function to load icons for tab bar, navigation bar ect.
 *  Every other icon need for NavigationBar or TabBar, should be added in this function...
 *   
 *  Wix Navigation currently has no support for vector icons, so this is a workaround.
 *  TO DO:
 *      - Replace when Wix implements this solution
 */
const getAppIcons = async () => {
  const iconSize = 20
  const homeIcon = await Entypo.getImageSource('home', iconSize)
  const newsIcon = await Entypo.getImageSource('news', iconSize)
  const feedbackIcon = await Entypo.getImageSource('megaphone', iconSize)
  const wikiIcon = await Entypo.getImageSource('network', iconSize)
  const moreIcon = await Entypo.getImageSource('user', iconSize)
  return { homeIcon, newsIcon, feedbackIcon, wikiIcon, moreIcon }
}

import MiddlewareAPI from './services/middleware/API'
import StorageService from './services/storage/index'
import config from './config/environment/config';
/**
 *  Setup app. Setup configuration, content and root controller.
 *  
 *  Props:
 *  @param {array} icons - Icons used for our tabBar controller
 */
const startApp = async () => {

  // Setup configuration
  const result = await configurationSetup()
  store.dispatch({ configuration: result, type: UPDATE_CONFIGURATION })

  // Update flag for first app launch
  const isFirstAppLaunch = await AsyncStorage.getItem(StorageService.keys.isFirstAppLaunch)
  if (isFirstAppLaunch == null) {
    await AsyncStorage.setItem(StorageService.keys.isFirstAppLaunch, 'false')
    store.dispatch({
      type: UPDATE_ROOT_CONTROLER,
      rootTypeController: 'singleScreenApp',
    })
  }

  // See if there is data in the storage...
  const retrieveData = await StorageService.retrieveAppData()
  if (retrieveData == null) {
    await updateAppData(null, false)
    return
  }

  // If there is data, try to get new one from server
  // And get data from server on AppLauch
  await updateAppData(retrieveData.data, true)
  await updateAppData(null, false)

  // Setup timer for retrieve new data
  scheduleDataRetrieveInterval(config.common.serverRetryDataInterval)

  // If not first appLaunch, try to auto-login
  const response = await autoLogin()
  const isAutoLoginSuccessfull = String(response.result).toLowerCase() == 'ok' ? true : false
  store.dispatch({
    type: UPDATE_USER_LOGGED_IN_STATUS,
    isLoggedIn: isAutoLoginSuccessfull
  })

  console.log('CURRENT STORE', store.getState())
}

const updateAppData = async (data = null, fromStorage = true) => {
  if (data == null && fromStorage == false) {
    data = await MiddlewareAPI.retriveAllData()
    await StorageService.storeAppData(data)
  }

  const { events, wiki, assets, news, notice } = data
  const { welcomeNotice, feedback } = notice
  const mainImageUrl = assets ? assets[0].url : ""
  const videoThumbnailUrl = assets ? assets[2].url : ""
  const videoUrl = assets ? assets[1].url : ""
  const homeScreen = {
    mainImageUrl,
    videoThumbnailUrl,
    videoUrl,
    welcomeNotice,
    events,
  }

  store.dispatch({ type: UPDATE_APPLICATION_ASSETS, assets })
  store.dispatch({ type: UPDATE_HOME_SCREEN, homeScreen })
  store.dispatch({ type: UPDATE_CONTENTFUL_NEWS, news })
  store.dispatch({ type: UPDATE_CONTENTFUL_WIKI, wiki })
  store.dispatch({ type: UPDATE_CONTENTFUL_FEEDBACK, feedback })
}

/**
 *  Setup root controler.
 * 
 *  Props:
 *  @param {string} rootType - Root type that we get from the redux store. Could be `tabBasedApp` or `singleScreenApp`
 *  @param {array} icons - The icons that we will use in our Navigation Controller
 */
const setupRootController = (rootType, icons) => {

  if (rootType === 'singleScreenApp') {
    Navigation.startSingleScreenApp({
      ...appStyle(),
      screen: {
        screen: Routes.LOGIN,
        navigatorStyle: {
          navBarHidden: true
        }
      },
      animationType: 'none'
    })

  } else {

    Navigation.startTabBasedApp({
      ...appStyle(),
      tabsStyle: {
        tabBarBackgroundColor: Colors.DARK_GRAY_COLOR,
        tabBarSelectedButtonColor: Colors.ORANGE_COLOR,
        tabBarButtonColor: 'white',
        tabBarTranslucent: false,
        initialTabIndex: 0,
      },
      tabs: [
        {
          label: strings.tabs.home,
          screen: Routes.HOME,
          icon: icons.homeIcon,
          navigatorStyle: {
            navBarHidden: true
          },
          overrideBackPress: false
        },

        {
          label: strings.tabs.news,
          screen: Routes.NEWS,
          icon: icons.newsIcon,
          title: strings.tabs.news,
          navigatorStyle: {
            navBarHidden: false
          },
          overrideBackPress: false
        },
        {
          label: strings.tabs.wiki,
          screen: Routes.WIKI,
          icon: icons.wikiIcon,
          title: strings.tabs.wiki,
          navigatorStyle: {
            navBarHidden: false
          },
          overrideBackPress: false
        },
        {
          label: strings.tabs.feedback,
          screen: Routes.FEEDBACK,
          title: strings.feedback,
          icon: icons.feedbackIcon,
          navigatorStyle: {
            navBarHidden: false
          },
          overrideBackPress: false
        },
        {
          label: strings.account_title,
          screen: Routes.MORE,
          icon: icons.moreIcon,
          title: strings.account_title,
          navigatorStyle: {
            navBarHidden: false
          },
          overrideBackPress: false
        },
      ]
    })
  }
}

// App interval to retrieve server data every hour during active app
let intervalRef = null
const scheduleDataRetrieveInterval = interval => {
  const minutes = interval * 60 * 1000
  intervalRef = setInterval(() => updateAppData(null, false), minutes)
}

/**
 *  Main module where all the setup is happening
 */
export const AppModule = async () => {

  // Intial setup
  cleanUpCredentials() // Old application remainings
  clearInterval(intervalRef)
  registerScreens(store, Provider)

  // On Android, there is a delay on start, so we use this isAppLauched() when we want to start the app...
  if (Platform.OS == 'ios') {
    startApp()
  } else {
    Navigation.isAppLaunched()
      .then((appLaunched) => {
        startApp()
      })
  }

  // Listen to event for change of the rootType
  store.subscribe(async () => {
    const root = store.getState().config.rootTypeController
    if (root != this.currentRoot) {
      this.currentRoot = root
      const icons = await getAppIcons()
      setupRootController(this.currentRoot, icons)
    }
  })
}

// Debugger warnings
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader'])