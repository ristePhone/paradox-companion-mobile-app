import { AsyncStorage } from 'react-native'

/**
 *  Keys used for data manipulation within AsyncStorage
 */
const keys = {
    storeData: 'storeData',
    isFirstAppLaunch: 'isFirstAppLaunch',
}

/**
 *  App data. Consist of object with following structure.
 *  Keys:
 *  @param {Date} lastModifiedDate - Always the current date, without long formating, just year, month, day, hour,
 *  @param {object} data - The data that need to be stored
 *
 */
const storeAppData = async (data) => {
    const storedDate = Date.now() // this is unix time
    const storedObject = {
        lastModifiedDate: storedDate,
        data: data,
    }
    const json = JSON.stringify(storedObject)
    const error = await AsyncStorage.setItem(keys.storeData, json)
    if (!error) {
        console.log('paradox: Success storing of data')
    } else {
        console.log('paradox: Faild to store of data')
    }

    return null
}

/**
 *  Retrieve all applicaliton data from local storage.
 *  This contains all the data like news, events, assets, items, ect,
 */
const retrieveAppData = async () => {
    const response = await AsyncStorage.getItem(keys.storeData)
    if (!response) {
        console.log('paradox: Failed retrieveing store data. No data stored:')
        return null
    }

    const retrievedData = JSON.parse(response)
    return retrievedData
}

const removeStoredData = async () => {
    const error = await AsyncStorage.removeItem(keys.storeData)
    return error
}

/**
 *  Method that is used to check if we should update date on certain interval
 *  The default interval is 1h
 *  Change the default intervalMinutes to change the range of update interval
 */
const shouldUpdateStoredData = async (intervalMinutes = 1) => {
    const { lastModifiedDate } = await retrieveAppData()
    const currentDate = Date.now()
    const diff = (currentDate - lastModifiedDate) / (1000 * 60)
    return parseInt(diff) > intervalMinutes
}

export default {
    keys,
    storeAppData,
    retrieveAppData,
    removeStoredData,
    shouldUpdateStoredData,
}