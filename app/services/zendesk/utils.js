import RNFetchBlob from 'rn-fetch-blob'
import { configurationSetup } from '../../config/config'
/**
 *  Sending image to Zendesk API
 * 
 *  Props:
 *  @prop {string} imageURL - Valid image url that we get from the image picker
 *  
 */
const sendImageToZendesk = async (imageURL) => {
    if (imageURL) {
        const { ZENDESK_BASIC_URL } = await configurationSetup()
        const baseUrl = `${ZENDESK_BASIC_URL}/api/v2/uploads.json?filename=attachment.png`
        const headers = {
            'Autorization': 'Basic cmlzdGUuc3Bhc2Vza2lAZGF0YWpvYi5zZS8yVzVKRGkxbGFLYVpnU0JHSVZWdWN4cmh3YUVNQ0t0b1AzMXkwWnFCOjJXNUpEaTFsYUthWmdTQkdJVlZ1Y3hyaHdhRU1DS3RvUDMxeTBacUI=',
            'Accept': 'application/json'
        }
    
        const wrapFileURL = RNFetchBlob.wrap(imageURL)
        return RNFetchBlob.fetch('POST',
            baseUrl,
            headers,
            wrapFileURL)
            .then(response => {
                return response.json()
            })
            .then(responseData => {
                return responseData
            })
            .catch(error => {
                return { error: error }
            })
    }

    return { error: 'No image url provided'}
}

export {
    sendImageToZendesk
}

// TO DO: TOMI:
// Use this imagePicker flow for usage of the function above...
// ImagePicker.launchImageLibrary(options, (response) => {
//     const fileUrl = response.origURL
//     call sendImageToZendesk(fileUrl)