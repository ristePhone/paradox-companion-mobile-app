import { store } from '../reducers/index'
import { AsyncStorage } from 'react-native'
import _ from 'lodash'

// Algorithm for creating HAWK authentication
const sha = require('sha.js')

// Retrieve baseUrl based on configuration that we get from the Store
const baseUrl = () => store.getState().config.BASE_URL

// User credentials
const cleanUpCredentials = async () => {
    return AsyncStorage.removeItem('credentials')
}

const retrieveCredentials = () => {
    return AsyncStorage.getItem('credentials', (error, result) => {
        return result
    })
}

const validateEmail = email => {
    let errMessage = null
    if (email && email != '') {
        let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        if (re.test(String(email).toLowerCase())) {
            errMessage = null
        } else {
            errMessage = 'Email format is not valid.\n\nPlease use valid format'
        }
    } else {
        errMessage = 'Email must not be empty'
    }

    return errMessage
}

// Create authentication header
const setupAuthorizationHeader = (email, password) => {
    return JSON.stringify({
        hawk: {
            email: email,
            'sha256(password+salt)': sha('sha256')
                .update(`${password}64DC4DC6508F4A9D823179D4D99DB4EE`, 'utf8')
                .digest('hex'),
        },
    })
}

const getStoredSession = async () => {
    return AsyncStorage.getItem('session').then(data => {
        if (data) {
            return JSON.parse(data)
        }

        return null
    })
}

/**
 * Session to be created after we get successful login, so the user can consume Paradox API
 * 
 * @param {string} username 
 * @param {string} password 
 */
const createSession = async (username, password) => {
    let url = `${baseUrl()}/accounts/sessions/octagon`
    return fetch(url, {
        method: 'PUT',
        headers: {
            'Authorization': setupAuthorizationHeader(username, password),
            'User-agent': 'octagon/0.0'
        }
    }).then(response => {
        return response.json()
    }).then(response => {
        console.log('Create sesion', response)
        return response
    })
}

/**
 *  Delete sesion (client). Log Out functionlity.
 */
const deleteSession = async () => {
    const session = await getStoredSession()
    console.log('STORED SESSION', session)
    const url = `${baseUrl()}/accounts/sessions/octagon`
    const authorizationHeader = { 'session': { 'token': session.token } }
    return fetch(url, {
        method: 'DELETE',
        headers: {
            'Authorization': JSON.stringify(authorizationHeader),
            'User-Agent': 'octagon/0.0'
        }
    }).then(response => {
        return response.json()
    }).then(response => {
        console.log('DELETE SESSION reponse', response)
        return response
    })
}

/**
 * Log user to the Paradox Companion application
 * 
 * @param {stirng} email 
 * @param {string} password 
 * @return {Promise} response - Returns response body as JSON
 */
const login = async (email, password) => {
    let url = `${baseUrl()}/accounts/sessions/accounts`
    return fetch(url, {
        method: 'PUT',
        headers: {
            'Authorization': setupAuthorizationHeader(email, password),
            'Content-type': 'application/json'
        }
    }).then(response => {
        return response.json()
    }).then(response => {
        console.log('Perfrom login', response)
        return response
    })
}

/**
 * Sign Up user to the Paradox Companion application
 * 
 * @param {ditionary} parameters - JSON structure based on API documentation
 * @returns {Promise}
 */
const signUp = async (parameters) => {
    let url = `${baseUrl()}/accounts`
    return fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(parameters)
    }).then(response => {
        return response.json()
    }).then(response => {
        return response
    })
}

/**
 * Auto login user every time user starts the app.
 * Fails if sessions is expired or has not stored any credentials
 * 
 * @returns {Promise} - JSON structure based on API documentation
 */
const autoLogin = async () => {
    const session = await getStoredSession()
    if (session) {
        const url = `${baseUrl()}/accounts/auth`
        const header = {
            'session': {
                'token': session.token
            }
        }
        return fetch(url, {
            method: 'GET',
            headers: { 'Authorization': JSON.stringify(header) }
        }).then(response => {
            return response.json()
        }).then(response => {
            console.log('AUTO LOGIN RESPONSE', response)
            return response
        }).catch(error => {
            console.log('AUTO LOGIN FAILED', error)
        })
    } else {
        const error = { error: 'No session token' }
        console.log('AUTO LOGIN FAILED', error)
        AsyncStorage.removeItem('username') //todo tomi: check this remove item
        
        return error
    }
}

/**
 *  Log Out functionality. Temporary solution.
 */
const logOut = async () => {
    return AsyncStorage.removeItem('session')
}

export {
    login,
    signUp,
    autoLogin,
    createSession,
    deleteSession,
    validateEmail,
    retrieveCredentials,
    cleanUpCredentials,
    logOut,
}