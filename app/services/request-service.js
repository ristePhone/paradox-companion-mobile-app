import React, { Component } from 'react'
import {
    StyleSheet,
    View,
    Text,
    Dimensions,
    TextInput,
    Image,
    ScrollView,
    TouchableOpacity,
    TouchableWithoutFeedback,
    TouchableNativeFeedback,
    Platform,
    Picker,
    KeyboardAvoidingView,
    Keyboard,
    Alert,
    TouchableHighlight,
    FlatList,
    ActivityIndicator
} from 'react-native'

import SpinnerView from '../components/views/spinner-view'
class RequestService {

    _showSpinner(){
        return <SpinnerView></SpinnerView>
    }

    post(url, params) {
        console.log('POSR REQUEST URL:', url, 'params:', params)
        return fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(params)
        })
            .then(response => { return response.json() })
            .then(response => {
                console.log('Reponse', response)
                return response
            })
            .catch(err => {
                console.log('Error', err)
            })
    }

    // get(url, params) {
    //     return fetch()
    //         .then(response => {
    //             console.log(response)
    //         })
    //         .then(response => {
    //             console.log(response)
    //         })
    //         .catch(err => {
    //             console.log(err)
    //         })
    // }   
    // ---------------------------------------------------------------------------------------
    // private methods
    // ---------------------------------------------------------------------------------------
     setHeaders(header)
    {
        const headers ={
            'Authorization' : header
        }
        return headers
    }
    setBody(bodinputObjecty){
        return  JSON.stringify(inputObject)
    }
    // ---------------------------------------------------------------------------------------
    // put methods
    // ---------------------------------------------------------------------------------------
    put(url, headers, inputObject ) {
        return fetch(url,{
            method: 'PUT',
            headers: headers,
            body: inputObject
        })
            .then(response => {
                console.log(response)
                return response.json()
            })  
            .then(response => {
                console.log(response)
                return response
            })
            .catch(err => {
                console.log(err)
            })
    }
    // ---------------------------------------------------------------------------------------
    // post methods
    // ---------------------------------------------------------------------------------------
    post1(url, headers, inputObject ) {
        return fetch(url,{
            method: 'POST',
            headers: headers,
            body: inputObject
        })
            .then(response => {
                console.log(response)
                return response.json()
            })  
            .then(response => {
                console.log(response)
                return response
            })
            .catch(err => {
                console.log(err)
            })
    }
    // ---------------------------------------------------------------------------------------
    // get methods
    // ---------------------------------------------------------------------------------------
    get(url, headers, inputObject ) {
        return fetch(url,{
            method: 'GET',
            headers: headers
        })
            .then(response => {
                console.log(response)
                return response.json()
            })
            .then(response => {
                console.log(response)
                return response
            })
            .catch(err => {
                console.log(err)
            })
    }

    async api(type, url, headers, inputObject ) {

        try{

        return fetch(url,{
            method: type,
            headers: headers,
            body: inputObject
        })
            .then(response => {
                console.log(response)
                return response.json()
            })
            .then(response => {
                console.log(response)
                response.ok = true
                return response
            })
             .catch(err => {
                 err.ok = false
                 console.log(err)
                 return err
             })
        }
        catch(error){
            return error
        }

    }
    // requestListTest(){
    //    // var url = 'https://data2278.zendesk.com/api/v2/requests.json?sort_by=created_at';
    //     var url = 'https://data2278.zendesk.com/api/v2/requests.json?status=open&sort_by=created_at';

    //     return fetch(url, {
    //         method: 'GET',
    //         headers: {
    //             'Authorization': 'Basic dG9taS5yaXN0b3Zza2kubWtAZ21haWwuY29tOmFzZGY4ODQ0QA',
    //             // 'Content-type': 'application/json'
    //         },
           
           
            
    //     }).then(response => {
    //         return response.json()
    //     }).then(response => { 
            
    //         return response
    //     }).catch(err => {
    //         console.log(err)
    //     })
    // }

    


}

export default new RequestService()