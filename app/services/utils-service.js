import _ from 'lodash'
import * as DeviceInfo from 'react-native-device-info'

const monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];

class UtilService {

    // Utils functions
    utislForFormatHour(objc) {
        if (objc < 10) {
            return `0${objc}`
        }

        return objc
    }

    // Used for UI and server API
    transformDate(selectedDate) {
        var date = new Date(selectedDate)
        var month = date.getMonth() + 1
        month = (month < 10) ? `0${month}` : month

        var day = date.getDate()
        day = (day < 10) ? `0${day}` : day

        return `${date.getFullYear()}-${month}-${day}`
    }
    transformDateEvents(selectedDate) {
        const monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ];
        var date = new Date(selectedDate)
        var month = date.getMonth() + 1
        month = (month < 10) ? `0${month}` : month

        var day = date.getDate()
        day = (day < 10) ? `0${day}` : day

        return `${monthNames[date.getMonth()]} ${day}, ${date.getFullYear()}`
    }
    formatDateToGMTOffset(selectedDate) {
        const monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ];
        var date = new Date(selectedDate)
        var GMT = this.getTimezoneOffset(date)
        var day = date.getDate()
        day = (day < 10) ? `0${day}` : day
        var hour = date.getHours()
        var minutes = date.getMinutes()
        let letFormatedMinutes = (i) => {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }
        let formatedMin = letFormatedMinutes(minutes)
        // function toLocaleTimeString() does not always work on Android devices
        // let timeINGMT = date.toLocaleTimeString(['en-GB'], {hour12: false, hour: '2-digit', minute: '2-digit'})
        return `${monthNames[date.getMonth()]} ${day}, ${date.getFullYear()} ${hour}:${formatedMin}h`
    }
    getTimezoneOffset(date) {
        function z(n) { return (n < 10 ? '0' : '') + n }
        var offset = new Date(date).getTimezoneOffset();
        var sign = offset < 0 ? '+' : '-';
        offset = Math.abs(offset);
        return sign + z(offset / 60 | 0) + z(offset % 60);
    }
    transformDateStart(selectedDate) {
        var date = new Date(selectedDate)
        var month = date.getMonth() + 1
        month = (month < 10) ? `0${month}` : month

        var day = date.getDate()

        day = (day < 10) ? `0${day}` : day
        var hour = date.getHours()
        var minuts = date.getMinutes()

        let letFormatedMinutes = (i) => {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }
        let fHours = letFormatedMinutes(hour)
        let res = letFormatedMinutes(minuts)
        return `${day}.${month}.${date.getFullYear()} ${fHours}:${res}`
    }


    getDeviceLocale() {
        return DeviceInfo.getDeviceLocale().split('-')[0]
    }


    // Using REGEX expresion as first line of check if email is valid, the second is server
    validateEmail(email, callback) {
        var errMessage = null
        if (email && email != '') {

            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            if (re.test(String(email).toLowerCase())) {
                errMessage = null
            } else {
                errMessage = 'Email format is not valid. \n Please use valid format'
            }
        } else {
            errMessage = 'Email must not be empty'
        }

        callback(errMessage)
    }

    /**
     * 
     *  Validate SingUp from
     *  @param params (Dictionary) - Keys: 
     *     email - Example: acme@acme.com 
     *     password
     *     dateOfBirth - Example format 2018-10-22
     *     country - Using country codes which are 2 letters word. Example Armenia is (AR)
     * 
     * */

    validateSignUPForm(params, callback) {
        var error = null
        this.validateEmail(params['email'], error => {
            if (!error) {
                if (params.password == '') {
                    error = 'Password must not be empty'
                } else if (params.details.dateOfBirth == '') {
                    error = 'Date of Birth must not be empty'
                } else if (params.details.country == '') {
                    error = 'Country must not be empty'
                } else {
                    error = null
                }
            }

            callback(error)
        })
    }

    /// FILTERING

    sordedByDate(list, date) {
        return _.sortBy(list, 'eventStart')
    }

    /**
     * 
     *  Filter by date
     * 
     *  @param list (array) - Array of event that needs to be filtered
     *  @param dateString (dateString) - DateString object later to be transform to Date Object. Cannot be null
     *  @param eventLevel (string) - Filter bt even level (emperor, king, any, baron)
     *  @param eventLocation (string) - Filter by event location (Auditorium, Board Room ...)
     * 
     */

    filteredEventList(list, dateString, eventLevel = 'all', eventLocation = 'all') {
        var temp = []
        for (var i = 0; i < list.length; i++) {
            var eventDate = new Date(list[i].eventStart)
            var date = new Date(dateString)
            if (eventDate.getFullYear() == date.getFullYear() &&
                eventDate.getMonth() == date.getMonth() && eventDate.getUTCDate() == date.getUTCDate()) {
                temp.push(list[i])
            }
        }

        temp = temp.filter(e => (e.eventLevel == eventLevel || e.eventLevel == 'all'))

        if (eventLocation != 'all') {

            /// Task #2: Please remove locations "Art lounge", "Gaming area" and "Food Alley" from agenda page. They can stay on map, and link to "General activities" from there.
            let generalEventLocationArray = Array('Art lounge', 'Gaming area', 'Food Alley', 'General activities')
            let eventLocationFilterArray = generalEventLocationArray.includes(eventLocation) ? generalEventLocationArray : Array(eventLocation)
            temp = temp.filter(e => { return eventLocationFilterArray.includes(e.eventLocation) });
        }

        return temp
    }

    getCommingUpEvents(eventList) {
        var temp = []
        for (var i = 0; i < eventList.length; i++) {
            var eventDate = new Date(eventList[i].eventStart)
            var date = new Date()
            if (eventDate >= date) {
                temp.push(eventList[i])
            }
        }

        return temp
    }

    /**
    * 
    *  Filter list base on event level(emperor, king, any, baron)
    * 
    *  @param list (array) - Array of event that needs to be filtered
    *  @param level (string) - One of the following event level strings (emperor, king, any, baron)
    * 
    */

    filterByLevel(list, level) {
        var filtered = _.filter(list, { 'eventLevel': String(level).toLocaleLowerCase() })
        return this.sordedByDate(filtered)
    }

    /// FORMATING 

    /***
     * 
     *  Format date for UI. For example 2018-05-18 to May 18
     * 
     *  @param dateString (string) - Datestring object
     *  @param type (string) - One of the following. Default is date
     *      - dateAgenda
     *      - date
     * 
     */

    formatDateString(dateString, type = 'date') {
        let dateObject = new Date(dateString)
        if (type == 'dateAgenda') {
            return `${this.utislForFormatHour(dateObject.getUTCFullYear())}-${this.utislForFormatHour(dateObject.getMonth() + 1)}-${this.utislForFormatHour(dateObject.getUTCDate())}`
        }
        if (type == 'news') {
            return `${monthNames[dateObject.getMonth()]} ${dateObject.getUTCDate()}, ${dateObject.getFullYear()}`
        }
        return `${monthNames[dateObject.getMonth()]} ${dateObject.getUTCDate()}`
    }

    formatHour(dateString) {
        let dateObject = new Date(dateString)
        return `${this.utislForFormatHour(dateObject.getHours())}:${this.utislForFormatHour(dateObject.getMinutes())}`
    }

    timeDifferenceInHours(eventItem) {
        var start = new Date(eventItem.eventStart)
        var end = new Date(eventItem.eventFinish)
        var diffInMinutes = (end - start) / (60 * 1000)

        // 1 hour is 100 height, so 1 min is 100/60
        var paddingForHour = 10
        var height = 200 / 60 * diffInMinutes + (paddingForHour * diffInMinutes / 60)

        // calculate top
        var topHour = new Date(start)
        topHour.setMinutes(0)
        var diffTop = (start - topHour) / (60 * 1000)
        var topPadding = 200 / 60 * diffTop + (paddingForHour * diffInMinutes / 60)

        return { height: height, top: topPadding }
    }


    /**
     * 
     *  Get unique dates for upcoming events base on data for Contentfull API
     * 
     *  @param eventList (array) - List of events...
     * 
     */

    uniqueEventDates(eventList) {
        var tempList = []
        for (var i = 0; i < eventList.length; i++) {
            var formatedDate = this.formatDateString(eventList[i].eventStart, 'dateAgenda')
            if (tempList.length == 0) {
                tempList.push(formatedDate)
            } else {
                var isFound = false
                for (var j = 0; j < tempList.length; j++) {
                    if (tempList[j] == formatedDate) {
                        isFound = true
                        break
                    }
                }
                if (!isFound) {
                    tempList.push(formatedDate)
                }
            }
        }

        return tempList.sort()
    }

    getFormatedCurrentDate() {
        let date = new Date()
        return transformDate(date)
    }
    fortmatNextThreeEvents(date) {

    }

    validateDateOfBirth(date) {
        let dateOfBirth = new Date(date)
        let now = new Date()
        let birthday = new Date(now.getFullYear, dateOfBirth.getMonth(), dateOfBirth.getDate())

        if(now >= birthday) 
            age = now.getFullYear() - dateOfBirth.getFullYear()
        else
            age = now.getFullYear() - dateOfBirth.getFullYear()

        if(age < 16)
            return false
        else 
            return true
    }
}

export default new UtilService()