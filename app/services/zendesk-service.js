import { store } from '../reducers/index'
import {
    AsyncStorage,
    KeyboardAvoidingView,
    Alert,
    TouchableWithoutFeedback
} from 'react-native'

const config = {
    apiToken:'2W5JDi1laKaZgSBGIVVucxrhwaEMCKtoP31y0ZqB'
}

// _retrieveData = async () => {
//     try {
//       const value = await AsyncStorage.getItem('username');
//       if (value !== null) {
//         return value
//       }
//      } catch (error) {
//        // Error retrieving data
//      }
//   }




class ZendeskService {
    async getTokenHeader(){
        const value = await AsyncStorage.getItem('username');      
        console.log("email value from header is")
        console.log(value)
        return 'Basic ' + Base64.encode(value + '/token:' + config.apiToken)
    }

    async setHeaders()
    {
        var token = await this.getTokenHeader()
        //Hawk id="64DC4DC6508F4A9D823179D4D99DB4EE", ts="1537184377", nonce="B5zQaR", mac="XTVHc+TBLyMQpLxe755N8e2dzmgUEZjboS8iE5iAQQ0="
        return { 'Authorization' : token }
    }

    async getEmail()
    {   
        const value = await AsyncStorage.getItem('username');      
        return value
    }
    async setJsonHeaders(){
        var token = await this.getTokenHeader()
        return { 
            'Authorization': token,
            'Content-Type': 'application/json'
        }
    }
}
export default new ZendeskService()