import { createClient } from 'contentful/dist/contentful.browser.min.js'
import _ from 'lodash'
import Configuration from '../config/environment/config'
import { store } from '../reducers/index'

// 
const contentfull = () => {
    const name = store.getState().config.name
    return Configuration.environment[name].contentfull
}

// Models
import NewsItem from '../model/news-item'
import Event from '../model/event'

// Helpers
const transformImageUrl = url => {
    let newUrl = String('https:').concat(url)
    return newUrl
}

/**
 *  Get content
 */
export async function getContent() {
    const client = createClient({
        ...contentfull().content.config
    })

    return client.getEntries()
}

/**
 *  Retrieve all the assets from ContentFul that are used in our application.
 */
export async function getAssets() {
    const content = await getContent()
    let assetsList = []
    for (let i = 0; i < content.includes.Asset.length; i++) {
        let asset = content.includes.Asset[i]
        const url = transformImageUrl(asset.fields.file.url)
        const assetObject = {
            title: asset.fields.title,
            url
        }
        assetsList.push(assetObject)
    }

    return assetsList
}

/**
 *  Get MainImage based on spaceId = '35EOEp7n20qqCKwkG2icYw'
 */
export async function getMainImage() {
    const content = await getContent()
    for (var i = 0; i < content.items.length; i++) {
        if (content.items[i].sys.id == contentfull().content.mainImageId) {
            const mainImageItem = content.items[i].fields
            return transformImageUrl(mainImageItem.assetLink.fields.file.url)
        }
    }

    return null
}

export async function getNews() {
    const client = createClient({
        ...contentfull().news
    })

    const environment = store.getState().config.name
    const newsContent = await client.getEntries()
    let news = []
    
    for (let i = 0; i < newsContent.items.length; i++) {
        let item = new NewsItem(newsContent.items[i])

        if (environment != 'production') {
            if (newsContent.items[i].fields.identifier) {
                item = new NewsItem(newsContent.items[i])
                if (item.publishDate) {
                    news.push(item)
                }
            }
        } else {
            item = new NewsItem(newsContent.items[i])
            if (item.publishDate) {
                news.push(item)
            }
        }
    }

    const sorted = _.sortBy(news, 'publishDate')
    return sorted.reverse()
}

export async function getEvents() {
    const content = await getContent()
    let events = []
    for (var i = 0; i < content.items.length; i++) {
        const eventItem = new Event(content.items[i].fields)
        if (eventItem.eventStart != null && eventItem.eventFinish != null) {
            events.push(eventItem)
        }
    }

    return events
}
export async function getNextThreeEvents() {
    //const data  = getNews();
    // const array = nextEvents;
    // function convert(conv) {
    //     let result = conv.sort(function (a, b) {
    //         return new Date(a.publishDate) - new Date(b.publishDate)
    //     })
    //     return result;
    // }
    // let tst = convert(array)
    // let start = Date.now();
    // const res123 = tst.find((res) => new Date(res.publishDate) > start)
    // let index = tst.indexOf(res123)
    // let finalRes = tst.slice(index-1, index + 3)
    // let events = []

    let events = await getNews()
    let nextThreeEvents = events.slice(0, 3)
    return nextThreeEvents
}
export function getWikiGames() {

    const data = wikiData
    console.log(data)
    return data
}
export async function getFeedback() {
    const content = await getContent()
    for (var i = 0; i < content.items.length; i++) {
        if (content.items[i].sys.id == contentfull().content.feedbackId) {
            return content.items[i].fields
        }
    }

    return null
}

export async function getInformationNotice() {
    const content = await getContent()
    for (var i = 0; i < content.items.length; i++) {
        if (content.items[i].sys.id == contentfull().content.informationNoticeId) {
            return content.items[i].fields
        }
    }

    return null
}

export async function getWelcomeNotice() {
    const content = await getContent()
    for (var i = 0; i < content.items.length; i++) {
        if (content.items[i].sys.id == contentfull().content.welcomeNoticeId) {
            return content.items[i].fields
        }
    }

    return null
}

// export async function getVideoThumbnail() {
//     const content = await getContent()
//     for(var i = 0; i < content.items.length; i++) {
//         if (content.items[i].sys.id) == contentfull().content.videoThumbnailId) {
//             const videoThumbnail = content.items[i].fields
//             return transformImageUrl(videoThumbnail.assetLink.fields.file.url)
//         }
//     }

//     return null
// }

// export async function getVideo() {
//     const content = await getContent()
//     for(var i = 0; i < content.items.length; i++) {
//         if (content.items[i].sys.id) == contentfull().content.videoId) {
//             const video = content.items[i].fields
//             return transformImageUrl(video.assetLink.fields.file.url)
//         }
//     }

//     return null
// }
