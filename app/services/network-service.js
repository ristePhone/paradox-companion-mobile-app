/**
 * 
 *  Contentful credentials:
 * 
 *  Space ID: x99xqpyz2b49
 *  Content Delivery API - access token: 47982320a90fff4f05e3ba018fd15698627a9f8b2f682b59f3ac42bf7b97b60f
 *  Content Preview API - access token: 03ee1c99da5fa844fc02d8b7bf9554ed81574c37622af71257ff107d7cbfa93e
 * 
 */

// const BASE_URL = 'https://api.paradoxplaza.com/sandbox'
const BASE_URL = 'https://api.paradoxplaza.com'

import { createClient } from 'contentful/dist/contentful.browser.min.js'
import RequestService from '../services/request-service'
import { AsyncStorage } from 'react-native'

const sha = require('sha.js')

class NetworkService {

    // Find credentials in AsyncStorage
    retrieveCredentials(callback) {
        AsyncStorage.getItem('credentials', (error, result) => {
            callback(result)
        })
    }

    storeCredentials(username, password) {
        let credentials = { username: username, password: password }
        AsyncStorage.removeItem('credentials', error => {
            if (error == null) {
                console.log('Old credentials removed')
                AsyncStorage.setItem('credentials', JSON.stringify(credentials), e => {
                    if (e == null) console.log('New Credential stored', credentials)
                })
            }
        })
    }

    // Authorization
    signIn(params) {
        const url = `${BASE_URL}/accounts`
        return RequestService.post(url, params)
    }

    logIn(username, password) {

        let url = `${BASE_URL}/accounts/sessions/accounts`

        const authHeader = JSON.stringify({
            hawk: {
                email: username,
                'sha256(password+salt)': sha('sha256')
                    .update(`${password}64DC4DC6508F4A9D823179D4D99DB4EE`, 'utf8')
                    .digest('hex'),
            },
        })
        return fetch(url, {
            method: 'PUT',
            headers: {
                'Authorization': authHeader,
                'Content-type': 'application/json'
            }
        }).then(response => {
            return response.json()
        }).then(response => {
            console.log('login response', response)
            return response
        })
    }

    autoLogin(callback) {
        this.retrieveCredentials(credentials => {
            if (credentials) {
                credentials = JSON.parse(credentials)
                let url = `${BASE_URL}/accounts/sessions/octagon`
                console.log('------- Auto login---------\n', 'Stored Credentials: ', credentials, '\n', 'Autologin URL: ', url)

                // Auto login with credentials
                const authHeader = JSON.stringify({
                    hawk: {
                        email: credentials.username,
                        'sha256(password+salt)': sha('sha256')
                            .update(`${credentials.password}64DC4DC6508F4A9D823179D4D99DB4EE`, 'utf8')
                            .digest('hex'),
                    },
                })
                return fetch(url, {
                    method: 'GET',
                    headers: {
                        'Authorization': authHeader,
                        'User-agent': 'octagon/0.0'
                    }
                }).then(response => {
                    return response.json()
                }).then(response => {
                    console.log('Auto login response', response)
                    callback(response)
                })
            }
        })
    }

    createSession(username, password) {
        let url = `${BASE_URL}/accounts/sessions/octagon`

        // Auto login with credentials
        const authHeader = JSON.stringify({
            hawk: {
                email: username,
                'sha256(password+salt)': sha('sha256')
                    .update(`${password}64DC4DC6508F4A9D823179D4D99DB4EE`, 'utf8')
                    .digest('hex'),
            },
        })
        return fetch(url, {
            method: 'PUT',
            headers: {
                'Authorization': authHeader,
                'User-agent': 'octagon/0.0'
            }
        }).then(response => {
            return response.json()
        }).then(response => {
            return response
        })
    }

// Content
getContent() {
    const client = createClient({
        space: 'x99xqpyz2b49',
        accessToken: '47982320a90fff4f05e3ba018fd15698627a9f8b2f682b59f3ac42bf7b97b60f'
    })
    return client.getEntries()
}

getNews() {
    const client = createClient({
        space: 's1cu6ii344ub',
        accessToken: '994aca6da3883c66d866d6a3791fd57e7d43b59c9ca8e43d55d77041a2819111'
    })
    return client.getEntries()
}

}

export default new NetworkService()
// export {
//     client,
//     getEntries
// }