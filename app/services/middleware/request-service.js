import Configuration from '../../config/environment/config'
import { store } from '../../reducers/index'
/**
 *  Wrapper for Middleware service.
 */

//const BASE_URL = 'http://207.154.211.132:3000/'
//const BASE_URL = 'https://companion.paradoxplaza.com/'

const BASE_URL = () => {
    const name = store.getState().config.name

    return Configuration.environment[name].MIDDLEWARE_BASE_URL
}


const queryParams = params => {
    return Object.keys(params)
        .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
        .join('&');
}

const get = async (parameters, body) => {
    const queryParameters = queryParams(parameters)
    const url = `${BASE_URL()}?${queryParameters}`
    return fetch(url, {
        method: 'GET'
    }).then(response => {
        return response.json()
    }).then(response => {
        return response
    }).catch(error => {
        return { error: error.message }
    })
}

const getCode = async (parameters) => {
    const url = `${BASE_URL()}?${parameters}`
    return fetch(url, {
        method: 'GET'
    }).then(response => {
        return response
    }).catch(error => {
        return { error: error.message }
    })
}

export default {
    get, getCode
}