/**
 *  Implementation for the middleware service implemented on Datajob side.
 */
import RequestService from './request-service'
import { store } from '../../reducers/index';
import _ from 'lodash'

import {
    transformImageUrl,
    getAssetsServerResponse,
    findAssetForID
} from './utils'

import Configuration from '../../config/environment/config'

// Models
import Event from '../../model/event'
import NewsItem from '../../model/news-item'

const getEnvironment = () => store.getState().config.name

const contentfull = () => {
    const name = store.getState().config.name
    return Configuration.environment[name].contentfull
}

/**
 *  GET /contentType=events&env={environment}
 * 
 *  Retrieve events from server
 */
const getEvents = async () => {
    let events = null
    const params = {
        contentType: 'events',
        env: getEnvironment()
    }

    const response = await RequestService.get(params)
    if (!response.error) {
        const assetArray = getAssetsServerResponse(response)
        const eventItems = response.items
        events = eventItems.map(item => {
            const event = item.fields
            const eventItem = new Event(event)
            if (event.eventImage) {
                const id = event.eventImage.sys.id
                const asset = findAssetForID(id, assetArray)
                eventItem.imageUrl = asset.url
            }

            return eventItem
        })
    } 

    return events
}

/**
 *  GET /contentType=newsItem&env={environment}
 *  
 *  Retrieve list of news
 */
const getNews = async () => {
    let news = null
    const params = {
        contentType: 'newsItem',
        env: getEnvironment()
    }

    const response = await RequestService.get(params)
    if (!response.error) {
        const assetArray = getAssetsServerResponse(response)
        news = response.items.map(item => {
            const newsItem = new NewsItem(item.fields)
            const id = item.fields.headerImage.sys.id
            const asset = findAssetForID(id, assetArray)
            newsItem.headerImage = asset.url
            return newsItem
        })
    } 

    const sorted = _.sortBy(news, 'publishDate').reverse()
    return sorted
}

/**
 *  GET /contentType=contentAsset&env={environment}
 * 
 *  Retrieve list of assets
 */
const getAssets = async () => {
    const params = {
        contentType: 'contentAssets',
        env: getEnvironment()
    }

    const response = await RequestService.get(params)
    if (!response.error) {
        const assetList = response.includes.Asset.map(item => {
            const assetItem = item.fields
            const url = transformImageUrl(assetItem.file.url)
            const assetObject = {
                title: assetItem.title,
                url
            }

            return assetObject
        })

        return assetList
    }

    return null
}

/**
 *  GET /contentType=wikis&env={environment}
 * 
 *  Retrieve wikis
 */
const getWikis = async () => {
    const params = {
        contentType: 'wikis',
        env: getEnvironment()
    }

    const response = await RequestService.get(params)
    if (!response.error) return response

    return null
}

/**
 *  GET /contentType=notice&env={environment}
 * 
 *  Retrieve notice = [welcome, feedback]
 */
const getNotice = async () => {
    let welcomeNotice = null, feedback = null
    const params = {
        contentType: 'notice',
        env: getEnvironment()
    }

    const response = await RequestService.get(params)
    if (!response.error) {
        response.items.map(item => {
            if (item.sys.id == contentfull().content.feedbackId) {
                feedback = item.fields
                return
            }
            if (item.sys.id == contentfull().content.welcomeNoticeId) {
                welcomeNotice = item.fields
                return
            }
        })
    }

    return { welcomeNotice, feedback }
}

/**
 *  Wrapper call to retrieve all data for the app
 */
const retriveAllData = async () => {
    const assets = await getAssets()
    const notice = await getNotice()
    const events = await getEvents()
    const news = await getNews()
    const wiki = await getWikis()
 
    return {
        assets,
        events,
        news,
        wiki,
        notice,
    }
}

export default {
    getEvents,
    getNews,
    getAssets,
    getWikis,
    getNotice,
    retriveAllData,
}