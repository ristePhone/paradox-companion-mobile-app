
const transformImageUrl = url => {
    let newUrl = String('https:').concat(url)
    return newUrl
}

const getAssetsServerResponse = response => {
    if (response.includes && response.includes.Asset) {
        return assetsArray = response.includes.Asset.map(item => {
            const assetItem = item.fields
            const url = transformImageUrl(assetItem.file.url)
            const assetObject = {
                id: item.sys.id,
                title: assetItem.title,
                url
            }
            return assetObject
        })
    }

    return []
}

const findAssetForID = (id, assetsArray) => {
    const asset = assetsArray.find(asset => { return asset.id == id })
    return asset
}

export {
    getAssetsServerResponse,
    findAssetForID,
    transformImageUrl
}