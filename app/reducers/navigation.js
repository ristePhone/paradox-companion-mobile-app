import {
    POP,
    PUSH,
    SETUP_NAVIGATION,
    UPDATE_SELECTED_NEWS_FEED_ITEM,
    UPDATE_SELECTED_EVENT,
    UPDATE_SELECTED_WIKI
} from '../actions/action-types'

export const navigation = (state = {}, actions) => {
    switch (actions.type) {
        case SETUP_NAVIGATION:
            return Object.assign({}, state, ...actions.parameters)
        case PUSH:
            return state.navigator.push(...actions.parameters)
        case POP:
            return state.navigator.POP(...actions.parameters)
        case UPDATE_SELECTED_NEWS_FEED_ITEM:
            return Object.assign({}, state, { selectedNewsFeedItem: actions.item })
        case UPDATE_SELECTED_NEWS_FEED_ITEM:
            return Object.assign({}, state, { selectedNewsFeedItem: actions.item })
        case UPDATE_SELECTED_EVENT:
            return Object.assign({}, state, { selectedEvent: actions.item })
        case UPDATE_SELECTED_WIKI:
            return Object.assign({}, state, { selectedWiki: actions.item })
        default:
            return state
    }
}