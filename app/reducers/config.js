import { UPDATE_CONFIGURATION, UPDATE_ROOT_CONTROLER } from '../actions/action-types'

export const config = (state = { rootTypeController: 'tabBasedApp' }, actions) => {
    switch (actions.type) {
        case UPDATE_CONFIGURATION:
            return Object.assign({}, state, actions.configuration)
        case UPDATE_ROOT_CONTROLER:
            return Object.assign({}, state, { rootTypeController: actions.rootTypeController })
        default:
            return state
    }
}