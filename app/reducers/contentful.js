import {
    UPDATE_CONTENTFUL_NEWS,
    UPDATE_CONTENTFUL_FEEDBACK,
    UPDATE_NEXT_THREE_EVENTS,
    UPDATE_APPLICATION_ASSETS,
    UPDATE_CONTENTFUL_WIKI,
    UPDATE_HOME_SCREEN
} from '../actions/action-types'

export const contentfulAPIcontent = (state = {}, actions) => {
    switch (actions.type) {
        case UPDATE_CONTENTFUL_NEWS:
            return Object.assign({}, state, { news: actions.news })
        case UPDATE_CONTENTFUL_FEEDBACK:
            return Object.assign({}, state, { feedback: actions.feedback })
        case UPDATE_CONTENTFUL_WIKI:
            return Object.assign({}, state, { wiki: actions.wiki })
        case UPDATE_NEXT_THREE_EVENTS:
            return Object.assign({}, state, { threeEvents: actions.threeEvents })
        case UPDATE_APPLICATION_ASSETS:
            return Object.assign({}, state, { assets: actions.assets })
        case UPDATE_HOME_SCREEN:
            return Object.assign({}, state, actions.homeScreen)
        default:
            return state
    }
}