import {
    UPDATE_PASSWORD,
    UDPATE_USERNAME,
    UPDATE_USER_LOGGED_IN_STATUS
} from '../actions/action-types'

const initialState = {
    username: 'user',
    passwor: 'password'
}

export const user = (state = initialState, actions) => {
    switch (actions.type) {
        case UPDATE_PASSWORD:
            return Object.assign({}, state, { email: actions.email })
        case UDPATE_USERNAME:
            return Object.assign({}, state, actions)
        case UPDATE_USER_LOGGED_IN_STATUS:
            return Object.assign({}, state, { isLoggedIn: actions.isLoggedIn })
        default:
            return state
    }
}