/**
 *  Initial Redux setup that includes store and reducers
 */

'use strict'

// Redux
import { combineReducers, createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk';

// Import Reducers
import { navigation } from './navigation'
import { user } from './user'
import { config } from './config'
import { contentfulAPIcontent } from './contentful'

// Combine reducers and create store...
// Add additional reducers to update the store...
const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);
const reducers = combineReducers({ config, user, navigation, contentfulAPIcontent })
const store = createStoreWithMiddleware(reducers)

export {
    store
}