var array = [
    {
        bodyText: "STOCKHOLM – Mar. 31, 2017 — Paradox Interactive, a publisher that built this city on con and sole, today announced that Cities: Skylines – Xbox One Edition will launch worldwide on April 21, 2017. Newly optimized for play on a controller by Tantalus Media, the Xbox One Edition brings the creative management game to consoles along with Cities: Skylines – After Dark, the game’s nightlife-focused expansion. Xbox One players will finally have the opportunity to experience the award-winning management game next month when it becomes available for purchase via disc or download, with pre-orders available starting today. Cities: Skylines – Xbox One Edition will retail for $39.99 / £31,99 / €39.99.Pre-orders of Cities: Skylines – Xbox One Edition are available [here](https://www.microsoft.com/en-us/store/p/cities-skylines-xbox-one-edition/C4GH8N6ZXG5L). Cities: Skylines is a best-selling management game, available for Windows, Mac, and Linux PCs, and coming very soon to Xbox One and Windows 10 with all its deep gameplay and unique charm along for the ride. The Xbox One Edition and Windows 10 Edition include the award-winning base game, bundled together with the popular After Dark expansion, allowing players to plan and design for their cities’ nightlife and tourist attractions.For more information on Cities: Skylines, visit [http://www.citiesskylines.com/](http://www.citiesskylines.com/).",
        headerImage: "http://images.ctfassets.net/s1cu6ii344ub/4lVu9loHPOaCIy4eoq6oWi/ffaad32b37563260c1c0bee9e996a6ca/csxb_pr_banner.png",
        headline: "http://images.ctfassets.net/s1cu6ii344ub/4lVu9loHPOaCIy4eoq6oWi/ffaad32b37563260c1c0bee9e996a6ca/csxb_pr_banner.png",
        publishDate: "2018-06-28T10:00+02:00"
    },
    {
        bodyText: `STOCKHOLM – May 12, 2017 – For 500 years, the princes of Russia have kept the faith. Through civil wars and Mongol conquest, the word of God has not just survived, but has united our people. Those princes who embrace the church will find glory, those who turn their back will be cast to dust. There have been two Romes, and both have fallen. Muscovy is the third.

        There shall be no fourth.
        
        Paradox Interactive and Paradox Development Studio are thrilled to announce Europa Universalis IV: Third Rome. The first immersion pack for the best-selling historical strategy game introduces a wealth of region-specific content, and takes a deeper look at one of the greatest powers of the early modern era – Russia. Russian cultured nations will find new interactions for their government and the Orthodox Patriarchate, deepening the experience of playing this rich and fascinating region.
        
        Russia now gets two unique governments, with special abilities that allows them to tighten their grip on power.  Use your administrative ability to reduce provincial autonomy. Call on your diplomatic resources to suppress incipient revolts. Use your military resources to quickly recruit streltsy units for your army. Rule with the iron fist that comes with a divine blessing.
        
        Features of Europa Universalis IV: Third Rome include:
        
        • Tsardoms and Principalities: New ranks of Russian government with new abilities and, for Tsars, strong bonuses including the right to claim entire States – not just provinces
        • Iconography: Commission great religious icons for the Orthodox Church, boosting your empire’s power depending on the saint you choose to revere
        • Metropolitans: Consecrate highly developed provinces with Metropolitans, adding to the authority of the Orthodox Church, but for a cost.
        • Streltsy: Special Russian soldiers that excel in combat, but raise the cost of stabilizing your empire
        • Siberian Frontier: Russia can slowly colonize uninhabited border regions, with no fear of native uprisings
        • New Graphics: Includes three full unit packs and new portraits for Eastern Europe.
        
        As usual, Third Rome will be accompanied by a free update to Europa Universalis IV, available for everyone who owns the base game. ""`,
        headerImage: "http://images.ctfassets.net/s1cu6ii344ub/5vlR7uKDw4GCIWi6WguCQs/8023f9ab46113bc8cfd3a3e3d425b7ce/Third_Rome.png",
        headline: "First Immersion Pack for Europa Universalis IV has Russian Focus",
        publishDate: "2018-06-30T10:00+02:00"
    },
    {
        bodyText: `STOCKHOLM - April 26, 2017 - Armchair generals extraordinaire Paradox Interactive today announced that Hearts of Iron IV players will soon have more difficult decisions to make with the launch of Death or Dishonor, a brand new country pack coming later this year. Death or Dishonor focuses on the countries of Europe caught between the dueling ambitions of the Soviet Union and Nazi Germany - faced with total destruction by a much larger force or making uncomfortable alliances, survival will take compromise, guile, and perhaps a teensy bit of luck. Death or Dishonor will be available for $9.99 on Windows, Mac, and Linux PCs -- and will be delivered for free to all Expansion Pass owners.

    Death or Dishonor introduces new National Focus trees for Hungary, Romania, Czechoslovakia, and Yugoslavia, which revolve around navigating the ideological (and physical) land mines of central Europe during World War II. This content furthers Paradox’s goal of capturing the unique wartime experience of every nation involved in World War II, but players will also find new opportunities to change the course of history if they so choose.
    
    Death or Dishonor - and the accompanying free update - will also include:
    
    • Revamp of Air Gameplay: Take to the skies with new mechanics and quality of life updates that make it easier to bring your airpower to bear.
    • Equipment Conversion: Update your arsenal by converting older units -- or make use of captured enemy vehicles and gear.
    • New Music and Art: 3 new thematic musical tracks from composer Andreas Waldetoft, new art content for the four focus nations, and new troop voiceovers.
    • New Diplomatic and Puppet Interactions: License military technology to bring other nations’ weapons to the field or sell your advances to the highest bidder. Fascist countries get new subject levels like Reichskommissariat, with access to licenses, industry and strategic resources. The instated governments are held in an iron grip, making it harder for them to break free.
    • Improved Map Design and Display: the world map has been clarified to make it easier to follow the action around the world. New impassable areas create tactical choke-points and more historical gameplay`,
        headerImage: "http://images.ctfassets.net/s1cu6ii344ub/1ZF3eioHOUY4GS42sya0a6/3e2461f58758c08b5db31005ab6b17e5/Pressrelease_banner_2.jpg",
        headline: "New Country Pack for Hearts of Iron IV Expands Gameplay for Countries Trapped in a Tough Spot",
        publishDate: "2018-06-30T10:20+02:00"
    },
    {
        bodyText: `STOCKHOLM, April 13th, 2017 - Paradox Interactive, a publisher that sometimes goes off-track, and genuine bona-fide electrified six-car developer Colossal Order today announced that “Mass Transit”, the next expansion for Cities: Skylines, will be available on May 18th, for $12.99.  Cities: Skylines - Mass Transit brings a blimp-load of new features to to the critically acclaimed city-builder, including new public transportation options such as monorails, cable-cars, ferries, and most majestic of all, blimps!  

        The expansion also includes new mass transit hubs, which allow passengers to easily switch between different public transportation options, showcased for the first time in this new trailer.
        
        Cities: Skylines - Mass Transit will feature:
        The Road to Riches:  Ferries, blimps, cable cars, and monorails help move your citizens around town, and get their money into your city's coffers.
        Home is where the Hub is: The transit hub connects all your transit together, letting citizens ail lines in one building, or hop from the bus onto the ferry, or even find their way through a sprawling monorail-train-metro station.
        The Traffic Must Flow: New scenarios will test your traffic management skills and transit system vision.  New road types, bridges and canals adds  variety to your city, and new ways to solve its challenges.  Become an expert in traffic flow, and then use that knowledge to improve your city!
        New Hats for Chirper: No expansion is complete without some new hats for fictional birds!`,
        headerImage: "http://images.ctfassets.net/s1cu6ii344ub/2QUNMD9a7SS8Q2wo4CAwOg/279e2301b7f3ce41530a1ab201979c44/cities_sjylines_mass_transit_1.jpg",
        headline: "Cities: Skylines - Mass Transit Out in May",
        publishDate: "2018-06-28T13:50+02:00"
    },
    {
        bodyText: `STOCKHOLM – Apr. 6, 2017 – With one eye on the rising sun and another eye on a glorious future, Europa Universalis IV: Mandate of Heaven is available today. Mandate of Heaven is the new expansion to Paradox Development Studio’s flagship franchise about exploration, religious turmoil, economic dominance and conquest. Taking its name from the Chinese belief that their empire was divinely ordained, Mandate of Heaven will bless your ambitions for years to come.

         At the center of Mandate of Heaven, players will find the new Historical Ages system and the associated Golden Eras. The four centuries of play are divided into four Historical Ages, each with seven objectives and seven special abilities that can be unlocked. If you accomplish three objectives in an Age, you may choose to initiate a Golden Era, giving your empire 50 years of prosperity and military prowess. But choose your time wisely! You only get one Golden Era per game.
            
            Mandate of Heaven’s other features include:
            
            • Empire of China: The Chinese Emperor has great powers at his disposal, and the efficient bureaucrats keep things running smoothly. But greedy neighbors may seek to take the Mandate of Heaven for themselves.
            • Ashikaga Shogunate: The Japanese Shogunate has been reworked for more interaction and political considerations among the daimyo. As Shogun, carefully balance the pride of your subjects or compel them to choose and honorable death.
            • Confucian Harmony: Keep order in your eastern realm or risk short term spiritual disruption in exchange for long term religious harmony.
            • Shinto Isolation: Will your Japanese kingdom seek benefits in an open society or promote greater unity through closed doors? Major incidents will challenge your aspirations.
            • Tributaries: Eastern empires can persuade weaker neighbors to pay an annual tribute in gold, manpower or monarch points in this new subject nation format.
            • Diplomatic Macro Builder: New addition to the left-hand side macrobuilder automates many repeated diplomatic tasks.
            • And more including Chinese meritocracy and Manchu Banner troops
            
            As usual, Mandate of Heaven launches alongside a major free update to Europa Universalis IV that improves and updates many aspects of the core game experience.
            
            Starting later today, a number of Paradox Interactive titles will go on sale on Steam for the weekend, including the Europa Universalis IV core game. Ambitious rulers who have yet to try their hand at world conquest will have the perfect opportunity to try the latest version of the game alongside its newest expansion. Europa Universalis IV: Mandate of Heaven is available now on Steam and the official Paradox Store for $19.99. [https://www.paradoxplaza.com/europa-universalis-iv-mandate-of-heaven](https://www.paradoxplaza.com/europa-universalis-iv-mandate-of-heaven)`,
        headerImage: "http://images.ctfassets.net/s1cu6ii344ub/4RnuiPGoFaK6G8GAC446cy/a27f1f22d7769e70c98ffbb1635c2f09/MoH.png",
        headline: "Go for the Golden Era in Mandate of Heaven",
        publishDate: "2018-06-30T19:50+02:00"
    },
    {
        bodyText: `STOCKHOLM – Apr. 6, 2017 – With one eye on the rising sun and another eye on a glorious future, Europa Universalis IV: Mandate of Heaven is available today. Mandate of Heaven is the new expansion to Paradox Development Studio’s flagship franchise about exploration, religious turmoil, economic dominance and conquest. Taking its name from the Chinese belief that their empire was divinely ordained, Mandate of Heaven will bless your ambitions for years to come.
    
                At the center of Mandate of Heaven, players will find the new Historical Ages system and the associated Golden Eras. The four centuries of play are divided into four Historical Ages, each with seven objectives and seven special abilities that can be unlocked. If you accomplish three objectives in an Age, you may choose to initiate a Golden Era, giving your empire 50 years of prosperity and military prowess. But choose your time wisely! You only get one Golden Era per game.
                
                Mandate of Heaven’s other features include:
                
                • Empire of China: The Chinese Emperor has great powers at his disposal, and the efficient bureaucrats keep things running smoothly. But greedy neighbors may seek to take the Mandate of Heaven for themselves.
                • Ashikaga Shogunate: The Japanese Shogunate has been reworked for more interaction and political considerations among the daimyo. As Shogun, carefully balance the pride of your subjects or compel them to choose and honorable death.
                • Confucian Harmony: Keep order in your eastern realm or risk short term spiritual disruption in exchange for long term religious harmony.
                • Shinto Isolation: Will your Japanese kingdom seek benefits in an open society or promote greater unity through closed doors? Major incidents will challenge your aspirations.
                • Tributaries: Eastern empires can persuade weaker neighbors to pay an annual tribute in gold, manpower or monarch points in this new subject nation format.
                • Diplomatic Macro Builder: New addition to the left-hand side macrobuilder automates many repeated diplomatic tasks.
                • And more including Chinese meritocracy and Manchu Banner troops
                
                As usual, Mandate of Heaven launches alongside a major free update to Europa Universalis IV that improves and updates many aspects of the core game experience.
                
                Starting later today, a number of Paradox Interactive titles will go on sale on Steam for the weekend, including the Europa Universalis IV core game. Ambitious rulers who have yet to try their hand at world conquest will have the perfect opportunity to try the latest version of the game alongside its newest expansion. Europa Universalis IV: Mandate of Heaven is available now on Steam and the official Paradox Store for $19.99. [https://www.paradoxplaza.com/europa-universalis-iv-mandate-of-heaven](https://www.paradoxplaza.com/europa-universalis-iv-mandate-of-heaven)`,
        headerImage: "http://images.ctfassets.net/s1cu6ii344ub/4RnuiPGoFaK6G8GAC446cy/a27f1f22d7769e70c98ffbb1635c2f09/MoH.png",
        headline: "Go for the Golden Era in Mandate of Heaven",
        publishDate: "2018-07-06T11:50+02:00"
    },
    {
        bodyText: `STOCKHOLM – Apr. 6, 2017 – With one eye on the rising sun and another eye on a glorious future, Europa Universalis IV: Mandate of Heaven is available today. Mandate of Heaven is the new expansion to Paradox Development Studio’s flagship franchise about exploration, religious turmoil, economic dominance and conquest. Taking its name from the Chinese belief that their empire was divinely ordained, Mandate of Heaven will bless your ambitions for years to come.

         At the center of Mandate of Heaven, players will find the new Historical Ages system and the associated Golden Eras. The four centuries of play are divided into four Historical Ages, each with seven objectives and seven special abilities that can be unlocked. If you accomplish three objectives in an Age, you may choose to initiate a Golden Era, giving your empire 50 years of prosperity and military prowess. But choose your time wisely! You only get one Golden Era per game.
            
            Mandate of Heaven’s other features include:
            
            • Empire of China: The Chinese Emperor has great powers at his disposal, and the efficient bureaucrats keep things running smoothly. But greedy neighbors may seek to take the Mandate of Heaven for themselves.
            • Ashikaga Shogunate: The Japanese Shogunate has been reworked for more interaction and political considerations among the daimyo. As Shogun, carefully balance the pride of your subjects or compel them to choose and honorable death.
            • Confucian Harmony: Keep order in your eastern realm or risk short term spiritual disruption in exchange for long term religious harmony.
            • Shinto Isolation: Will your Japanese kingdom seek benefits in an open society or promote greater unity through closed doors? Major incidents will challenge your aspirations.
            • Tributaries: Eastern empires can persuade weaker neighbors to pay an annual tribute in gold, manpower or monarch points in this new subject nation format.
            • Diplomatic Macro Builder: New addition to the left-hand side macrobuilder automates many repeated diplomatic tasks.
            • And more including Chinese meritocracy and Manchu Banner troops
            
            As usual, Mandate of Heaven launches alongside a major free update to Europa Universalis IV that improves and updates many aspects of the core game experience.
            
            Starting later today, a number of Paradox Interactive titles will go on sale on Steam for the weekend, including the Europa Universalis IV core game. Ambitious rulers who have yet to try their hand at world conquest will have the perfect opportunity to try the latest version of the game alongside its newest expansion. Europa Universalis IV: Mandate of Heaven is available now on Steam and the official Paradox Store for $19.99. [https://www.paradoxplaza.com/europa-universalis-iv-mandate-of-heaven](https://www.paradoxplaza.com/europa-universalis-iv-mandate-of-heaven)`,
        headerImage: "http://images.ctfassets.net/s1cu6ii344ub/2QUNMD9a7SS8Q2wo4CAwOg/279e2301b7f3ce41530a1ab201979c44/cities_sjylines_mass_transit_1.jpg",
        headline: "Go for the Golden Era in Mandate of Heaven",
        publishDate: "2018-07-05T12:50+02:00"
    },
    {
        bodyText: `STOCKHOLM – Apr. 6, 2017 – With one eye on the rising sun and another eye on a glorious future, Europa Universalis IV: Mandate of Heaven is available today. Mandate of Heaven is the new expansion to Paradox Development Studio’s flagship franchise about exploration, religious turmoil, economic dominance and conquest. Taking its name from the Chinese belief that their empire was divinely ordained, Mandate of Heaven will bless your ambitions for years to come.

         At the center of Mandate of Heaven, players will find the new Historical Ages system and the associated Golden Eras. The four centuries of play are divided into four Historical Ages, each with seven objectives and seven special abilities that can be unlocked. If you accomplish three objectives in an Age, you may choose to initiate a Golden Era, giving your empire 50 years of prosperity and military prowess. But choose your time wisely! You only get one Golden Era per game.
            
            Mandate of Heaven’s other features include:
            
            • Empire of China: The Chinese Emperor has great powers at his disposal, and the efficient bureaucrats keep things running smoothly. But greedy neighbors may seek to take the Mandate of Heaven for themselves.
            • Ashikaga Shogunate: The Japanese Shogunate has been reworked for more interaction and political considerations among the daimyo. As Shogun, carefully balance the pride of your subjects or compel them to choose and honorable death.
            • Confucian Harmony: Keep order in your eastern realm or risk short term spiritual disruption in exchange for long term religious harmony.
            • Shinto Isolation: Will your Japanese kingdom seek benefits in an open society or promote greater unity through closed doors? Major incidents will challenge your aspirations.
            • Tributaries: Eastern empires can persuade weaker neighbors to pay an annual tribute in gold, manpower or monarch points in this new subject nation format.
            • Diplomatic Macro Builder: New addition to the left-hand side macrobuilder automates many repeated diplomatic tasks.
            • And more including Chinese meritocracy and Manchu Banner troops
            
            As usual, Mandate of Heaven launches alongside a major free update to Europa Universalis IV that improves and updates many aspects of the core game experience.
            
            Starting later today, a number of Paradox Interactive titles will go on sale on Steam for the weekend, including the Europa Universalis IV core game. Ambitious rulers who have yet to try their hand at world conquest will have the perfect opportunity to try the latest version of the game alongside its newest expansion. Europa Universalis IV: Mandate of Heaven is available now on Steam and the official Paradox Store for $19.99. [https://www.paradoxplaza.com/europa-universalis-iv-mandate-of-heaven](https://www.paradoxplaza.com/europa-universalis-iv-mandate-of-heaven)`,
        headerImage: "http://images.ctfassets.net/s1cu6ii344ub/2QUNMD9a7SS8Q2wo4CAwOg/279e2301b7f3ce41530a1ab201979c44/cities_sjylines_mass_transit_1.jpg",
        headline: "Go for the Golden Era in Mandate of Heaven",
        publishDate: "2018-07-03T11:50+02:00"
    },
    {
        bodyText: `STOCKHOLM – May 12, 2017 – For 500 years, the princes of Russia have kept the faith. Through civil wars and Mongol conquest, the word of God has not just survived, but has united our people. Those princes who embrace the church will find glory, those who turn their back will be cast to dust. There have been two Romes, and both have fallen. Muscovy is the third.

        There shall be no fourth.
        
        Paradox Interactive and Paradox Development Studio are thrilled to announce Europa Universalis IV: Third Rome. The first immersion pack for the best-selling historical strategy game introduces a wealth of region-specific content, and takes a deeper look at one of the greatest powers of the early modern era – Russia. Russian cultured nations will find new interactions for their government and the Orthodox Patriarchate, deepening the experience of playing this rich and fascinating region.
        
        Russia now gets two unique governments, with special abilities that allows them to tighten their grip on power.  Use your administrative ability to reduce provincial autonomy. Call on your diplomatic resources to suppress incipient revolts. Use your military resources to quickly recruit streltsy units for your army. Rule with the iron fist that comes with a divine blessing.
        
        Features of Europa Universalis IV: Third Rome include:
        
        • Tsardoms and Principalities: New ranks of Russian government with new abilities and, for Tsars, strong bonuses including the right to claim entire States – not just provinces
        • Iconography: Commission great religious icons for the Orthodox Church, boosting your empire’s power depending on the saint you choose to revere
        • Metropolitans: Consecrate highly developed provinces with Metropolitans, adding to the authority of the Orthodox Church, but for a cost.
        • Streltsy: Special Russian soldiers that excel in combat, but raise the cost of stabilizing your empire
        • Siberian Frontier: Russia can slowly colonize uninhabited border regions, with no fear of native uprisings
        • New Graphics: Includes three full unit packs and new portraits for Eastern Europe.
        
        As usual, Third Rome will be accompanied by a free update to Europa Universalis IV, available for everyone who owns the base game.`,
        headerImage: "http://images.ctfassets.net/s1cu6ii344ub/6vEJ8GtR5KiAAgUequ4UCK/5e464f717937d1153b0f40c0fa68250e/pdx-twitch.jpg",
        headline: "Go for the Golden Era in Mandate of Heaven",
        publishDate: "2018-07-02T13:50+02:00"
    },
    
]
export default array;