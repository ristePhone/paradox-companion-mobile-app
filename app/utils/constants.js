
import { Dimensions } from 'react-native'
const { height, width } = Dimensions.get('window');

const TERMS_AND_CONDITIONS_URL = 'https://legal.paradoxplaza.com/terms-of-use?locale='
const PRIVACY_POLICY_URL = 'https://legal.paradoxplaza.com/privacy?locale='
const DISCORD_URL = 'https://discord.gg/9KhNc7D'
const PARADOX_EMAIL_URL = 'paradox-app+pdxconfeedback@paradoxinteractive.com'
const FORGOT_PASSWORD_URL = 'https://accounts.paradoxplaza.com/reset-password'

const SCREEN_WIDTH = width
const SCREEN_HEIGHT = height

const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

// Strings
const ERROR_TITLE_STRING = 'Something went wrong'

// ContentFull
const Contentful = {
  notice_key: '3PvUdYpnAcKgsgCoiGGagO',
  feedback_key: '3S5e7jyWUMeeguyQ8m0sgu'
}

export {
    TERMS_AND_CONDITIONS_URL,
    PRIVACY_POLICY_URL,
    DISCORD_URL,
    SCREEN_HEIGHT,
    SCREEN_WIDTH,
    monthNames,
    PARADOX_EMAIL_URL,
    ERROR_TITLE_STRING,
    FORGOT_PASSWORD_URL,
    Contentful
}