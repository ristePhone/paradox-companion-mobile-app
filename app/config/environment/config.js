const environment = {
    dev: {
        name: 'dev',
        BASE_URL: 'https://api.paradoxplaza.com/sandbox',
        contentfull: {
            content: {
                config: {
                    space: 'ss2rj80a713w',
                    accessToken: 'dd89ee5abf4bc2a0c70b0127d6dd51df86e6553d35a0d2600e60f2b184a19060'
                },
                mainImageId: '61qopZo7cc0Mwe0EqssycI',
                feedbackId: '6KfU0rNlkI0GYkmkusI6uo',
                informationNoticeId: '6xKhrFPCfKOGoeKUemCgCe',
                welcomeNoticeId: '1le8t1PSf6MmEmMgemGmAw',
                videoThumbnailId: '3E3NMUIeZqKgOacgWmKWem',
                videoId: '2vr2feglbSG0AwWoUAQWUg'
            },
            news: {
                space: 'ss2rj80a713w',
                accessToken: 'dd89ee5abf4bc2a0c70b0127d6dd51df86e6553d35a0d2600e60f2b184a19060'
            }
        },
        MIDDLEWARE_BASE_URL: 'http://207.154.211.132:3000/'
    },
    stage: {
        name: 'stage',
        BASE_URL: 'https://api.paradoxplaza.com',
        contentfull: {
            content: {
                config: {
                    space: 'ss2rj80a713w',
                    accessToken: 'dd89ee5abf4bc2a0c70b0127d6dd51df86e6553d35a0d2600e60f2b184a19060'
                },
                mainImageId: '61qopZo7cc0Mwe0EqssycI',
                feedbackId: '6KfU0rNlkI0GYkmkusI6uo',
                informationNoticeId: '6xKhrFPCfKOGoeKUemCgCe',
                welcomeNoticeId: '1le8t1PSf6MmEmMgemGmAw',
                videoThumbnailId: '3E3NMUIeZqKgOacgWmKWem',
                videoId: '2vr2feglbSG0AwWoUAQWUg'
            },
            news: {
                space: 'ss2rj80a713w',
                accessToken: 'dd89ee5abf4bc2a0c70b0127d6dd51df86e6553d35a0d2600e60f2b184a19060'
            }
        },
        MIDDLEWARE_BASE_URL: 'http://207.154.211.132:3000/'
    },
    production: {
        name: 'production',
        BASE_URL: 'https://api.paradoxplaza.com',
        contentfull: {
            content: {
                config: {
                    space: 'x99xqpyz2b49',
                    accessToken: '47982320a90fff4f05e3ba018fd15698627a9f8b2f682b59f3ac42bf7b97b60f'
                },
                mainImageId: '35EOEp7n20qqCKwkG2icYw',
                feedbackId: '3S5e7jyWUMeeguyQ8m0sgu',
                informationNoticeId: '6xKhrFPCfKOGoeKUemCgCe', 
                welcomeNoticeId: '3PvUdYpnAcKgsgCoiGGagO',
                videoThumbnailId: '23r6RLOL2YGkik4IUw0CQg',
                videoId: '4veME5TTTyeywMM4s8CQ8C'
            },
            news: {
                space: 's1cu6ii344ub',
                accessToken: '994aca6da3883c66d866d6a3791fd57e7d43b59c9ca8e43d55d77041a2819111'
            }
        },
        MIDDLEWARE_BASE_URL: 'https://companion.paradoxplaza.com/'
    }
}

const common = {
    serverRetryDataInterval: 10, // in minutes 
    TERMS_AND_CONDITIONS_URL: 'https://legal.paradoxplaza.com/terms-of-use?locale=',
    PRIVACY_POLICY_URL: 'https://legal.paradoxplaza.com/privacy?locale=',
    DISCORD_URL: 'https://discord.gg/9KhNc7D',
    PARADOX_EMAIL_URL: 'paradox-app+pdxconfeedback@paradoxinteractive.com',
    FORGOT_PASSWORD_URL: 'https://accounts.paradoxplaza.com/reset-password',

    // zendesk constants
    //ZENDESK_BASIC_URL:'https://data2278.zendesk.com',
    ZENDESK_BASIC_URL:'https://paradox.zendesk.com',
    ZENDESK_REQUESTS_SEGMENT_URL:'/api/v2/requests',
    ZENDESK_REQUESTS_COMMENTS_SEGMENT_URL: '/comments',
    ZENDESK_REQUESTS_JSON_URL:'.json',
    ZENDESK_TICKET_FORMS_STRING:'/api/v2/ticket_forms.json',
    //ZENDESK_URL_FILTER_OPEN_ASC : '?status=open&sort_by=created_at',
    ZENDESK_URL_FILTER_OPEN_ASC : '?status=new,open,pending,hold,solved&sort_by=created_at',
    ZENDESK_URL_FILTER_ASC: '?sort_by=created_at',
    ZENDESK_TEMP_AUTH :{ 'Authorization': 'Basic dG9taS5yaXN0b3Zza2kubWtAZ21haWwuY29tOmFzZGY4ODQ0QA'},
    ZENDESK_TEMP_AUTH_APP_JSON :{ 
        'Authorization': 'Basic dG9taS5yaXN0b3Zza2kubWtAZ21haWwuY29tOmFzZGY4ODQ0QA',
        'Content-Type': 'application/json'
    },

    // zendesk url segments
    ZENDESK_BASIC_REQUESTS_JSON(){
        return this.ZENDESK_BASIC_REQUESTS_SEGMENT_URL() + this.ZENDESK_REQUESTS_JSON_URL
    },
    ZENDESK_BASIC_REQUESTS_SEGMENT_URL(){
        return this.ZENDESK_BASIC_URL + this.ZENDESK_REQUESTS_SEGMENT_URL
    },
    ZENDESK_ID_PARAM(id){ 
        return '/' + id 
    },
    ZENDESK_REQUEST_CREATE_URL(){
        return this.ZENDESK_BASIC_REQUESTS_JSON()
    },
    ZENDESK_REQUEST_CLOSE(id){
        return this.ZENDESK_BASIC_REQUESTS_SEGMENT_URL() + this.ZENDESK_ID_PARAM(id) + this.ZENDESK_REQUESTS_JSON_URL
    },
    ZENDESK_REQUEST_UPDATE_COMMENT (id) {
        return this.ZENDESK_BASIC_REQUESTS_SEGMENT_URL() + this.ZENDESK_ID_PARAM(id) + this.ZENDESK_REQUESTS_JSON_URL
    },
    ZENDESK_REQUESTS_SHOW_OPEN_URL(){
        //return this.ZENDESK_BASIC_REQUESTS_JSON() + this.ZENDESK_URL_FILTER_ASC //+ this.ZENDESK_URL_FILTER_OPEN_ASC
        return this.ZENDESK_BASIC_REQUESTS_JSON() +  this.ZENDESK_URL_FILTER_OPEN_ASC
    },
    ZENDESK_REQUESTS_LIST(){
        return 
    },
    ZENDESK_REQUEST_DETAILS(id){
        return this.ZENDESK_BASIC_REQUESTS_SEGMENT_URL() + this.ZENDESK_ID_PARAM(id) + this.ZENDESK_REQUESTS_COMMENTS_SEGMENT_URL + this.ZENDESK_REQUESTS_JSON_URL
    },
    ZENDESK_TICKET_FORMS(){
        return this.ZENDESK_BASIC_URL + this.ZENDESK_TICKET_FORMS_STRING
    },

    // wikis
  //  WIKI_URL:'http://207.154.211.132:3000/?contentType=wikis'
    WIKI_URL:'https://companion.paradoxplaza.com/?contentType=wikis'
    
}



export default {
    environment,
    common
}