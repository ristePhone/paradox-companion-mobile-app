/**
 *  Used for setting up and manipulate with configuration 
 */

'use strict'
import { NativeModules, Platform } from 'react-native'
import CONFIGURATION from './environment/config'

// Wrapper for using different bridge module for each platform
const configurationManager = Platform.OS == 'ios' ? NativeModules.ConfigurationManager : NativeModules.ConfigurationModule

/**
 * Get configuration from config.json file, based on environment for Native implementation
 * 
 * Props:
 * @param {string} environment 
 */
const getConfiguration = environment => {
    switch (environment) {
        case CONFIGURATION.environment.dev.name:
            return CONFIGURATION.environment.dev
        case CONFIGURATION.environment.stage.name:
            return CONFIGURATION.environment.stage
        case CONFIGURATION.environment.production.name:
        default:
            return CONFIGURATION.environment.production
    }
}

/**
 *  Setup initial configuration
 *  @returns {Dictionary} - Returns the Promise based on setup in config.json file
 */
export async function configurationSetup() {
    const environment = configurationManager.environment
    const configuration = getConfiguration(environment)
    const common = CONFIGURATION.common
    const result = Object.assign({}, configuration, common)
    return result
}