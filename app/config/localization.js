import LocalizedStrings from 'react-native-localization';

const strings = new LocalizedStrings({
    en: {
        signUp: {
            failed: 'Sign Up Failed',
            success: {
                title: 'Congratulations',
                message: 'Your account have been successfully created',
                button: 'Next',
            },
            read_terms_of_service: 'Read Terms of Service',
            read_privacy_policy: 'Read Privacy Policy',
            title: 'Sign up for Paradox Account',
            email_description: 'We use this to contact you and verify account ownership if this account is ever compromised or to send you reset options for your password.',
            already_have_account: 'Already have an account?',
        },
        login: {
            failed: 'Login Failed',
            error: {
                pass_not_long:'Password must be longer than 6 characters',
                email_incorrect: 'Email does not match',
                pass_incorrect: 'Password does not match'
            }
        },
        error: {
            unknown: 'Something went wrong',
        },
        retry: {
            buttonTitle: 'Retry',
            title: 'Uh oh!',
            message: 'Something went wrong while retrieving the data from the server',
        },
    

        email_address_placeholder: 'Email address',
        password: 'Password',
        forgot_password: 'Forgot your password?',
        dont_have_account: 'Don\'t have an account?',
        country: 'Country',
        date_of_birth: 'Date of birth',
        sign_up_description: 'Now is a great time to create one! Sign up takes less than two minutes and will give you access to all Paradox things!',
        skip_for_now: 'Skip for now',
        sign_up_with_paradox_account: 'Sign up for \na Paradox Account',
        join_paradox_comunity: 'Yes, I want to join Paradox Community \nnewsleter',
        terms_of_service_checkbox_text: 'I hereby certify, that I have read and agree to\nParadox\'s Terms of Service ',
        privacy_policy_checkbox_text: 'I hereby certify, that I have read and agree to \nParadox\'s Privacy Policy ',
        feedback: 'Feedback',
        showHelpCenter: 'Show Help Center',

        // account
        account_title: 'Account',

        // login screen
         login_button_create_account:'Create account',
         login_button_login:'Login',
         login_to_paradox: 'Login with your \nParadox Account',

        // request submit

        title_create_request: 'Submit a request',
        label__request_ticket_form_id: 'Please select what your problem is regarding and provide all the necessary information  ',
        label_request_subject: 'Summary',
        label_hint_request_subject_hint: 'Provide a good summary of your problem, e.g. "Can' + 't start the game" or "Unable to purchase Ducats using credit card".  ',
        label_request_description: 'Description',
        label_hint_request_description_hint: 'Please describe your problem in detail. If relevant, please describe your PC hardware configuration, your Windows version and web browser version.  ',
        label_custom_fields: 'Custom fields',
        request_submit_notify_request_crated: 'The request has been submited!',
        request_submit_validation_select_issue: 'Please select Issue type',
        request_submit_validation_select_summary: 'Please enter Summary',
        request_submit_validation_select_description: 'Please enter Description',
        request_submit_button_next: 'Next',

        // request details
        request_details_button_send:'Submit',
        request_details_button_close:'Close ticket',
        request_details_label_comment:'Enter comment',
        reqeust_details_label_header_comment:'Comment:',
        reqeust_details_label_header_date:'Date:',
        request_details_title_navigation: 'Request details',
        request_details_notify_request_closed: 'Request solved',
        request_details_notify_comment_updated: 'Comment updated',
        request_details_validation_enter_comment: 'Please enter comment',

        // reqeust feedback
        request_feedback_label_subject:'Subject',
        request_feedback_label_message:'Message',
        request_feedback_label_email:'Email',
        request_feedback_label_not_robot:'I' + "'" + 'm not a robot',
        request_feedback_placeholder_email:'Email',
        request_feedback_placeholder_subject:'Subject',
        request_feedback_placeholder_message:'Write a message',
        request_feedback_button_submit:'Submit',
        request_feedback_notify_email_not_valid:'Email format is not valid',
        request_feedback_notify_subject:'Subject is required',
        request_feedback_notify_message:'Message is required',
        request_feedback_notify_succeed:'Thank you for your feedback',
        request_feedback_notify_not_robot:'Confirm you are not a robot.',
      

        // request list
        request_list_label_subject: 'Subject: ',
        request_list_label_date: 'Date:',
        request_list_label_id: 'ID: ',
        request_list_label_state: 'State: ',
        request_list_label_no_open_reqeusts: 'There is no open requests',
        
        // request titles 
        request_navigator_title_submit_request: 'Submit request',
        request_navigator_title_open_requests: 'Open requests',
        request_navigator_title_send_feedback: 'Send feedback',

        // feedback 
        feedback_button_submit_request:'Submit request',
        feedback_button_open_requests:'Open requests',
        feedback_button_send_feedback:'Send feedback',


        // wikis
        search_wikis: 'Search wikis',

        tabs: {
            home: 'Home',
            news: 'News',
            feedback: 'Feedback',
            wiki: 'Wikis'
        },
        navigation_titles: {
            eventDetails: 'Event details',
            newsDetails: 'News details',
            terms_of_service: 'Terms of Service',
            privacy_policy: 'Privacy Policy',
        },
        event: {
            header: 'Upcoming Events',
            start: 'Start',
            startLong: 'Event Start',
            finish: 'Finish',
            finishLong: 'Event finish',
            location: 'Location',
            tags: 'Tags',
            postedOn: 'Posted on',
            noUpcommingEvents: 'No upcoming events',
        }
    },
})

export {
    strings
}