// Configuration
export const UPDATE_CONFIGURATION = "UPDATE_CONFIGURATION"
export const UPDATE_ROOT_CONTROLER = 'UPDATE_ROOT_CONTROLER'

// Navigation
export const SETUP_NAVIGATION = 'SETUP_NAVIGATION'
export const PUSH = 'PUSH'
export const POP = 'POP'
export const SHOW_MODAL = 'SHOW_MODAL'
export const SHOW_LIGHT_BOX = 'SHOW_LIGHT_BOX'

// User
export const UDPATE_USERNAME = 'UDPATE_USERNAME'
export const UPDATE_PASSWORD = 'UPDATE_PASSWORD'
export const UPDATE_USER_LOGGED_IN_STATUS = 'UPDATE_USER_LOGGED_IN_STATUS'

// Contentful
export const UPDATE_HOME_SCREEN = "UPDATE_HOME_SCREEN"
export const UPDATE_CONTENTFUL_API_CONTENT = 'UPDATE_CONTENTFUL_API_CONTENT'
export const UPDATE_CONTENTFUL_NEWS = 'UPDATE_CONTENTFUL_NEWS'
export const UPDATE_CONTENTFUL_FEEDBACK = 'UPDATE_CONTENTFUL_FEEDBACK'
export const UPDATE_CONTENTFUL_WIKI = 'UPDATE_CONTENTFUL_WIKI'
export const UPDATE_APPLICATION_ASSETS = 'UPDATE_APPLICATION_ASSETS'

// News Details
export const UPDATE_SELECTED_NEWS_FEED_ITEM = 'UPDATE_SELECTED_NEWS_FEED_ITEM'

// Event Details
export const UPDATE_SELECTED_EVENT = 'UPDATE_SELECTED_EVENT'

//Wiki Details
export const UPDATE_SELECTED_WIKI = "UPDATE_SELECTED_WIKI"