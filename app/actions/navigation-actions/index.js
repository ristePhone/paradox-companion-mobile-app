import { Navigation } from 'react-native-navigation'
import { defaultLightBoxStyle } from './utils'

/**
 *  LightBox. Overflow screen that shows when something is wrong.
 *  Can be used for Custom Alert Screens
 */
const SHOW_LIGHT_BOX_TIMEOUT = 2000

const showLightBox = params => {
    // TODO: Petar - Uncomment code for retry screen, when the implementation is tested
    //               and all the use cases are covered

    // const style = params.style ? params.style : defaultLightBoxStyle
    // const passProps = params.passProps ? params.passProps : null
    // setTimeout(() => {
    //     Navigation.showLightBox({
    //         screen: params.screen,
    //         passProps,
    //         ...style,
    //     })
    // }, SHOW_LIGHT_BOX_TIMEOUT);
}

const hideLightBox = () => {
    Navigation.dismissLightBox()
}

export default {
    showLightBox,
    hideLightBox,
}
 