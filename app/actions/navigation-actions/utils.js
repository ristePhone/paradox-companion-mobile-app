
const defaultLightBoxStyle = {
    style: {
        backgroundBlur: 'dark', // 'dark' / 'light' / 'xlight' / 'none' - the type of blur on the background
        backgroundColor: 'transparent', // tint color for the background, you can specify alpha here (optional)
        tapBackgroundToDismiss: false // dismisses LightBox on background taps (optional)
    }
}

export {
    defaultLightBoxStyle,
}