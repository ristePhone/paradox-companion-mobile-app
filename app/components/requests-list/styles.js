import glamorous from 'glamorous-native'
import Colors from '../../config/colors'
import { Dimensions } from 'react-native'
let { height, width } = Dimensions.get('window')

export const RowContainer = glamorous.view({
    flexDirection: 'column',
    backgroundColor: Colors.DARK_GRAY_COLOR,
    justifyContent: 'space-between',
    flex: 1,
    height: 101,
   // height: 121,
    borderRadius:6
})

export const RowInContainer = glamorous.view({
    flexDirection: 'row',
    backgroundColor: Colors.DARK_GRAY_COLOR,
    alignItems: 'center',
    justifyContent: 'space-between',
    flex: 1,
    padding: 14,
    borderRadius:6,
  //  height:60
})

export const RowSeparatorContainer = glamorous.view({
    flexDirection: 'row',
    justifyContent: 'center',
    paddingHorizontal:15, 
    //flex:1,

})
    
export const RowInConatainerSeparator = glamorous.view({
    flexDirection: 'row',
    backgroundColor: Colors.REQUEST_LIST_DETAILS_WHITE,
    backgroundColor: '#222222',
    justifyContent: 'center',
    flex: 1,
    height: 2,
})

export const RowCenter = glamorous.view({
    flexDirection: 'row',
    flex:1,
    justifyContent: 'center',
})
export const ColumnCenter = glamorous.view({
    flexDirection: 'column',
    flex:1,
    justifyContent: 'center',
})

export const NoRequestView = glamorous.text({
    color: 'white',
    textAlign: 'center',
    fontSize: 15,
    fontFamily: 'Arial',
    marginRight: 20
})

export const RowTitleView = glamorous.text({
    color: 'white',
    textAlign: 'left',
    fontSize: 13,
    fontFamily: 'Arial',
    marginRight: 20
})

export const RowDetailsView = glamorous.text({
    color: 'white',
    textAlign: 'left',
    fontSize: 11.5,
    fontFamily: 'Arial',
    //marginRight: 20
})

export const SeparatorContainer = glamorous.view({
    height: 10,
    width: 1
})

export const HeaderFooterSeparator = glamorous.view({
    height: 15,
    width: 1
})

export const GrayRow = glamorous.text({
    //color: '#adadad',
    color: Colors.REQUEST_LIST_DETAILS_GRAY,
    
})
export const WhiteRow = glamorous.text({
    //color: '#eeeeee',
    color: Colors.REQUEST_LIST_DETAILS_WHITE
})