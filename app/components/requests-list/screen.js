import React, { Component } from 'react'
import { View, TouchableWithoutFeedback, FlatList, Text, AppState } from 'react-native'
import { RowContainer, RowCenter, NoRequestView,  RowTitleView, SeparatorContainer,
     HeaderFooterSeparator, RowInContainer, RowInConatainerSeparator, RowSeparatorContainer,RowDetailsView,
     GrayRow, WhiteRow } from './styles'

// services 
import RequestService from '../../services/request-service'
import Utils from '../../services/utils-service'

// constants
import Configuration from '../../config/environment/config'
import Colors from '../../config/colors'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { strings } from '../../config/localization'
//import {decode as atob, encode as btoa} from 'base-64'
import { Base64 } from 'js-base64';
import  ZendeskService  from '../../services/zendesk-service'
import SpinnerView from '../common/spinner/ui'

export default class RequestHistoryScreen extends Component {
    constructor(props) {
        super(props)
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this))
        this.state = {
            requests: [],
            resultsExist: false,
            loadingText: '',
            spinnerVisible:false,
            appState: AppState.currentState
        }
    }
    
    onNavigatorEvent(event) {
        switch(event.id) {
            case 'didDisappear':  this.resetScrollToTop()   // reset scroll to top position when tab is closed
            break;
        }
    }
    resetScrollToTop = () => {
        if(this.flatList != undefined) { this.flatList.scrollToOffset({x: 0, y: 0, animated: false}) }
    }
    
    // initialize
    componentWillMount() {
        if (this.props.notify == true) {
            alert(this.props.notifyMessage)
        }
        this._showOpenRequests()
       
    }

    componentDidMount() {
        AppState.addEventListener('change', this._handleAppStateChange);
      }
    
      componentWillUnmount() {
        AppState.removeEventListener('change', this._handleAppStateChange);
        console.log('component will unmount')
      }
    
      _handleAppStateChange = (nextAppState) => {
        if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
          console.log('App has come to the foreground!')
          this._showOpenRequests()
        }
        this.setState({appState: nextAppState});
      }

    
    // api call to get all open requests
    async _showFields(){

    }

    async _showOpenRequests() {  
        this.setState({ spinnerVisible: true })
        let url = Configuration.common.ZENDESK_REQUESTS_SHOW_OPEN_URL()
        //var headers = Configuration.common.ZENDESK_TEMP_AUTH
        //var headers = { 'Authorization': this.setHeaders()}
        var headers = await ZendeskService.setHeaders()
        let result = await RequestService.get(url,headers)
        this.setState({ requests: result.requests, loadingText:strings.request_list_label_no_open_reqeusts })
        //this.setState({ requests: result.requests })
       // alert(result.requests.length)
        this.setState({ spinnerVisible: false })
        if(result.requests != undefined && result.requests.length!=0)
        {
            this.setState({ resultsExist:true, loadingText: strings.request_list_label_no_open_reqeusts })
            return
        }

         //alert("it's not ok")
        
       
    }
    
    _renderRow = ({ item, index }) => {
        return (
            <TouchableWithoutFeedback onPress={() => this.props._onPressButton(this.props, item)}>
                <RowContainer>

                    <RowInContainer>
                        <RowTitleView>
                            <GrayRow>{strings.request_list_label_subject}</GrayRow>
                            <WhiteRow>{item.subject}</WhiteRow>
                        </RowTitleView>
                        <RowTitleView>
                           <Ionicons name='ios-arrow-forward' size={22} color='white' />
                        </RowTitleView>
                    </RowInContainer>
                    
                    <RowSeparatorContainer>
                        <RowInConatainerSeparator></RowInConatainerSeparator>
                    </RowSeparatorContainer>

                    <RowInContainer>
                        <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
                            <RowDetailsView>
                                <GrayRow>{strings.request_list_label_id}</GrayRow>
                                <WhiteRow> #{item.id} </WhiteRow>
                             </RowDetailsView>
                            <RowDetailsView> 
                                <WhiteRow>{Utils.formatDateToGMTOffset(item.created_at)}</WhiteRow>
                            </RowDetailsView>
                            <RowDetailsView>
                                <GrayRow>{strings.request_list_label_state}</GrayRow>
                                <WhiteRow>{item.status}</WhiteRow>
                            </RowDetailsView>
                         </View>
                    </RowInContainer>

                </RowContainer> 
            </TouchableWithoutFeedback>
        )
    }

    _renderSeparatorComponent = () => <SeparatorContainer />

    _renderHeaderFooterComponent = () => <HeaderFooterSeparator />

    _renderHeaderFooterEmptyComponent = () => {
        return (

            <View style={{
                flex: 1,
                flexDirection: 'column',
                justifyContent: 'center',
              }}>

                <View style={{width: 50, height: 50, backgroundColor: 'skyblue'}} />
              
              </View>

            // <ColumnCenter>

            //     <Text>{strings.request_list_label_no_open_reqeusts} </Text>

            //  </ColumnCenter>
        )
       
    }
    
    render() {
        const noitems = <View style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'center',
          }}>


                <NoRequestView>{this.state.loadingText}</NoRequestView>

          
          </View> ;
          
        const items =  <FlatList
        ref={component => this.flatList = component}
        indicatorStyle='white'
        style={{ flex: 1, paddingHorizontal: 10, backgroundColor: Colors.BACKGROUND_COLOR}}
        data={this.state.requests}
        keyExtractor={(item, index) => `${item}-${index}`}
        renderItem={this._renderRow} 
        ItemSeparatorComponent={this._renderSeparatorComponent}
        ListHeaderComponent={this._renderHeaderFooterComponent}
        ListFooterComponent={this._renderHeaderFooterComponent}/>;

        return (
        <View style={{flex:1}}>
                 <SpinnerView visible={this.state.spinnerVisible} />
                 { this.state.resultsExist ? items : noitems}
            </View>
           

           
        )
    }
}
