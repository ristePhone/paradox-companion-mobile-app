import { connect } from 'react-redux'
import RequestHistoryScreen from '../requests-list/screen'
import * as Constants from '../../utils/constants'
import { signUp, createSession, validateEmail } from '../../services/authentication'
import Routes from '../../config/routes'
import { store } from '../../reducers/index'
import { UPDATE_ROOT_CONTROLER, UPDATE_USER_LOGGED_IN_STATUS } from '../../actions/action-types'
import { strings } from '../../config/localization'

const mapStateToProps = state => ({
    mainImageUrl: state.contentfulAPIcontent.mainImageUrl
})

const mapDispatchToProps = dispatch => ({
    _onPressButton: (props, item) =>{
        props.navigator.push({
            screen: Routes.REQUEST_DETAILS,
            title: strings.request_details_title_navigation,
            passProps: {
                item: item
            }
        })
    },
    _backOnFeedback: props  =>{
        props.navigator.push({
            screen: Routes.FEEDBACK,
            title: strings.request_details_title_navigation,
           // passProps: {
                //item: item
            //}
        })
    },
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RequestHistoryScreen)