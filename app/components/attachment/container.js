import { connect } from 'react-redux'
import FeedbackCreateScreen from '../attachment/screen'
import * as Constants from '../../utils/constants'
import { signUp, createSession, validateEmail } from '../../services/authentication'
import Routes from '../../config/routes'
import { store } from '../../reducers/index'
import { UPDATE_ROOT_CONTROLER, UPDATE_USER_LOGGED_IN_STATUS } from '../../actions/action-types'
import { strings } from '../../config/localization'
const mapStateToProps = state => ({
    mainImageUrl: state.contentfulAPIcontent.mainImageUrl
})

const mapDispatchToProps = dispatch => ({
    _navigateToRequestList: props => {
        //    props.navigator.push({
                props.navigator.resetTo({
                screen: Routes.FEEDBACK,
                title: strings.request_navigator_title_open_requests,
                passProps: {
                    notify: true,
                    notifyMessage: strings.request_feedback_notify_succeed
                },
            })
        }
    // onSignUp: parameters => {
    //     signUp(parameters).then(response => {
    //         if (String(response.result).toLowerCase() == 'ok') {
    //             createSession(parameters.email, parameters.password).then(result => {
    //                 if (String(response.result).toLowerCase() == 'ok') {

    //                     AsyncStorage.setItem('credentials', JSON.stringify({ email, password }))
                        
    //                     dispatch({ type: UPDATE_USER_LOGGED_IN_STATUS, isLoggedIn: true })
    //                     dispatch({ type: UPDATE_ROOT_CONTROLER, rootTypeController: 'tabBasedApp' })
    //                 }
    //             })
    //         } else {
    //             alert(response.errorMessage)
    //         }
    //     })
    // },
    // onTermsAndConditionButtonPress: props => {
    //     props.navigator.push({
    //         screen: Routes.WEB,
    //         passProps: {
    //             url: store.getState().config.TERMS_AND_CONDITIONS_URL
    //         }
    //     })
    // },
    // onPrivacyPolicyButtonPress: props => {
    //     props.navigator.push({
    //         screen: Routes.WEB,
    //         passProps: {
    //             url: store.getState().config.PRIVACY_POLICY_URL
    //         }
    //     })
    // },
    // onLogin: props => {
    //     props.navigator.pop()
    // }
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(FeedbackCreateScreen)