import glamorous from 'glamorous-native'
import { Dimensions, Platform } from 'react-native'
import colors from '../../config/colors';
let { height, width } = Dimensions.get('window')
let AndroidBTn
// if (Platform.OS == 'android') {
//     AndroidBTn = 10
//     if (height < 660) {
//         height = 140
//     } else {
//         height = 190
//     }
// } else {
//     if (height < 680) {
//         height = 140
//     } else {
//         height = 190
//     }
//     AndroidBTn = 50
// }
export const ScrollViewContainer = glamorous.scrollView({
    flex: 1,
    paddingTop: 15,
    paddingHorizontal: 10,
})
export const FormContainer = glamorous.view({
    paddingHorizontal: 15,
   // paddingTop:10,
    //paddingVertical: 15,
    paddingBottom:15,
    width: width - 2 * 15,
    backgroundColor: colors.DARK_GRAY_COLOR,
    borderRadius: 4
})

export const RequestHeadersTextView = glamorous.text({
    color: colors.FEEDBACK_HEADERS_WHITE,
    fontSize: 12,
    marginTop:10
})

export const RequestBodyTextView = glamorous.text({
    color: colors.FEEDBACK_HEADERS_WHITE,
    fontSize: 12,
    marginTop: 15
})

export const FormTextInputField = glamorous.textInput(props => ({
        height: 37,
        backgroundColor: 'white',
        borderRadius: 2,
        paddingLeft: 15.5,
       // marginTop: 8,
        marginTop: props.marginTop ? props.marginTop : 10,
        fontSize: 12
    }))


export const FormTextInputMessageField = glamorous.textInput(props => ({
        textAlignVertical: 'top',
       // height: height,
        height: 200,
        backgroundColor: 'white',
        borderRadius: 2,
        paddingLeft: 15,
        paddingTop: 15,
       // marginTop: 8,
        marginTop: props.marginTop ? props.marginTop : 10,
        fontSize: 12,

    }))


export const ActionButtonContainer = glamorous.view({
    justifyContent: 'center',
    marginTop: AndroidBTn,
    flexDirection: 'row'
})

const Error_Color = '#fe0642'
export const ErrorContainerView = glamorous.view({
    marginTop:10,
    backgroundColor: Error_Color,
    height:75, 
    borderRadius:2,
    padding:2
})
export const ErrorContainerMessageView = glamorous.view({
    marginTop:10,
    backgroundColor: Error_Color,
    height: height + 30, 
    borderRadius:2,
    padding:2
})
export const ErrorView = glamorous.view(props => ({
    backgroundColor: props.error ? props.color : '',
    height:25, 
    alignItems: 'center',
    justifyContent: 'center',
}))

export const ErrorText = glamorous.text({
    flex: 1,
    fontFamily: 'Arial',
    color: 'white',
    fontSize: 13,
    marginTop:5,
    color:'white'
})