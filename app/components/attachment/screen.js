import React, { Component} from 'react'
import {
    StyleSheet,
    Dimensions,
    KeyboardAvoidingView,
    Keyboard,
    ScrollView,
    Platform,
    View,
    Text,
    BackHandler, 
    Alert ,
    AsyncStorage,
    Image
} from 'react-native'
import {  validateEmail } from '../../services/authentication'
import RequestService from '../../services/request-service'
import Colors from '../../config/colors'
//import countries from '../../utils/countries.json'
//import issueTypes from '../../utils/issueTypes.json'
//import Utils from '../../services/utils-service'
import ActionButton from '../views/action-button'
//import LogoImageView from '../common/main-image-view/logo-image-view'
//import PickerView from '../views/native-picker'
import BackgroundView from '../views/background-view'
//import { validateEmail } from '../../services/authentication'
//const FONT_COLOR = '#EEF0F2'
import ZendeskService from '../../services/zendesk-service'
import {
    ScrollViewContainer,
    ScrollViewContentView,
    FormContainer,
    FormTitleContainer,
    FormTitleTextView,
    FormTextInputField,
    FormTextInputField1,
    ActionButtonContainer,
    CheckBoxContainer,
    FooterView,
    BottomContainer,
    AlreadyHaveAcountTextView,
    RequestHeadersTextView,
    RequestHeadersHintTextView,
    RequestBodyTextView,
    FormTextInputMessageField,
    ErrorContainerView,
    ErrorView,
    ErrorText,
    ErrorContainerMessageView
} from './styles'
import RNFetchBlob from 'rn-fetch-blob'
import { configurationSetup } from '../../config/config'

import CheckBoxView from '../common/checkbox-view'
import { store } from '../../reducers/index'
import { strings } from '../../config/localization'
import Configuration from '../../config/environment/config'

//constants
const  middleware_zendesk  = () => {
    const name = store.getState().config.name
    //return Configuration.environment[name].MIDDLEWARE_BASE_URL     
    return Configuration.environment[name].MIDDLEWARE_BASE_URL + '?env=' + name
}

const requeiredSign = "*"

 

export default class FeedbackCreateScreen extends Component {
    constructor(props) {
        let { height, width } = Dimensions.get('window')
        super(props)
        this.state = {
            description: '',
            email: '',
            subject: '',
            message: '',
            height: height,
            checkBoxNotRobot: false,
            url:''
        }
    }

    componentDidMount() {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
       // this.setState({email: store.getState().username})
        AsyncStorage.getItem('username').then((data) => {
            if (data != null) {
                this.setState({ email: data })
            } 
            //else {
                //this.setState({email: 'You are not logged in!'})
            //}

        })
    }

    componentWillUnmount(props) {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    _keyboardDidShow() {
        this._hideNavBar()
    }

    _keyboardDidHide() {
        this._showNavBar()
    }

    _showNavBar() {
        this.props.navigator.toggleTabs({
            to: 'shown',
            animated: false
        });
    }

    _hideNavBar() {
        this.props.navigator.toggleTabs({
            to: 'hidden',
            animated: false
        });
    }

    _validateForm() {
        let email = this.state.email.trim()
                
        if (this.state.subject.trim().length == 0) {
            //Keyboard.dismiss()
           // setTimeout(() => {
                Alert.alert(strings.request_feedback_notify_subject)
                return false

            //}, 300);
        }

        let error = validateEmail(this.state.email)
        if(error != null && email.length>0)
        {
           // this.setState({emailFieldError: true, emailFieldErrorMessage: error})
           // return error
           Alert.alert(strings.request_feedback_notify_email_not_valid)

           return false
        }

        if (this.state.message.trim().length == 0) {
           // Keyboard.dismiss()

            //setTimeout(() => {
                Alert.alert(strings.request_feedback_notify_message)
                return false

            //}, 300);

            // alert(strings.request_feedback_notify_message)
        } 
        
        if(!this.state.checkBoxNotRobot)
        {
            Alert.alert(strings.request_feedback_notify_not_robot)
            return false
        }

        return true

        
    }
    _clearState() {
        this.setState({ subject: '', message: '' })
    }
    async sendImageToZendesk () {
       // if (imageURL) {
            const { ZENDESK_BASIC_URL } = await configurationSetup()
            const baseUrl = `${ZENDESK_BASIC_URL}/api/v2/uploads.json?filename=attachment.png`
            const headers = {
                'Autorization': 'Basic cmlzdGUuc3Bhc2Vza2lAZGF0YWpvYi5zZS8yVzVKRGkxbGFLYVpnU0JHSVZWdWN4cmh3YUVNQ0t0b1AzMXkwWnFCOjJXNUpEaTFsYUthWmdTQkdJVlZ1Y3hyaHdhRU1DS3RvUDMxeTBacUI=',
                'Accept': 'application/json'
            }
        
           // const wrapFileURL = RNFetchBlob.wrap(imageURL)
            const wrapFileURL = RNFetchBlob.wrap(this.state.response.origURL)
            return RNFetchBlob.fetch('POST',
                baseUrl,
                headers,
                wrapFileURL)
                .then(response => {
                    console.log(response)
                    return response.json()
                })
                .then(responseData => {
                    console.log(responseData)
                    return responseData
                })
                .catch(error => {
                    console.log(responseData)
                    return { error: error }
                })
        //}
    
        return { error: 'No image url provided'}
    }
    _onSubmitSend(){
        console.log(this.state.response)
        console.log(this.state.response.origURL)
        let test =  this.sendImageToZendesk()
        console.log(test)
    }

    _onSubmit() {
        var ImagePicker = require('react-native-image-picker');

        // More info on all the options is below in the README...just some common use cases shown here
        var options = {
          title: 'Select Avatar',
          customButtons: [
            {name: 'fb', title: 'Choose Photo from Facebook'},
          ],
          storageOptions: {
            skipBackup: true,
            path: 'images'
          }
        };
        
        /**
         * The first arg is the options object for customization (it can also be null or omitted for default options),
         * The second arg is the callback which sends object: response (more info below in README)
         */
        ImagePicker.showImagePicker(options, (response) => {
          console.log('Response = ', response);
        
          if (response.didCancel) {
            console.log('User cancelled image picker');
          }
          else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          }
          else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
            // Open Image Library:
        ImagePicker.launchImageLibrary(options, (response)  => {
            // Same code as in above section!
            let source = { uri: response.uri };
            let source1 = { uri: 'data:image/jpeg;base64,' + response.data };
            console.log('after select attach response is ')
            console.log(response)
           // console.log(source1)
            //console.log(response.uri)
            this.setState({
                url: source,
                url1: source1,
                response: response
            })
        
          });
          }
          else {
            let source = { uri: response.uri };
        
            // You can also display the image using data:
             let source1 = { uri: 'data:image/jpeg;base64,' + response.data };
            console.log('source 1 is ')
            console.log(source1)
            this.setState({
              avatarSource: source
            });
          }
        });




        return


        if (!this._validateForm())
            return

        var url = `${middleware_zendesk()}`

         var body = JSON.stringify({
             mail: this.state.email,
             subject: this.state.subject,
             body: this.state.message
         })
         
        var headers = {'Content-Type': 'application/json'}

         console.log("feedback url is")
         console.log(url)
         console.log(body)
         //return

        RequestService.post1(url, headers, body)

        this._clearState()
        this.props._navigateToRequestList(this.props)

    }
    setSel(input) {
        if (Platform.OS == 'android') {

            if (input == 'inputArea') {
                if (this.state.height < 660) {
                    setTimeout(() => {
                        this.refs._scrollView.scrollTo({ x: 0, y: 200, animated: true })
                    }, 500)
                    //    this.setKybordOpenAnroid()
                } else {
                    setTimeout(() => {
                        this.refs._scrollView.scrollToEnd({ animated: true })
                    }, 500)
                    // this.setKybordOpenAnroid()
                }

            } else {
                //this.setKybordOpenAnroid()
            }
        }
        if (input == 'input') {
            this.refs._scrollView.scrollTo({ x: 0, y: 0, animated: true })

        } else {
            setTimeout(() => {
                this.refs._scrollView.scrollTo({ x: 0, y: 0, animated: true })
            }, 300);
        }

    }
    hideKeyboard() {
        try {
            this.refs._scrollView.scrollTo({ x: 0, y: 0, animated: true })
        }
        catch (err) {
            return
        }
    }
    handleBackButtonClick() {
       // alert('test')
    }
    _renderEmailInput(){return(
        <FormTextInputField
            keyboardType = "email-address"
            autoCapitalize ="none"
            value={this.state.email}
            autoCorrect={false}
            // onBlur={()=>this.hideNav()}
            onFocus={() => this.setSel('input')}
            placeholder={strings.request_feedback_placeholder_email}
            onChangeText={(text) => { this.setState({ email: text }) }}
            underlineColorAndroid='white' />
    )}
    _renderSubjectInput(){return(
            <FormTextInputField
                value={this.state.subject}
                autoCorrect={false}
                // onBlur={()=>this.hideNav()}
                onFocus={() => this.setSel('input')}
                placeholder={strings.request_feedback_placeholder_subject}
                onChangeText={(text) => { this.setState({ subject: text }) }}
                underlineColorAndroid='white' />
    )}
    _renderErrorSubject(){return(
        <ErrorContainerView> 
             <ErrorView>
                <ErrorText>error</ErrorText> 
            </ErrorView>
        </ErrorContainerView> 
    )}
    _renderMessageInput(){return(
                <FormTextInputMessageField
                    selection={this.state.select}
                    multiline={true}
                    //selection={{start: 0,end: 0}}
                    // multiline 
                    //   onBlur={()=>this.hideNav()}
                    innerRef={(node) => { this.inputNode = node; }} onFocus={() => this.setSel('inputArea')}
                    value={this.state.message}
                    autoCorrect={false}
                    placeholder={strings.request_feedback_placeholder_message}
                    onChangeText={(text) => { this.setState({ message: text }) }}
                    underlineColorAndroid='white' />
    )}
    _renderErrorMessage(){return(
        <ErrorContainerMessageView> 
            {this._renderMessageInput()}
            <ErrorView>
                <ErrorText>error</ErrorText>
            </ErrorView>
        </ErrorContainerMessageView> 
    )}
    // Lifecycle
    render() {
       
        return (
            <View
                style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginTop: 20
                }}>
                <ScrollView ref="_scrollView" keyboardShouldPersistTaps='handled'>
                    <FormContainer ref='modal'>

                        <Image
          style={{width: 150, height: 150}}
          source={this.state.url}
        />
                        <ActionButtonContainer>
                            <ActionButton
                                title='attach'
                                backgroundColor={Colors.GREEN_COLOR}
                                onPress={() => this._onSubmit()} />
                        </ActionButtonContainer> 
                        <ActionButtonContainer>
                            <ActionButton
                                title='send attach'
                                backgroundColor={Colors.GREEN_COLOR}
                                onPress={() => this._onSubmitSend()} />
                        </ActionButtonContainer> 



                        {/* <RequestHeadersTextView>{strings.request_feedback_label_subject}<Text style={{color: 'red'}}> {requeiredSign} </Text></RequestHeadersTextView>
                        {this._renderSubjectInput()}

                        <RequestHeadersTextView>{strings.request_feedback_label_email}</RequestHeadersTextView>
                        {this._renderEmailInput()}

                        <RequestBodyTextView>{strings.request_feedback_label_message}<Text style={{color: 'red'}}> {requeiredSign} </Text></RequestBodyTextView>
                        {this._renderMessageInput()}

                        <CheckBoxView
                                    title={strings.request_feedback_label_not_robot}
                                    selected={this.state.checkBoxNotRobot}
                                    onCheckBoxButtonPress={() => {
                                        this.setState({ checkBoxNotRobot: !this.state.checkBoxNotRobot })
                                    }}
                                />
                        
                        <ActionButtonContainer>
                            <ActionButton
                                title={strings.request_feedback_button_submit}
                                backgroundColor={Colors.GREEN_COLOR}
                                onPress={() => this._onSubmit()} />
                        </ActionButtonContainer> */}
                    </FormContainer>

                </ScrollView>
            </View>


        )
    }
}
