import React, { Component } from 'react'
import { View, StyleSheet, TouchableWithoutFeedback, Text, Image } from 'react-native'

import Colors from '../../../config/colors'

import * as Animatable from 'react-native-animatable';


export default class LoginCard extends Component {
    render() {
        return (
            <Animatable.View style={styles.container} animation='fadeInUp'>
                <Text style={styles.text} numberOfLines={2}>Login to get access to content custom-tailored just for you</Text>
                <TouchableWithoutFeedback onPress={() => this.props.onLogin()}>
                    <View style={styles.buttonContainer}>
                        <Image source={require('../../../resources/platypus_logo.png')} resizeMode='contain' />
                        <Text style={styles.text}>LOGIN NOW</Text>
                    </View>
                </TouchableWithoutFeedback>
            </Animatable.View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: 135,
        paddingHorizontal: 32,
        paddingVertical: 20,
        borderRadius: 8,
        alignItems: 'center',
        backgroundColor: '#313539',
        shadowOffset: { width: 0, height: 8 },
        shadowColor: 'rgba(0,0,0,0.2)',
        shadowOpacity: 2,
        shadowRadius: 5,
    },
    buttonContainer: {
        height: 40,
        width: 140,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
        borderRadius: 25,
        backgroundColor: Colors.GREEN_COLOR,
    },
    text: {
        fontSize: 14,
        lineHeight: 18,
        marginLeft: 6,
        color: 'white',
        textAlign: 'center'
    }
})