/**
 * 
 * 
 * @param items (array) - List of event items
 * 
 */
import React, { Component } from 'react'
import { StyleSheet, View, Text, TouchableHighlight } from 'react-native'

import Colors from '../../../config/colors'
import UtilsService from '../../../services/utils-service'

export default class ComingUpCard extends Component {
    render() {
        var moment = require('moment');

        var sorted = UtilsService.sordedByDate(this.props.items)
        console.log(sorted)
        return (
            <View style={styles.container}>
                <View style={styles.headerContainer}>
                    <Text style={styles.headerTitle}>EVENTS</Text>
                </View>
                {
                    sorted.map((item, index) => {
                        console.log(item)
                        //     console.log(item.eventStart)

                        //    let newDate = moment(Date(item.eventFinish)).format('YYYY,MMMM DD');
                        //     let startDate = moment(Date(item.eventStart)).format('DD.MM.YYYY,   HH : mm');
                        //     let finishDate = moment(Date(item.eventFinish)).format('DD.MM.YYYY   HH : mm');
                        //     console.log(startDate)
                        //     console.log(finishDate)
                        return (
                            <View>
                                <TouchableHighlight onPress={() => this.props.onPress(item)}>
                                    <View key={index} style={styles.itemContainer}>
                                        <View style={{ justifyContent: 'space-between', flexDirection: 'row', flex: 1 }}>
                                            <Text style={styles.itemContainerText} numberOfLines={1}>{item.eventHeadline}</Text>
                                            <Text style={styles.dataFormat} numberOfLines={1}>{UtilsService.transformDateEvents(item.eventFinish)}</Text>


                                        </View>
                                        <View style={styles.eventCont}>
                                            <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                                                <Text style={styles.dataFormatStart} numberOfLines={1}>Start: <Text style={{ color: '#fff',padding:8 }}>{UtilsService.transformDateStart(item.eventStart)}</Text></Text>
                                                <Text style={styles.dataFormatStart} numberOfLines={1}>Finish: <Text style={{ color: '#fff' ,padding:8}}>{UtilsService.transformDateStart(item.eventFinish)}</Text></Text>

                                            </View>
                                            <View style={{ justifyContent: 'space-between', flexDirection: 'row',}}>
                                                <Text style={styles.dataFormatStart} numberOfLines={1}>Location: <Text style={{ color: '#fff' }}>{item.eventLocation}</Text></Text>

                                            </View>
                                        </View>

                                    </View>
                                </TouchableHighlight>
                            </View>
                        )
                    })
                }
            </View>
        )
    }
}

{/* <Text style={styles.itemContainerText}>{UtilsService.formatDateString(item.eventStart)}, {UtilsService.formatHour(item.eventStart)}</Text>
 <Text style={[styles.itemContainerText, { marginTop: 5 }]} numberOfLines={2}transformDateEvents>{item.eventHeadline}</Text> */}
var styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#1a1a1a',
        borderRadius: 8
    },
    headerContainer: {
        height: 40,
        justifyContent: 'center',
        marginLeft: 20
    },
    dataFormatStart: {
        fontSize: 12,
        color: '#adadad',
        padding:8
    },
    eventCont:{
        flex:1,
        backgroundColor:'#222222',
    },
    separatorContainer: {
        flex: 1,
        height: 1,
        backgroundColor: 'rgba(255, 255, 255, 0.05)'
    },
    dataFormat: {
        fontSize: 12,
        color: '#adadad',
        padding:5

    },
    headerTitle: {
        fontFamily: 'OpenSans',
        fontSize: 16,
        color: '#fff',
        lineHeight: 18
    },
    itemContainer: {
        flex: 1,
        flexDirection: 'column',
        padding: 20,
        borderTopWidth: 0.5,
        borderColor: 'rgba(255, 255, 255, 0.05)',
    },
    itemContainerText: {
        fontFamily: 'OpenSans',
        fontSize: 16,
        lineHeight: 18,
        padding:5,
        color: 'white',
        marginRight: 15
    },
})