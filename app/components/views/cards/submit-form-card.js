import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    TextInput,
    Text
} from 'react-native'

import { SCREEN_HEIGHT, SCREEN_WIDTH } from '../../../utils/constants';
import Colors from '../../../config/colors'

import ActionButton from '../action-button';

export default class SubmitFormCard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            textInput: ''
        };
    }

    render() {
        return (
            <View style={styles.container}>
                <TextInput style={styles.textInput}
                    multiline={true}
                    placeholder='Write your heart out...'
                    placeholderTextColor='gray'
                    onChangeText={(text) => this.setState({ textInput: text })}
                    blurOnSubmit={true} />
                <View style={styles.acceptanceWrapper}>
                    <Text style={styles.acceptanceWrapperText}>Paradox may contact me to follow up on my feedback.</Text>
                </View>
                <View style={styles.buttonWrapper}>
                    <ActionButton
                        title='Submit'
                        backgroundColor='green'
                        onPress={(press) => { console.log("tapp") }}
                    />
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgba(0, 0, 0, 0.2)',
        borderColor: 'rgba(238, 240, 242, 0.1)',
        borderWidth: 1,
        borderRadius: 8,
        padding: 30
    },
    textInput: {
        flex: 10,
        backgroundColor: 'white',
        borderRadius: 2,
        padding: 15,
        paddingTop: 8,
        marginBottom: 6,
        lineHeight: 18
    },
    acceptanceWrapper: {
        flex: 2,
        justifyContent: 'center',
        marginBottom: 6
    },
    acceptanceWrapperText: {
        textAlign: 'left',
        fontSize: 12,
        color: 'white',
        marginLeft: 40,
        lineHeight: 16
    },
    buttonWrapper: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center'
    }
})