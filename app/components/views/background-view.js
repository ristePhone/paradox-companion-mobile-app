import React from 'react'
import { ImageBackground } from 'react-native'

export default class BackgroundView extends React.Component {
    render() {
        return (
            <ImageBackground
                style={{ flex: 1 , backgroundColor:'#222222'}}
                // source={require('../../resources/background.png')}
                resizeMode='cover'>
                {this.props.children}
            </ImageBackground>
        )
    }
}