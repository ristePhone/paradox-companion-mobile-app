
/**
 * 
 *  Component config based on one of the types: baron, king, emperor
 * 
 *  @param type (string)
 *  @param enabled (bool) - By default is false.
 *  @param alignment (string):
 *      - center (default)
 *      - left
 *      - right
 *  @param onSelection (callback) - Return a function
 */


import React from 'react'
import {
    View,
    StyleSheet,
    Text,
    Image,
    TouchableOpacity,
    ViewProps
} from 'react-native'
import { } from 'prop-types'

import Colors from '../../config/colors'

export default class EventLevelView extends React.Component {

    _config() {
        var icon, color, title, fontColor = 'white'
        switch (this.props.type) {
            case 'emperor':
                icon = require('../../resources/emperor.png')
                color = Colors.AMBER_COLOR
                title = 'emperor'.toUpperCase()
                width = 94
                break
            case 'baron':
                icon = require('../../resources/baron.png')
                color = 'gray'
                title = 'baron'.toUpperCase()
                width = 80
                break
            case 'king':
                icon = require('../../resources/king.png')
                color = Colors.RED_COLOR
                title = 'king'.toUpperCase()
                width = 70
                break
            case 'all':
            default:
                icon = require('../../resources/any_icon.png')
                color = 'white'
                title = 'all'.toUpperCase()
                width = 65
                fontColor = 'black'
                break
        }
        return { icon, color, title, width, fontColor }
    }

    render() {
        return (
            <TouchableOpacity onPress={() => this.props.onSelection(this.props.type)} disabled={this.props.isDisabled}>
                <View style={[styles.subContainer, { backgroundColor: this.props.isDisabled ? this._config().color : 'transparent', width: this._config().width, height: 26 }]}>
                    <Image style={styles.imageContainer} source={this._config().icon} />
                    <Text style={[styles.title, { color: this._config().fontColor }]}>{this._config().title}</Text>
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: 26,
        flex: 1,
        backgroundColor: 'purple',
        justifyContent: 'center',
        alignItems: 'center',
    },
    subContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 15
    },
    imageContainer: {
        width: 14,
        height: 14,
        marginRight: 8,
        marginLeft: 9
    },
    title: {
        fontFamily: 'Entypo',
        fontSize: 10,
        lineHeight: 18,
        color: 'white',
        marginRight: 10
    }
})