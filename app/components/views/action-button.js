/**
 * 
 *  Wrapper for all rounded button for the application
 * 
 * @param title (string)
 * @param backgroundColor (string)
 * @param onPress (function)
 * @param {object} style - Overides default font style
 */

import React, { Component } from 'react'
import { StyleSheet, View, TouchableOpacity, Text, TouchableHighlight } from 'react-native'
import Colors from '../../config/colors'

export default class ActionButton extends Component {
    constructor(props) {
        super(props)
        this.state = {
            height: 0,
            width: 0,
        }
    }

    _getColorBasedOnTitle() {
        switch (this.props.title) {
            case 'login':
            case 'submit':
                return Colors.GREEN_COLOR
            case 'sign up':
                return Colors.RED_COLOR
            case 'see agenda':
                return 'blue'
            default:
                return 'purple'
        }
    }

    render() {
        return (
            <TouchableOpacity disabled={this.props.disabled} onPress={() => this.props.onPress()}>
                <View style={[styles.container, {
                    backgroundColor: this.props.backgroundColor,
                    width: this.state.width,
                    height: this.state.height,
                    borderRadius: this.state.height
                }]}>
                    <Text style={[styles.text, this.props.style]} onLayout={(event) => {
                        var offset = 15
                        this.setState({ height: event.nativeEvent.layout.height + offset, width: event.nativeEvent.layout.width + 3 * offset })
                    }}>{this.props.title.toUpperCase()}</Text>
                </View>
            </TouchableOpacity>
        )
    }
}

var styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 4
    },
    text: {
        color: 'white',
        fontSize: 14.5,
        lineHeight: 18,
        textAlign: 'center',
        fontFamily: 'Entypo'
    }
})