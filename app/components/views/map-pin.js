import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    Text,
    TouchableOpacity
} from 'react-native'

export default class MapPin extends Component {
    render() {
        return (
            <TouchableOpacity onPress={() => this.props.onPinPress()} disabled={this.props.isDisabled}>
                <View style={[styles.container,
                {
                    backgroundColor: this.props.section.sectionColor,
                    borderWidth: this.props.section.sectionTag == 'WC' ? 2 : 0,
                    borderColor: this.props.section.sectionTag == 'WC' ? 'black' : 'transparent'
                }
                ]}>
                    <Text style={[styles.text, { fontSize: this.props.section.sectionTag == 'WC' ? 18 : 22, }]}>{this.props.section.sectionTag}</Text>
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        width: 44,
        height: 44,
        borderRadius: 22,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 3,
        shadowOpacity: 0.3,
        shadowOffset: {
            width: 1,
            height: 1
        },
    },
    text: {
        fontSize: 22,
        fontFamily: 'Entypo',
        textAlign: 'center',
        color: 'white'
    }
})