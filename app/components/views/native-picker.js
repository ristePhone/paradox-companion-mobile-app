/**
 * 
 *  Wrapper component for Android/IOS picker
 * 
 *  @param type (string) - Could be date, or sth other. Default is other.
 *  @param dataSource (array) - Must transform array before sending it to the component [ {label: '', value ''}]
 *  @param onDoneButtonPress (function)
 *  @param selectedValue (string)
 *  @param chosenDate (string) - Only if type is date. Expects Date() Object
 * 
 */

import React, { Component } from 'react'
import { Picker, DatePickerAndroid, DatePickerIOS, Platform, StyleSheet, Dimensions, Modal, View, Button, Text } from 'react-native'
const { height, width } = Dimensions.get('window')

var moment = require('moment');

export default class PickerView extends Component {
    constructor(props) {
        super(props)
        this.state = {
            selectedValue: (this.props.selectedValue) ? this.props.selectedValue : this.props.dataSource[0],
            selectedDate: (this.props.selectedDate) ? this.props.selectedDate : new Date()
        }
    }

    _minimumDate() {

    }

    _renderPickerView() {
        return (
            <Picker
                style={(Platform.OS == 'ios') ? styles.containerIOS : { flex: 1 }}
                selectedValue={this.state.selectedValue['name']}
                mode='dialog'
                prompt='Choose country'
                onValueChange={(value, position) => {
                    this.setState({ selectedValue: this.props.dataSource[position] })
                    if (Platform.OS != 'ios') this.props.onDoneButtonPress(this.props.dataSource[position])
                }} >
                {
                    this.props.dataSource.map(item => {
                        return <Picker.Item key={item.name} label={item.name} value={item.name} />
                    })
                }
            </Picker>
        )
    }

    render() {

        var maximumDate = new Date()
        maximumDate.setFullYear(maximumDate.getFullYear())

        if (Platform.OS == 'ios') {
            return (
                <Modal visible={true} transparent={true}>
                    <View style={{ flex: 1, backgroundColor: '#00000077' }}>
                        <View style={{ flex: 1 }} />
                        <View style={{ width: width, height: 0.5, backgroundColor: 'gray' }} />
                        <View style={{ width: width, height: 43, backgroundColor: 'white', flexDirection: 'row', justifyContent: 'space-between' }} >
                            <View style={{ flex: 1 }} />
                            <View style={{ width: 80, height: 43, right: 0, top: 0, bottom: 0 }} >
                                <Button title='Done' onPress={() => {
                                    this.props.onDoneButtonPress((this.props.type != 'date') ? this.state.selectedValue : this.state.selectedDate)
                                }} />
                            </View>
                        </View>
                        <View style={{ width: width, height: 0.5, backgroundColor: 'gray' }} />
                        {
                            (this.props.type == 'date') ?
                                <DatePickerIOS
                                    style={styles.containerIOS}
                                    date={this.state.selectedDate}
                                    onDateChange={(newDate) => { this.setState({ selectedDate: newDate }) }}
                                    mode='date'
                                    maximumDate={maximumDate}
                                /> : this._renderPickerView()

                        }
                    </View>
                </Modal>
            )
        }
    }
}

var styles = StyleSheet.create({
    containerIOS: {
        height: 230,
        width: width,
        backgroundColor: 'white',
        bottom: 0,
        right: 0,
        left: 0,
    }
})