import { connect } from 'react-redux'
import RetryDataScreen from './screen'
import {
    UPDATE_APPLICATION_ASSETS,
    UPDATE_MAIN_IMAGE,
    UPDATE_CONTENTFUL_EVENTS,
    UPDATE_CONTENTFUL_NEWS,
    UPDATE_CONTENTFUL_FEEDBACK,
    UPDATE_CONTENTFUL_WIKI,
    UPDATE_CONTENTFUL_WELCOME_NOTICE,
    UPDATE_HOME_SCREEN,
} from './../../../actions/action-types'

import MiddlewareAPI from '../../../services/middleware/API'

const mapStateToProps = state => ({

})

const mapDispatchToProps = dispatch => ({
    onRetry: async () => {
        const { events, wiki, notice, news, assets } = await MiddlewareAPI.retriveAllData()
        const { welcomeNotice, feedback } = notice
        const mainImageUrl = assets[0].url

        const homeScreen = { mainImageUrl, welcomeNotice, events }
        dispatch({ type: UPDATE_HOME_SCREEN, homeScreen })
        dispatch({ type: UPDATE_APPLICATION_ASSETS, assets })
        dispatch({ type: UPDATE_CONTENTFUL_NEWS, news })
        dispatch({ type: UPDATE_CONTENTFUL_WIKI, wiki })
        dispatch({ type: UPDATE_CONTENTFUL_FEEDBACK, feedback })
    }
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RetryDataScreen)