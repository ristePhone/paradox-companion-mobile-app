/**
 *  Screen used for retry failed API call
 *  
 *  Props:
 *  @prop {string} title - Title when visible = true and isRetrying = false
 *  @prop {bool} isRetrying - Default is false.
 *  @prop {function} onRetry - Event send back when Retry button is pressed
 */

import React from 'react'
import { ActivityIndicator } from 'react-native'
import {
    Container,
    TitleWrapperContainer,
    WrapperContainer,
    Message,
    Title,
} from './styles'
import ActionButton from '../../views/action-button'
import { strings } from '../../../config/localization'
import Colors from '../../../config/colors'

export default class RetryDataScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isRetrying: false,
        }
    }

    _onPress = () => {
        this.setState({ isRetrying: !this.state.isRetrying })
        this.props.onRetry()
    }

    _renderRetryView = () => {
        const title = this.props.title ? this.props.title : strings.retry.title
        const message = this.props.message ? this.props.message : strings.retry.message
        return (
            <WrapperContainer>
                <TitleWrapperContainer>
                    <Title>{title}</Title>
                    <Message>{message}</Message>
                </TitleWrapperContainer>
                <ActionButton
                    title={strings.retry.buttonTitle}
                    backgroundColor={Colors.GREEN_COLOR}
                    onPress={this._onPress}
                />
            </WrapperContainer>
        )
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps !== this.props ||
            nextState !== this.state
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            isRetrying: nextProps.isRetrying ? true : false,
        })
    }

    render() {
        return <Container>
            {!this.state.isRetrying ? this._renderRetryView() : null}
            <ActivityIndicator
                animating={this.state.isRetrying}
                color='white'
                size='large'
            />
        </Container>
    }
}