import glamorous from 'glamorous-native'
import { Dimensions } from 'react-native'
const { width, height } = Dimensions.get('window')

import Colors from '../../../config/colors'

export const Container = glamorous.view({
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
})

export const WrapperContainer = glamorous.view({
    width: Math.min(width, height) - 2 * 16,
    height: 200,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.DARK_GRAY_COLOR,
})

export const TitleWrapperContainer = glamorous.view({
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 16,
    marginBottom: 20,
})

export const Title = glamorous.text({
    fontFamily: 'SourceSansPro-Regular',
    fontSize: 24,
    color: 'white',
    fontWeight: '600',
    marginBottom: 15,
})

export const Message = glamorous.text({
    fontFamily: 'SourceSansPro-Regular',
    fontSize: 16,
    color: 'white',
    textAlign: 'center',
})
