import { connect } from 'react-redux'
import MoreScreen from './screen'
import { logOut } from '../../services/authentication'
import { UPDATE_USER_LOGGED_IN_STATUS, UPDATE_ROOT_CONTROLER } from '../../actions/action-types'
import {AsyncStorage} from 'react-native'
const mapStateToProps = state => ({
    environment: state.config.name,
    moreInfo: state.config,
    isUserLoggedIn: state.user.isLoggedIn,
})

const mapDispatchToProps = dispatch => ({
    onLogOutButtonPress: async () => {
        AsyncStorage.removeItem('username')
        await logOut()
        dispatch({ type: UPDATE_USER_LOGGED_IN_STATUS, isLoggedIn: false })
        dispatch({ type: UPDATE_ROOT_CONTROLER, rootTypeController: 'singleScreenApp' })
    },
    onLogInButtonPress: async () => {
        dispatch({ type: UPDATE_ROOT_CONTROLER, rootTypeController: 'singleScreenApp' })
    }
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MoreScreen)