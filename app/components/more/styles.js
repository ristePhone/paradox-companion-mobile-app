import glamorous from 'glamorous-native'
import { Dimensions } from 'react-native'
import Colors from '../../config/colors'
const { width } = Dimensions.get('window')

export const ItemContainer = glamorous.view({
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 60,
    paddingHorizontal: 16,
    alignItems: 'center'
})

export const InfoContainer = glamorous.text({
    fontSize: 12,
    color: Colors.WHITE_GRAY,
    paddingVertical: 5

})

export const DescriptionContainer = glamorous.text({
    fontSize: 14,
    color: 'white'

})

export const SeparatorComponent = glamorous.view({
    flex: 1,
    height: 1,
    backgroundColor: 'rgba(255, 255, 255, 0.05)',
})

export const LogOutButtonContainer = glamorous.view({
    position: 'absolute',
    backgroundColor: Colors.BACKGROUND_COLOR,
    alignItems: 'center',
    bottom: 0,
    justifyContent: 'center',
    height: 60,
   // marginBottom:10,
    width: width,
})
export const LogInButtonContainer = glamorous.view({
    position: 'absolute',
    backgroundColor: Colors.BACKGROUND_COLOR,
    alignItems: 'center',
    bottom: 0,
    justifyContent: 'center',
    height: 60,
   // marginBottom:10,
    width: width,
})