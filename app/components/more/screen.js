import React from 'react'
import { FlatList, View, AsyncStorage } from 'react-native'
import ActionButton from '../views/action-button'
import Colors from '../../config/colors'

import {
    ItemContainer,
    InfoContainer,
    DescriptionContainer,
    SeparatorComponent,
    LogOutButtonContainer,
    LogInButtonContainer
} from './styles'

import DeviceInfo from 'react-native-device-info'

export default class MoreScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            username: 'You are not logged in!'
        }

    }

    componentDidMount(){
        if( this.props.isUserLoggedIn == true)
        {
            AsyncStorage.getItem('username').then((data) => {
                if (data != null) {
                    this.setState({ username: data })
                } else {
                    this.setState({username: 'You are not logged in!'})
                }
    
            })
        }
    }

    config = index => {
        let title, description
        switch (index) {
            case 0:
                //if(this.state.username != ''){
                title = 'User:'
                description = this.state.username

                //}else{
                // title = 'User'
                //description = 'You are not logged in'
                //}
                break

            case 1:
                title = 'Platform:'
                description = DeviceInfo.getSystemName()
                break
            case 2:
                title = 'App version:'
                description = DeviceInfo.getVersion() + ' (' + DeviceInfo.getBuildNumber() + ')'
                break
            case 3:
                title = 'OS Version:'
                description = DeviceInfo.getSystemVersion()
                break
            case 4:
                title = 'Model:'
                description = DeviceInfo.getModel()
                break
            case 5:
                title = 'Environment:'
                description = this.props.environment
                break;
            case 6:
                title = 'News Contentful ID:'
                description = this.props.moreInfo.contentfull.news.space
                break;
            case 7:
                title = 'Events Contentful ID:'
                description = this.props.moreInfo.contentfull.content.config.space
                break;
            case 8:
                title = 'Wiki endpoint:'
                description = this.props.moreInfo.WIKI_URL
                break;
            case 9:
                title = 'Zendesk account:'
                description = this.props.moreInfo.ZENDESK_BASIC_URL
                break;


            default:
                break;
        }

        return { title, description }
    }

    _renderItem = item => {
        return (
            <ItemContainer>
                <View>
                    <InfoContainer>{this.config(item.index).title}</InfoContainer>
                    <DescriptionContainer>{this.config(item.index).description}</DescriptionContainer>
                </View>
            </ItemContainer>
        )
    }

    _renderSeparatorComponent = () => <SeparatorComponent />
    

    render() {
        let dataArray = Array(11).fill('')
        
        // do not show other details if build is for production
        if(this.props.environment == 'production') {
            dataArray = Array(1).fill('')
        }

        return (
            <View style={{ flex: 1, backgroundColor: Colors.BACKGROUND_COLOR }}>
                <FlatList
                    style={{ flex: 1 }}
                    data={dataArray}
                    renderItem={this._renderItem}
                    keyExtractor={(item, index) => `${item} ${index}`}
                    ItemSeparatorComponent={this._renderSeparatorComponent}
                />
             {
                    this.props.isUserLoggedIn ?
                        <LogOutButtonContainer>
                            <ActionButton
                                title='Log Out'
                                backgroundColor={Colors.GREEN_COLOR}
                                onPress={() => this.props.onLogOutButtonPress()} />
                        </LogOutButtonContainer>
                        :
                        <LogInButtonContainer>
                            <ActionButton
                                title='Log In'
                                backgroundColor={Colors.GREEN_COLOR}
                                onPress={() => this.props.onLogInButtonPress()} />
                        </LogInButtonContainer>
                }
            </View>
        )
    }
}       