import React, { Component } from 'react'
import {
    StyleSheet,
    View,
    Button,
    TouchableHighlight,
    Text,
    TextInput,
    Dimensions,
    Image,
    ScrollView,
    BackHandler,
    DatePickerAndroid,
    Platform,
    KeyboardAvoidingView,
    Alert,
    Linking
} from 'react-native'

var { height, width } = Dimensions.get('window')

import Colors from '../../utils/colors'
import ActionButton from '../views/action-button'
import LogoImageView from '../views/logo-image-view'
import Utils from '../../services/utils-service'
import NetworkService from '../../services/network-service'
import BackgroundView from '../views/background-view'

import EventLevelView from '../views/event-level-view'

import * as Constants from '../../utils/constants'

const FONT_COLOR = '#EEF0F2'


export default class LoginPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: ''
        }
    }

    // Navigation bar setup
    static navigationOptions = {
        header: null
    }

    _onSignUp = () => {
        this.props.navigation.navigate('SignUp')
    }

    _onLogin = () => {
        Utils.validateEmail(this.state.email, error => {
            if (error) {
                Alert.alert(Constants.ERROR_TITLE_STRING, error)
            } else {
                NetworkService.logIn(this.state.email, this.state.password).then(response => {
                    if (String(response.result).toLocaleLowerCase() == 'ok') {


                        NetworkService.createSession(this.state.email, this.state.password).then(() => {

                            // Store credentials for auto login...
                            NetworkService.storeCredentials(this.state.email, this.state.password)
                            
                            // Navigate
                            this.props.navigation.navigate('Main', { showLogin: false })
                        })

                    } else {
                        Alert.alert(response.result, response.errorMessage)
                    }
                })
            }
        })
    }

    _onForgotPasswordButtonPress = () => {
        Linking.openURL(Constants.FORGOT_PASSWORD_URL)
    }

    // Lifecycle
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', () => {
            if (this.props.navigation.state.routeName == 'Login') {
                return false
            }
            return true
        })
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress');
    }

    render() {
        return (
            <BackgroundView>
                <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">

                    <ScrollView style={{ flex: 1, paddingTop: 50, paddingHorizontal: 16 }} showsVerticalScrollIndicator={false}>

                        <View style={styles.container}>

                            <LogoImageView />

                            <View style={styles.formContainer}>

                                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={styles.formTitle} numberOfLines={2}>Login with your {"\n"}Paradox Account</Text>
                                </View>

                                <View style={{ flex: 1, justifyContent: 'center' }}>
                                    <View style={[styles.textInputContainer, { marginBottom: 8 }]}>
                                        <TextInput
                                            style={styles.textInputContainer}
                                            placeholder='Email address'
                                            onChangeText={(text) => this.setState({ email: text })}
                                            underlineColorAndroid='white'
                                            autoCorrect={false}
                                            autoCapitalize='none'
                                        />
                                    </View>
                                    <View style={styles.textInputContainer}>
                                        <TextInput
                                            style={styles.textInputContainer}
                                            placeholder='Password'
                                            secureTextEntry={true}
                                            underlineColorAndroid='white'
                                            onChangeText={(text) => this.setState({ password: text })}
                                        />
                                    </View>
                                </View>

                                <View style={{ justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', paddingHorizontal: 10 }}>
                                    <Text style={styles.forgotPassword} onPress={this._onForgotPasswordButtonPress} suppressHighlighting={true}>Forgot your password?</Text>
                                    <ActionButton
                                        title='login'
                                        backgroundColor={Colors.GREEN_COLOR}
                                        onPress={this._onLogin}
                                        underlineColorAndroid='white' />
                                </View>

                            </View>


                            <View style={{ marginVertical: 20, alignItems: 'center', paddingHorizontal: 20 }}>
                                <Text style={styles.dontHaveAccountTitle}>Don't have an account?{'\n'}
                                    <Text style={styles.dontHaveAccontDescription}>Now is a great time to create one! Sign up takes less than two minutes and will give you access to all Paradox things!</Text>
                                </Text>
                                <ActionButton
                                    title='sign up'
                                    backgroundColor={Colors.RED_COLOR}
                                    onPress={this._onSignUp} />
                            </View>
                            <Text
                                style={{ color: 'rgba(238, 240, 242, 0.8)', marginTop: 0 }}
                                selectionColor='rgba(238, 240, 242, 0.8)'
                                onPress={() => this.props.navigation.navigate('Main', { showLogin: true })}>Skip for now</Text>
                        </View>

                        <View style={{ width: 1, height: 100 }} />
                    </ScrollView>
                </KeyboardAvoidingView>
            </BackgroundView >
        )
    }
}

// Styles
var styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    formContainer: {
        marginHorizontal: 16,
        paddingHorizontal: 16,
        paddingVertical: 30,
        width: width - 2 * 16,
        height: 290,
        backgroundColor: 'rgba(32, 41, 48, 0.8)',
        borderWidth: 1,
        borderColor: 'rgba(238, 240, 242, 0.1)',
        borderRadius: 8
    },
    formTitle: {
        color: FONT_COLOR,
        textAlign: 'center',
        fontSize: 18,
        fontFamily: 'Entypo',
        lineHeight: 20
    },
    formForgotPassword: {
        color: FONT_COLOR,
        fontSize: 13,
        fontFamily: 'Entypo',
    },
    textInputContainer: {
        height: 44,
        backgroundColor: 'white',
        borderRadius: 4,
        paddingLeft: 8
    },
    logoContainer: {
        width: width * 3 / 4,
        height: 100,
        marginBottom: 60
    },
    dontHaveAccountTitle: {
        fontFamily: 'Entypo',
        fontSize: 16,
        lineHeight: 20,
        color: FONT_COLOR,
        textAlign: 'center',
        marginBottom: 20
    },
    dontHaveAccontDescription: {
        fontSize: 14,
        lineHeight: 18,
        textAlign: 'center',
    },
    forgotPassword: {
        fontFamily: 'Entypo',
        fontSize: 13,
        color: 'rgb(238, 240, 242)'
    }
})