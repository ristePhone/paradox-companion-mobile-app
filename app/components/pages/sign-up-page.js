import React, { Component } from 'react'
import {
    StyleSheet,
    View,
    Text,
    Dimensions,
    TextInput,
    Image,
    ScrollView,
    TouchableOpacity,
    TouchableWithoutFeedback,
    TouchableNativeFeedback,
    Platform,
    Picker,
    Modal,
    KeyboardAvoidingView,
    Alert
} from 'react-native'

var { height, width } = Dimensions.get('window')

import Colors from '../../utils/colors'
import countries from '../../utils/countries.json'
import { TERMS_AND_CONDITIONS_URL, PRIVACY_POLICY_URL } from '../../utils/constants'

import NetworkService from '../../services/network-service'
import Utils from '../../services/utils-service'

import ActionButton from '../views/action-button'
import LogoImageView from '../views/logo-image-view'

import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import PickerView from '../views/native-picker'
import BackgroundView from '../views/background-view'

import * as Constants from '../../utils/constants'


const FONT_COLOR = '#EEF0F2'


export default class SignUpPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            dateOfBirth: '',
            country: '',
            joinParadoxCommunity: false,
            acceptedTermsAndConditions: false,
            acceptedPrivacyPolicy: false,
            signUpButtonEnabled: true,
            pickerType: 'date',
            showPicker: false,
            selectedCountry: countries[0]
        }
    }

    // Header setup
    static navigationOptions = {
        header: null
    }

    _onSignUp = () => {

        const signUpParameters = {
            email: this.state.email,
            password: this.state.password,
            details: {
                dateOfBirth: this.state.dateOfBirth,
                country: this.state.country['code'],
            },
            options: {
                marketingPermission: this.state.joinParadoxCommunity
            },
            sourceService: 'pdx_mobile_app'
        }

        Utils.validateSignUPForm(signUpParameters, error => {
            if (!error) {

                this.setState({ signUpButtonEnabled: !this.state.signUpButtonEnabled })
                if (this.state.acceptedPrivacyPolicy && this.state.acceptedTermsAndConditions) {
                    NetworkService.signIn(signUpParameters).then(response => {
                        this.setState({ signUpButtonEnabled: !this.state.signUpButtonEnabled })
                        if (!response.errorMessage) {
                            this.props.navigation.navigate('Main', { showLogin: false })
                        } else {
                            Alert.alert(response.result, response.errorMessage)
                        }
                    })
                } else {
                    Alert.alert('Accept terms and conditions before we proceed')
                }
            } else {
                Alert.alert(Constants.ERROR_TITLE_STRING, error)
            }
        })
    }

    _renderCountryView() {
        return (
            <View style={[styles.textInputContainer, { marginTop: 8 }]}>
                {
                    (Platform.OS == 'ios') ?
                        <TouchableWithoutFeedback onPress={() => { this.setState({ pickerType: 'country', showPicker: true }) }}>
                            <TextInput
                                editable={false}
                                style={styles.textInputContainer}
                                placeholder='Country'
                                value={this.state.country['name']}
                                underlineColorAndroid='white'
                                onTouchStart={() => {
                                    this.setState({ pickerType: 'country', showPicker: true })
                                }}
                            />
                        </TouchableWithoutFeedback> :
                        <Picker
                            style={{ height: 44, flex: 1, marginRight: 8 }}
                            selectedValue={this.state.selectedCountry.name}
                            prompt='Choose country'
                            mode='dialog'
                            onValueChange={(value, position) => { this.setState({ selectedCountry: countries[position] }) }} >
                            {
                                countries.map(item => {
                                    return <Picker.Item key={item.name} label={item.name} value={item.name} />
                                })
                            }
                        </Picker>
                }

            </View>
        )
    }

    // Lifecycle
    render() {
        return (
            <BackgroundView>
                <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
                    <ScrollView style={{ flex: 1, paddingTop: 70, paddingHorizontal: 16 }} showsVerticalScrollIndicator={false}>
                        <View style={styles.container}>

                            <LogoImageView />

                            {/* SIGNUP FORM UI*/}
                            <View style={styles.formContainer}>

                                <View style={{ alignItems: 'center', justifyContent: 'center', marginBottom: 20 }}>
                                    <Text style={styles.formTitle} numberOfLines={2}>Sign up for {"\n"}a Paradox Account</Text>
                                </View>

                                <View style={[styles.textInputContainer, { marginTop: 8 }]}>
                                    <TextInput
                                        autoCorrect={false}
                                        style={styles.textInputContainer}
                                        placeholder='Your email address'
                                        onChangeText={(text) => { this.setState({ email: text }) }}
                                        underlineColorAndroid='white'
                                    />
                                </View>
                                <View style={[styles.textInputContainer, { marginTop: 8 }]}>
                                    <TextInput
                                        autoCorrect={false}
                                        style={styles.textInputContainer}
                                        placeholder='Password'
                                        secureTextEntry={true}
                                        onChangeText={(text) => { this.setState({ password: text }) }}
                                        underlineColorAndroid='white'

                                    />
                                </View>

                                <View style={[styles.textInputContainer, { marginTop: 8 }]}>
                                    <TouchableOpacity activeOpacity={1} onPress={() => { this.setState({ pickerType: 'date', showPicker: true }) }}>
                                        <TextInput
                                            autoCorrect={false}
                                            editable={false}
                                            style={styles.textInputContainer}
                                            placeholder='Date of Birth'
                                            value={this.state.dateOfBirth}
                                            onTouchStart={() => { this.setState({ pickerType: 'date', showPicker: true }) }}
                                            underlineColorAndroid='white'
                                        />
                                    </TouchableOpacity>
                                </View>

                                {this._renderCountryView()}

                                <View style={{ marginVertical: 10, flexDirection: 'row', alignItems: 'center' }}>
                                    <Icon size={30} color='white' name={(this.state.joinParadoxCommunity) ? 'checkbox-marked' :
                                        'checkbox-blank'
                                    } onPress={() => {
                                        this.setState({ joinParadoxCommunity: !this.state.joinParadoxCommunity })
                                    }} />
                                    <Text style={styles.formCheckBoxText} numberOfLines={3}>Yes, I want to join Paradox Community {'\n'}newsleter</Text>
                                </View>

                                <View style={{ flexDirection: 'row' }}>
                                    <Icon size={30} color='white' name={(this.state.acceptedTermsAndConditions) ? 'checkbox-marked' : 'checkbox-blank'} onPress={() => {
                                        this.setState({ acceptedTermsAndConditions: !this.state.acceptedTermsAndConditions })
                                    }} />
                                    <Text style={styles.formCheckBoxText} numberOfLines={3}>
                                        I hereby certify, that I have read and agree to Paradox's{' '}
                                        <Text style={{ textDecorationLine: 'underline' }} onPress={() => {
                                            this.props.navigation.navigate('TermsAndConditions', { url: TERMS_AND_CONDITIONS_URL })
                                        }}>Terms of Service</Text>
                                    </Text>
                                </View>

                                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                    <Icon size={30} color='white' name={(this.state.acceptedPrivacyPolicy) ? 'checkbox-marked' : 'checkbox-blank'} onPress={() => {
                                        this.setState({ acceptedPrivacyPolicy: !this.state.acceptedPrivacyPolicy })
                                    }} />
                                    <Text style={styles.formCheckBoxText} numberOfLines={3}>
                                        I hereby certify, that I have read and agree to Paradox's{' '}
                                        <Text style={{ textDecorationLine: 'underline' }} onPress={() => {
                                            this.props.navigation.navigate('TermsAndConditions', { url: PRIVACY_POLICY_URL })
                                        }}>Privacy Policy</Text>
                                    </Text>
                                </View>

                                <View style={{ justifyContent: 'center', marginTop: 25, flexDirection: 'row' }}>
                                    <ActionButton
                                        title='signUP'
                                        backgroundColor={Colors.GREEN_COLOR}
                                        onPress={this._onSignUp}
                                    />
                                </View>
                            </View>


                            {/* Bottom container */}
                            <View style={[styles.bottomContainer, { width: width - 2 * 16 }]}>
                                <Text style={styles.bottomContainerText}>Already have an account?</Text>
                                <ActionButton
                                    title='login'
                                    backgroundColor={Colors.DARK_GRAY_COLOR}
                                    onPress={() => this.props.navigation.goBack()} />
                            </View>

                        </View>


                        {
                            (this.state.showPicker) ?
                                <PickerView
                                    type={this.state.pickerType}
                                    dataSource={countries}
                                    onDoneButtonPress={(selectedValue) => {
                                        if (this.state.pickerType == 'date') {
                                            this.setState({ dateOfBirth: Utils.transformDate(selectedValue), showPicker: false })
                                        } else {
                                            this.setState({ country: selectedValue, showPicker: false })
                                        }
                                    }} /> : null
                        }

                        <View style={{ width: 1, height: 100 }} />

                    </ScrollView>
                </KeyboardAvoidingView>
            </BackgroundView >
        )
    }
}

// Styles
var styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    formContainer: {
        marginHorizontal: 16,
        paddingHorizontal: 16,
        paddingVertical: 30,
        width: width - 2 * 16,
        height: 520,
        backgroundColor: 'rgba(32, 41, 48, 0.8)',
        borderWidth: 1,
        borderColor: 'rgba(238, 240, 242, 0.1)',
        borderRadius: 8
    },
    formTitle: {
        color: FONT_COLOR,
        textAlign: 'center',
        fontSize: 18,
        fontFamily: 'Entypo',
        lineHeight: 20
    },
    textInputContainer: {
        height: 44,
        backgroundColor: 'white',
        borderRadius: 4,
        paddingLeft: 8,
    },
    bottomContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 16,
        marginTop: 20
    },
    bottomContainerText: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 15
    },
    formTitleText: {

    },
    formCheckBoxText: {
        color: 'white',
        textAlign: 'left',
        fontSize: 13,
        marginLeft: 10,
    }

})