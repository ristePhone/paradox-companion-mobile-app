import React, { Component } from 'react'
import {
    View,
    Text,
    StyleSheet,
    Alert,
    Platform,
    Linking
} from 'react-native'

import SubmitFormCard from '../views/cards/submit-form-card';
import BackgroundView from '../views/background-view';
import Colors from '../../utils/colors';

import Mailer from 'react-native-mail';
import ActionButton from '../views/action-button'

import { PARADOX_EMAIL_URL } from '../../utils/constants'

export default class ChatPage extends Component {

    constructor(props) {
        super(props)
        this.state = {
            feedback: null
        }
    }

    emailHandler = () => {
        if (Platform.OS == 'ios') {
            Mailer.mail({
                subject: 'Feedback',
                recipients: [PARADOX_EMAIL_URL],
            }, (error, event) => {
                if (error) {
                    AlertIOS.alert('Error', `Could not send mail. Please send a mail to ${PARADOX_EMAIL_URL}`);
                }
            });
        } else {
            Linking.openURL(`mailto:${PARADOX_EMAIL_URL}?subject=Feedback fanq&body=`)
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ feedback: nextProps.screenProps.feedback })
    }

    componentDidMount() {
        if (this.props.screenProps && this.props.screenProps.feedback) {
            this.setState({ feedback: this.props.screenProps.feedback })
        }
    }

    render() {
        let config = this.state.feedback ? { title: this.state.feedback.noticeHeadline1, description: this.state.feedback.noticeContent1 } : { title: '', description: '' }
        return (
            <BackgroundView>
                <View style={styles.container}>
                    <Text style={[styles.text, { fontSize: 18, lineHeight: 20 }]}>{config.title}</Text>
                    <Text style={[styles.text, styles.textDesc]}>
                        {config.description}
                    </Text>
                    <ActionButton
                        title='Feedback'
                        backgroundColor='green'
                        onPress={this.emailHandler}
                    />
                </View>
            </BackgroundView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 20,
        paddingTop: 44,
        paddingBottom: 30,
        alignItems: 'center',
        justifyContent: 'center'
    },
    text: {
        fontFamily: 'Entypo',
        color: 'white',
        textAlign: 'center',
        fontSize: 20,
        lineHeight: 20
    },
    textDesc: {
        fontFamily: 'Entypo',
        fontSize: 14,
        paddingTop: 10,
        paddingHorizontal: 32,
        marginBottom: 30,
        lineHeight: 22
    }
})