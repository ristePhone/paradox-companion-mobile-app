import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    FlatList,
    Text,
    Dimensions,
    ScrollView,
    TouchableWithoutFeedback,
    Platform
} from 'react-native'
var { height, width } = Dimensions.get('window')

const ROW_HEIGHT = 100
const SEPARATOR_ROW_HEIGHT = 1

import ViewOverflow from 'react-native-view-overflow';
import AgendaInnerCard from '../views/cards/agenda-inner-card'
import UtilService from '../../services/utils-service'
import Event from '../model/event'
import NetworkService from '../../services/network-service'

import DateSelectionView from '../views/agenda-page/date-selection-view';
import LevelSelectionView from '../views/agenda-page/level-selection-view';
import StageSelectionView from '../views/agenda-page/stage-selection-view';
import colors from '../../utils/colors';

export default class AgendaPage extends Component {
    constructor(props) {
        super(props)
        this.itemList = this.createHourList()
        this.state = {
            agendaList: this.props.screenProps.eventArray,
            eventArray: [], // contains filtered list base on stage, level, date...
            selectedDate: '2018-05-19', // filter date
            selectedStage: 'General activities', // filter stage
            selectedLevel: 'all' // filter level
        }
    }

    /// UTILS
    createHourList() {
        var tempList = []
        for (var i = 0; i < 24; i++) {
            tempList.push(i)
        }
        return tempList
    }

    _formatHour(hour) {
        if (hour < 10) {
            return `0${hour}:00`
        }
        return `${hour}:00`
    }


    /// FLAT LIST
    _renderItem = (item, index) => {

        // Logic for rendering agenda card
        var agentdaTimeInHours = 0, startTime = item, renderAgendaCard = false, eventItem = null, renderCardList = [], renderEventItemList = []
        var sorted = this.state.eventArray.sort((a, b) => {
            return new Date(b.date) - new Date(a.date);
        })

        for (var i = 0; i < sorted.length; i++) {
            eventItem = sorted[i]
            var start = new Date(eventItem.eventStart)
            if (item == start.getHours()) {
                var config = UtilService.timeDifferenceInHours(eventItem)
                renderCardList.push(config)
                renderEventItemList.push(eventItem)
            }
        }

        var top = (100 / 60)
        const timeLabelWidth = 100
        const agendaItemWidth = width - timeLabelWidth - 2 * 10
        return (
            <ViewOverflow key={item} style={{ flex: 1, height: 200, }}>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={{ flex: 1, width: 100, marginTop: 16, marginLeft: 16, fontFamily: 'Entypo' }}>{this._formatHour(item)}</Text>
                    {
                        renderCardList.map((config, index) => {
                            return <AgendaInnerCard
                                key={index}
                                hours={config.height == 1 ? config.height : (config.height + 3 + (config.height - 1))}
                                style={{ width: agendaItemWidth, height: config.height, marginTop: config.top, position: 'absolute' }}
                                item={renderEventItemList[index]}
                            />
                        })
                    }
                </View>
            </ViewOverflow>
        )
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.navigation.state.params) {
            let stage = UtilService.getFilteredGeneralActivities(nextProps.navigation.state.params.stage.sectionName)
            const array = UtilService.filteredEventList(
                nextProps.screenProps.eventArray,
                this.state.selectedDate,
                this.state.selectedLevel,
                stage
            )
            
            this.setState({ selectedStage: stage, eventArray: array })
        }

        if (nextProps.screenProps) {
            const array = UtilService.filteredEventList(
                nextProps.screenProps.eventArray,
                this.state.selectedDate,
                this.state.selectedLevel,
                UtilService.getFilteredGeneralActivities(this.state.selectedStage)
            )
            this.setState({ agendaList: nextProps.screenProps.eventArray, eventArray: array })
        }
    }

    componentDidMount() {
        var stage = this.state.selectedStage
        stage = UtilService.getFilteredGeneralActivities(stage)

        let dates = UtilService.uniqueEventDates(this.props.screenProps.eventArray)
        var currentDate = UtilService.uniqueEventDates(this.props.screenProps.eventArray)[0]
        for (let i = 0; i < dates; i++) {
            if (dates[i] == UtilService.getFormatedCurrentDate()) {
                currentDate = UtilService.getFormatedCurrentDate()
                break
            }
        }

        const array = UtilService.filteredEventList(
            this.state.agendaList,
            currentDate,
            this.state.selectedLevel,
            stage
        )

        this.setState({
            eventArray: array,
            selectedStage: stage,
            selectedDate: currentDate
        })
    }

    render() {
        var _this = this
        return (
            <View style={styles.container}>
                <DateSelectionView
                    dateList={UtilService.uniqueEventDates(this.state.agendaList)}
                    selectedDate={this.state.selectedDate}
                    onDateSelection={(date) => {
                        var filtered = UtilService.filteredEventList(this.state.agendaList, date, this.state.selectedLevel, this.state.selectedStage)
                        this.setState({ selectedDate: date, eventArray: filtered })
                    }}
                />
                <LevelSelectionView
                    selectedLevel={this.state.selectedLevel}
                    onLevelSelected={(levelSelected) => {
                        var filtered = UtilService.filteredEventList(this.state.agendaList, this.state.selectedDate, levelSelected, this.state.selectedStage)
                        this.setState({ selectedLevel: levelSelected, eventArray: filtered })
                    }}
                />
                <StageSelectionView
                    selectedStage={this.state.selectedStage}
                    onStageSelected={(stageSelected) => {
                        var filtered = UtilService.filteredEventList(this.state.agendaList, this.state.selectedDate, 'all', stageSelected)
                        this.setState({ selectedLevel: 'all', selectedStage: stageSelected, eventArray: filtered })
                    }}
                />
                <ScrollView
                    style={{ flex: 1, backgroundColor: '#E9EDEF' }}
                    contentContainerStyle={{ backgroundColor: '#E9EDEF' }}>
                    {
                        this.itemList.map(this._renderItem)
                    }
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: Platform.OS == 'ios' ? 44 : 0,
        backgroundColor: colors.TAB_BAR_COLOR
    },
    agendaItem: {
        backgroundColor: 'white',
        borderRadius: 10,
        marginTop: 10,
        shadowColor: 'black',
        shadowOpacity: 0.3,
        shadowOffset: {
            width: 1,
            height: 1
        },
        elevation: 10,
    }
})