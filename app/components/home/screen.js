import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    FlatList,
    Text
} from 'react-native'
import Colors from '../../config/colors'

// Cards
import LogoImageView from '../common/main-image-view/logo-image-view'
import InfoHeaderCard from '../common/info-header/ui'
import WelcomeCard from '../common/home/welcome-view/ui'
import JoinUsCard from '../views/cards/join-us-card'
import LoginCard from '../views/cards/login-card'
import EventCard from '../common/event/card/ui'

export default class HomePage extends Component {
    constructor(props) {
        super(props)
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this))
        this.state = {
            pauseVideo: true,
            renderVideo:true
        }
    }

    _nextThreeEvents = events => {
        return events.map(event => {
            const eventStartDate = new Date(event.eventStart)
            const currentDate = new Date()
            const isNext = currentDate < eventStartDate
            return isNext ? event : null
        })
    }

    _hasData = props => {
        const welcomeNotice = props.welcomeNotice
        return welcomeNotice
    }

    onNavigatorEvent(event) {
        if (event.id == 'didDisappear') {
            this.setState({ pauseVideo: true, renderVideo:false })
            this.resetScrollToTop()
        }
        if (event.id == 'didAppear') {
            this.setState({ renderVideo:true })
        }
    }

    resetScrollToTop = () => {
        if(this.flatList != undefined) { this.flatList.scrollToOffset({x: 0, y: 0, animated: false}) }
    }


    _renderItem = (item) => {
        switch (item.index) {
            case 0:
                return (
                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                        <LogoImageView url={this.props.mainImageUrl} />
                    </View>)
            case 1:
                return (!this.props.isLoggedIn) ?
                    <LoginCard onLogin={() => this.props.onLogin(this.props)} /> : null
            case 2:
                return this.state.renderVideo? <WelcomeCard
                    urlThumbnail={this.props.videoThumbnailUrl}
                    urlVideo={this.props.videoUrl}
                    pause={this.state.pauseVideo}
                    title={(this.props.welcomeNotice) ? this.props.welcomeNotice.noticeHeadline2 : ''}
                    description={(this.props.welcomeNotice) ? this.props.welcomeNotice.noticeContent2 : ''}
                />:null
            case 3:
                return <JoinUsCard />
            case 4:
                return (
                    <View style={styles.infoCardContainer}>
                        <InfoHeaderCard type='info' />
                        <View style={{ paddingHorizontal: 20, paddingTop: 20, paddingBottom: 40 }}>
                            <Text style={styles.text}>{(this.props.welcomeNotice) ? this.props.welcomeNotice.noticeContent1 : ''}</Text>
                        </View>
                    </View >
                )
            case 5:
                const nextThree = this._nextThreeEvents(this.props.events)
                const filteredNextThreeEvents = nextThree.filter(item => item != null)
                return <EventCard
                    items={filteredNextThreeEvents}
                    onItemSelect={item => this.props.selectEvent(this.props, item)}
                />
        }
    }

    _renderHeader = () => <View style={{ flex: 1, height: 80 }} />

    _renderFooter = () => <View style={{ flex: 1, height: 60 }} />

    _renderSeparator = () => <View style={styles.separatorContainer} />

    /**
     *  Lifecycle
     *  @memberof HomePage
     *  
     */

    componentDidMount() {
        if (!this._hasData(this.props)) {
            this.props.onRetryDataShow()
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this._hasData(nextProps)) {
            this.props.onRetryDataHide()
        } else {
            this.props.onRetryDataShow()
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        const newEvents = JSON.stringify(nextProps.events) !== JSON.stringify(this.props.events)
        const newMainImage = nextProps.mainImageUrl !== this.props.mainImageUrl
        const isLoggedIn = nextProps.isLoggedIn !== this.props.isLoggedIn
        const welcomeNotice = JSON.stringify(nextProps.welcomeNotice) !== JSON.stringify(this.props.welcomeNotice)
        const shouldUpdate = newMainImage || newEvents || isLoggedIn || welcomeNotice || this.state !== nextState
        return shouldUpdate
    }

    render() {
        const data = this._hasData(this.props)
        return data ?
            <FlatList
                ref={component => this.flatList = component}
                style={styles.container}
                showsVerticalScrollIndicator={false}
                data={['', '', '', '', '', '']}
                ItemSeparatorComponent={this._renderSeparator}
                renderItem={this._renderItem}
                keyExtractor={(item, index) => `${index}`}
                ListFooterComponent={this._renderFooter}
                ListHeaderComponent={this._renderHeader}
            /> : null
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.BACKGROUND_COLOR,
        paddingHorizontal: 20,
    },
    separatorContainer: {
        height: 10,
        width: 10,
        backgroundColor: 'transparent'
    },
    infoCardContainer: {
        flex: 1,
        backgroundColor: 'white',
        borderRadius: 8
    },
    text: {
        fontSize: 14,
        lineHeight: 20,
    },
    comingUpCardContainer: {
        backgroundColor: Colors.DARK_GRAY_COLOR,
        borderRadius: 8,
    },
    headerContainer: {
        height: 40,
        justifyContent: 'center',
        marginLeft: 20
    },
    headerTitle: {
        fontFamily: 'OpenSans',
        fontSize: 10,
        color: Colors.WHITE_80,
        lineHeight: 18
    },
})