import { connect } from 'react-redux'
import HomePage from './screen'
import Routes from '../../config/routes'
import { strings } from '../../config/localization'
import NavigationActions from '../../actions/navigation-actions/index'
import {
    UPDATE_ROOT_CONTROLER,
    UPDATE_SELECTED_EVENT,
} from '../../actions/action-types'

const mapStateToProps = state => ({
    welcomeNotice: state.contentfulAPIcontent.welcomeNotice,
    isLoggedIn: state.user.isLoggedIn,
    events: state.contentfulAPIcontent.events,
    mainImageUrl: state.contentfulAPIcontent.mainImageUrl,
    videoThumbnailUrl: state.contentfulAPIcontent.videoThumbnailUrl,
    videoUrl: state.contentfulAPIcontent.videoUrl
})

const mapDispatchToProps = dispatch => ({
    onLogin: props => {
        dispatch({ type: UPDATE_ROOT_CONTROLER, rootTypeController: 'singleScreenApp' })
    },
    selectEvent: (props, item) => {
        dispatch({ type: UPDATE_SELECTED_EVENT, item })
        props.navigator.push({
            screen: Routes.EVENT_DETAILS,
            title: strings.navigation_titles.eventDetails,
            navigatorStyle: {
                navBarHidden: false,
            }
        })
    },
    onBack: props => {
        props.navigator.pop()
    },
    onRetryDataShow: async (props) => {
        NavigationActions.showLightBox({
            screen: Routes.RETRY,
            passProps: {
                isRetrying: false,
            }
        })
    },
    onRetryDataHide: () => {
        NavigationActions.hideLightBox()
    }
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomePage)