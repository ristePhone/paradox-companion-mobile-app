import glamorous from 'glamorous-native'
import Colors from '../../config/colors'
import * as Animatable from 'react-native-animatable';

export const ImageWrapperContainer = glamorous.view({
    flex: 1,
    height: 83,
    marginVertical: 15,
    paddingHorizontal: 10,
    paddingVertical: 15
})

export const ImageContainer = glamorous.image({
    flex: 1,
})

export const RowContainer = glamorous.view({
    flexDirection: 'row',
    backgroundColor: Colors.DARK_GRAY_COLOR,
    alignItems: 'center',
    justifyContent: 'space-between',
    flex: 1,
    height: 50,
    borderRadius: 4,
    padding: 14
})

export const RowExpandedContainer = Animatable.createAnimatableComponent(glamorous.view({
    flex: 1, 
    alignItems: 'center',
    justifyContent: 'center'
}))

export const RowTitleView = glamorous.text({
    color: 'white',
    textAlign: 'left',
    fontSize: 16,
    fontFamily: 'Arial',
    marginRight: 20
})

export const SeparatorContainer = glamorous.view({
    height: 10,
    width: 1
})