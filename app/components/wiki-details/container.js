import { connect } from 'react-redux'
import WikiDetailsScreen from './screen'
import Routes from '../../config/routes'
import { UPDATE_SELECTED_NEWS_FEED_ITEM } from '../../actions/action-types'
import { strings } from '../../config/localization'

const mapStateToProps = state => ({

})

const mapDispatchToProps = dispatch => ({
    onSectionNavigation: (props, navTitle) => {
        props.navigator.push({
            screen: Routes.WIKI_SECTION,
            title: navTitle,
            passProps: {
                
            }
        })
    }
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(WikiDetailsScreen)