import React, { Component } from 'react'
import {
    View,
    Text,
    StyleSheet,
    Alert,
    Platform,
    Linking,
    FlatList,
    Image,
    ScrollView
} from 'react-native'
import Colors from '../../config/colors'
import HTMLView from 'react-native-htmlview';

import ViewCard from '../wiki-section/wiki-sections'

export default class VIewCard extends Component {

    constructor(props) {
        super(props)
        this.state = {
            //viewCard: ViewCard
            viewCard: ['']
        }
        console.log(this.props.item)
        console.log(this.state.viewCard)
        var last = this.state.viewCard.length;
        var index = this.state.viewCard.find((wiki,id)=>{
            return last  == id+1
        })
        console.log(last)
    }


    _renderRow = ({ item }) => {
             // { setTimeout(() => this.refs.flatList.scrollToEnd(true), 200)}
        //const htmlContent = `<h2 id=\"mf-content\" style=\"border:none; background:#3c5a6e; padding:0.2em 0; margin:0; color:#fff; font-size:125%; font-weight:bold; text-indent:0.5em; font-variant:small-caps;border:1px solid #cccccc;\"><span class=\"mw-headline\" id=\"Important_pages\">Important pages</span></h2>\n<div id=\"mf-content\" style=\"margin-bottom:1em; padding:0.25em 0.4em 0.25em 0.4em;\">\n<div id=\"list\"><img alt=\"EU4 icon.png\" src=\"http://eu4.paradoxwikis.com/images/thumb/f/ff/EU4_icon.png/28px-EU4_icon.png\" width=\"28\" height=\"28\" data-file-width=\"54\" data-file-height=\"54\" /> <a href=\"http://localhost:3000/Beginner%27s_guide\" title=\"Beginner's guide\">Beginner's guide</a> <img alt=\"Unknown.png\" src=\"http://eu4.paradoxwikis.com/images/thumb/d/dd/Unknown.png/28px-Unknown.png\" width=\"28\" height=\"28\" data-file-width=\"48\" data-file-height=\"48\" /><a href=\"http://localhost:3000/User_interface\" title=\"User interface\">User interface</a>\n<p><img alt=\"Legitimacy.png\" src=\"http://eu4.paradoxwikis.com/images/thumb/2/25/Legitimacy.png/28px-Legitimacy.png\" width=\"28\" height=\"28\" data-file-width=\"48\" data-file-height=\"48\" /> <a href=\"http://localhost:3000/Countries\" title=\"Countries\">Countries</a> <img alt=\"Imperial authority.png\" src=\"http://eu4.paradoxwikis.com/images/thumb/4/46/Imperial_authority.png/28px-Imperial_authority.png\" width=\"28\" height=\"28\" data-file-width=\"48\" data-file-height=\"48\" /> <a href=\"http://localhost:3000/Holy_Roman_Empire\" title=\"Holy Roman Empire\">Holy Roman Empire</a>\n</p><p><img alt=\"Ironman icon.png\" src=\"http://eu4.paradoxwikis.com/images/thumb/0/0f/Ironman_icon.png/28px-Ironman_icon.png\" width=\"28\" height=\"28\" data-file-width=\"42\" data-file-height=\"42\" /> <a href=\"http://localhost:3000/Mechanics\" title=\"Mechanics\">Mechanics</a> <img alt=\"Administrative power.png\" src=\"http://eu4.paradoxwikis.com/images/e/ef/Administrative_power.png\" width=\"28\" height=\"28\" data-file-width=\"28\" data-file-height=\"28\" /> <a href=\"http://localhost:3000/Console_commands\" title=\"Console commands\">Console commands</a>\n</p>\n<h2 style=\"margin-top:0.3em\">`
        //const htmlContent = `<p style='color:white'>test parag</p>`
        const htmlContent = this.props.item.body
        return (
            <View>
             

                <View style={styles.viewBox}>
                <HTMLView
                            value={htmlContent}
                            stylesheet={styles}
                        /> 
                     {/* <Image
                        style={{ width: 30, height: 30 }}
                        source={{ uri: item.url }}
                    />
                    <Text style={{ color: 'white' }} numberOfLines={0}>{item.name}</Text>  */}

                </View> 


                {/* {this.state.viewCard == this.state.viewCard.pop()}{
                    <Text style={{ color: 'white' }} numberOfLines={0}>END</Text>
                } */}
            </View>
        )

    }

    render() {
        return (
            <ScrollView contentContainerStyle={{flexGrow: 1}}>


            <FlatList
                ref="flatList"
                style={{ flex: 1 }}
                data={this.state.viewCard}
                renderItem={this._renderRow}
                keyExtractor={(item, index) => `${item} ${index}`}
                ItemSeparatorComponent={this._renderSeparatorComponent}
                ListHeaderComponent={this._renderHeaderComponent}
                numColumns={2}
            />
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 5,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff',
    },
    viewBox: {
        fontFamily: 'Entypo',
        color: 'white',
        fontSize: 20,
        lineHeight: 20,
        flexDirection: "row",
        alignItems: 'center',
        padding: 12,
        
        margin: 5,
        borderRadius: 20,
      //  backgroundColor: "#1a1a1a"

    },
    textDesc: {
        fontFamily: 'Entypo',
        fontSize: 14,
        paddingTop: 10,
        paddingHorizontal: 32,
        marginBottom: 30,
        lineHeight: 22
    }
})