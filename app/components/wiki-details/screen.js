import React from 'react'
import { FlatList, TouchableWithoutFeedback, Text, View, Platform, Image, ScrollView, StyleSheet, Button } from 'react-native'
import {
    RowContainer,
    RowTitleView,
    SeparatorContainer,
    ImageWrapperContainer,
    ImageContainer,
    RowExpandedContainer
} from './styles'

import Ionicons from 'react-native-vector-icons/Ionicons'
import Colors from '../../config/colors'
import dataJson from '../wiki-section/wiki-sections'
import DetailsCard from './wikiCard'
export default class WikiDetailsScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            dataJson: dataJson,
            selectedIndex: null
        }
        {
            this.state.dataJson.map((data, key) => {
                console.log(data.url)
            })
        }
    }
    renderVi(index){
      
        if(Platform.OS =='ios'){
            // if(index =='center'){
            //     this.flatListRef.scrollToIndex({index: 1, animated: true});

            // }
           this.flatListRef.scrollToIndex({index: this.state.selectedIndex, animated: true});

        }else{
            let wait = new Promise((resolve) => setTimeout(resolve, 5));  // Smaller number should work
            wait.then( () => {
                this.flatListRef.scrollToIndex({index: this.state.selectedIndex, animated: true});
            });
        }
  
    }

    // Render Row

    _renderRow = ({ item, index }) => {
        
        const rotated = index == this.state.selectedIndex ? '90deg' : '0deg'
        return (
            <View>
                <TouchableWithoutFeedback onPress={() => {
                    this.setState({ selectedIndex: index == this.state.selectedIndex ? null : index })
                    this.renderVi(index)
                    // if(rotated == '0deg'){
                    //     this.renderVi(test = 'center')

                    // }

                }}>
                    <View>
                        <RowContainer >
                            <RowTitleView numberOfLines={2}>{String(item.title).toUpperCase()}</RowTitleView>
                            <Ionicons name='ios-arrow-forward' size={22} color='white' style={{ transform: [{ rotate: rotated }] }} />
                        </RowContainer>
                        {
                            index == this.state.selectedIndex ?
                            
                                <RowExpandedContainer animation='fadeInDown' duration={100} >
{/*                                 
                                    <DetailsCard item={item} /> */}
                                    <DetailsCard item={item}/>

                                </RowExpandedContainer>
                                : null
                        }
                    </View>
                </TouchableWithoutFeedback>
            </View>
        )
    }

    _renderHeaderComponent = () => {
        let imageUrl = this.props.item.wiki_logo
        if (Platform.OS == 'ios') {
            imageUrl = String(imageUrl).replace('http', 'https')
        }
        return (
            <ImageWrapperContainer>
                <ImageContainer
                    source={{ uri: imageUrl }}
                    resizeMode='contain'
                />
            </ImageWrapperContainer>
        )
    }
    _renderSeparatorComponent = () => <SeparatorContainer />

    render() {

        const wikiSections = Object.values(this.props.item.wiki_sections)
        return (
            //centerContent={}
            <View  style={{flex:1,marginBottom:1}}>
                <FlatList
                    ref={(ref) => { this.flatListRef = ref; }}
                    style={{ flex: 1, paddingHorizontal: 10, backgroundColor: Colors.BACKGROUND_COLOR }}
                    data={wikiSections}
                    keyExtractor={(item, index) => `${item} ${index}`}
                    renderItem={this._renderRow}
                    ItemSeparatorComponent={this._renderSeparatorComponent}
                    ListHeaderComponent={this._renderHeaderComponent}
                />
            </View>


        )
    }
}

const styles = StyleSheet.create({
    box: {
        position: 'absolute',
        top: 300
    }
})