import { connect } from 'react-redux'
import { store } from '../../reducers/index'
import FeedbackScreen from './screen'
import Mailer from 'react-native-mail'
import { Platform, Linking ,Alert} from 'react-native'
//import ZendeskSupport from 'react-native-zendesk-support'
import Routes from '../../config/routes';
// Redux
const mapStateToProps = state => ({
    feedback: state.contentfulAPIcontent.feedback,
    isLoggedIn: state.user.isLoggedIn

})

const mapDispatchToProps = dispatch => ({
    
    // showHelpCenter: () => {
    //     const config = {
    //         appId: '83ec369c3788912227b6e910ceec0508cbbb6d99dce23bb6',
    //         zendeskUrl: 'https://paradox.zendesk.com',
    //         clientId: 'mobile_sdk_client_73f3402433c939801212'
    //     }

    //     ZendeskSupport.initialize(config)

    //     const identity = {
    //         customerEmail: 'foo@bar.com',
    //         customerName: 'Foo Bar'
    //     }

    //     ZendeskSupport.setupIdentity(identity)

    //     //ZendeskSupport.supportHistory()
    //     ZendeskSupport.showHelpCenter()

    // },
    requestFeedback:(props)=>{
        props.navigator.push({
            screen: Routes.REQUEST_FEEDBACK,
            title: 'Send feedback',
            navigatorStyle: {
                navBarHidden: false,
            }
        })
    },
    openRequests:(props) =>{
        props.navigator.push({
            screen: Routes.REQUESTS_LIST,
            title: 'Open requests',
            navigatorStyle: {
                navBarHidden: false,
            }
        })
    },
    submitRequest:(props)=>{
        props.navigator.push({
            screen: Routes.REQUEST_CREATE,
            title: 'Submit request',
            navigatorStyle: {
                navBarHidden: false,
            }
        })
    },

    onFeedback: () => {
        const PARADOX_EMAIL_URL = store.getState().config.PARADOX_EMAIL_URL
        if (Platform.OS == 'ios') {
            Mailer.mail({
                subject: 'Feedback',
                recipients: [PARADOX_EMAIL_URL],
            }, (error, event) => {
                if (error) {
                    Alert.alert('Error', `Could not send mail. Please send a mail to ${PARADOX_EMAIL_URL}`);
                }
            });
        } else {
            Linking.openURL(`mailto:${PARADOX_EMAIL_URL}?subject=Feedback fanq&body=`)
        }
    }
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(FeedbackScreen)