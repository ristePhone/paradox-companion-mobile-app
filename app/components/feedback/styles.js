import glamorous from 'glamorous-native'
import {Dimensions} from 'react-native'
import Colors from '../../config/colors'
import { normalize } from 'react-native-elements'

let { height, width } = Dimensions.get('window')
let FONT_TEXT, TETX_HEIGHT
height < 680 ? FONT_TEXT = normalize(14) :FONT_TEXT= normalize(16)
height < 680 ? TETX_HEIGHT = normalize(13) : TETX_HEIGHT = normalize(20)
export const FeedbackContainer = glamorous.view({
    flex: 1,
    paddingHorizontal: 20,
    paddingBottom: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.BACKGROUND_COLOR,
})

export const FeedbackContainerTitleView = glamorous.text({
    fontFamily: 'OpenSans',
    color: 'white',
    textAlign: 'center',
    fontSize: FONT_TEXT,
    lineHeight: 20
})

export const FeedbackContainerDescriptionView = glamorous.text({
    fontFamily: 'OpenSans',
    color: 'white',
    textAlign: 'center',
    fontSize: FONT_TEXT - 4,
    paddingTop: 10,
    paddingHorizontal: 32,
    marginBottom: 30,
    lineHeight:TETX_HEIGHT
})