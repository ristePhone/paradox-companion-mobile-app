import React, { Component } from 'react'
import { View, Alert } from 'react-native'
import ActionButton from '../views/action-button'
import { strings } from '../../config/localization'
import Colors from '../../config/colors'
import {
    FeedbackContainer,
    FeedbackContainerTitleView,
    FeedbackContainerDescriptionView
} from './styles'

export default class FeedbackScreen extends Component {
    componentWillMount() {
        if (this.props.notify == true) {
            Alert.alert(this.props.notifyMessage)
        }
       
    }
    render() {
        const title = (this.props.feedback && this.props.feedback.noticeHeadline1) ? this.props.feedback.noticeHeadline1 : ''
        const description = (this.props.feedback && this.props.feedback.noticeContent1) ? this.props.feedback.noticeContent1 : ''
        return (
            <FeedbackContainer>
                <FeedbackContainerTitleView>{title}</FeedbackContainerTitleView>
                <FeedbackContainerDescriptionView>{description}</FeedbackContainerDescriptionView>
                {this.props.isLoggedIn ?
                    <ActionButton
                        title={strings.feedback_button_submit_request}
                        backgroundColor={Colors.GREEN_COLOR}
                        onPress={() => this.props.submitRequest(this.props)}
                    /> : null}

                <View style={{ width: 1, height: 20 }} />
                {this.props.isLoggedIn ?
                    <ActionButton
                        title={strings.feedback_button_open_requests}
                        backgroundColor={Colors.GREEN_COLOR}
                        onPress={() => this.props.openRequests(this.props)}
                    /> : null
                }


                <View style={{ width: 1, height: 20 }} />
                <ActionButton
                    title={strings.feedback_button_send_feedback}
                    backgroundColor={Colors.GREEN_COLOR}
                    onPress={() => this.props.requestFeedback(this.props)}
                />
                {/* <ActionButton
                        title={strings.feedback}
                        backgroundColor='green'
                        onPress={() => this.props.onFeedback()}
                    />
                    <View style={{ width: 1, height: 20 }} />
                    <ActionButton
                        title={strings.showHelpCenter}
                        backgroundColor='blue'
                        onPress={() => this.props.showHelpCenter()}
                    /> */}
            </FeedbackContainer>
        )
    }
}