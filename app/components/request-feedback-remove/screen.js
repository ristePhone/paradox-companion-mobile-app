import React, { Component } from 'react'
import {
    StyleSheet,
    Dimensions,
    KeyboardAvoidingView,
    Keyboard,
    ScrollView,
    Platform
} from 'react-native'

var { height, width } = Dimensions.get('window')

import Colors from '../../config/colors'
//import countries from '../../utils/countries.json'
//import issueTypes from '../../utils/issueTypes.json'
//import Utils from '../../services/utils-service'
import ActionButton from '../views/action-button'
//import LogoImageView from '../common/main-image-view/logo-image-view'
//import PickerView from '../views/native-picker'
import BackgroundView from '../views/background-view'
//import { validateEmail } from '../../services/authentication'
//const FONT_COLOR = '#EEF0F2'

import {
    ScrollViewContainer,
    ScrollViewContentView,
    FormContainer,
    FormTitleContainer,
    FormTitleTextView,
    FormTextInputField,
    FormTextInputField1,
    ActionButtonContainer,
    CheckBoxContainer,
    FooterView,
    BottomContainer,
    AlreadyHaveAcountTextView,
    RequestHeadersTextView,
    RequestHeadersHintTextView
} from './styles'

import CheckBoxView from '../common/checkbox-view'

import { strings } from '../../config/localization'

export default class FeedbackCreateScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            description: '',
            email: '',
            subject: '',
            message: ''
        }
    }
    componentWillMount() {
        this.props.navigator.toggleTabs({
            to: 'shown',
            animated: false
            });
    }

    _validateForm() {
        if (this.state.subject.trim().length == 0) {
            alert("Please enter Subject")
            return false
        }
        if (this.state.message.trim().length == 0) {
            alert("Please enter Message")
            return false
        }
        return true
    }
    _clearState() {
        this.setState({ subject: '', message: '' })
        alert("Thank you for your feedback")
    }

    _onSubmit() {
        if (!this._validateForm())
            return

        this._clearState()
    }
    setSel(input) {
        if (Platform.OS == 'android') {
            if(input == 'inputArea'){
                setTimeout(() => {
                    this.refs._scrollView.scrollTo({ x: 0, y: 155, animated: true })
                }, 300)
    
                this.props.navigator.toggleTabs({
                    to: 'hidden',
                    animated: false
                });
            }else{
                    
                this.props.navigator.toggleTabs({
                    to: 'hidden',
                    animated: false
                });
            }
          
        }

    }
    // componentWillReceiveProps(nextProps) {
    //     this.setState({ mainImageUrl: nextProps.mainImageUrl })
    // }
    // Lifecycle
    render() {
        Keyboard.addListener('keyboardDidHide',()=>{
                this.props.navigator.toggleTabs({
                    to: 'shown',
                    animated: false
                    });
        });
        return (
            <ScrollView ref="_scrollView">
            <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">

            <BackgroundView>
                <ScrollViewContentView>
                    <FormContainer>
                        <RequestHeadersTextView>{strings.request_feedback_label_subject}</RequestHeadersTextView>
                        <FormTextInputField
                            value={this.state.subject}
                            autoCorrect={false}
                           // onBlur={()=>this.hideNav()}
                           onFocus={()=>this.setSel('input')}
                           placeholder={strings.request_feedback_placeholder_subject}
                            onChangeText={(text) => { this.setState({ subject: text }) }}
                            underlineColorAndroid='white' />
                        <RequestHeadersTextView>{strings.request_feedback_label_message}</RequestHeadersTextView>
                    
                        <FormTextInputField1
                            selection={this.state.select}
                            multiline={true}
                            // multiline 
                          //   onBlur={()=>this.hideNav()}
                          onFocus={()=>this.setSel('inputArea')}
                          value={this.state.message}
                            autoCorrect={false}
                            placeholder={strings.request_feedback_placeholder_message}
                            onChangeText={(text) => { this.setState({ message: text }) }}
                            underlineColorAndroid='white' />
                        <ActionButtonContainer>
                            <ActionButton
                                title={strings.request_feedback_button_submit}
                                backgroundColor={Colors.GREEN_COLOR}
                                onPress={() => this._onSubmit()} />
                        </ActionButtonContainer>
                    </FormContainer>
                    <FooterView />
                </ScrollViewContentView>
            </BackgroundView >
                        </KeyboardAvoidingView>
                        </ScrollView>

        )
    }
}

// Styles
// var styles = StyleSheet.create({
//     textInputContainer: {
//         height: 44,
//         backgroundColor: 'white',
//         borderRadius: 4,
//         paddingLeft: 8,
//     },
// })