import React from 'react'
import { Image, ImageBackground, StyleSheet, View } from 'react-native'
import PropTypes from 'prop-types'

// Placeholders
const smallPlaceholder = require('../../../resources/placeholder_small.png')
const largePlaceholder = require('../../../resources/placeholder_big.png')

export default class PDXImage extends React.Component {
    static propTypes = {
        style: PropTypes.object.isRequired,
        source: PropTypes.string.isRequired,
        placeholderType: PropTypes.oneOf(['small', 'large']),
        resizeMode: PropTypes.string,
        resizeMethod: PropTypes.string,
    }

    _getPlaceholderForType = type => {
        switch (type) {
            case 'small':
                return smallPlaceholder
            case 'large':
            default:
                return largePlaceholder
        }
    }

    _onLoadImageEnd = e => {
        this.setState({ isImageLoaded: true })
    }

    _onError = () => {
        this.setState({ error: true })
    }

    /**
     *  Lifecycle
     */
    constructor(props) {
        super(props)
        this.state = {
            isImageLoaded: false,
            error: false,
        }
    }

    render() {
        const {
            style,
            source,
            placeholderType,
            resizeMode,
            resizeMethod,
        } = this.props
        const placeholderImage = this._getPlaceholderForType(placeholderType)
        return (
            <ImageBackground
                style={style}
                source={{ uri: source }}
                onLoadEnd={this._onLoadImageEnd}
                onError={this._onError}
                resizeMode={resizeMode ? resizeMode : 'contain'}
                resizeMethod={resizeMethod ? resizeMethod : 'scale'}
            >
                {
                    !this.state.isImageLoaded || this.state.error ?
                        <View style={styles.placeholderContainer}>
                            <Image
                                style={{ height: style.height * 3/4, width: style.height/2 }}
                                source={placeholderImage}
                                resizeMode='contain'
                            />
                        </View>
                        : null
                }
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    placeholderContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
    },
})