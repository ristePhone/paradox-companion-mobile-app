/**
 *  Component used for main application logo
 *  
 *  Props:
 *  @param {string} url - Image url. 
 */

import React, { Component } from 'react'
import { ImageContainer, MainContainer } from './styles'

export default class LogoImageView extends Component {
    render() {
        return (
            <MainContainer>
                <ImageContainer
                    source={{ uri: this.props.url }}
                    resizeMode='contain' />
            </MainContainer>
        )
    }
}