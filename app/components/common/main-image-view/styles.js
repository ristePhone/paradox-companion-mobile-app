import glamorous from 'glamorous-native'

export const MainContainer = glamorous.view({
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
    flex: 1,
})

export const ImageContainer = glamorous.image({
    width: 70,
    height: 70,
})