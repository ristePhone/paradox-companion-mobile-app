import React from 'react'
import {
    StyleSheet,
    TouchableHighlight,
    Platform
} from 'react-native'

import {
    CardContainer,
    ContentContainer,
    HeaderContainer,
    DateContainer,
    TitleContainer,
    TitleWrapperContainer,
    ShareWrapperContainer,
    imageStyle
} from './styles'

import Utils from '../../../services/utils-service'
import EntypoIcons from 'react-native-vector-icons/Entypo'
import colors from '../../../config/colors';
import PDXImage from '../image/ui'


export default class NewsFeedItem extends React.Component {

    _onShare = type => {
        this.props.onSharePress(type)
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps !== this.props
    }

    render() {
        const { headerImage, headline, publishDate } = this.props.item
        const publicatioDate = Utils.formatDateString(publishDate, 'news')
        return (
            <TouchableHighlight onPress={this.props.onItemPress}>
                <CardContainer>
                    <PDXImage
                        style={imageStyle}
                        source={headerImage}
                        placeholderType='small'
                        resizeMode='cover'
                    />
                    <ContentContainer>
                        <HeaderContainer>
                            <DateContainer numberOfLines={1}>
                                {publicatioDate}
                            </DateContainer>
                            {/* <ShareWrapperContainer>
                                <EntypoIcons
                                    style={{ marginLeft: 10 }}
                                    size={24}
                                    color='#adadad'
                                    name='facebook-with-circle'
                                    suppressHighlighting={true}
                                    onPress={() => this._onShare('facebook')} />
                                <EntypoIcons
                                    style={{ marginLeft: 10 }}
                                    size={24}
                                    color='#adadad'
                                    name='twitter-with-circle'
                                    suppressHighlighting={true}
                                    onPress={() => this._onShare('twitter')} />
                                <TouchableWithoutFeedback onPress={() => this._onShare('link')}>
                                    <View style={styles.linkIconContainer}>
                                        <EntypoIcons
                                            style={styles.linkIcon}
                                            size={13}
                                            color={colors.DARK_GRAY_COLOR}
                                            name='link' />
                                    </View>
                                </TouchableWithoutFeedback>
                            </ShareWrapperContainer> */}
                        </HeaderContainer>
                        <TitleWrapperContainer>
                            <TitleContainer numberOfLines={3}>
                                {headline}
                            </TitleContainer>
                        </TitleWrapperContainer>
                    </ContentContainer>
                </CardContainer>
            </ TouchableHighlight>
        )
    }
}

const styles = StyleSheet.create({
    linkIconContainer: {
        height: 24,
        width: 24,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#adadad',
        borderRadius: 24 / 2,
        marginLeft: 10,
    },
    linkIcon: {
        left: Platform.OS == 'ios' ? 1 : 0,
        top: Platform.OS == 'ios' ? 1 : 0,
    }
})