import glamorous from 'glamorous-native'
import Colors from '../../../config/colors'

const CONTAINER_HEIGHT = 116
const CONTAINER_PADDING = 2
const BORDER_RADIUS = 4
const IMAGE_SIZE = CONTAINER_HEIGHT - 2 * CONTAINER_PADDING

export const CardContainer = glamorous.view({
    flex: 1,
    height: CONTAINER_HEIGHT,
    flexDirection: 'row',
    padding: CONTAINER_PADDING,
    backgroundColor: Colors.DARK_GRAY_COLOR,
    borderRadius: BORDER_RADIUS,
})

export const ContentContainer = glamorous.view({
    flex: 1,
    marginHorizontal: 11,
})

export const HeaderContainer = glamorous.view({
    flex: 1 / 3,
    paddingVertical: 8,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
})

export const DateContainer = glamorous.text({
    fontSize: 12,
    color: '#adadad',
    fontFamily: 'Arial',
})

export const ShareWrapperContainer = glamorous.view({
    right: 0,
    top:0,
    bottom: 0,
    flexDirection: 'row',
})

export const TitleWrapperContainer = glamorous.view({
    flex: 2 / 3,
    justifyContent: 'center',
    paddingBottom: 8,
})

export const TitleContainer = glamorous.text({
    fontFamily: 'Arial',
    fontSize: 14,
    color: 'white',
    textAlign: 'left'
})

// Custom styles objects, not glamorous
export const imageStyle = {
    height: IMAGE_SIZE,
    width: IMAGE_SIZE,
    borderRadius: BORDER_RADIUS,
}

