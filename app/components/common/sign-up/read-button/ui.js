import React from 'react'

import {
    TouchableButtonContainer,
    TitleContainer
} from './styles'

export default class ReadButton extends React.Component {
    render() {
        const { title } = this.props
        return (
            <TouchableButtonContainer onPress={this.props.onPress}>
                <TitleContainer>
                    {title}
                </TitleContainer>
            </TouchableButtonContainer>
        )
    }
}