import glamorous from 'glamorous-native'
import Colors from '../../../../config/colors'

export const TouchableButtonContainer = glamorous.touchableHighlight({
    flex: 1,
    height: 30,
    borderRadius: 15,
    backgroundColor: Colors.BACKGROUND_COLOR,
    alignItems: 'center',
    justifyContent: 'center',
})

export const TitleContainer = glamorous.text({
    fontFamily: 'Arial',
    fontSize: 12,
    color: '#ababab',
    textAlign: 'center',
})