import React from 'react'
import { Image } from 'react-native'
import Markdown from 'react-native-markdown-renderer'
import { Text, View } from 'react-native'
import PropTypes from 'prop-types'
import { getStylesForType, customComponentStyles } from './styles'
import { hasParents } from './utils'


/**
 *  Custom markdown elements to be ovewriten
 */
const list_item = (node, children, parent, styles) => {
    if (hasParents(parent, 'bullet_list')) {
        return (
            <View key={node.key} style={styles.list_item.wrapper}>
                <Text style={styles.list_item.text}>{'\u00B7'}</Text>
                <View style={{ flex: 1, flexWrap: 'wrap' }}>{children}</View>
            </View>
        );
    }
}

const heading = (node, children, parent, styles, num) => {
    const style = styles[`heading${num}`]
    const child = React.cloneElement(children[0], { style: style }, children[0].props.children)
    return <Text>{child}}{'\n'}</Text>
}

const image = (node, children, parent, styles) => {
    const source = node && node.attributes ? node.attributes.src : null
    if (source) {
        return <Image
            style={{ width: 100, height: 100, }}
            source={{ uri: source }}
            resizeMode="contain"
        />
    }

    return null
}

/**
 *  Component implementation
 */
export default class MarkdownElement extends React.Component {
    static propTypes = {
        text: PropTypes.string.isRequired,
        type: PropTypes.string.isRequired,
    }

    render() {
        const { type } = this.props
        const markdownStyles = getStylesForType(type)
        return (
            <Markdown style={markdownStyles} rules={{
                list_item: (node, children, parent, styles) => list_item(node, children, parent, markdownStyles),
                heading1: (node, children, parent, styles) => heading(node, children, parent, markdownStyles, 1),
                heading2: (node, children, parent, styles) => heading(node, children, parent, markdownStyles, 2),
                heading3: (node, children, parent, styles) => heading(node, children, parent, markdownStyles, 3),
                heading4: (node, children, parent, styles) => heading(node, children, parent, markdownStyles, 4),
                heading5: (node, children, parent, styles) => heading(node, children, parent, markdownStyles, 5),
                heading6: (node, children, parent, styles) => heading(node, children, parent, markdownStyles, 6),
            }
            } >
                {this.props.children}
            </ Markdown>
        )
    }
}