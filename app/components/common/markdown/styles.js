
import { headings } from './utils'
const fontFamily = 'Arial'

const headingStyle = {
    fontFamily: fontFamily,
    fontSizeList: [32, 24, 18, 16, 13, 11], // list for size heading from 1-6
    color: 'white',
}


// ============ News Details =================== //
// ============================================= // 
const newsDetais = {
    ...headings({ headingStyle }),
    strong: {
        fontFamily: fontFamily,
        fontWeight: '600',
        fontSize: 12,
        color: '#eeeeee',
    },
    text: {
        fontFamily: fontFamily,
        color: '#eeeeee',
        fontSize: 12,
        lineHeight: 20,
    },
    list_item: {
        wrapper: {
            flexDirection: 'row',
            alignItems: 'flex-start',
        },
        text: {
            top: 2,
            left: -5,
            fontSize: 28,
            color: '#eeeeee',
        }
    },
    link: {
        color: '#eeeeee',
        fontWeight: 'bold',
    },
    em: {
        fontFamily: fontFamily,
        fontSize: 12,
        fontStyle: 'italic',
        color: 'white',
    },
    codeInline: {
        padding: 20,
        backgroundColor: 'gray',
    },
    codeBlock: {
        padding: 10,
        borderColor: 'gray',
        backgroundColor: 'white',
        borderRadius: 4,
        justifyContent: 'center',
    },
    blockquote: {
        paddingHorizontal: 20,
        marginHorizontal: 20,
        paddingVertical: 5,
        borderLeftWidth: 2,
        borderColor: 'white',
    }
}

// ============ Welcome notice =================== //
// ============================================= // 
const welcomeNoticeTitle = {
    strong: {
        fontFamily: fontFamily,
        color: 'black',
        fontSize: 24,
        lineHeight: 33,
        textAlign: 'center',
        fontWeight: '600',
    },
    text: {
        fontFamily: fontFamily,
        color: 'black',
        fontSize: 24,
        lineHeight: 33,
        textAlign: 'center',
    },
}

const welcomeNoticeDescription = {
    text: {
        fontFamily: fontFamily,
        color: 'black',
        fontSize: 14,
        lineHeight: 18,
        textAlign: 'center',
    },
}

// ============ Information notice =================== //
// =================================================== // 
const informationNotice = {
    strong: {
        fontFamily: fontFamily,
        color: 'black',
        fontSize: 24,
        lineHeight: 33,
        fontWeight: '600',
    },
    text: {
        fontFamily: fontFamily,
        color: 'black',
        fontSize: 14,
        lineHeight: 18,
        textAlign: 'left',
    },
}


/**
 * Get markdown configuration based on different type.
 * 
 * @param {string} type - Describes some feature/screen. For example newsDetails, home, info ect
 */
export const getStylesForType = type => {
    switch (type) {
        case 'informationNotice':
            return informationNotice
        case 'newsDetais':
            return newsDetais
        case 'welcomeNoticeTitle':
            return welcomeNoticeTitle
        case 'welcomeNoticeDescription':
            return welcomeNoticeDescription
        default:
            return newsDetais
    }
}