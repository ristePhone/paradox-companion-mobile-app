export const hasParents = (parents, type) => {
    return parents.findIndex(el => el.type === type) > -1
}

/** 
    - Generating markdown heading function. We need to create
    - 6 types of heading according to the GitHub documentation

    Props:
    @param {object} style - Style of the headings. 
                            Default for color and size.
**/
export const headings = style => {
    const { headingStyle } = style
    let headings = {}
    if (headingStyle) {
        let heading = {}
        for (var i = 1; i <= 6; i++) {
            const headingKey = `heading${i}`
            const headingValue = { 
                fontFamily: headingStyle.fontFamily,
                fontSize: headingStyle.fontSizeList ? headingStyle.fontSizeList[i-1] : 12,
                color: headingStyle.color ? headingStyle.color : 'white',
            }

            heading[headingKey] = headingValue
            headings = Object.assign({}, headings, heading)
        }
    }

    return headings
}
