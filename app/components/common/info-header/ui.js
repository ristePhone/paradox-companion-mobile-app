/**
 * 
 *  @param type (string) - One of the following types: shuttle, open, help, info 
 */

import React, { Component } from 'react'
import { StyleSheet, View, Text } from 'react-native'
import PropTypes from 'prop-types'

// Icons
import EntypoIcons from 'react-native-vector-icons/Entypo'
import EvilIcons from 'react-native-vector-icons/EvilIcons'
import MaterialIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Ionicons from 'react-native-vector-icons/Ionicons'

import Colors from '../../../config/colors'

// Component
export default class InfoHeaderCard extends Component {
    static propTypes = {
        type: PropTypes.string.isRequired,
    }

    _config() {
        return { color: Colors.BLUE_COLOR, iconSize: 12 }
    }

    _getTitleBasedOnType() {
        switch (this.props.type) {
            case 'open':
                return 'Opening hours'
            case 'shuttle':
                return 'Shuttle'
            case 'help':
                return 'need help?'
            case 'info':
            default:
                return 'information notice'
        }
    }

    _renderIconViewBasedOnType() {
        switch (this.props.type) {
            case 'open':
                return <EvilIcons size={this._config().iconSize} color={this._config().color} name='clock' />
            case 'shuttle':
                return <MaterialIcons size={this._config().iconSize} color={this._config().color} name='bus-side' />
            case 'help':
                return <Ionicons size={this._config().iconSize} color={this._config().color} name='ios-help-buoy-outline' />
            case 'info':
                return <EntypoIcons size={this._config().iconSize} color={this._config().color} name='info-with-circle' />
        }
    }

    render() {
        return (
            <View style={[styles.container]}>
                <View style={{ marginTop: 1 }}>
                    {this._renderIconViewBasedOnType()}
                </View>
                < Text style={[{ color: this._config().color }, styles.text]}> {this._getTitleBasedOnType().toUpperCase()}</Text >
            </View >
        )
    }
}

// Styles
var styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        height: 40,
        paddingHorizontal: 20,
        paddingVertical: 12,
        backgroundColor: 'white',
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(26, 26, 26, 0.05)'
    },
    text: {
        fontSize: 10,
        marginLeft: 8
    }
})