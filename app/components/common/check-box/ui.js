/**
 *  Props:
 *  @param {dictionary} style - Overides style, or add aditional style to the component
 *  @param {bool} selected
 *  @param {string} title - Title for the component. Optional
 *  @param {function} onCheckBoxButtonPress - Handle the press on checkbox icon
 */

import React from 'react'
import { TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import {
    Container,
    Title
} from './style'

export default class CheckBoxView extends React.Component {
    render() {
        const { title, selected } = this.props
        return (
            <TouchableOpacity onPress={this.props.onCheckBoxButtonPress}>
                <Container>
                    <Icon
                        size={16} color='white'
                        name={selected ? 'checkbox-marked' : 'checkbox-blank'}
                    />
                    <Title>
                        {title}
                    </Title>
                </Container>
            </TouchableOpacity>
        )
    }
}