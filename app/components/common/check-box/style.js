import glamorous from 'glamorous-native'

export const Container = glamorous.view(props => ({
    marginVertical: 10,
    flexDirection: 'row',
    alignItems: 'center'
}))

export const Title = glamorous.text({
    color: 'white',
    textAlign: 'left',
    fontSize: 12,
    marginLeft: 10,
    marginRight: 5
})