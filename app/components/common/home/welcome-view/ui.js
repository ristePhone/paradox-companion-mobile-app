import React, { Component } from 'react'
import { StyleSheet, View, Text, TouchableWithoutFeedback, Image, Dimensions } from 'react-native'
var { height, width } = Dimensions.get('window')

import Video from 'react-native-video'
import EntypoIcons from 'react-native-vector-icons/Entypo'

import Markdown from '../../markdown/ui'

export default class WelcomeCard extends Component {
    constructor(props) {
        super(props)
        this.state = {
            duration: 0,
            remain: '',
            isPaused: true,
            isFirstPlay: true
        }
    }

    _renderTimeLeftVideo(total, remain) {
        if (total - remain <= 0) {
            return '0:00'
        } else {
            var min = parseInt((total - remain) / 60)
            var sec = parseInt((total - remain) % 60)
            sec = (sec < 10) ? `0${sec}` : sec
            return `${min}:${sec}`
        }
    }

    componentWillReceiveProps(nextProps) {
        if (!this.state.isFirstPlay) this.setState({ isPaused: nextProps.pause })
    }

    render() {
        const { title, description } = this.props
        return (
            <View style={styles.container}>
                <View style={{ padding: 30, alignItems: 'center', justifyContent: 'center' }}>
                    <Markdown type='welcomeNoticeTitle'>{title}</Markdown>
                    <Markdown type='welcomeNoticeDescription'>{description}</Markdown>
                </View>
                <TouchableWithoutFeedback onPress={() => this.setState({ isPaused: !this.state.isPaused, isFirstPlay: !this.state.isFirstPlay })}>
                    <View style={styles.videoContainer}>
                        {
                            this.state.duration == 0 && this.state.isPaused ?
                                <Image
                                    style={{ flex: 1, width: width - 2 * 19, left: -1 }}
                                    // source={require('../../../../resources/video_thumbnail.jpeg')}
                                    source={{ uri: this.props.urlThumbnail }}
                                    resizeMode='stretch' />
                                :
                                <Video
                                    ref={(component) => this.video = component}
                                    // source={require('../../../../resources/fans_video_hiring.mp4')}
                                    source={{ uri: this.props.urlVideo}}
                                    playInBackground={false}
                                    playWhenInactive={false}
                                    repeat={true}
                                    paused={this.state.isPaused}
                                    resizeMode="cover"
                                    style={{ flex: 1 }}
                                    progressUpdateInterval={500}
                                    onLoad={(data) => this.setState({ remain: this._renderTimeLeftVideo(data.duration, 0), duration: parseInt(data.duration) })}
                                    onProgress={(data) => { this.setState({ remain: this._renderTimeLeftVideo(this.state.duration, data.currentTime) }) }}
                                    onEnd={() => this.setState({ isPaused: true, duration: 0 })} >
                                </Video>
                        }
                    </View>
                </TouchableWithoutFeedback>
                {this.state.duration == 0 && this.state.isPaused ? null : <Text style={styles.videoDurationText}>{this.state.remain}</Text>}
            </View >
        )
    }
}

// Stylesheet
var styles = StyleSheet.create({
    container: {
        flex: 1,
        borderRadius: 8,
        backgroundColor: 'white',
        overflow: 'hidden'
    },
    videoContainer: {
        height: 166,
        flex: 1,
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8,
        backgroundColor: 'transparent'
    },
    videoDurationText: {
        bottom: 0,
        right: 5,
        bottom: 5,
        position: 'absolute',
        fontSize: 7,
        color: 'white'
    }
})