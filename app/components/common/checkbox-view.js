/**
 * 
 *  Checkbox components
 * 
 *  Props:
 *  @param {dictionary} style - Overides style, or add aditional style to the component
 *  @param {bool} selected
 *  @param {string} title - Title for the component. Optional
 *  @param {function} onTextPress - Handle the press action back to the component that invoked it
 *  @param {function} onCheckBoxButtonPress - Handle the press on checkbox icon
 */

import React, { Component } from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

export default class CheckBoxView extends React.Component {
    render() {
        let firstString = this.props.title
        let secondString = undefined
        const spitString = 'Paradox\'s '
        const stringArray = this.props.title.split(spitString)
        if (stringArray.length == 2) {
            firstString = String(stringArray[0]).concat(`${spitString} `)
            secondString = String(stringArray[1])
        }
        return (
            <TouchableOpacity  onPress={this.props.onCheckBoxButtonPress}>
            <View  style={{ marginVertical: 10, flexDirection: 'row', alignItems: 'center', ...this.props.style }}>
              
                <Icon
                    size={16} color='white'
                    name={(this.props.selected) ? 'checkbox-marked' : 'checkbox-blank'}
                />
                {
                    this.props.title ?
                        <Text
                            style={styles.formCheckBoxText}
                            numberOfLines={3}
                        >{firstString}
                            {
                                secondString ? <Text style={[styles.formCheckBoxText, { textDecorationLine: 'underline' }]}
                                    onPress={this.props.onTextPress}
                                >
                                    {secondString}
                                </Text> : null
                            }
                        </Text>
                        : null
                }

            </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    formCheckBoxText: {
        color: 'white',
        textAlign: 'left',
        fontSize: 12,
        marginLeft: 10,
        marginRight:5
    }
})