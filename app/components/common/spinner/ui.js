/**
 *  Component used for showing sppiner in the application
 * 
 *  Props:
 *  @param {bool} visible - Flag that indicates if the component should be shown
 *  @param {function} onClose - Send back close event
 */

import React from 'react'
import {
    StyleSheet,
    Modal,
    ActivityIndicator,
    View,
} from 'react-native'

export default class SpinnerView extends React.Component {

    shouldComponentUpdate(nextProps) {
        console.log('PETAR', nextProps)
        return nextProps !== this.props
    }

    render() {
        return (
            <Modal  
                visible={this.props.visible}
                transparent={true}
                hardwareAccelerated={true}
                onRequestClose={() => console.log('Closed')}
            >
                <View style={styles.container}>
                    <ActivityIndicator
                        animating={this.props.visible}
                        color='white'
                        size='large'
                    />
                </View>
            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.4)',
    }
})