/**
 *  Component for showing the event in Home Screenn
 * 
 *  Props:
 *  @param {array} item - List of event items to be shown   
 *  @param {function} onItemSelect - Return the touch event
 */

import React from 'react'
import { View } from 'react-native'
import { CardContainer, CardSubContainer, Title } from './styles'
import EventCardHeader from './header/ui'
import EventListItem from '../event-list-item/ui'
import { strings } from '../../../../config/localization'

export default class EventCard extends React.Component {

    _renderItems = () => {
        const items = this.props.items
        return items.map((item, index) => {
            return <EventListItem
                key={index}
                item={item}
                onPress={() => this.props.onItemSelect(item)}
            />
        })
    }

    render() {
        const items = this.props.items
        return (
            <CardContainer>
                {
                    items.length ?
                        <View>
                            <EventCardHeader />
                            {this._renderItems()}
                        </View> :
                        <CardContainer>
                            <Title numberOfLines={0}>{strings.event.noUpcommingEvents}</Title>
                        </CardContainer>
                }
            </CardContainer>
        )
    }
}