import glamorous from 'glamorous-native'

export const Container = glamorous.view({
    flex: 1,
    height: 20,
    justifyContent: 'center',
})

export const Title = glamorous.text({
    fontFamily: 'SourceSansPro-Regular',
    fontSize: 16,
    fontWeight: '600',
    color: 'white',
    textAlign: 'left'
})