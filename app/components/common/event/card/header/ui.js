import React from 'react'
import {
    Container,
    Title
} from './styles'
import { strings } from '../../../../../config/localization'

export default class EventCardHeader extends React.Component {
    render() {
        const title = strings.event.header.toUpperCase()
        return (
            <Container>
                <Title>{title}</Title>
            </Container>
        )
    }
}