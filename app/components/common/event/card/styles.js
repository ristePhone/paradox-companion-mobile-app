import glamorous from 'glamorous-native'

export const CardContainer = glamorous.view({
    flex: 1,
    borderRadius: 4,
    paddingVertical: 24,
    paddingHorizontal: 18,
    backgroundColor: '#1a1a1a'
})

export const CardSubContainer = glamorous.view({
    flex: 1,
    height: 100,
    justifyContent: 'center',
    alignItems: 'center',
})

export const Title = glamorous.text({
    fontFamily: 'SourceSansPro-Regular',
    fontSize: 24,
    color: 'white',
    textAlign: 'center'
})