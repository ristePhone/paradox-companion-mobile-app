import glamorous from 'glamorous-native'

export const WrapperContainer = glamorous.view( props => ({
    flex: 1,
    marginTop: 15,
    backgroundColor: '#1a1a1a'
}))

export const TopSeparator = glamorous.view({
    height: 1,
    flex: 1,
    backgroundColor: '#222222'
})

export const HeaderContainer = glamorous.view({
    height: 40,
    flex: 1,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
})

export const Title = glamorous.text({
    flex: 4,
    fontFamily: 'SourceSansPro-Regular',
    fontSize: 16,
    textAlign: 'left',
    color: 'white',
})

export const Date = glamorous.text({
    flex: 2,
    fontFamily: 'SourceSansPro-Regular',
    fontSize: 11.5,
    textAlign: 'right',
    color: '#adadad',
})
