import React from 'react'
import { TouchableWithoutFeedback } from 'react-native'
import {
    WrapperContainer,
    HeaderContainer,
    Title,
    Date,
    TopSeparator
} from './styles'
import UtilsService from '../../../../services/utils-service'
import EventInfo from '../event-info/ui'

export default class EventListItem extends React.Component {
    render() {
        const { eventHeadline } = this.props.item
        const eventStart = UtilsService.transformDateEvents(this.props.item.eventStart)
        return (
            <TouchableWithoutFeedback onPress={() => this.props.onPress()}>
                <WrapperContainer>
                    <TopSeparator />
                    <HeaderContainer>
                        <Title
                            numberOfLines={1}
                            lineBreakMode='tail'
                        >
                            {eventHeadline}</Title>
                        <Date numberOfLines={1}>{eventStart}</Date>
                    </HeaderContainer>
                    <EventInfo {...this.props.item} />
                </WrapperContainer>
            </TouchableWithoutFeedback>
        )
    }
}