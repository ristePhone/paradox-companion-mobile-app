const formatDay = day => {
    if (day < 10) {
        return `0${day}`
    }

    return `${day}`
}

export const transformDate = dateString => {
    const date = new Date(dateString)
    const day = formatDay(date.getDate())
    const month = formatDay(date.getMonth() + 1)
    const hours = formatDay(date.getHours())
    const minutes = formatDay(date.getMinutes())
    const formatedDate = `${day}.${month}.${date.getFullYear()} ${hours}:${minutes}h`
    return formatedDate
}