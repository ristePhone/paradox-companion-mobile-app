import glamorous from 'glamorous-native'

const PADDING_VERTICAL = 15

export const WrapperContainer = glamorous.view({
    flex: 1,
    paddingHorizontal: 9,
    borderRadius: 4,
    backgroundColor: '#222222',
    justifyContent: 'space-between',
})

export const TopContainer = glamorous.view({
    flex: 1,
    marginVertical: PADDING_VERTICAL,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
})

export const TextWrapperContainer = glamorous.view({
    flex: 1,
})

export const BottomContainer = glamorous.view({
    flex: 1,
    marginBottom: PADDING_VERTICAL,
})

export const Title = glamorous.text((props) => ({
    flex: 1,
    fontSize: 11,
    letterSpacing: -0.2,
    color: '#adadad',
}))

export const Description = glamorous.text({
    flex: 1,
    color: '#eeeeee',
})

