import React from 'react'
import {
    WrapperContainer,
    TopContainer,
    TextWrapperContainer,
    BottomContainer,
    Title,
    Description
} from './styles'
import { strings } from '../../../../config/localization'
import { transformDate } from './utils'
import UtilService from '../../../../services/utils-service'

export default class EventInfo extends React.Component {
    render() {
        const { eventLocation } = this.props
        const addString = ': '
        const startTitle = strings.event.start.concat(addString)
        const finishTitle = strings.event.finish.concat(addString)
        const locationTitle = strings.event.location.concat(addString)
        const eventStart = UtilService.formatDateToGMTOffset(this.props.eventStart)
        const eventFinish = UtilService.formatDateToGMTOffset(this.props.eventFinish)
        return (
            <WrapperContainer>
                <TopContainer>
                    <TextWrapperContainer marginRight={10}>
                        <Title>{startTitle}
                            <Description>{eventStart}</Description>
                        </Title>
                    </TextWrapperContainer>
                    <TextWrapperContainer>
                        <Title 
                            style={{textAlign: 'right'}}>{finishTitle}
                            <Description>{eventFinish}</Description>
                        </Title>
                    </TextWrapperContainer>
                </TopContainer>
                <BottomContainer>
                    <Title>{locationTitle}
                        <Description>{eventLocation}</Description>
                    </Title>
                </BottomContainer>
            </WrapperContainer>
        )
    }
}