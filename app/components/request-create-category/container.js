import { connect } from 'react-redux'
import CreateRequestScreen from '../request-create-category/screen'
import Routes from '../../config/routes';
import { strings } from '../../config/localization'

const mapStateToProps = state => ({
    mainImageUrl: state.contentfulAPIcontent.mainImageUrl
})

const mapDispatchToProps = dispatch => ({
    _navigateToRequestList: (props, element, types, idparam, title) => {
        props.navigator.push({
            screen: Routes.REQUEST_CREATE3,
            title: strings.request_navigator_title_submit_request,
            passProps: {
                notify: true,
                notifyMessage: strings.request_submit_notify_request_crated,
                element: element,
                type: types,
                id: 44,
                title: title
            },
        })
    }
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CreateRequestScreen)