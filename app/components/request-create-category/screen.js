import React, { Component } from 'react'
import { StyleSheet, View, TextInput, TouchableWithoutFeedback, Platform, Picker, KeyboardAvoidingView, Keyboard, ScrollView, Alert } from 'react-native'

import RequestService from '../../services/request-service'
import Configuration from '../../config/environment/config'
import issueTypes from '../../utils/issueTypes.json'

import PickerView from '../views/native-picker'
import BackgroundView from '../views/background-view'

import ZendeskService from '../../services/zendesk-service'
import SpinnerView from '../common/spinner/ui'
import { store } from '../../reducers/index'
import { FormContainer, FooterView,  RequestHeaderTextView, ActionButtonContainer} from './styles'
import Colors from '../../config/colors'
import { strings } from '../../config/localization'
import ActionButton from '../views/action-button'
const  middleware_zendesk  = () => {
    const name = store.getState().config.name
    return Configuration.environment[name].MIDDLEWARE_BASE_URL
}

export default class CreateRequestScreen extends Component {
    constructor(props) {
        super(props)
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this))
        this.state = {
            issueType: '',
            issueTypes: '',
            pickerType: 'issueType',
            showPicker: false,
            selecteIssueType: '',
            loading: false,
            test:[],
            body:[],
            custom_fields:[],
            ticket_fields:null,
            ticket_fields_selection:[],
            emptyValue:'',
            mainPickerOption:[],
            pickerSource:[],
            pickerId:0,
            tempCustomFields:[],
            randomVal:'',
            textInput : [],
            selectedDropDown:'',
            selectedCategory: {id:0},
            showNextButton:false
        }
    }
    onNavigatorEvent(event) {
        if (event.id == 'didAppear') {
            if(this.state.selectedCategory.id != 0)
            {
                this.setState({showNextButton: true})
            }
        }
    }
    // ---------------------------------------------------------------------------------------
    // helpers start
    // ---------------------------------------------------------------------------------------
    _startLoading() {
        this.setState({ loading: true })
    }

    _endLoading() {
        this.setState({ loading: false })
    }

    apiResultToModel(result){
        var array = []
        var forms = result.ticket_forms

        var temp = { name: '-', code: '-', id: 0, ticket_field_ids: '-' }
        this.setState({selectedCategory: temp})
        array.push(temp)

        forms.forEach(function(element) {
            if(element.active)
            {
                var temp = { name: element.name,code: 'test', id: element.id, ticket_field_ids: element.ticket_field_ids }
                array.push(temp)
            }
          });
        this.setState({issueTypes: array, mainPickerOption: array, pickerSource: array})
    }

    renderCustomFields(selectedElement){
        this._getTicketFieldsById(selectedElement)
    }
   
    // ---------------------------------------------------------------------------------------
    // helpers end
    // ---------------------------------------------------------------------------------------

    // ---------------------------------------------------------------------------------------
    // methods start
    // ---------------------------------------------------------------------------------------
    async _apiCallGetAllTicketForms(){
        var url = Configuration.common.ZENDESK_TICKET_FORMS()
        var result = await this.localApiHandler('GET', url)
        this.apiResultToModel(result)
    }
    async  _getTicketFieldsById(item){
        if(item.id==0)
            return
    
        let queryParameters = 'contentType=zendeskFields&id='+item.id+'&env=' + store.getState().config.name
        var url = `${middleware_zendesk()}?${queryParameters}`

        var result = await this.localApiHandler('GET', url)
        let element = {
            id: item.id,
            title: 'General Issue',
            ticket_fields: result,
        }
        // if(result.ok == false)
        // {
        //         Alert.alert("Please check your internet connection")
        //         return result
        // }
        this.setState({ticket_fields: result})
        this.props._navigateToRequestList(this.props, element, issueTypes, null, item.name)
        return result
    }
    async localApiHandler(type, url, body){
        this._startLoading()
        var result = await RequestService.api(type, url, await ZendeskService.setJsonHeaders(), body)
        this._endLoading()
        return result
    }
    // ---------------------------------------------------------------------------------------
    // methods end
    // ---------------------------------------------------------------------------------------
    componentWillMount() {
        this._apiCallGetAllTicketForms()
    }
    _onSubmitNext(){
        this.renderCustomFields(this.state.selectedCategory)
    }
    _showNextButton(item){
        if(item.id == 0) {
            this.setState({showNextButton:false})
        }
    }
    _showPicker = (props) => { return (this.state.showPicker) ?
            <PickerView
                selectedValue={this.state.selectedCategory}
                type={this.state.pickerType}
                dataSource={this.state.pickerSource}
                onDoneButtonPress={(selectedValue) => {
                    Keyboard.dismiss()
                    this._showNextButton(selectedValue)
                    this.setState({ issueType: selectedValue, showPicker: false, selectedCategory: selectedValue})
                    this.renderCustomFields(selectedValue)}} /> 
            : null
    }
    _renderPicker() {
        return (
            <View style={[styles.textInputContainer, { marginTop: 8 }]}>
                { (Platform.OS == 'ios') ?
                        <TouchableWithoutFeedback onPress={() => { this.setState({ showPicker: true }) }}>
                            <TextInput
                                editable={false}
                                style={styles.textInputContainer}
                                placeholder=''
                                value={this.state.issueType['name']}
                                underlineColorAndroid='white'
                                onTouchStart={() => { this.setState({ showPicker: true, pickerSource: this.state.mainPickerOption }) }} 
                                onValueChange={(value, position) => { 
                                    this.setState({ selecteIssueType: this.state.issueTypes[position] }) }}/>
                        </TouchableWithoutFeedback> 
                        :
                        <Picker
                            style={{ height: 44, flex: 1, marginRight: 8 }}
                            selectedValue={this.state.selectedCategory.name}
                            prompt='Please select what your problem is regarding with'
                            mode='dialog'
                            onValueChange={(value, position) => {  
                                let selected = this.state.pickerSource[position]
                                this._showNextButton(selected)
                                this.renderCustomFields(selected)
                                this.setState({selectedDropDown: this.state.pickerSource[position], selectedCategory: this.state.pickerSource[position]})}} >
                            { this.state.pickerSource.map(item => { return <Picker.Item key={item.name} label={item.name} value={item.name} /> })}
                        </Picker>
                }
            </View>
    )}
    _renderNextButton(){return(
        <ActionButtonContainer>
            <ActionButton
                    disabled={this.state.loading}
                    title={strings.request_submit_button_next}
                    backgroundColor={Colors.GREEN_COLOR}
                    onPress={() => this._onSubmitNext(this.props)} />
        </ActionButtonContainer>
    )}
    renderView() { return (
            <BackgroundView>
                  <SpinnerView visible={this.state.loading} />
                <FormContainer ref={(ref) => { this.flatListRef = ref; }}>
                    <RequestHeaderTextView>{strings.label__request_ticket_form_id}</RequestHeaderTextView>
                    {this._renderPicker(this.state.mainPickerOption)}
                    {this.state.showNextButton? this._renderNextButton() : null}
                </FormContainer>
                <FooterView />
                {this._showPicker(this.props)}
            </BackgroundView >
    )}
   
    // Lifecycle
    render(key) { return (
                <KeyboardAvoidingView style={{ flex: 1,marginTop:20 }} behavior="padding">
                    <ScrollView ref="_scrollView" key={key}
                        keyboardShouldPersistTaps='handled'  >
                        {this.renderView()}
                    </ScrollView>
                </KeyboardAvoidingView>
    )}}

// Styles
var styles = StyleSheet.create({
    textInputContainer: {
        height: 44,
        backgroundColor: 'white',
        borderRadius: 4,
        paddingLeft: 8,
    },
})