import React, { Component} from 'react'
import { Dimensions, Keyboard, ScrollView, Platform, View, Text, AsyncStorage } from 'react-native'
import { validateEmail } from '../../services/authentication'
import { store } from '../../reducers/index'
import { strings } from '../../config/localization'
import { FormContainer, FormTextInputField, ActionButtonContainer, RequestHeadersTextView, RequestBodyTextView, FormTextInputMessageField,
       ErrorContainerView, ErrorView, ErrorText, ErrorContainerMessageView, ErrorCheckboxContainerView } from './styles'
import RequestService from '../../services/request-service'
import Colors from '../../config/colors'
import ActionButton from '../views/action-button'
import CheckBoxView from '../common/checkbox-view'
import Configuration from '../../config/environment/config'

//constants
const requeiredSign = "*"
const middleware_zendesk  = () => {
    const name = store.getState().config.name
    return Configuration.environment[name].MIDDLEWARE_BASE_URL + '?env=' + name
}

export default class FeedbackCreateScreen extends Component {
    constructor(props) {
        let { height, width } = Dimensions.get('window')
        super(props)
        this.state = {
            description: '',
            email: '',
            subject: '',
            message: '',
            height: height,
            checkBoxNotRobot: false,

            // validation errors and error messages
            subjectError: false,
            subjectErrorMessage: strings.request_feedback_notify_subject,
            messageError:false,
            messageErrorMessage: strings.request_feedback_notify_message,
            emailError:false,
            emailErrorMessage: strings.request_feedback_notify_email_not_valid,
            robotError:false,
            robotErrorMessage: strings.request_feedback_notify_not_robot
        }
    }

    componentDidMount() {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
       // this.setState({email: store.getState().username})
        AsyncStorage.getItem('username').then((data) => {
            if (data != null) {
                this.setState({ email: data })
            } 
            //else {
                //this.setState({email: 'You are not logged in!'})
            //}

        })
    }

    componentWillUnmount(props) {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    _keyboardDidShow() {
        this._hideNavBar()
    }

    _keyboardDidHide() {
        this._showNavBar()
    }

    _showNavBar() {
        this.props.navigator.toggleTabs({
            to: 'shown',
            animated: false
        });
    }

    _hideNavBar() {
        this.props.navigator.toggleTabs({
            to: 'hidden',
            animated: false
        });
    }

    _resetValidationErrors()
    {
        this.setState({subjectError:false, emailError:false, messageError:false, robotError:false})
    }

    _validateForm() {
        this._resetValidationErrors()
        let validattionResult = true
        let email = this.state.email.trim()
        if (this.state.subject.trim().length == 0) {
            this.setState({subjectError:true})
            validattionResult = false
        }
        let error = validateEmail(this.state.email)
        if(error != null && email.length>0)
        {
           this.setState({emailError: true})
           validattionResult = false
        }
        if (this.state.message.trim().length == 0) {
            this.setState({messageError:true})
            validattionResult = false
        } 
        if(!this.state.checkBoxNotRobot)
        {
            this.setState({robotError:true})
            validattionResult = false
        }
        return validattionResult    
    }

    _onSubmit() {
        if (!this._validateForm())
            return

        var url = `${middleware_zendesk()}`
        var body = JSON.stringify({
             mail: this.state.email,
             subject: this.state.subject,
             body: this.state.message
         })
         
        var headers = {'Content-Type': 'application/json'}
        RequestService.post1(url, headers, body)
        this.props._navigateToRequestList(this.props)
    }

    setSel(input) {
        if (Platform.OS == 'android') {

            if (input == 'inputArea') {
                if (this.state.height < 660) {
                    setTimeout(() => {
                        this.refs._scrollView.scrollTo({ x: 0, y: 200, animated: true })
                    }, 500)
                    //    this.setKybordOpenAnroid()
                } else {
                    setTimeout(() => {
                        this.refs._scrollView.scrollToEnd({ animated: true })
                    }, 500)
                    // this.setKybordOpenAnroid()
                }
            } else {
                //this.setKybordOpenAnroid()
            }
        }
        if (input == 'input') {
            this.refs._scrollView.scrollTo({ x: 0, y: 0, animated: true })
        } else {
            setTimeout(() => {
                this.refs._scrollView.scrollTo({ x: 0, y: 0, animated: true })
            }, 300);
        }
    }

    hideKeyboard() {
        try {
            this.refs._scrollView.scrollTo({ x: 0, y: 0, animated: true })
        }
        catch (err) {
            return
        }
    }

    _renderEmailInput(marginTop){return(
        <FormTextInputField
            marginTop={marginTop}
            keyboardType = "email-address"
            autoCapitalize ="none"
            value={this.state.email}
            autoCorrect={false}
            // onBlur={()=>this.hideNav()}
            onFocus={() => this.setSel('input')}
            placeholder={strings.request_feedback_placeholder_email}
            onChangeText={(text) => { this.setState({ email: text }) }}
            underlineColorAndroid='white' />
    )}
    _renderErrorEmail(marginTop){return(
        <ErrorContainerView> 
              {this._renderEmailInput(marginTop)}
             <ErrorView>
                <ErrorText>{this.state.emailErrorMessage}</ErrorText> 
            </ErrorView>
        </ErrorContainerView> 
    )}

    _renderSubjectInput(marginTop){return(
            <FormTextInputField
                marginTop={marginTop}
                value={this.state.subject}
                autoCorrect={false}
                // onBlur={()=>this.hideNav()}
                onFocus={() => this.setSel('input')}
                placeholder={strings.request_feedback_placeholder_subject}
                onChangeText={(text) => { this.setState({ subject: text }) }}
                underlineColorAndroid='white' />
    )}
    _renderErrorSubject(marginTop){return(
        <ErrorContainerView> 
              {this._renderSubjectInput(marginTop)}
             <ErrorView>
                <ErrorText>{this.state.subjectErrorMessage}</ErrorText> 
            </ErrorView>
        </ErrorContainerView> 
    )}
    _renderMessageInput(marginTop){return(
                <FormTextInputMessageField
                   marginTop={marginTop}
                    selection={this.state.select}
                    multiline={true}
                    //selection={{start: 0,end: 0}}
                    // multiline 
                    //   onBlur={()=>this.hideNav()}
                    innerRef={(node) => { this.inputNode = node; }} onFocus={() => this.setSel('inputArea')}
                    value={this.state.message}
                    autoCorrect={false}
                    placeholder={strings.request_feedback_placeholder_message}
                    onChangeText={(text) => { this.setState({ message: text }) }}
                    underlineColorAndroid='white' />
    )}
    _renderErrorMessage(){return(
        <ErrorContainerMessageView> 
            {this._renderMessageInput(1)}
            <ErrorView>
                <ErrorText>{this.state.messageErrorMessage}</ErrorText>
            </ErrorView>
        </ErrorContainerMessageView> 
    )}

    _renderCheckboxErrorView(error){ return( 
        <ErrorCheckboxContainerView>
            <ErrorView>
                <ErrorText>{error}</ErrorText> 
            </ErrorView>
        </ErrorCheckboxContainerView>
    )}
    
    _renderErrorNotRobotCheckBox(){return(
        <CheckBoxView
            title={strings.request_feedback_label_not_robot}
            selected={this.state.checkBoxNotRobot}
            onCheckBoxButtonPress={() => {
                this.setState({ checkBoxNotRobot: !this.state.checkBoxNotRobot })
            }}/> 
    )}

    _renderSubmitButton(){return(
        <ActionButtonContainer>
            <ActionButton
                title={strings.request_feedback_button_submit}
                backgroundColor={Colors.GREEN_COLOR}
                onPress={() => this._onSubmit()} />
        </ActionButtonContainer>
    )}

    // Lifecycle
    render() {
        if (Platform.OS == 'android') {
            Keyboard.addListener('keyboardDidHide', () => {
                try {
                    this.inputNode.blur()

                }
                catch (err) {

                }
            });
            // Keyboard.addListener('keyboardWillShow', (e) => {
            //this.setKybordOpenAnroid()
            //});
        }
        if (Platform.OS == 'ios') {
            Keyboard.addListener('keyboardWillHide', () => {
                //  this.setKeybordShow()
                this.hideKeyboard()
            });
        } else {
            Keyboard.addListener('keyboardDidHide', () => {
                // this.setKeybordShow()
                this.hideKeyboard()
            });
        }
        return (
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 20 }}>
                <ScrollView ref="_scrollView" keyboardShouldPersistTaps='handled'>
                    <FormContainer ref='modal'>
                        <RequestHeadersTextView>{strings.request_feedback_label_subject}<Text style={{color: 'red'}}> {requeiredSign} </Text></RequestHeadersTextView>
                        {this.state.subjectError? this._renderErrorSubject(1): this._renderSubjectInput()}

                        <RequestHeadersTextView>{strings.request_feedback_label_email}</RequestHeadersTextView>
                        {this.state.emailError? this._renderErrorEmail(1) : this._renderEmailInput()}

                        <RequestBodyTextView>{strings.request_feedback_label_message}<Text style={{color: 'red'}}> {requeiredSign} </Text></RequestBodyTextView>
                        {this.state.messageError? this._renderErrorMessage(1): this._renderMessageInput()}

                        {this._renderErrorNotRobotCheckBox()}
                        {this.state.robotError? this._renderCheckboxErrorView(this.state.robotErrorMessage): null }
                        
                        {this._renderSubmitButton()}
                    </FormContainer>
                </ScrollView>
            </View>
        )}
}
