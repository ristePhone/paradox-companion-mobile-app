import { connect } from 'react-redux'
import CreateRequestScreen from '../request-create-remove/screen'

const mapStateToProps = state => ({
    mainImageUrl: state.contentfulAPIcontent.mainImageUrl
})

const mapDispatchToProps = dispatch => ({
   
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CreateRequestScreen)