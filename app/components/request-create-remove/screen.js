import React, { Component } from 'react'
//import { connect } from 'react-redux'
import {
    StyleSheet,
    View,
    //Text,
    Dimensions,
    TextInput,
   // Image,
    //ScrollView,
    //TouchableOpacity,
   TouchableWithoutFeedback,
    TouchableNativeFeedback,
    Platform,
    Picker,
    KeyboardAvoidingView,
    Keyboard,
    Alert
} from 'react-native'
import RequestService from '../../services/request-service'
//var { height, width } = Dimensions.get('window')
//import { store } from '../../reducers/index'
//import Configuration from '../../config/environment/config'
import Configuration from '../../config/environment/config'

//import { configurationSetup } from '../../config/config'
// const zendeskUrl = () => {
//      return Configuration.common
//  }
import Colors from '../../config/colors'
import countries from '../../utils/countries.json'
import issueTypes from '../../utils/issueTypes.json'
import Utils from '../../services/utils-service'
import ActionButton from '../views/action-button'
import LogoImageView from '../common/main-image-view/logo-image-view'
import PickerView from '../views/native-picker'
import BackgroundView from '../views/background-view'
import SpinnerView from '../views/spinner-view'
import { validateEmail } from '../../services/authentication'
import Routes from '../../config/routes';
const FONT_COLOR = '#EEF0F2'
import {
    ScrollViewContainer,
    ScrollViewContentView,
    FormContainer,
   // FormTitleContainer,
    //FormTitleTextView,
    FormTextInputField,
    FormTextInputFieldList,
    ActionButtonContainer,
    //CheckBoxContainer,
    FooterView,
    //BottomContainer,
    //AlreadyHaveAcountTextView,
    RequestHeadersTextView,
    RequestHeadersHintTextView
} from './styles'

//import CheckBoxView from '../common/checkbox-view'

import { strings } from '../../config/localization'

export default class CreateRequestScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            description: '',
            summary: '',
            country: '',
            issueType: issueTypes[0],
            issueTypes: '',
            pickerType: 'issueType',
            showPicker: false,
            selectedCountry: countries[0],
            selecteIssueType: issueTypes[0],
            mainImageUrl: this.props.mainImageUrl
        }
    }

    _clearState() {
        this.setState({ summary: '', description: '', issueType: '' })
    }
    _checkField(field, message) {
        let check = field
        if (field == undefined || field.trim().length == 0) {
            alert(message)
            return false
        }

        return true
    }
    _checkValidation() {
        if (this.state.issueType == '') {
            alert("Please select Issue type")
            return false
        }

        if (this.state.summary.trim().length == 0) {
            alert("Please enter Summary")
            return false
        }

        if (this.state.description.trim().length == 0) {
            alert("Please enter Description")
            return false
        }

        return true
    }

    async _apiCallSubmitRequest(props){
        var url = Configuration.common.ZENDESK_URL;
        var headers = {
            'Authorization': 'Basic dG9taS5yaXN0b3Zza2kubWtAZ21haWwuY29tOmFzZGY4ODQ0QA',
            'Content-Type': 'application/json'
        }
        var body = JSON.stringify({
            request: {
                requester: { name: 'tomi name' },
                subject: this.state.summary,
                comment: { body: this.state.description }
            }
        }) 
        var result = await RequestService.post1(url, headers, body).then(
            this._navigateToRequestList(this.props)
        )
        
    }
    componentWillMount() {
        this.showTickets()
            this.setState({ id: this.props.url })
            this._showDetails(this.props.url)
            this.props.navigator.toggleTabs({
                to: 'hidden',
                animated: false
                });
    }
    async showTickets() {
        var url = 'https://data2278.zendesk.com/api/v2/requests.json?status=open';
        var headers = {
            'Authorization': 'Basic dG9taS5yaXN0b3Zza2kubWtAZ21haWwuY29tOmFzZGY4ODQ0QA'
        }
        var result = await RequestService.get(url, headers, "")
         //this.setState({ requests: result.requests })
    }
    async apiSubmitNewRequest() {
         var result = await RequestService.requestListTest()    
         this.setState({ requests: result.requests })
    }

    // getApiResult(){
    //     var url = 'https://data2278.zendesk.com/api/v2/requests.json?status=open';
    //     return  RequestService.get(url, null)
    // }

    _navigateToRequestList(props){
        props.navigator.push({
            screen: Routes.REQUESTS_LIST,
            title:'Open requests',
            passProps: {
                notify: true,
                notifyMessage: 'Request created'
            },
        })
    }

    _onSubmit(props) {
        //this.formsubmittin = true
        //this.insertRequest()

        if (!this._checkValidation())
            return
        //let request = this.formToRequest()
       this._apiCallSubmitRequest(props)

       // return response
       
    }

    //_formToRequest(){
        //let result = 
        // result.name = this.form.name
        //result.message = this.form.message
        //etc
   // }

    _showDatePicker = () => {
        return (this.state.showPicker) ?
            <PickerView
                type={this.state.pickerType}
                dataSource={issueTypes}
                onDoneButtonPress={(selectedValue) => {
                    Keyboard.dismiss()
                    if (this.state.pickerType == 'date') {
                        this.setState({ dateOfBirth: Utils.transformDate(selectedValue), showPicker: false })
                    } else {
                        this.setState({ country: selectedValue, showPicker: false })
                        this.setState({ issueType: selectedValue, showPicker: false })

                    }
                }} /> : null
    }

    _renderIssuteTypeView() {
        return (
            <View style={[styles.textInputContainer, { marginTop: 8 }]}>
                {
                    (Platform.OS == 'ios') ?
                        <TouchableWithoutFeedback onPress={() => { this.setState({ pickerType: 'country', showPicker: true }) }}>
                            <TextInput
                                editable={false}
                                style={styles.textInputContainer}
                                placeholder=''
                                value={this.state.issueType['name']}
                                underlineColorAndroid='white'
                                onTouchStart={() => {this.setState({ pickerType: 'country', showPicker: true })}}/>
                        </TouchableWithoutFeedback> :
                        <Picker
                            style={{ height: 44, flex: 1, marginRight: 8 }}
                            selectedValue={this.state.selecteIssueType.name}
                            prompt='Choose issue type'
                            mode='dialog'
                            onValueChange={(value, position) => {this.setState({ selecteIssueType: issueTypes[position] })}} >
                            {
                                issueTypes.map(item => {
                                    return <Picker.Item key={item.name} label={item.name} value={item.name} />
                                })
                            }
                        </Picker>
                }
            </View>
        )
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ mainImageUrl: nextProps.mainImageUrl })
    }

    // Lifecycle
    render() {
        return (
            <BackgroundView>
                <ScrollViewContainer>
                    <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
                        <ScrollViewContentView>
                            <FormContainer>
                                <RequestHeadersTextView>{strings.label__request_ticket_form_id}</RequestHeadersTextView>
                                {this._renderIssuteTypeView()}
                                <RequestHeadersTextView>{strings.label_request_subject}</RequestHeadersTextView>
                                <FormTextInputField
                                    maxLength={40}
                                    value={this.state.summary}
                                    autoCorrect={false}
                                    placeholder=''
                                    onChangeText={(text) => { this.setState({ summary: text }) }}
                                    underlineColorAndroid='white' />
                                <RequestHeadersHintTextView>{strings.label_hint_request_subject_hint}</RequestHeadersHintTextView>


                                <RequestHeadersTextView>{strings.label_request_description}</RequestHeadersTextView>
                                <FormTextInputFieldList
                                    multiline={true}
                                    value={this.state.description}
                                    autoCorrect={false}
                                    placeholder=''
                                    onChangeText={(text) => {
                                        this.setState({ description: text })
                                    }}
                                    underlineColorAndroid='white' />
                                <RequestHeadersHintTextView>{strings.label_hint_request_description_hint}</RequestHeadersHintTextView>


                                <ActionButtonContainer>
                                    <ActionButton
                                        title='Submit'
                                        backgroundColor={Colors.GREEN_COLOR}
                                        onPress={() => this._onSubmit(this.props)}
                                    />
                                </ActionButtonContainer>
                            </FormContainer>
                            <FooterView />
                        </ScrollViewContentView>
                        {this._showDatePicker()}
                    </KeyboardAvoidingView>
                </ScrollViewContainer>
            </BackgroundView >
        )
    }
}

// Styles
var styles = StyleSheet.create({
    textInputContainer: {
        height: 44,
        backgroundColor: 'white',
        borderRadius: 4,
        paddingLeft: 8,
    },
})