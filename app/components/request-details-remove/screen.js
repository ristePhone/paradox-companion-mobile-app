import React, { Component } from 'react'
import {
    StyleSheet,
    View,
    Text,
    Dimensions,
    TextInput,
    TouchableWithoutFeedback,
    Platform,
    Picker,
    KeyboardAvoidingView,
    //Keyboard,
    //Alert,
    TouchableHighlight,
    FlatList
} from 'react-native'
import Routes from '../../config/routes';
var { height, width } = Dimensions.get('window')
import RequestService from '../../services/request-service'
import Colors from '../../config/colors'
//import countries from '../../utils/countries.json'
import issueTypes from '../../utils/issueTypes.json'
import Utils from '../../services/utils-service'
import ActionButton from '../views/action-button'
//import LogoImageView from '../common/main-image-view/logo-image-view'
//import PickerView from '../views/native-picker'
import BackgroundView from '../views/background-view'
//import { validateEmail } from '../../services/authentication'
const FONT_COLOR = '#EEF0F2'
import {
    ScrollViewContainer,
    ScrollViewContentView,
    FormContainer,
    FormTextInputField,
    ActionButtonContainer,
    //CheckBoxContainer,
    FooterView,
   
} from './styles'
//import CheckBoxView from '../common/checkbox-view'
import { strings } from '../../config/localization'

export default class RequestDetailsScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            id: '',
            comments: [],
            comment: ''
        }
    }
    componentWillMount() {
        this.setState({ id: this.props.url })
        this._showDetails(this.props.url)
        this.props.navigator.toggleTabs({
            to: 'hidden',
            animated: false
            });
    }
     async _showDetails(id) {
        var url = 'https://data2278.zendesk.com/api/v2/requests/' + id + '/comments.json';
        var headers = {
            'Authorization': 'Basic dG9taS5yaXN0b3Zza2kubWtAZ21haWwuY29tOmFzZGY4ODQ0QA',
        }
        let result = await RequestService.get(url,headers)
        this.setState({ comments: result.comments })
    }

    closeRequest = () => {
        this._closeRequest()
    }

    async _closeRequest  (id) {
        var url = 'https://data2278.zendesk.com/api/v2/requests/' + this.state.id + '.json';
        var headers = {
            'Authorization': 'Basic dG9taS5yaXN0b3Zza2kubWtAZ21haWwuY29tOmFzZGY4ODQ0QA',
            'Content-type': 'application/json'
        }
        var body = 
             JSON.stringify({
                request: { 
                    comment: { body: "Thank you. It's solved now..."} ,
                    solved: true
                }
            })  
        let result = await RequestService.put(url,headers, body)
       // alert("ticket closed")
        this.props.navigator.push({
            screen: Routes.REQUESTS_LIST,
            title: 'Open requests',
            navigatorStyle: {
                navBarHidden: false,
            }
        })
        //this.setState({ comments: result.comments })    

    }

    async createNewComment  () {

        var id = this.state.id
        var url = 'https://data2278.zendesk.com/api/v2/requests/' + id + '.json';

        if (this.state.comment.trim().length == 0) {
            alert("Please enter comment")
            return
        }

        var headers = {
            'Authorization': 'Basic dG9taS5yaXN0b3Zza2kubWtAZ21haWwuY29tOmFzZGY4ODQ0QA',
            'Content-type': 'application/json'
        }
        var body = JSON.stringify({
                    request: { comment: { body: this.state.comment } }
                })
        let result = await RequestService.put(url,headers, body)
        this._showDetails(this.state.id)
        this.setState({ comment: '' })
        alert("Comment updated")

    }

    _renderItem = (item) => {
        return (
            <TouchableHighlight >
                <View style={{ padding: 10, paddingHorizontal: 10, backgroundColor: 'white', marginBottom: 10 }}>
                    <Text>{strings.reqeust_details_label_header_comment} {item.item.body} </Text>
                    <Text>{strings.reqeust_details_label_header_date} {Utils.formatDateString(item.item.created_at)} - {Utils.formatHour(item.item.created_at)} h</Text>
                </View>
            </TouchableHighlight>
        )}

    componentWillReceiveProps(nextProps) {
        this.setState({ mainImageUrl: nextProps.mainImageUrl })
    }
 
    // Lifecycle
    render() {
        return (
            <BackgroundView>
                <ScrollViewContainer>
  
                        <ScrollViewContentView>
                        <KeyboardAvoidingView style={{ flex: 1 }} 
                         behavior="position"
                    >
                            <FormContainer>
                                <FlatList
                                    data={this.state.comments}
                                    keyExtractor={this._keyExtractor}
                                    renderItem={this._renderItem} />
                                <FormTextInputField
                                    value={this.state.comment}
                                    autoCorrect={false}
                                   // onFocus={()=>this.setView()}
                                    placeholder={strings.request_details_label_comment}
                                    onChangeText={(text) => { this.setState({ comment: text }) }}
                                    underlineColorAndroid='white' />
                                <ActionButtonContainer>
                                <ActionButton
                                        title={strings.request_details_button_send}
                                        backgroundColor={Colors.GREEN_COLOR}
                                        onPress={() => this.createNewComment()}/>
                                <ActionButton
                                        title={strings.request_details_button_close}
                                        backgroundColor={Colors.GREEN_COLOR}
                                        onPress={() => this.closeRequest()}/>
                                </ActionButtonContainer>
                            </FormContainer>
                            <FooterView />
                            </KeyboardAvoidingView>

                        </ScrollViewContentView>
                </ScrollViewContainer>
            </BackgroundView >
        )
    }
}

// Styles
// var styles = StyleSheet.create({
//     textInputContainer: {
//         height: 44,
//         backgroundColor: 'white',
//         borderRadius: 4,
//         paddingLeft: 8,
//     },
// })