import { connect } from 'react-redux'
//import CreateRequestScreen from '../create-request/screen'
import RequestDetailsScreen from '../request-details-remove/screen'
import * as Constants from '../../utils/constants'
import { signUp, createSession, validateEmail } from '../../services/authentication'
import Routes from '../../config/routes'
import { store } from '../../reducers/index'
import { UPDATE_ROOT_CONTROLER, UPDATE_USER_LOGGED_IN_STATUS } from '../../actions/action-types'

const mapStateToProps = state => ({
    mainImageUrl: state.contentfulAPIcontent.mainImageUrl
})

const mapDispatchToProps = dispatch => ({
    
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RequestDetailsScreen)