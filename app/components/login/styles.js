import glamorous from 'glamorous-native'
import { Dimensions ,PixelRatio} from 'react-native'
let { height, width } = Dimensions.get('window')
import Colors from '../../config/colors'
const FONT_COLOR = '#EEF0F2'
import { normalize } from 'react-native-elements'
//alert(height)
let FONT_TEXT, TETX_HEIGHT, FONT_TITLE
height < 680 ? FONT_TEXT = normalize(10) : FONT_TEXT= normalize(14)
height < 680 ? FONT_TITLE = normalize(14) : FONT_TITLE = normalize(16)
height < 680 ? TETX_HEIGHT = normalize(30) : TETX_HEIGHT = normalize(50)

const Error_Color = '#fe0642'
export const ScrollViewContainer = glamorous.scrollView({
    flex: 1,
    paddingTop: TETX_HEIGHT,
    paddingHorizontal: 10,
    backgroundColor: Colors.BACKGROUND_COLOR,
})

export const ScrollViewContentView = glamorous.view({
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
})

export const FormContainer = glamorous.view({
    marginHorizontal: 10,
    paddingHorizontal: 16,
    paddingVertical: 20,
    width: width - 2 * 10,
    backgroundColor: Colors.DARK_GRAY_COLOR,
    borderRadius: 4
})

export const FormTitleContainer = glamorous.view({
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 20
})

export const FormTitleTextView = glamorous.text({
    fontFamily: 'SourceSansPro-Regular',
    color: 'white',
    textAlign: 'center',
    fontSize: FONT_TEXT
})

export const FormInnerContainer = glamorous.view({
    flex: 1,
    justifyContent: 'center'
})

export const FormTextInputField = glamorous.textInput(props => ({
    height: 40,
    backgroundColor: 'white',
    borderRadius: 2,
    paddingLeft: 16,
    marginTop: props.marginTop ? props.marginTop : 10,
}))

export const FormBottomContainer = glamorous.view({
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 10,
    marginTop: 20
})

export const ForgotPasswordTextView = glamorous.text({
    flex: 1,
    fontFamily: 'SourceSansPro-Regular',
    fontSize: FONT_TEXT,
    color: FONT_COLOR,
})

export const BottomContainerView = glamorous.view({
    marginTop: 20,
    alignItems: 'center',
    paddingHorizontal: 20
})

export const BottomContainerTitleView = glamorous.text({
    fontFamily: 'SourceSansPro-Regular',
    fontSize: FONT_TITLE,
    lineHeight: 20,
    color: FONT_COLOR,
    textAlign: 'center',
    marginBottom: 20
})

export const BottomContainerDescriptionView = glamorous.text({
    fontSize: FONT_TEXT,
    lineHeight: 18,
    textAlign: 'center',
})

export const SkipfornowContainer = glamorous.view({
    justifyContent: 'center',
    alignItems: 'center',
    width: 120,
    height: 60
})

export const SkipForNowTextView = glamorous.text({
    fontFamily: 'SourceSansPro-Regular',
    color: 'rgba(238, 240, 242, 0.8)',
    textAlign: 'center',
})

export const FooterView = glamorous.view({
    width: 1,
    height: 100
})

export const ErrorContainerView = glamorous.view({
    marginTop:10,
    backgroundColor: Error_Color,
    height:75, 
    borderRadius:2,
    padding:2
})

export const ErrorView = glamorous.view(props => ({
    backgroundColor: props.error ? props.color : '',
    height:25, 
    alignItems: 'center',
    justifyContent: 'center',
}))

export const ErrorText = glamorous.text({
    flex: 1,
    fontFamily: 'Arial',
    color: 'white',
    fontSize: 13,
    marginTop:5,
    color:'white'
})