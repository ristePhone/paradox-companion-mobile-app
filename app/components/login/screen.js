import React, { Component } from 'react'
import {
    AsyncStorage,
    KeyboardAvoidingView,
    Alert,
    TouchableWithoutFeedback
} from 'react-native'

import Colors from '../../config/colors'
import ActionButton from '../views/action-button'
import LogoImageView from '../common/main-image-view/logo-image-view'
import SpinnerView from '../common/spinner/ui'

import {
    ScrollViewContainer,
    ScrollViewContentView,
    FormContainer,
    FormTitleContainer,
    FormTitleTextView,
    FormInnerContainer,
    FormTextInputField,
    FormBottomContainer,
    ForgotPasswordTextView,
    BottomContainerView,
    BottomContainerTitleView,
    BottomContainerDescriptionView,
    SkipForNowTextView,
    SkipfornowContainer,
    FooterView,
    ErrorContainerView,
    ErrorView,
    ErrorText
} from './styles'

import { strings } from '../../config/localization'
import { validateEmail, login } from '../../services/authentication'

export default class LoginScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            mainImageUrl: this.props.mainImageUrl,
            spinnerVisible: false,
            loginButtonColor: Colors.WHITE_GRAY,
            loginButtonValid: false,
            emailFieldError: false,
            emailFieldErrorMessage:'',
            passwordFieldError: false, 
            passwordFieldErrorMessage: ''
        }
    }

    _showError = errorMessage => {
        this.setState({ spinnerVisible: false })
        setTimeout(() => {
            Alert.alert(
                strings.login.failed,
                errorMessage,
                [
                    { text: 'OK' },
                ]
            )
        }, 100);
    }

    _showInlineErrors = response => {
        this.setState({ spinnerVisible: false })
        if(response.errorCode=="not-authorized")
        {
            this.setState({ emailFieldError:true, emailFieldErrorMessage: strings.login.error.email_incorrect,
                            passwordFieldError:true, passwordFieldErrorMessage: strings.login.error.pass_incorrect})
            return
        }
        this._showError(response.errorMessage)
    }

    _resetInlineErrors()
    {
        this.setState({emailFieldError: false, passwordFieldError: false})
    }

    _validateForm = (email, pass) => {
        let minPassLength = 6
        const error = validateEmail(email)
        if (!error) {
            if (pass == '' || pass.length < minPassLength) {
                return strings.login.error.pass_not_long
            }
        }

        return error
    }
    _storeUsername = async (email) => {
        try {
            await AsyncStorage.setItem('username', email);
        } catch (error) {
            // Error saving data
        }
    }

    liveValidation(email, pass) {
        // validate password and email fields, while user is typing (condtions: email format and min 6 character for password)
        this.setState({ email: email, password: pass })
        const error = this._validateForm(email, pass)
        if (!error) {
            this.setState({ loginButtonColor: Colors.GREEN_COLOR, loginButtonValid: true })
        }
        else {
            this.setState({ loginButtonColor: Colors.WHITE_GRAY, loginButtonValid: false })
        }
    }
    _onLogin = async () => {
        this.setState({ spinnerVisible: true })
        const { email, password } = this.state
        //const error = this._validateForm()
        const error = this._validateForm(email, password)
        if (!error) {
            const response = await login(email, password)
            const success = String(response.result) == "OK"
            if (success) {

                this.setState({ spinnerVisible: false })
                this._resetInlineErrors()
                const session = JSON.stringify(response.session)
                AsyncStorage.setItem('session', session)
                this._storeUsername(email)

                setTimeout(() => {
                    this.props.onLoginSuccess()
                }, 200);
                return
            }

           // this._showError(response.errorMessage)
           this._showInlineErrors(response)
           return
        }

        this._showError(error)
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ mainImageUrl: nextProps.mainImageUrl })
    }
    _renderEmailInput(){ return(
        <FormTextInputField
            marginTop={1}
            keyboardType='email-address'
            placeholder={strings.email_address_placeholder}
            onChangeText={(text) => {
                //this.setState({ email: text })
                this.liveValidation(text, this.state.password)
            }}
            value={this.state.email}
            underlineColorAndroid='white'
            autoCorrect={false}
            autoCapitalize='none'
                                />
    )}

    _renderPasswordInput(marginTop){return (
        <FormTextInputField
        marginTop={marginTop}
        placeholder={strings.password}
        secureTextEntry={true}
        underlineColorAndroid='white'
        onChangeText={(text) => {
            //this.setState({ password: text })
            this.liveValidation(this.state.email, text)
        }}

        value={this.state.password}
    />
    )}
    _renderInputError(view, error){ return(
        <ErrorContainerView> 
            {view}
            <ErrorView>
                <ErrorText>{error}</ErrorText> 
            </ErrorView>
        </ErrorContainerView> 
    )}

    render() {
        return (
            <ScrollViewContainer keyboardShouldPersistTaps='handled'
                showsVerticalScrollIndicator={false} >
                <SpinnerView visible={this.state.spinnerVisible} />
                <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
                    <ScrollViewContentView>
                        <LogoImageView url={this.state.mainImageUrl} />
                        <FormContainer>
                            <FormTitleContainer>
                                <FormTitleTextView numberOfLines={2}>{strings.login_to_paradox.toUpperCase()}</FormTitleTextView>
                            </FormTitleContainer>
                            <FormInnerContainer>


                            {this.state.emailFieldError ? this._renderInputError(this._renderEmailInput(), this.state.emailFieldErrorMessage) : this._renderEmailInput()}

                                {/* <FormTextInputField
                                    marginTop={0}
                                    keyboardType='email-address'
                                    placeholder={strings.email_address_placeholder}
                                    onChangeText={(text) => {
                                        //this.setState({ email: text })
                                        this.liveValidation(text, this.state.password)
                                    }}
                                    value={this.state.email}
                                    underlineColorAndroid='white'
                                    autoCorrect={false}
                                    autoCapitalize='none'
                                /> */}


                            {this.state.passwordFieldError? this._renderInputError(this._renderPasswordInput(1), this.state.passwordFieldErrorMessage): this._renderPasswordInput(0)}

                                {/* <FormTextInputField
                                    placeholder={strings.password}
                                    secureTextEntry={true}
                                    underlineColorAndroid='white'
                                    onChangeText={(text) => {
                                        //this.setState({ password: text })
                                        this.liveValidation(this.state.email, text)
                                    }}

                                    value={this.state.password}
                                /> */}
                                <FormBottomContainer>
                                    <ForgotPasswordTextView
                                        numberOfLines={2}
                                        suppressHighlighting={true}
                                        onPress={() => this.props.onForgotPassword(this.props.forgotPasswordUrl)}
                                    >
                                        {strings.forgot_password}
                                    </ForgotPasswordTextView>
                                    <ActionButton
                                        disabled={!this.state.loginButtonValid}
                                        title={strings.login_button_login}
                                        backgroundColor={this.state.loginButtonColor}
                                        onPress={this._onLogin}
                                        underlineColorAndroid='white' />
                                </FormBottomContainer>
                            </FormInnerContainer>
                        </FormContainer>
                        <BottomContainerView>
                            <BottomContainerTitleView>{strings.dont_have_account}{`\n`}
                                <BottomContainerDescriptionView>{strings.sign_up_description}</BottomContainerDescriptionView>
                            </BottomContainerTitleView>
                            <ActionButton
                                title={strings.login_button_create_account}
                                backgroundColor={Colors.GREEN_COLOR}
                                onPress={() => this.props.onSignUp(this.props)} />
                        </BottomContainerView>
                        <TouchableWithoutFeedback onPress={() => this.props.onSkip(this.props)}>
                            <SkipfornowContainer>
                                <SkipForNowTextView>{strings.skip_for_now}</SkipForNowTextView>
                            </SkipfornowContainer>
                        </TouchableWithoutFeedback>
                        <FooterView />
                    </ScrollViewContentView>
                </KeyboardAvoidingView>
            </ScrollViewContainer>
        )
    }
}