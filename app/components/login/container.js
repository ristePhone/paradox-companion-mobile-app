import { connect } from 'react-redux'
import { Linking } from 'react-native'
import LoginScreen from '../login/screen'
import Routes from '../../config/routes';
import { UPDATE_ROOT_CONTROLER, UPDATE_USER_LOGGED_IN_STATUS } from '../../actions/action-types'

// Redux
const mapStateToProps = state => ({
    mainImageUrl: state.contentfulAPIcontent.mainImageUrl,
    forgotPasswordUrl: state.config.FORGOT_PASSWORD_URL
})

const mapDispatchToProps = dispatch => ({
    onLoginSuccess: async (email, password) => {
        dispatch({ type: UPDATE_USER_LOGGED_IN_STATUS, isLoggedIn: true })
        dispatch({ type: UPDATE_ROOT_CONTROLER, rootTypeController: 'tabBasedApp' })
    },
    onSignUp: props => {
        props.navigator.push({
            screen: Routes.SIGN_UP,
            navigatorStyle: {
                navBarHidden: true,
            }
        })
    },
    onSkip: () => {
        dispatch({ type: UPDATE_USER_LOGGED_IN_STATUS, isLoggedIn: false })
        dispatch({ type: UPDATE_ROOT_CONTROLER, rootTypeController: 'tabBasedApp', })
    },
    onForgotPassword: forgotPasswordUrl => {
        Linking.openURL(forgotPasswordUrl)
    }
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LoginScreen)