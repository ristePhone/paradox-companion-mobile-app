import React, { Component } from 'react'
import {
    StyleSheet,
    View,
    Text,
    Dimensions,
    TextInput,
    Image,
    ScrollView,
    TouchableOpacity,
    TouchableWithoutFeedback,
    TouchableNativeFeedback,
    Platform,
    Picker,
    KeyboardAvoidingView,
    Keyboard,
    Alert,
    TouchableHighlight,
    FlatList,
    ActivityIndicator
} from 'react-native'

var { height, width } = Dimensions.get('window')

import Colors from '../../config/colors'
import countries from '../../utils/countries.json'
import issueTypes from '../../utils/issueTypes.json'
import Utils from '../../services/utils-service'
import ActionButton from '../views/action-button'
import LogoImageView from '../common/main-image-view/logo-image-view'
import PickerView from '../views/native-picker'
import BackgroundView from '../views/background-view'
import { validateEmail } from '../../services/authentication'
import Configuration from '../../config/environment/config'
const FONT_COLOR = '#EEF0F2'

import {
    ScrollViewContainer,
    ScrollViewContentView,
    FormContainer,
    FormTitleContainer,
    FormTitleTextView,
    FormTextInputField,
    ActionButtonContainer,
    CheckBoxContainer,
    FooterView,
    BottomContainer,
    AlreadyHaveAcountTextView,
    RequestHeadersTextView,
    RequestHeadersHintTextView
} from './styles'
import RequestService from '../../services/request-service'

import CheckBoxView from '../common/checkbox-view'

import { strings } from '../../config/localization'

export default class RequestHistoryScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
           // email: '',
           // password: '',
           // dateOfBirth: '',
           // country: '',
           // issueType: '',
            //issueTypes: '',
            //joinParadoxCommunity: false,
           // acceptedTermsAndConditions: false,
            //acceptedPrivacyPolicy: false,
            //signUpButtonEnabled: true,
           // pickerType: 'issueType',
           // showPicker: false,
           // selectedCountry: countries[0],
           // selecteIssueType: issueTypes[0],
            //mainImageUrl: this.props.mainImageUrl,
            requests: []
        }
        
    }

    componentWillMount() {
        this.props.navigator.toggleTabs({
            to: 'hidden',
            animated: false
            });
        //this.setState({id: this.props.url})
        if (this.props.notify == true) {
            alert("Request created")
        }
        this.showTickets()
    }

    async showTickets() {
       // var result = await this.getApiResult()        
        let url = Configuration.common.ZENDESK_URL
        let filter = Configuration.common.ZENDESK_URL_FILTER_OPEN_ASC
        let urlParam = url+filter
        //var result = await RequestService.requestListTest()    
        var headers = {
            'Authorization': 'Basic dG9taS5yaXN0b3Zza2kubWtAZ21haWwuY29tOmFzZGY4ODQ0QA',
            'Content-Type': 'application/json'
        }
        let result = await RequestService.get(urlParam,headers)
        this.setState({ requests: result.requests })
        // var url = 'https://data2278.zendesk.com/api/v2/requests.json';



        // return fetch(url, {
        //     method: 'GET',
        //     headers: {
        //         'Authorization': 'Basic dG9taS5yaXN0b3Zza2kubWtAZ21haWwuY29tOmFzZGY4ODQ0QA'
        //         // 'Content-type': 'application/json'
        //     }
        // }).then(response => {
        //     return response.json()
        // }).then(response => { 
        //     this.setState({ requests: response.requests })
        //     return response
        // })

    }

    // getApiResult(){
    //     var url = 'https://data2278.zendesk.com/api/v2/requests.json?sort_by=created_at';

    //     return RequestService.get(url, null)
    // }
  
    componentWillReceiveProps(nextProps) {
        this.setState({ mainImageUrl: nextProps.mainImageUrl })
    }

    _renderItem = (item) => {
        return (
            <TouchableHighlight onPress={() => this.props._onPressButton(this.props, item.item.id)}>
                <View style={{ padding: 10, paddingHorizontal: 10, backgroundColor: 'white', marginBottom: 10 }}>
                    <Text>Subject: {item.item.subject} </Text>
                    <Text>Date:  {Utils.formatDateString(item.item.created_at)} - {Utils.formatHour(item.item.created_at)} h </Text>
                </View>
            </TouchableHighlight>
        )
    }

    // Lifecycle
    render() {
        return (
            <BackgroundView>
            <ScrollViewContainer>
            {/* <View style={[styles.container, styles.horizontal]}>
              <ActivityIndicator size="large" color="#0000ff" />
            </View> */}
                <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
                    {/* <ScrollViewContainer> */}
                    {/* <ScrollViewContentView> */}
                    {/* <LogoImageView url={this.state.mainImageUrl} /> */}
                    <FormContainer>
                        <FlatList
                            data={this.state.requests}
                            keyExtractor={this._keyExtractor}
                            renderItem={this._renderItem} />
                    </FormContainer>
                    <FooterView />
                    {/* </ScrollViewContentView> */}
                    {/* </ScrollViewContainer> */}
                </KeyboardAvoidingView>
                </ScrollViewContainer>
            </BackgroundView >
        )
    }
}

// Styles
var styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
      },
      horizontal: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: 10
      },
    textInputContainer: {
        height: 44,
        backgroundColor: 'white',
        borderRadius: 4,
        paddingLeft: 8,
    },
})