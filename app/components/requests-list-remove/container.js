import { connect } from 'react-redux'
//import CreateRequestScreen from '../create-request/screen'
import RequestHistoryScreen from '../requests-list-remove/screen'
import * as Constants from '../../utils/constants'
import { signUp, createSession, validateEmail } from '../../services/authentication'
import Routes from '../../config/routes'
import { store } from '../../reducers/index'
import { UPDATE_ROOT_CONTROLER, UPDATE_USER_LOGGED_IN_STATUS } from '../../actions/action-types'

const mapStateToProps = state => ({
    mainImageUrl: state.contentfulAPIcontent.mainImageUrl
})

const mapDispatchToProps = dispatch => ({
    onSignUp: parameters => {
        signUp(parameters).then(response => {
            if (String(response.result).toLowerCase() == 'ok') {
                createSession(parameters.email, parameters.password).then(result => {
                    if (String(response.result).toLowerCase() == 'ok') {

                        AsyncStorage.setItem('credentials', JSON.stringify({ email, password }))
                        
                        dispatch({ type: UPDATE_USER_LOGGED_IN_STATUS, isLoggedIn: true })
                        dispatch({ type: UPDATE_ROOT_CONTROLER, rootTypeController: 'tabBasedApp' })
                    }
                })
            } else {
                alert(response.errorMessage)
            }
        })
    },
    onTermsAndConditionButtonPress: props => {
        props.navigator.push({
            screen: Routes.WEB,
            passProps: {
                url: store.getState().config.TERMS_AND_CONDITIONS_URL
            }
        })
    },
    onPrivacyPolicyButtonPress: props => {
        props.navigator.push({
            screen: Routes.WEB,
            passProps: {
                url: store.getState().config.PRIVACY_POLICY_URL
            }
        })
    },
    onLogin: props => {
        props.navigator.pop()
    },
    _onPressButton: (props, id) =>{
        props.navigator.push({
            screen: Routes.REQUEST_DETAILS,
            title:'Request details',
            passProps: {
                url: id
            }
        })
    }
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RequestHistoryScreen)