import glamorous from 'glamorous-native'
import { Dimensions } from 'react-native'
let { height, width } = Dimensions.get('window')

const FONT_COLOR = '#EEF0F2'

export const ScrollViewContainer = glamorous.scrollView({
    flex: 1,
    paddingTop: 25,
   // paddingHorizontal: 16,
})

export const ScrollViewContentView = glamorous.view({
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
})

export const FormContainer = glamorous.view({
    marginHorizontal: 16,
    paddingHorizontal: 16,
    paddingVertical: 30,
    width: width - 2 * 16,
   // height: 540,
    backgroundColor: 'rgba(32, 41, 48, 0.8)',
    borderWidth: 1,
    borderColor: 'rgba(238, 240, 242, 0.1)',
    borderRadius: 8
})

export const FormTitleContainer = glamorous.view({
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 20
})

export const FormTitleTextView = glamorous.text({
    color: FONT_COLOR,
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'OpenSans',
    lineHeight: 20
})

export const FormInnerContainer = glamorous.view({
    flex: 1,
    justifyContent: 'center'
})

export const FormTextInputField = glamorous.textInput(
    {
        height: 44,
        backgroundColor: 'white',
        borderRadius: 4,
        paddingLeft: 8,
        marginTop: 8
    }
)

export const CheckBoxContainer = glamorous.view({
    marginVertical: 10,
    flexDirection: 'row',
    alignItems: 'center'
})

export const ActionButtonContainer = glamorous.view({
    justifyContent: 'center',
    marginTop: 25,
    flexDirection: 'row'
})

export const FooterView = glamorous.view({
    width: 1,
    height: 100
})

export const BottomContainer = glamorous.view(
    {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 16,
        marginTop: 20
    },
    props => ({
        width: width ? width - 2 * 16 : width
    })
)

export const AlreadyHaveAcountTextView = glamorous.text({
    color: 'white',
    fontWeight: 'bold',
    fontSize: 15
})

export const RequestHeadersTextView = glamorous.text({
    color: 'white',
    fontWeight: 'bold',
    fontSize: 13
})

export const RequestHeadersHintTextView = glamorous.text({
    color: 'white',
    fontSize: 10
})