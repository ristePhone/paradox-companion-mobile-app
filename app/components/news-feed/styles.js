import glamorous from 'glamorous-native'

export const NewsFeedFlatList = glamorous.flatList({
    flex: 1,
    paddingHorizontal: 16,
})

export const NewsFeedFlatListFooterView = glamorous.view({
    width: 1
})

export const NewsFeedFlatListHeaderView = glamorous.view({
    width: 1,
    height: 10
})

export const NewsFeedFlatListSeparatorView = glamorous.view({
    width: 1,
    height: 10
})