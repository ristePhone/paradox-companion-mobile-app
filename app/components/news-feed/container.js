import { connect } from 'react-redux'
import NewsFeedScreen from './screen'
import Routes from '../../config/routes'
import { UPDATE_SELECTED_NEWS_FEED_ITEM } from '../../actions/action-types'
import { strings } from '../../config/localization'

import NavigationActions from '../../actions/navigation-actions/index'

const mapStateToProps = state => ({
    news: state.contentfulAPIcontent.news
})

const mapDispatchToProps = dispatch => ({
    onItemPress: (props, item) => {
        dispatch({ type: UPDATE_SELECTED_NEWS_FEED_ITEM, item })
        props.navigator.push({
            screen: Routes.NEWS_DETAILS,
            title: strings.navigation_titles.newsDetails,
            navigatorStyle: {
                navBarHidden: false,
            }
        })
    },
    onNewsFeedRetryShow: () => {
        NavigationActions.showLightBox({
            screen: Routes.RETRY,
            passProps: {
                isRetrying: false,
            }
        })
    },
    onNewsFeedRetryHide: () => {
        NavigationActions.hideLightBox()
    }
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NewsFeedScreen)