import React, { Component } from 'react'
import { FlatList, Share } from 'react-native'
import Colors from '../../config/colors'

import {
    NewsFeedFlatListHeaderView,
    NewsFeedFlatListFooterView,
    NewsFeedFlatListSeparatorView
} from './styles'

import NewsFeedItem from '../common/news-feed-item/ui'

export default class NewsFeedScreen extends Component {
    constructor(props) {
        super(props)
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    }

    onNavigatorEvent(event) {
        if (event.id == 'didDisappear') {
            this.resetScrollToTop()
        }
    }

    resetScrollToTop = () => {
        if (this.flatList) { this.flatList.scrollToOffset({ x: 0, y: 0, animated: false }) }
    }

    _onShare = (type, item) => {
        const { headline, publishDate, bodyText } = item
        const content = {
            title: `${headline} - ${publishDate}`,
            message: `${bodyText}`
        }

        Share.share(content)
    }

    _renderRow = ({ item, index }) => {
        return <NewsFeedItem
            key={index}
            item={item}
            onItemPress={() => this.props.onItemPress(this.props, item)}
            onSharePress={type => this._onShare(type, item)}
        />
    }

    _renderFooterView = () => <NewsFeedFlatListFooterView />

    _renderHeaderView = () => <NewsFeedFlatListHeaderView />

    _renderSeparatorView = () => <NewsFeedFlatListSeparatorView />

    /**
     *  Lifecycle
     *  @memberof NewsFeedScreen
     */

    componentDidMount() {
        if (this.props.news) {
            this.props.onNewsFeedRetryHide()
        } else {
            this.props.onNewsFeedRetryShow()
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.news) {
            this.props.onNewsFeedRetryHide()
        } else {
            this.props.onNewsFeedRetryShow()
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        const nextNews = JSON.stringify(nextProps.news)
        const currentNews = JSON.stringify(this.props.news)
        return nextNews !== currentNews
    }

    render() {
        const news = this.props.news ? this.props.news : []
        return (
            <FlatList
                ref={component => this.flatList = component}
                style={{ flex: 1, paddingHorizontal: 10, backgroundColor: Colors.BACKGROUND_COLOR }}
                data={news}
                indicatorStyle='white'
                renderItem={this._renderRow}
                keyExtractor={(item, index) => `${item}-${index}`}
                ItemSeparatorComponent={this._renderSeparatorView}
                ListFooterComponent={this._renderFooterView}
                ListHeaderComponent={this._renderHeaderView}
                initialNumToRender={5} />
        )
    }
}

