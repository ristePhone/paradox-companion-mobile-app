import React, { Component } from 'react'
import { StyleSheet, View, Text, TextInput, TouchableWithoutFeedback, 
    Platform, Picker, KeyboardAvoidingView, Keyboard, ScrollView, Dimensions, Alert } from 'react-native'

import RequestService from '../../services/request-service'
import Configuration from '../../config/environment/config'
import Colors from '../../config/colors'
import countries from '../../utils/countries.json'
import issueTypes from '../../utils/issueTypes.json'
import Utils from '../../services/utils-service'
import ActionButton from '../views/action-button'

import PickerView from '../views/native-picker'
import BackgroundView from '../views/background-view'

import ZendeskService from '../../services/zendesk-service'
import SpinnerView from '../common/spinner/ui'
import { store } from '../../reducers/index'
import {
    ScrollViewContentView, FormContainer, FormTextInputField, FormTextInputFieldList, ActionButtonContainer, FooterView,
    RequestHeadersTextView, RequestHeaderTextView, RequestHeadersHintTextView, FormTextInputField1
} from './styles'

import { strings } from '../../config/localization'
const  middleware_zendesk  = () => {
    const name = store.getState().config.name
    return Configuration.environment[name].MIDDLEWARE_BASE_URL
}

export default class CreateRequestScreen extends Component {
    constructor(props) {
        super(props)
        let { height, width } = Dimensions.get('window')

        this.state = {
            description: '',
            summary: '',
            height: height,
            country: '',
            issueType: issueTypes[0],
            issueTypes: '',
            pickerType: 'issueType',
            showPicker: false,
            selectedCountry: countries[0],
            selecteIssueType: issueTypes[0],
            mainImageUrl: this.props.mainImageUrl,
            loading: false,
            spinnerVisible:false,
            ticketFiels:[],
            loaded:false, 
            test:[],
           //someValue: issueTypes[0],
            someValue: {
                22:'name on some value'
            },
            pickerSource:null,
            customFieldsValues:[],
            customFieldsDetails:[], 
            types:[],
            ticket_form_id:0, 
            selection:[
                {id:1, body:'test1'},
                {id:2, body: 'test2'}
            ],
            selectedDropDown:'',
            pickerSource:[],
            mainPickerOption:[],
            someValue1:'some val1'

        }
    }
    apiResultToModel(result){
        var array = []
        var forms = result.ticket_forms


        var temp = {
            name: '-',
            code: '-',
            id: 0,
            ticket_field_ids: '-'
            
        }
        array.push(temp)

        forms.forEach(function(element) {

            if(element.active)
            {
                var temp = {
                    name: element.name,
                    code: 'test',
                    id: element.id,
                    ticket_field_ids: element.ticket_field_ids
                    
                }
                array.push(temp)
            }

            
           
           
          });
        this.setState({issueTypes: array})
        this.setState({mainPickerOption: array})
        this.setState({pickerSource: array})
        
    }

    async localApiHandler(type, url, body){
        this._startLoading()
        var result = await RequestService.api(type, url, await ZendeskService.setJsonHeaders(), body)
        this._endLoading()
        return result
    }
    async _apiCallGetAllTicketForms(){
        var url = Configuration.common.ZENDESK_TICKET_FORMS()
        var result = await this.localApiHandler('GET', url)
        console.log("result from all ticket forms is")
        console.log(result)
        this.apiResultToModel(result)
        
    }

    _clearState() {
        this.setState({ summary: '', description: '' })
    }

    componentDidMount () {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
      }
    
      componentWillUnmount (props) {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
      }
    
      _keyboardDidShow () {
        this._hideNavBar()
      }
    
      _keyboardDidHide () {
        this._showNavBar()
      }

    _showNavBar()
    {
        this.props.navigator.toggleTabs({
            to: 'shown',
            animated: false
        });
    }

    _hideNavBar()
    {
        this.props.navigator.toggleTabs({
            to: 'hidden',
            animated: false
        });
    }
    // ---------------------------------------------------------------------------------------
    // validation methods start
    // ---------------------------------------------------------------------------------------
    _checkValidation1() {
        let customFieldsDetails =  this.state.customFieldsDetails
        for(var i=0;i<customFieldsDetails.length;i++)
        {
            if(customFieldsDetails[i].required == true)
            {
                if(customFieldsDetails[i].value =="")
                {
                    Alert.alert("Please fill all required fields")
                    return false
                }
            }

        }
        return true
    }

    // ---------------------------------------------------------------------------------------
    // validation methods end
    // ---------------------------------------------------------------------------------------

    // ---------------------------------------------------------------------------------------
    // helpers start
    // ---------------------------------------------------------------------------------------
    _startLoading() {
        this.setState({ loading: true, spinnerVisible: true})
    }

    _endLoading() {
        this.setState({ loading: false, spinnerVisible: false})
    }

    _getValueById(tempCustomFields, id){
        for(var i=0;i<tempCustomFields.length;i++)
        {
            if(tempCustomFields[i].id==id)
            {

                return tempCustomFields[i].value
            }
        }

    }
    // ---------------------------------------------------------------------------------------
    // helpers end
    // ---------------------------------------------------------------------------------------

    async _apiCallSubmitRequest(props, newobject) {
        this._startLoading()
        Keyboard.dismiss()
        var url = Configuration.common.ZENDESK_REQUEST_CREATE_URL()
        var headers = await ZendeskService.setJsonHeaders()
        var body = JSON.stringify({
            request: {
                requester: { name: 'user name' },
                subject: this.state.summary,
                comment: { body: this.state.description },
                 custom_fields:[{
                     id:456811,
                     value:'agod'}]
            }
        })

        let custom_fields = objectparametar=[]
        for(var i =0; i<newobject.length; i++)
        {
            objectparametar.push(newobject[i])
        }
         var body1 = JSON.stringify({
            request: {
                requester: { name: 'user name' },
                //subject: this.state.summary,
                subject: this._getValueById(custom_fields,"408382") ,
                //comment: { body: this.state.description },
                comment: { body: this._getValueById(custom_fields,"408383") },
               // ticket_form_id: 116747,  // general issue
               // ticket_form_id: 117297,     // forum / account issues
                ticket_form_id: this.state.ticket_form_id,
                //custom_fields:[{456811:'test'}]
                custom_fields
            }
        })
        //408382 summary
        //408383 description

        var result = await RequestService.post1(url, headers, body1)
        // if(result.ok == false)
        // {
        //     this._endLoading()
        //     alert("Something went wrong, please try again later")
        //     return
        // }
        this._clearState()
        this._endLoading()
        this.props._navigateToRequestList(this.props)
    }

    componentWillMount() {
        //alert("will mount")
        this._apiCallGetAllTicketForms()
    }

    //componentWillMount() {
        // let types 
        // let types1 = []
        //  for(var i = 0; i< this.props.element.ticket_fields.length; i++)
        //  {
        //     var temp = {
        //         id:this.props.element.ticket_fields[i].id,
        //         value:''
        //     }
        //     var temp1 = {
        //         id:this.props.element.ticket_fields[i].id,
        //         value:'',
        //         required: this.props.element.ticket_fields[i].required
        //     }
        //     this.state.customFieldsDetails.push(temp1)
        //     this.state.customFieldsValues.push(temp)
        //      if(this.props.element.ticket_fields[i].type == "Drop-down")  
        //      {
        //         types = JSON.parse(this.props.element.ticket_fields[i].custom_field_options)
        //         //types = JSON.parse(this.props.element.ticket_fields[i].custom_field_options)
        //         //let someValue = []
        //         //types1[this.props.element.id] = types
        //         let id = this.props.element.ticket_fields[i].id
        //         let ojbectTemp = {
        //             id: id,
        //             item: types
        //         }
        //        // types1.assign(ojbectTemp)
        //         types1.push(ojbectTemp)
        //         //types1.push(someValue)
        //         //types1  = someValue
        //      }
        //  }
        // this.setState({
        //     ticketFields:this.props.element.ticket_fields, loaded: true, 
        //     pickerSource: types, 
        //     types: types1,
        //     ticket_form_id: this.props.element.id
        // })
   // }

    _updateCustomFieldsValue(id, value)
    {
        console.log("id")
        console.log(id)
        console.log("value")
        console.log(value)
        let tempCustomFields = this.state.customFieldsValues
        let tempCustomFieldsDetails = this.state.customFieldsDetails
        for(var i=0; i<tempCustomFields.length;i++)
        {
            
            if(tempCustomFields[i].id==id)
            {
                tempCustomFields[i].value = value
                this.setState({customFieldsValues: tempCustomFields})
               
                tempCustomFieldsDetails[i].value = value
                this.setState({customFieldsDetails: tempCustomFieldsDetails})

                let test = this._getValueById(tempCustomFields,408382) 
                console.log("tempCustomFieldsDetails")
                console.log(tempCustomFieldsDetails)
                console.log("tempCustomFields")
                console.log(tempCustomFields)
                return
            }
        }
    }
    _onSubmit(props) {
         if (!this._checkValidation1())
             return
         let response = this._apiCallSubmitRequest(props, this.state.customFieldsValues)
    }
    _getSource(id){
        console.log("get source is")
        console.log(id)
        console.log(this.state.types)
        for(var i=0; i< this.state.types.length; i++)
        {
            if(this.state.types[i].id == id)
            {
                return this.state.types[i].item
            }
        }
    }
    // _showDatePicker = () => {
    //     return (this.state.showPicker) ?
    //         <PickerView
    //             type={this.state.pickerType}
    //             dataSource={this.state.pickerSource}
    //             onDoneButtonPress={(selectedValue) => {
    //                 Keyboard.dismiss()

    //                this.setState({ issueType: selectedValue, showPicker: false })
    //                this.renderCustomFields(selectedValue)
                    
    //             }} /> : null
    // }

    _showDatePicker = () => {
        return (this.state.showPicker) ?
            <PickerView
                type={this.state.pickerType}
                dataSource={this.state.pickerSource}
                onDoneButtonPress={(selectedValue) => {
                    Keyboard.dismiss()
                    console.log("picker ios on done button press")
                    console.log(this.state.pickerType)

                    console.log("get val is  vallllllllllllllll from show date picker")
                    console.log(this.state.someValue)
                    console.log(this.state.someValue[22])

                    console.log(selectedValue.name)
                    if(this.state.pickerType=='country')
                    {
                        this.setState({ issueType: selectedValue, showPicker: false })
                        this.renderCustomFields(selectedValue)
                        return
                    }
                   
                    let newsomeValue = this.state.someValue
                    let idpicker = this.state.pickerType
                    newsomeValue[idpicker] = selectedValue.name
                    newsomeValue[22] = selectedValue.name
                    this._updateCustomFieldsValue(idpicker, selectedValue.value)
                    this.setState({ showPicker: false, someValue : newsomeValue })

                    this.setState({ someValue1: selectedValue.name })

                    
                   // .renderCustomFields(selectedValue)
                }} /> : null
    }

    _getVal(id)
    {
        console.log("get val is  vallllllllllllllll")
        console.log(this.state.someValue)
        return this.state.someValue[id]
    }

    _renderIssuteTypeView1(key, id) {
        let idstring = id.toString()
        return (
            
            <View key={key + 'ffj93'} style={[styles.textInputContainer, { marginTop: 8 }]}>
                {(Platform.OS == 'ios') ?
                        <TouchableWithoutFeedback onPress={() => { this.setState({ pickerType: id, showPicker: true }) }}>
                            <TextInput
                            key={key+'e'}
                                editable={false}
                                style={styles.textInputContainer}
                                placeholder=''
                                value={this.state.someValue[id]}
                               

                                //value={this.state.issueType['name']}
                                underlineColorAndroid='white'
                                onTouchStart={() => { 
                                    console.log("before data source is")
                                    let datasource = this._getSource(id)
                                    console.log("data source is")
                                    console.log(datasource)
                                    this.setState({ pickerType: id,  pickerSource: datasource }) 
                                    this.setState({showPicker: true})
                                    }} />
                        </TouchableWithoutFeedback> :
                        <Picker
                            style={{ height: 44, flex: 1, marginRight: 8 }}
                            selectedValue = {this.state.someValue[id]}
                            prompt='Please select'
                            mode='dialog'
                            onValueChange={(value, position) => { 
                                let option1 = this._getSource(id)
                                let newsomeValue = this.state.someValue
                                newsomeValue[id] = option1[position].name
                                this._updateCustomFieldsValue(id, option1[position].value)
                                this.setState({  someValue : newsomeValue })
                                return
                                    let option = this._getSource(id)
                                    this.setState({selection: value})
                                    this._updateCustomFieldsValue(id, this.state.pickerSource[position].value)
                                 }} >
                            {
                                this._getSource(id).map(item => {       
                                    return <Picker.Item key={item.name} label={item.name} value={item.name} />
                                })
                            }
                        </Picker>
                }
            </View>
        )
    }

    // _getOne(){
    //     for(var i=0; i<this.state.selection.length; i++)
    //     {
    //         if(this.state.selection.id==1)
    //         {
    //             return this.state.body
    //         }
    //     }
    // }
    
    componentWillReceiveProps(nextProps) {
        this.setState({ mainImageUrl: nextProps.mainImageUrl })
    }
    setSel(input) {
        let offset
        if (input == 'inputArea') {
            offlset = 210
            if (Platform.OS == 'ios') {
                setTimeout(() => {
                    this.refs._scrollView.scrollToEnd({ animated: true })
                }, 300)
            } else {
                if (this.state.height < 660) {
                    setTimeout(() => {
                        this.refs._scrollView.scrollTo({ x: 0, y: 215, animated: true })
                    }, 300)
                }else{
                    setTimeout(() => {
                        this.refs._scrollView.scrollTo({ x: 0, y: 175, animated: true })
                    }, 300)
                }
       
            }
           // this.setKybordOpenAnroid()
        } else {

           // this.setKybordOpenAnroid()
        }


    }

    _renderTest = (ticket_field_type, key, required, keyboardType) => {
        return (
            <View key = {key + "fewf"} >
                <RequestHeadersTextView>{ticket_field_type.title} <Text style={{color: 'red'}}>{required}</Text></RequestHeadersTextView>
                <FormTextInputField
                   // ref='tomi1'
                   key={key+'essw3'}
                    keyboardType={keyboardType}
                    maxLength={40}
                    defaultValue={this.state.emptyValue}
                    value={this.state.emptyValue}
                    autoCorrect={false}
                    onFocus={() => this.setSel('inputArea')}
                    onFocus={() => this.setSel('input')}
                    underlineColorAndroid='white'
                    onChangeText={(text) => { 
                        this._updateCustomFieldsValue(ticket_field_type.id, text)
                        console.log(text)
                    }} />
                     <RequestHeadersHintTextView>{ticket_field_type.footer}</RequestHeadersHintTextView>
                    {/* {this._renderDropDown()} */}
            </View> )}

     _renderTextBox = (ticket_field_type, key, required) => {
        return ( <View key = {key + 'f4398'} >
                <RequestHeadersTextView>{ticket_field_type.title} <Text style={{color: 'red'}}>{required}</Text></RequestHeadersTextView>
                <FormTextInputField1
                 key={key+'e12'}
                   // ref='tomi1'
                    multiline={true}
                  //  maxLength={40}
                    defaultValue={this.state.emptyValue}
                    value={this.state.emptyValue}
                    autoCorrect={false}
                    onFocus={() => this.setSel('inputArea')}
                    onFocus={() => this.setSel('input')}
                    underlineColorAndroid='white'
                    onChangeText={(text) => { 
                        this._updateCustomFieldsValue(ticket_field_type.id, text)
                    }} />
                     <RequestHeadersHintTextView>{ticket_field_type.footer}</RequestHeadersHintTextView>
                    {/* {this._renderDropDown()} */}
            </View> )}

    renderView() {
        if(Platform.OS == 'android'){
            Keyboard.addListener('keyboardDidHide', () => {
                try{
                    this.inputNode.blur()          
                }
                catch(err){
                    
                }
            
        });
        }

        // let test = [];
        // let type = [];
        // for(var i = 0; i< this.props.element.ticket_fields.length; i++)
        // {
        //   var id = this.props.element.ticket_fields[i].id
        //   if(this.props.element.ticket_fields[i].type == "Text" || this.props.element.ticket_fields[i].type == "E-mail")
        //   {
        //     let required = ''
        //     if(this.props.element.ticket_fields[i].required == true)
        //     {
        //         required = "*"
        //     }

        //     let keyboardType = "default"
        //     if(this.props.element.ticket_fields[i].type == "E-mail")
        //     {
        //         keyboardType = "email-address"
        //     }

        //       test.push(this._renderTest(this.props.element.ticket_fields[i], id, required, keyboardType) )
        //   }
          
        //   else if(this.props.element.ticket_fields[i].type=="Multi-line")
        //   {
        //     let required = ''
        //     if(this.props.element.ticket_fields[i].required == true)
        //     {
        //         required = "*"
        //     }
        //     test.push( this._renderTextBox(this.props.element.ticket_fields[i], id, required))
        //   }
        //   else if(this.props.element.ticket_fields[i].type == "Drop-down")
        //   {
        //     var options = JSON.parse(this.props.element.ticket_fields[i].custom_field_options)
        //     let idval = this.props.element.ticket_fields[i].id
        //     var test1=new Array();
        //     test1[idval] = options
        //     type = options
        //     let required = ''
        //     if(this.props.element.ticket_fields[i].required == true)
        //     {
        //         required = "*"
        //     }
        //     test.push( <RequestHeadersTextView>{this.props.element.ticket_fields[i].title}  <Text style={{color: 'red'}}>{required}</Text></RequestHeadersTextView>)
        //     test.push(this._renderIssuteTypeView1(i+200, idval))
        //   }
        // }

        return (<BackgroundView>
                  <SpinnerView visible={this.state.spinnerVisible} />
                <FormContainer ref={(ref) => { this.flatListRef = ref; }}>
                    <RequestHeaderTextView>{strings.label__request_ticket_form_id}</RequestHeaderTextView>
                    {this._renderIssuteTypeView(this.state.mainPickerOption)}

                    {/* {this.state.test}  */}
                    {this._showNewItes()}

                    {/* <ActionButtonContainer>
                        <ActionButton
                            disabled={this.state.loading}
                            title='Submit'
                            backgroundColor={Colors.GREEN_COLOR}
                            onPress={() => this._onSubmit(this.props)}
                        />
                    </ActionButtonContainer> */}
                </FormContainer>
                <FooterView />
                {this._showDatePicker()}
            </BackgroundView >
        )
    }

    _showNewItes(){ return(
        this.state.test
    )}

    _renderIssuteTypeView() {
        return (
            <View key={'888jf924'} style={[styles.textInputContainer, { marginTop: 8 }]}>
                {
                    (Platform.OS == 'ios') ?
                        <TouchableWithoutFeedback onPress={() => { this.setState({ pickerType: 'country', showPicker: true }) }}>
                            <TextInput
                                key={'f439hosdnuj'}
                                editable={false}
                                style={styles.textInputContainer}
                                placeholder=''
                                value={this.state.issueType['name']}
                                underlineColorAndroid='white'
                                onTouchStart={() => { 
                                    this.setState({ pickerType: 'country', showPicker: true, 
                                    pickerSource: this.state.mainPickerOption }) 
                                }} 
                                onValueChange={(value, position) => { 
                                    console.log("changed")
                                    this.setState({ selecteIssueType: this.state.issueTypes[position] })
                                }}/>
                        </TouchableWithoutFeedback> :
                        <Picker
                            style={{ height: 44, flex: 1, marginRight: 8 }}
                            selectedValue={this.state.selectedDropDown.name}
                            prompt='Please select what your problem is regarding with'
                            mode='dialog'
                            onValueChange={(value, position) => {  

                                let selected = this.state.pickerSource[position]
                                this.renderCustomFields(selected)


                                this.setState({selectedDropDown: this.state.pickerSource[position]})
                             


                                
                                //let selected = this.state.pickerSource[position]
                                //this.setState({ pickerSource:selected, showPicker: false })

                                //this.renderCustomFields(selected)
                                

                                // this.setState({ selecteIssueType: this.state.issueTypes[position] })
                                //this.setState({ pickerType: id, showPicker: true })
                               
                            }}  >
                             
                            {
                                // this.state.issueTypes.map(item => {
                                //     return <Picker.Item key={item.name} label={item.name} value={item.name} />
                                // })
                                
                                 this.state.pickerSource.map(item => {
                                     return <Picker.Item key={item.name} label={item.name} value={item.name} />
                                 })
                            }
                        </Picker>
                }
            </View>
        )
    }
    hideKeyboard() {
        try {
            this.refs._scrollView.scrollTo({ x: 0, y: 0, animated: true })
        }
        catch (err) {
            return
        }
    }
    showNavAndroidKeyboard() {
        try {
            this.refs._scrollView.scrollTo({ x: 0, y: 120, animated: true })
        }
        catch (err) {
            return
        }
    }
    async  _getTicketFieldsById(item){

        //alert(item.id)
            if(item.id==0)
                return

           // var url = 'http://207.154.211.132:3000/?contentType=zendeskFields&id='+item.id+'&env=dev'
                    // http://207.154.211.132:3000/?contentType=zendeskFields&id=117297&env=dev
           // var url = 'https://companion.paradoxplaza.com/?contentType=zendeskFields&id='+item.id+'&env=stage'
           
           //var url = this.middleware_zendesk() + 'contentType=zendeskFields&id='+item.id+'&env=stage' + store.getState()
           // .config.name               
           this.setState({ticket_form_id: item.id})

            let queryParameters = 'contentType=zendeskFields&id='+item.id+'&env=' + store.getState().config.name
            var url = `${middleware_zendesk()}?${queryParameters}`

            var result = await this.localApiHandler('GET', url)
            //var result = await this._getTicketFieldsById2(item)
            

            let element = {
                //id: 116747,
                id: item.id,
                title: 'General Issue',
                ticket_fields: result,
                
            }

             if(result.ok == false)
            {
                 console.log("please check your internet connection")
                 return result
             }

            this.setState({ticket_fields: result})
            console.log("api result from zendesk is ")
            console.log(result)
            //this.props._navigateToRequestList(this.props, element, issueTypes)



            let test = [];
            for(var i = 0; i<result.length;i++)
            {
                console.log(result[i])

            let item  = result[i]
        
         let type = [];
        
        //   var id = this.props.element.ticket_fields[i].id
           var id = item.id

        //   if(this.props.element.ticket_fields[i].type == "Text" || this.props.element.ticket_fields[i].type == "E-mail")
           if(item.type == "Text" || item.type == "E-mail")
           {

             let required = ''
             if(item.required == true)
             {
                 required = "*"
             }
            
            let keyboardType = "default"
            if(item.type == "E-mail")
            {
                keyboardType = "email-address"
            }

              test.push(this._renderTest(item, id, required, keyboardType) )
            
          }
         
        
          else if(item.type=="Multi-line")
          {
            let required = ''
            if(item.required == true)
            {
                required = "*"
            }
            test.push( this._renderTextBox(item, id, required))
          }
          else if(item.type == "Drop-down")
          {
            var options = JSON.parse(item.custom_field_options)
            let idval = item.id
            var test1=new Array();
            test1[idval] = options
            type = options
            let required = ''
            if(item.required == true)
            {
                required = "*"
            }
            test.push( <RequestHeadersTextView>{item.title}  <Text style={{color: 'red'}}>{required}</Text></RequestHeadersTextView>)
            test.push(this._renderIssuteTypeView1(i+200, idval))
          }
         
         
        }
        this.setState({test:test})








        //componentWillMount() {
         let types 
         let types1 = []
        //  for(var i = 0; i< this.props.element.ticket_fields.length; i++)
        for(var i = 0; i<result.length;i++)
            {
            let item= result[i]
            var temp = {
                id:item.id,
                value:''
            }
            var temp1 = {
                id:item.id,
                value:'',
                required: item.required
            }
            this.state.customFieldsDetails.push(temp1)
            this.state.customFieldsValues.push(temp)

            console.log(i)
            console.log(item.type)
             if(item.type == "Drop-down")  
             {
                types = JSON.parse(item.custom_field_options)
                console.log("types from inside")
                console.log(types)
                //types = JSON.parse(this.props.element.ticket_fields[i].custom_field_options)
                //let someValue = []
                //types1[this.props.element.id] = types
                let id = item.id
                let ojbectTemp = {
                    id: id,
                    item: types
                }
               // types1.assign(ojbectTemp)
                types1.push(ojbectTemp)
                //types1.push(someValue)
                //types1  = someValue
             }
         
    }
    console.log("types ")
    console.log(types1)
    this.setState({
        // ticketFields:this.props.element.ticket_fields, loaded: true, 
         ticketFields:result, loaded: true, 
         pickerSource: types, 
         types: types1,
        // ticket_form_id: this.props.element.id
     })














            return result
        }
    renderCustomFields(selectedElement){
        this._getTicketFieldsById(selectedElement)
        return       
    }
    // setKeybordShow() {
    //     this.props.navigator.toggleTabs({
    //         to: 'shown',
    //         animated: false
    //     });
    // }
    // setKybordOpenAnroid() {
    //     this.props.navigator.toggleTabs({
    //         to: 'hidden',
    //         animated: false
    //     });
    // }
    // Lifecycle
    render() {
        // Keyboard.addListener('keyboardWillShow', () => {
        //     this.props.navigator.toggleTabs({
        //         to: 'hidden',
        //         animated: false
        //     });
        // });
        if (Platform.OS == 'ios') {
            Keyboard.addListener('keyboardWillHide', () => {
               // this.setKeybordShow()
                this.hideKeyboard()
            });
        } else {
            Keyboard.addListener('keyboardDidHide', () => {
               // this.setKeybordShow()
                this.showNavAndroidKeyboard()
            });
        }
     return (Platform.OS == 'android' ?
                <ScrollView ref="_scrollView"
                    keyboardShouldPersistTaps='handled'
                    style={{margintop:20}}>
                    {this.renderView()}
                </ScrollView> :
                <KeyboardAvoidingView style={{ flex: 1,marginTop:20 }} behavior="padding">
                    <ScrollView ref="_scrollView"
                        keyboardShouldPersistTaps='handled'  >
                        {this.renderView()}
                    </ScrollView>
                </KeyboardAvoidingView>
        )
    }
}

// Styles
var styles = StyleSheet.create({
    textInputContainer: {
        height: 44,
        backgroundColor: 'white',
        borderRadius: 4,
        paddingLeft: 8,
    },
})