/**
 *  Component used for loading web content like Terms of Use and Privacy Policy
 * 
 *  Props:
 *  @param {string} url - The urlString is taken from the navigation route parameters
 */


import React, { Component } from 'react'
import { WebView, StyleSheet, View } from 'react-native'
import Colors from '../../config/colors'

export default class WebScreen extends Component {
    render() {
        return (
            <WebView style={styles.container} source={{ uri: this.props.url }} />
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.DARK_GRAY_COLOR
    }
})