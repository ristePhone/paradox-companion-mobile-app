import { connect } from 'react-redux'
import CreateRequestScreen from '../request-create/screen'
import Routes from '../../config/routes';
import { strings } from '../../config/localization'

const mapStateToProps = state => ({
    mainImageUrl: state.contentfulAPIcontent.mainImageUrl
})

const mapDispatchToProps = dispatch => ({
    _navigateToRequestList: props => {
    //    props.navigator.push({
            props.navigator.resetTo({
            screen: Routes.FEEDBACK,
            title: strings.request_navigator_title_open_requests,
            passProps: {
                notify: true,
                notifyMessage: strings.request_submit_notify_request_crated
            },
        })
    }
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CreateRequestScreen)