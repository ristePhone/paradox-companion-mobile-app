import React, { Component } from 'react'
import { StyleSheet, View, Text, TextInput, TouchableWithoutFeedback, 
    Platform, Picker, KeyboardAvoidingView, Keyboard, ScrollView, Dimensions, Alert, TouchableOpacity } from 'react-native'
import Entypo from 'react-native-vector-icons/Entypo'

import RequestService from '../../services/request-service'
import Configuration from '../../config/environment/config'
import Colors from '../../config/colors'
import countries from '../../utils/countries.json'
import issueTypes from '../../utils/issueTypes.json'
import Utils from '../../services/utils-service'
import ActionButton from '../views/action-button'

import PickerView from '../views/native-picker'
import BackgroundView from '../views/background-view'
import { configurationSetup } from '../../config/config'
import RNFetchBlob from 'rn-fetch-blob'
import ZendeskService from '../../services/zendesk-service'
//import { sendImageToZendesk } from '../../services/zendesk/utils'
import SpinnerView from '../common/spinner/ui'

import {
    ScrollViewContentView, FormContainer, FormTextInputField, FormTextInputFieldList, ActionButtonContainer, FooterView,
    RequestHeadersTextView, RequestHeaderTextView, RequestHeadersHintTextView, FormTextInputField1, TitleView, FormTitleTextView,
    AttachmentContainer, AttachmentItem
} from './styles'

import { strings } from '../../config/localization'

export default class CreateRequestScreen extends Component {
    constructor(props) {
        super(props)
        let { height, width } = Dimensions.get('window')

        this.state = {
            description: '',
            summary: '',
            height: height,
            country: '',
            issueType: issueTypes[0],
            issueTypes: '',
            pickerType: 'issueType',
            showPicker: false,
            selectedCountry: countries[0],
            selecteIssueType: issueTypes[0],
            mainImageUrl: this.props.mainImageUrl,
            loading: false,
            spinnerVisible:false,
            ticketFiels:[],
            loaded:false, 
            test:[],
            title:'',
           //someValue: issueTypes[0],
            someValue: {
                22:'name on some value'
            },
            pickerSource:null,
            customFieldsValues:[],
            customFieldsDetails:[], 
            types:[],
            ticket_form_id:0, 
            selection:[
                {id:1, body:'test1'},
                {id:2, body: 'test2'}
            ],
            //selectedDropDown:''
            dropdownSelection:{},
            attachmentList:[],
            uploads:[]
        }
    }

    _clearState() {
        this.setState({ summary: '', description: '' })
    }

    componentDidMount () {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
      }
    
      componentWillUnmount (props) {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
      }
    
      _keyboardDidShow () {
        this._hideNavBar()
      }
    
      _keyboardDidHide () {
        this._showNavBar()
      }

    _showNavBar()
    {
        this.props.navigator.toggleTabs({
            to: 'shown',
            animated: false
        });
    }

    _hideNavBar()
    {
        this.props.navigator.toggleTabs({
            to: 'hidden',
            animated: false
        });
    }
    // ---------------------------------------------------------------------------------------
    // validation methods start
    // ---------------------------------------------------------------------------------------
    _checkValidation1() {
        let customFieldsDetails =  this.state.customFieldsDetails
        for(var i=0;i<customFieldsDetails.length;i++)
        {
            if(customFieldsDetails[i].required == true)
            {
                if(customFieldsDetails[i].value =="")
                {
                    Alert.alert("Please fill all required fields")
                    return false
                }
            }

        }
        return true
    }

    // ---------------------------------------------------------------------------------------
    // validation methods end
    // ---------------------------------------------------------------------------------------

    // ---------------------------------------------------------------------------------------
    // helpers start
    // ---------------------------------------------------------------------------------------
    _startLoading() {
        this.setState({ loading: true, spinnerVisible: true})
    }

    _endLoading() {
        this.setState({ loading: false, spinnerVisible: false})
    }

    _getValueById(tempCustomFields, id){
        for(var i=0;i<tempCustomFields.length;i++)
        {
            if(tempCustomFields[i].id==id)
            {

                return tempCustomFields[i].value
            }
        }

    }
    // ---------------------------------------------------------------------------------------
    // helpers end
    // ---------------------------------------------------------------------------------------

    async _apiCallSubmitRequest(props, newobject) {
        this._startLoading()
        Keyboard.dismiss()
        var url = Configuration.common.ZENDESK_REQUEST_CREATE_URL()
        var headers = await ZendeskService.setJsonHeaders()
        var body = JSON.stringify({
            request: {
                requester: { name: 'user name' },
                subject: this.state.summary,
                comment: { body: this.state.description },
                 custom_fields:[{
                     id:456811,
                     value:'agod'}]
            }
        })

        let custom_fields = objectparametar=[]
        for(var i =0; i<newobject.length; i++)
        {
            objectparametar.push(newobject[i])
        }
        const emailUser = await ZendeskService.getEmail()
         var body1 = JSON.stringify({
            request: {
              //  requester: { name: 'user name'},
                //requester: { email: emailUser , name:'username'},
                requester: { email: emailUser , name: emailUser},
                //subject: this.state.summary,
                subject: this._getValueById(custom_fields,"408382") ,
                //comment: { body: this.state.description },
                comment: { body: this._getValueById(custom_fields,"408383"), "uploads": this.state.uploads },
               // ticket_form_id: 116747,  // general issue
               // ticket_form_id: 117297,     // forum / account issues
                ticket_form_id: this.state.ticket_form_id,
                //custom_fields:[{456811:'test'}]
                custom_fields,

            }
        })
        console.log("body1 equelsss ")
        console.log(body1)
        console.log("headers")
        console.log(headers)
        //408382 summary
        //408383 description

        var result = await RequestService.post1(url, headers, body1)
        // if(result.ok == false)
        // {
        //     this._endLoading()
        //     alert("Something went wrong, please try again later")
        //     return
        // }
        this._clearState()
        this._endLoading()
        this.props._navigateToRequestList(this.props)
    }
    componentWillMount() {
        let types 
        let types1 = []
        let newsomeValue = this.state.dropdownSelection

        console.log("selection is")
        console.log(this.props.title)

         for(var i = 0; i< this.props.element.ticket_fields.length; i++)
         {
            var temp = {
                id:this.props.element.ticket_fields[i].id,
                value:''
            }
            var temp1 = {
                id:this.props.element.ticket_fields[i].id,
                value:'',
                required: this.props.element.ticket_fields[i].required
            }
            this.state.customFieldsDetails.push(temp1)
            this.state.customFieldsValues.push(temp)
             if(this.props.element.ticket_fields[i].type == "Drop-down")  
             {
                types = JSON.parse(this.props.element.ticket_fields[i].custom_field_options)
                //types = JSON.parse(this.props.element.ticket_fields[i].custom_field_options)
                //let someValue = []
                //types1[this.props.element.id] = types
                let id = this.props.element.ticket_fields[i].id
                let ojbectTemp = {
                    id: id,
                    item: types,
                    selected: types[0]
                }
               // types1.assign(ojbectTemp)
                types1.push(ojbectTemp)
                //types1.push(someValue)
                //types1  = someValue


                //let newsomeValue = this.state.someValue
                let idpicker = id
                newsomeValue[idpicker] = types[0]

             }
         }
        this.setState({
            ticketFields:this.props.element.ticket_fields, loaded: true, 
            pickerSource: types, 
            types: types1,
            ticket_form_id: this.props.element.id,
            dropdownSelection: newsomeValue
        })

       

        console.log("types 1")
        console.log(types1)
        console.log(newsomeValue)
    }

    _updateCustomFieldsValue(id, value)
    {
        let tempCustomFields = this.state.customFieldsValues
        let tempCustomFieldsDetails = this.state.customFieldsDetails
        for(var i=0; i<tempCustomFields.length;i++)
        {
            
            if(tempCustomFields[i].id==id)
            {
                tempCustomFields[i].value = value
                this.setState({customFieldsValues: tempCustomFields})
               
                tempCustomFieldsDetails[i].value = value
                this.setState({customFieldsDetails: tempCustomFieldsDetails})

                let test = this._getValueById(tempCustomFields,408382) 
                return
            }
        }
    }
    _onSubmit(props) {
         if (!this._checkValidation1())
             return
         let response = this._apiCallSubmitRequest(props, this.state.customFieldsValues)
    }
    _getSource(id){
        for(var i=0; i< this.state.types.length; i++)
        {
            if(this.state.types[i].id == id)
            {
                return this.state.types[i].item
            }
        }
    }
    _showDatePicker = () => {
        let id = this.state.pickerType
        console.log("picker selection")
        console.log(this.state.dropdownSelection[id])
        return (this.state.showPicker) ?
       
            <PickerView
               selectedValue={this.state.dropdownSelection[id]}
                type={this.state.pickerType}
                dataSource={this.state.pickerSource}
                onDoneButtonPress={(selectedValue) => {
                    Keyboard.dismiss()
                    let newsomeValue = this.state.someValue
                    let idpicker = this.state.pickerType
                    newsomeValue[idpicker] = selectedValue.name

                   // let selection = selectedValue
                    let dropdownSelected  = this.state.dropdownSelection
                    dropdownSelected[idpicker] = selectedValue


                    this._updateCustomFieldsValue(idpicker, selectedValue.value)
                    this.setState({ showPicker: false, someValue : newsomeValue, dropdownSelection: dropdownSelected})
                }} /> : null
    }
    _renderIssuteTypeView1(key, id) {
        return (
            <View key={key} style={[styles.textInputContainer, { marginTop: 8 }]}>
                {(Platform.OS == 'ios') ?
                        <TouchableWithoutFeedback onPress={() => { this.setState({ pickerType: id, showPicker: true }) }}>
                            <TextInput
                            key={key+'e'}
                                editable={false}
                                style={styles.textInputContainer}
                                placeholder=''
                                value={this.state.someValue[id]}
                                underlineColorAndroid='white'
                                onTouchStart={() => { 
                                    let datasource = this._getSource(id)
                                    this.setState({ pickerType: id,  pickerSource: datasource }) 
                                    this.setState({showPicker: true})
                                    }} />
                        </TouchableWithoutFeedback> :
                        <Picker
                            style={{ height: 44, flex: 1, marginRight: 8 }}
                            //selectedValue = {this.state.someValue[id]}
                            selectedValue={this.state.dropdownSelection[id]}
                            prompt='Please select'
                            mode='dialog'
                            onValueChange={(value, position) => { 
                                let option1 = this._getSource(id)
                                let newsomeValue = this.state.someValue
                                newsomeValue[id] = option1[position].name

                                let dropdownSelected  = this.state.dropdownSelection
                                dropdownSelected[id] = option1[position].name


                                this._updateCustomFieldsValue(id, option1[position].value)
                                this.setState({  someValue : newsomeValue, dropdownSelection:dropdownSelected})
                                return
                                    let option = this._getSource(id)
                                    this.setState({selection: value})
                                    this._updateCustomFieldsValue(id, this.state.pickerSource[position].value)
                                 }} >
                            {
                                this._getSource(id).map(item => {       
                                    return <Picker.Item key={item.name} label={item.name} value={item.name} />
                                })
                            }
                        </Picker>
                }
            </View>
        )
    }

    // _getOne(){
    //     for(var i=0; i<this.state.selection.length; i++)
    //     {
    //         if(this.state.selection.id==1)
    //         {
    //             return this.state.body
    //         }
    //     }
    // }
    
    componentWillReceiveProps(nextProps) {
        this.setState({ mainImageUrl: nextProps.mainImageUrl })
    }
    setSel(input) {
        let offset
        if (input == 'inputArea') {
            offlset = 210
            if (Platform.OS == 'ios') {
                setTimeout(() => {
                    this.refs._scrollView.scrollToEnd({ animated: true })
                }, 300)
            } else {
                if (this.state.height < 660) {
                    setTimeout(() => {
                        this.refs._scrollView.scrollTo({ x: 0, y: 215, animated: true })
                    }, 300)
                }else{
                    setTimeout(() => {
                        this.refs._scrollView.scrollTo({ x: 0, y: 175, animated: true })
                    }, 300)
                }
       
            }
           // this.setKybordOpenAnroid()
        } else {

           // this.setKybordOpenAnroid()
        }


    }

    _renderTest = (ticket_field_type, key, required, keyboardType, autocapitalize) => {
        return (
            <View key = {key} >
                <RequestHeadersTextView>{ticket_field_type.title} <Text style={{color: 'red'}}>{required}</Text></RequestHeadersTextView>
                <FormTextInputField
                   // ref='tomi1'
                    keyboardType={keyboardType}
                    autoCapitalize ={autocapitalize}
                    maxLength={40}
                    defaultValue={this.state.emptyValue}
                    value={this.state.emptyValue}
                    autoCorrect={false}
                    onFocus={() => this.setSel('inputArea')}
                    onFocus={() => this.setSel('input')}
                    underlineColorAndroid='white'
                    onChangeText={(text) => { 
                        this._updateCustomFieldsValue(ticket_field_type.id, text)
                    }} />
                     <RequestHeadersHintTextView>{ticket_field_type.footer}</RequestHeadersHintTextView>
                    {/* {this._renderDropDown()} */}
            </View> )}

     _renderTextBox = (ticket_field_type, key, required) => {
        return ( <View key = {key} >
                <RequestHeadersTextView>{ticket_field_type.title} <Text style={{color: 'red'}}>{required}</Text></RequestHeadersTextView>
                <FormTextInputField1
                   // ref='tomi1'
                    multiline={true}
                  //  maxLength={40}
                    defaultValue={this.state.emptyValue}
                    value={this.state.emptyValue}
                    autoCorrect={false}
                    onFocus={() => this.setSel('inputArea')}
                    onFocus={() => this.setSel('input')}
                    underlineColorAndroid='white'
                    onChangeText={(text) => { 
                        this._updateCustomFieldsValue(ticket_field_type.id, text)
                    }} />
                     <RequestHeadersHintTextView>{ticket_field_type.footer}</RequestHeadersHintTextView>
                    {/* {this._renderDropDown()} */}
            </View> )}

    _renderTitle(title){return(
        <TitleView><Text>{title}</Text></TitleView>
    )}
     _onPressAttachment = async () =>  
    {
        var ImagePicker = require('react-native-image-picker');

        // More info on all the options is below in the README...just some common use cases shown here
        var options = {
          title: 'Select Avatar',
          customButtons: [
            {name: 'fb', title: 'Choose Photo from Facebook'},
          ],
          storageOptions: {
            skipBackup: true,
            path: 'images'
          }
        };
        
        ImagePicker.launchImageLibrary(options, async (response)  => {
             if(response.didCancel)
             return

            console.log("response is")
            console.log(response)
            console.log("response.uri 18 18")
            console.log(response.uri)

            // if(parseInt(response.fileSize) >604800)
            // {
            //     Alert.alert("File is too big")
            //     return
            // }
            this._startLoading()
            // Same code as in above section!
           // let source = { uri: response.uri };
            //let source1 = { uri: 'data:image/jpeg;base64,' + response.data };
            
            var attList  = this.state.attachmentList
            attList.push(response)
            //this.setState({attachmentList: attList})
            var attach = null
            if(Platform.OS == 'android'){
                 let dirs = RNFetchBlob.fs.dirs
                 // const attach  = await this.sendImageToZendesk(dirs.DocumentDir +response.path)
                  attach  = await this.sendImageToZendesk(response.uri)
                 // const attach  = await this.sendImageToZendesk(response.uri)
                  console.log("attachis")
                  console.log(attach)
            }
            else{
                 attach  = await this.sendImageToZendesk(response.origURL)
            }
            this._endLoading()
           
           

            let up = this.state.uploads
            up.push(attach.upload.token)
            this.setState({uploads:up, attachmentList: attList})

          
        
          });

          //return





       


        // ImagePicker.launchImageLibrary(options, (response) => {
        //     console.log("result from picker is")
        //     console.log(response)
        //     console.log(response.fileName)
        //     console.log(response.origURL)
        //     //this._addAttach(response.origURL)
        //     //var attach  = this.state.attachmentList
        //     //attList.push(response.origURL)
        //     this.setState({attachmentList: response})
        // })
    }

    async sendImageToZendesk (origURL) {
       // if (imageURL) {
            const { ZENDESK_BASIC_URL } = await configurationSetup()
            const baseUrl = `${ZENDESK_BASIC_URL}/api/v2/uploads.json?filename=attachment.png`
            const headers = {
                'Autorization': 'Basic cmlzdGUuc3Bhc2Vza2lAZGF0YWpvYi5zZS8yVzVKRGkxbGFLYVpnU0JHSVZWdWN4cmh3YUVNQ0t0b1AzMXkwWnFCOjJXNUpEaTFsYUthWmdTQkdJVlZ1Y3hyaHdhRU1DS3RvUDMxeTBacUI=',
                'Accept': 'application/json'
            }
        
           // const wrapFileURL = RNFetchBlob.wrap(imageURL)
            const wrapFileURL = RNFetchBlob.wrap(origURL)
            return RNFetchBlob.fetch('POST',
                baseUrl,
                headers,
                wrapFileURL)
                .then(response => {
                    console.log(response)
                    return response.json()
                    //return response
                })
                .then(responseData => {
                    console.log(responseData)
                    return responseData
                })
                .catch(error => {
                    //console.log(responseData)
                    return { error: error }
                })
        //}
    
        return { error: 'No image url provided'}
    }
    renderView() {
        if(Platform.OS == 'android'){
            Keyboard.addListener('keyboardDidHide', () => {
                try{
                    this.inputNode.blur()          
                }
                catch(err){
                    
                }
            
        });
        }

        let test = [];
        let type = [];
        let title = []
        title.push(this._renderTitle(this.props.title))
        for(var i = 0; i< this.props.element.ticket_fields.length; i++)
        {
          var id = this.props.element.ticket_fields[i].id
          if(this.props.element.ticket_fields[i].type == "Text" || this.props.element.ticket_fields[i].type == "E-mail")
          {
            let required = ''
            if(this.props.element.ticket_fields[i].required == true)
            {
                required = "*"
            }

            let keyboardType = "default"
            let autocapitalize = "sentences"
            if(this.props.element.ticket_fields[i].type == "E-mail")
            {
                keyboardType = "email-address"
                autocapitalize="none"
            }

              test.push(this._renderTest(this.props.element.ticket_fields[i], id, required, keyboardType, autocapitalize) )
          }
          
          else if(this.props.element.ticket_fields[i].type=="Multi-line")
          {
            let required = ''
            if(this.props.element.ticket_fields[i].required == true)
            {
                required = "*"
            }
            test.push( this._renderTextBox(this.props.element.ticket_fields[i], id, required))
          }
          else if(this.props.element.ticket_fields[i].type == "Drop-down")
          {
            var options = JSON.parse(this.props.element.ticket_fields[i].custom_field_options)
            let idval = this.props.element.ticket_fields[i].id
            var test1=new Array();
            test1[idval] = options
            type = options
            let required = ''
            if(this.props.element.ticket_fields[i].required == true)
            {
                required = "*"
            }
            test.push( <RequestHeadersTextView>{this.props.element.ticket_fields[i].title}  <Text style={{color: 'red'}}>{required}</Text></RequestHeadersTextView>)
            test.push(this._renderIssuteTypeView1(i+200, idval))
          }
        }

        return (<BackgroundView>
                  <SpinnerView visible={this.state.spinnerVisible} />
                     <FormContainer ref={(ref) => { this.flatListRef = ref; }}>
                     {title}
                    <RequestHeaderTextView>{strings.label__request_ticket_form_id}</RequestHeaderTextView>
                    {test}
                    <RequestHeaderTextView>Attachment</RequestHeaderTextView>

                    {this.state.attachmentList.map(item => {       
                                    return  <AttachmentItem>{item.fileName}
                                    </AttachmentItem>
                                })}
                    {/* <AttachmentItem>image1.png</AttachmentItem>
                    <AttachmentItem>image2.png</AttachmentItem> */}
                    <TouchableWithoutFeedback onPress={(e)=>{this._onPressAttachment(e)}}>
                        <AttachmentContainer>
                            <Entypo size={30} color='white' name='attachment' />
                            <RequestHeaderTextView>Add file here</RequestHeaderTextView>
                        </AttachmentContainer>
                    </TouchableWithoutFeedback>
                    <ActionButtonContainer>
                        <ActionButton
                            disabled={this.state.loading}
                            title='Submit'
                            backgroundColor={Colors.GREEN_COLOR}
                            onPress={() => this._onSubmit(this.props)}
                        />
                    </ActionButtonContainer>
                </FormContainer>
                <FooterView />
                {this._showDatePicker()}
            </BackgroundView >
        )
    }
    hideKeyboard() {
        try {
            this.refs._scrollView.scrollTo({ x: 0, y: 0, animated: true })
        }
        catch (err) {
            return
        }
    }
    showNavAndroidKeyboard() {
        try {
            this.refs._scrollView.scrollTo({ x: 0, y: 120, animated: true })
        }
        catch (err) {
            return
        }
    }
    // setKeybordShow() {
    //     this.props.navigator.toggleTabs({
    //         to: 'shown',
    //         animated: false
    //     });
    // }
    // setKybordOpenAnroid() {
    //     this.props.navigator.toggleTabs({
    //         to: 'hidden',
    //         animated: false
    //     });
    // }
    // Lifecycle
    render() {
        // Keyboard.addListener('keyboardWillShow', () => {
        //     this.props.navigator.toggleTabs({
        //         to: 'hidden',
        //         animated: false
        //     });
        // });
        if (Platform.OS == 'ios') {
            Keyboard.addListener('keyboardWillHide', () => {
               // this.setKeybordShow()
                this.hideKeyboard()
            });
        } else {
            Keyboard.addListener('keyboardDidHide', () => {
               // this.setKeybordShow()
                this.showNavAndroidKeyboard()
            });
        }
     return (Platform.OS == 'android' ?
                <ScrollView ref="_scrollView"
                    keyboardShouldPersistTaps='handled'
                    style={{margintop:20}}>
                    {this.renderView()}
                </ScrollView> :
                <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
                    <ScrollView ref="_scrollView"
                        keyboardShouldPersistTaps='handled'  >
                        {this.renderView()}
                    </ScrollView>
                </KeyboardAvoidingView>
        )
    }
}

// Styles
var styles = StyleSheet.create({
    textInputContainer: {
        height: 44,
        backgroundColor: 'white',
        borderRadius: 4,
        paddingLeft: 8,
    },
})