import { connect } from 'react-redux'
import EventDetails from './screen'

const mapStateToProps = state => ({
    item: state.navigation.selectedEvent
})

const mapDispatchToProps = dispatch => ({

})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EventDetails)