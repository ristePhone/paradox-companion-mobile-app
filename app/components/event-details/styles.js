import glamorous from 'glamorous-native'

const MARGIN = 15
const IMAGE_HEIGHT = 150

export const ScrollViewContainer = glamorous.scrollView({
    flex: 1,
    paddingHorizontal: 10,
})

export const ScrollViewContent = glamorous.view({
    flex: 1,
    top: MARGIN,
    borderRadius: 4,
    backgroundColor: '#1a1a1a',
})

export const WrapperContainer = glamorous.view({
    flex: 1,
    paddingHorizontal: MARGIN,
})

export const ImageWrapperContainer = glamorous.view({
    flex: 1,
    height: IMAGE_HEIGHT,
    padding: 5,
    borderRadius: 4,
})

export const TitleContainer = glamorous.view({
    flex: 1,
    paddingVertical: MARGIN,
})

export const Title = glamorous.text(props => ({
    flex: 1,
    fontFamily: 'SourceSansPro-Regular',
    fontSize: props.fontSize,
    color: props.color,
    fontWeight: props.fontWeight,
    marginLeft: props.marginLeft,
    letterSpacing: -0.2,
}))

export const Description = glamorous.text( props => ({
    flex: 1,
    color: props.color,
}))

export const Date = glamorous.text({
    fontFamily: 'SourceSansPro-Regular',
    marginTop: 7,
    fontSize: 12,
    color: '#adadad',
})

export const DateLocationWrapperContainer = glamorous.view({
    flex: 1,
    paddingVertical: MARGIN,
    justifyContent: 'space-between'
})

export const EventDateContainer = glamorous.view({
    flex: 1,
    flexDirection: 'row',
})

export const LocationContainer = glamorous.view({
    flex: 1,
    marginBottom: MARGIN,
})

export const EventDateWrapperContainer = glamorous.view({
    flexDirection: 'row',
    flex: 1,
    backgroundColor: 'red',
})

export const SeparatorLine = glamorous.view({
    height: 1,
    flex: 1,
    backgroundColor: '#222222',
})

export const InfoTextWrapperContainer = glamorous.view({
    flex: 1,
    paddingVertical: 10,
    justifyContent: 'center',
    marginVertical: MARGIN,
})

export const InfoText = glamorous.text({
    fontFamily: 'SourceSansPro-Regular',
    fontSize: 12,
    fontWeight: '600',
    color: 'white',
    textAlign: 'left',
})

export const TagContainer = glamorous.view({
    flex: 1,
    paddingVertical: MARGIN,
})

export const TagTitleText = glamorous.text({
    fontFamily: 'SourceSansPro-Regular',
    color: '#adadad',
    fontSize: 12,
})

export const TagDescriptionText = glamorous.text({
    fontFamily: 'SourceSansPro-Regular',
    color: 'white',
    fontSize: 12,
})

export const FooterView = glamorous.view({
    height: 100,
    flex: 1,
})

// Custom styles, aparat from glamorous
export const imageStyle = {
    flex: 1,
    height: IMAGE_HEIGHT - 2 * 2,
}