import React from 'react'
import {
    ScrollViewContainer,
    ScrollViewContent,
    WrapperContainer,
    TitleContainer,
    ImageWrapperContainer,
    Image,
    SeparatorLine,
    DateLocationWrapperContainer,
    EventDateContainer,
    LocationContainer,
    Title,
    Description,
    Date,
    InfoText,
    InfoTextWrapperContainer,
    TagContainer,
    TagTitleText,
    TagDescriptionText,
    FooterView,
    imageStyle,
} from './styles'

import PDXImage from '../common/image/ui'
import Markdown from '../common/markdown/ui'

import { strings } from '../../config/localization'
import { transformDate } from '../../services/formating/format-date'
import UtilsService from '../../services/utils-service'

export default class EventDetails extends React.Component {

    _capitalizeFirstLetter = string => {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    // _getTagDescriptionText = eventTags => {
    //     let description = ''
    //     eventTags.map((tag, index) => {
    //         let formatTag = String(tag).split('#')[1]
    //         formatTag = this._capitalizeFirstLetter(formatTag)
    //         description = description.concat(formatTag)
    //         if (index != eventTags.length - 1) {
    //             description = description.concat(', ')
    //         }
    //     })

    //     return description
    // }

    render() {
        const {
            eventDescription,
            // eventsTags,
            imageUrl,
            eventHeadline,
            eventStart,
            eventFinish,
            eventLocation
        } = this.props.item
        const formatedPostedOnDate = UtilsService.transformDateEvents(eventStart)
        const startDate = UtilsService.formatDateToGMTOffset(eventStart)
        const endDate = UtilsService.formatDateToGMTOffset(eventFinish)
        const postedOnTitle = `${strings.event.postedOn}: ${formatedPostedOnDate}`
        const startDateTitle = `${strings.event.startLong}: `
        const endDateTitle = `${strings.event.finishLong}: `
        const locationTitle = `${strings.event.location}: `
        return (
            <ScrollViewContainer
                scrollEnabled={true}
                horizontal={false}
            >
                <ScrollViewContent>
                    <ImageWrapperContainer>
                        <PDXImage
                            style={imageStyle}
                            source={imageUrl}
                            resizeMode='contain'
                            placeholderType='large'
                        />
                    </ImageWrapperContainer>
                    <WrapperContainer>
                        <TitleContainer>
                            <Title fontSize={22} fontWeight='600' color='white'>{eventHeadline}</Title>
                            <Date>{postedOnTitle}</Date>
                        </TitleContainer>
                        <SeparatorLine />
                        <DateLocationWrapperContainer>
                            <EventDateContainer>
                                <Title
                                    fontSize={12}
                                    fontWeight='normal'
                                    color='#adadad'
                                >
                                    {startDateTitle}
                                    <Description color='white'>{startDate}</Description>
                                </Title>
                                <Title
                                    marginLeft={10}
                                    fontSize={12}
                                    fontWeight='normal'
                                    color='#adadad'
                                >
                                    {endDateTitle}
                                    <Description color='white'>{endDate}</Description>
                                </Title>
                            </EventDateContainer>
                        </DateLocationWrapperContainer>
                        <LocationContainer>
                            <Title
                                fontSize={12}
                                fontWeight='normal'
                                color='#adadad'
                            >
                                {locationTitle}
                                <Description color='white'>{eventLocation}</Description>
                            </Title>
                        </LocationContainer>
                        <SeparatorLine />
                        <InfoTextWrapperContainer>
                            <Markdown>{eventDescription}</Markdown>
                        </InfoTextWrapperContainer>
                        <SeparatorLine />
                        {/* <TagContainer>
                            <TagTitleText numberOfLines={0}>
                                {tagTitle}
                                <TagDescriptionText>{tagDescription}</TagDescriptionText>
                            </TagTitleText>
                        </TagContainer> */}
                    </WrapperContainer>
                </ScrollViewContent>
                <FooterView />
            </ScrollViewContainer>
        )
    }
}