import React, { Component } from 'react'
import { FlatList, TouchableHighlight, Platform, StyleSheet, View, Animated, Linking, Keyboard } from 'react-native'
import Colors from '../../config/colors'
import { SearchBar } from 'react-native-elements'
import {
    strings
} from '../../config/localization'

import {
    FooterContainer,
    SeparatorContainer,
    RowContainer,
    TitleContainer,
    TitleTextContainer,
    imageStyle,
} from './styles'

import PDXImage from '../common/image/ui'

export default class WikiPageScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            fileredData: props.wiki,
            searchText: '',
            data: this.props.wiki,
            expanded: false,
            fadeAnim: new Animated.Value(0),  // Initial value for opacity: 0
        }
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    }

    onNavigatorEvent(event) {
        switch(event.id) {
            case 'didDisappear':  this.resetScrollToTop()   // reset scroll to top position when tab is closed
            break;
        }
    }
    resetScrollToTop = () => {
        if(this.flatList != undefined) { this.flatList.scrollToOffset({x: 0, y: 0, animated: false}) }
    }

    _onItemSelection = item => {
        console.log("fetch wiki details  is ")
        console.log(item)
        this._fetchWikiDetails(item).then(response => {
            console.log("fetch wiki details is ")
            console.log(response)
            this.props.onWikiSelection(this.props, response)
       })

        // setTimeout(() => {
        //     Linking.openURL(item.urlPath).catch(err => Alert.alert('An error occurred', err))
        // }, 200);
    }
        _fetchWikiDetails = item => {
                const url = 'http://207.154.211.132:3000/?contentType=wikis2&id=' + item.param;
                return fetch(url, {
                    method: 'GET',
                }).then(response => {
                    return response.json()
                }).then(response => {
                    return response
                }).catch(err => {
                    alert('Network request failed. No server data for that wiki.')
                })
                //this.props.navigator.setOnNavigatorEvent(()=> this.setState({ expanded: false }));
     }
        
    _performSearch = text => {
        this.setState({ searchText: text })
        let text1 = text.toLowerCase()
        let filteredName

        filteredName = this.state.data.filter((item) => {
            return item.name.toLowerCase().match(text1)
        })
        if (!text1 || text1 === '') {
            this.setState({
                fileredData: this.state.data
            })
        } else if (!Array.isArray(filteredName) && !filteredName.length) {
            // set no data flag to true so as to render flatlist conditionally
            this.setState({
                noData: true
            })
        }
        else if (Array.isArray(filteredName)) {
            this.setState({
                fileredData: filteredName
            })
        }
    }

    _renderHeaderContainer = () => {
        // let { fadeAnim } = this.state;

        // Animated.timing(                  // Animate over time
        //     this.state.fadeAnim,            // The animated value to drive
        //     {
        //         toValue: 1,                   // Animate to opacity: 1 (opaque)
        //         duration: 1000,              // Make it take a while
        //     }
        // ).start();
        const clearIconStyle = {
            style: {
                color: 'white',
                height: 55,
                width: 40,
                top: 0,
                paddingLeft: 20,
                paddingTop: Platform.OS == 'ios' ? 15 : 20
            }
        }
        const showClearIcoStyle = this.state.searchText ? clearIconStyle : null

        return (
            //this.state.expanded == true ?

            // <Animated.View                 // Special animatable View
            //     style={{
            //         opacity: fadeAnim,
            //         // Bind opacity to animated value
            //     }}
            // >
            <SearchBar
                round={true}
                platform={Platform.OS}
                value={this.state.searchText}
                clearIcon={showClearIcoStyle}
                searchIcon={{ size: 20 }}
                onChangeText={this._performSearch}
                onClear={this.clearMethod}
                placeholder={strings.search_wikis.concat('...')}
                inputStyle={styles.searchBarInputContainer}
                selectionColor={Colors.ORANGE_COLOR}
            />

            //   </Animated.View>
            // : null

        )
    }

    _renderRow = ({ item }) => {
        return (
            <TouchableHighlight underlayColor='gray' onPress={() => this._onItemSelection(item)}>
                <RowContainer>
                    <PDXImage
                        style={imageStyle}
                        source={item.image}
                        placeholderType='small'
                        resizeMode="contain"
                    />
                    <TitleContainer>
                        <TitleTextContainer numberOfLines={3}>
                            {item.name}
                        </TitleTextContainer>
                    </TitleContainer>
                </RowContainer>
            </TouchableHighlight>
        )
    }

    _renderFooterContainer = () => <FooterContainer />

    _renderSeparatorContainer = () => <SeparatorContainer />

    /**
     *  Lifecycle
     *  @memberof WikiPageScreen    
     */

    // componentDidMount() {
    //     if (!this.props.wiki) {
    //         this.props.onWikiRetryShow()
    //     }
    // }
    componentDidMount () {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
        if (!this.props.wiki) {
            this.props.onWikiRetryShow()
        }
      }
    
      componentWillUnmount () {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
      }
    
      _keyboardDidShow () {
        this._hideNavBar()
      }
    
      _keyboardDidHide () {
        this._showNavBar()
      }

    componentWillMount() {
       
    }

    _showNavBar()
    {
        this.props.navigator.toggleTabs({
            to: 'shown',
            animated: false
        });
    }

    _hideNavBar()
    {
        this.props.navigator.toggleTabs({
            to: 'hidden',
            animated: false
        });
    }

    componentWillReceiveProps(nextProps) {
        const wiki = nextProps.wiki
        if (nextProps.wiki) {
            this.setState({
                data: wiki,
                fileredData: wiki,
                searchText: '',
            })
        } else {
            this.props.onWikiRetryHide()
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps != this.props ||
            nextState != this.state
    }

    render() {
        const hasData = this.state.fileredData ? true : false
        return hasData ?
            <FlatList
                ref={component => this.flatList = component}
                style={{ flex: 1, paddingHorizontal: 1 }}
                data={this.state.fileredData}
                showsVerticalScrollIndicator={false}
                renderItem={this._renderRow}
                onScroll={(e) => { this.setState({ expanded: true }) }}
                ListFooterComponent={this._renderFooterContainer}
                ListHeaderComponent={this._renderHeaderContainer}
                numColumns={4}
                stickyHeaderIndices={[0]}
            /> : null
    }
}

const styles = StyleSheet.create({
    searchBarInputContainer: {
        fontSize: 15,
        color: 'white',
        borderWidth: 0,
    }
})