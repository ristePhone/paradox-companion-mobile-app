import { connect } from 'react-redux'
import WikiPageScreen from './screen'
import Routes from '../../config/routes'
import NavigationActions from '../../actions/navigation-actions/index'

const mapStateToProps = state => ({
    wiki: state.contentfulAPIcontent.wiki
})

const mapDispatchToProps = dispatch => ({
    onWikiSelection: (props, item) => {
        props.navigator.push({
            screen: Routes.WIKI_DETAILS,
            title: item.wiki_name,
            passProps: {
                item,
                url: item.urlPath
            }
        })
    },
    onWikiRetryShow: async () => {
        NavigationActions.showLightBox({
            screen: Routes.RETRY,
            passProps: {
                isRetrying: false,
            }
        })
    },
    onWikiRetryHide: () => {
        NavigationActions.hideLightBox()
    }
})

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(WikiPageScreen)