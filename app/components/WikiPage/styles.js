// @Riste: Add styles from the screen here with glamorous-native
// Use glamorous-native except when styling the Flatlist, has some rerendering issues there

import glamorous from 'glamorous-native'
import { Dimensions } from 'react-native'
import Colors from '../../config/colors'

const { width } = Dimensions.get('window')
const ITEM_BORDER_SIZE = 1
const ITEM_WIDTH = (width - 2 * ITEM_BORDER_SIZE) / 4
const ITEM_HEIGHT = 150

export const RowContainer = glamorous.view({
    width: ITEM_WIDTH,
    flex: 1,
    backgroundColor: '#1a1a1a',
    padding: 5,
    borderRightWidth: ITEM_BORDER_SIZE,
    borderBottomWidth: ITEM_BORDER_SIZE,
    borderColor: Colors.BACKGROUND_COLOR,
})

export const TitleContainer = glamorous.view({
    flex: 1,
    justifyContent: 'center',
})

export const TitleTextContainer = glamorous.text({
    fontFamily: 'SourceSansPro-Regular',
    color: '#fff',
    textAlign: 'left',
    fontSize: 12,
})

export const HeaderContainer = glamorous.view({
    width: 1,
    height: 20
})

export const FooterContainer = glamorous.view({
    width: 1,
    height: 20
})

export const SeparatorContainer = glamorous.view({
    width: ITEM_BORDER_SIZE,
    height: ITEM_BORDER_SIZE,
    backgroundColor: Colors.BACKGROUND_COLOR,
})

// Custom styles, apart from glamorous
export const imageStyle = {
    width: ITEM_WIDTH - 2 * 7,
    height: ITEM_HEIGHT * 2/3,
    marginBottom: 10,
}