import { connect } from 'react-redux'
import WikiSectionScreen from './screen'

const mapStateToProps = state => ({

})

const mapDispatchToProps = dispatch => ({

})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(WikiSectionScreen)