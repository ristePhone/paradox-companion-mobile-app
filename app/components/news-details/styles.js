import glamorous from 'glamorous-native'
import Colors from '../../config/colors'

const IMAGE_HEIGHT = 200
const offset = IMAGE_HEIGHT + 20

export const MainContainer = glamorous.view({
    flex: 1,
    backgroundColor: Colors.BACKGROUND_COLOR,
})

export const ScrollViewContainer = glamorous.scrollView({
    flex: 1,
    backgroundColor: 'transparent',
})

export const ContentWrapperContainer = glamorous.view({
    paddingTop: offset,
    backgroundColor: 'transparent',
})

export const ContentContainer = glamorous.view({
    flex: 1,
    padding: 20,
    backgroundColor:'#1a1a1a',
})

export const TitleContainer = glamorous.text({
    color: '#eeeeee',
    textAlign: 'left',
    fontSize:16,
    fontFamily:'OpenSans-Bold'
   // backgroundColor: Colors.BACKGROUND_COLOR,
})
export const PublishedContainer = glamorous.text({
    color: '#adadad',
    textAlign: 'left',
    fontSize:12,
    marginTop:10
   // backgroundColor: Colors.BACKGROUND_COLOR,
})

export const DateContainer = glamorous.text({
    color: 'white',
    fontFamily:'OpenSans-bold',
    fontSize: 16,
    paddingTop: 20,
    textAlign: 'center',
})

export const InfoContainer = glamorous.text({
    color: '#eeeeee',
    fontSize: 12,
    lineHeight: 20,
    paddingTop: 20,
    fontFamily:'OpenSans-Bold'

})

// Custom styles, appart from glamorous
export const imageStyle = {
    top: 0,
    right: 0,
    left: 0,
    height: IMAGE_HEIGHT,
    position: 'absolute',
    resizeMode: 'center',
    justifyContent: 'space-around'
}