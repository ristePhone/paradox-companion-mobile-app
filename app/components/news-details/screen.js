import React, { Component } from 'react'
import Utils from '../../services/utils-service'
import {
    MainContainer,
    ContentContainer,
    ContentWrapperContainer,
    ScrollViewContainer,
    TitleContainer,
    DateContainer,
    InfoContainer,
    PublishedContainer,
    imageStyle,
} from './styles'
import { View, Text } from 'react-native'

import EntypoIcons from 'react-native-vector-icons/Entypo'

import Markdown from '../common/markdown/ui'

import PDXImage from '../common/image/ui'

export default class NewsDetails extends Component {
    _onShare = type => {
        this.props.onSharePress(type)
    }

    render() {
        const {
            headline,
            headerImage,
            publishDate,
            bodyText
        } = this.props.item

        this.txt = headline.substring(0, 11)

        return (
            <MainContainer>
                <PDXImage
                    style={imageStyle}
                    source={headerImage}
                    placeholderType='large'
                />
                <ScrollViewContainer
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                >
                    <ContentWrapperContainer>
                        <ContentContainer>
                            <View style={{
                                flex: 1,
                                flexDirection: 'row',
                                justifyContent: 'space-between',

                            }}>
                                <View style={{ flex: 1, flexDirection: 'column' }}>
                                    <TitleContainer>
                                        {headline}
                                    </TitleContainer>
                                    <PublishedContainer>
                                        <Text>Published: </Text> {Utils.formatDateToGMTOffset(this.props.item.publishDate)}
                                    </PublishedContainer>
                                </View>
                            </View>
                            <Markdown>{bodyText}</Markdown>
                        </ContentContainer>
                    </ContentWrapperContainer>
                </ScrollViewContainer>
            </MainContainer>
        )
    }
}

