import { connect } from 'react-redux'
import NewsDetailsScreen from './screen'

const mapStateToProps = state => ({
    item: state.navigation.selectedNewsFeedItem
})

const mapDispatchToProps = dispatch => ({

})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NewsDetailsScreen)