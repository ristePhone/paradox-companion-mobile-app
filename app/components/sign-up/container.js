import { connect } from 'react-redux'
import SignUpScreen from '../sign-up/screen'
import Routes from '../../config/routes'
import { UPDATE_ROOT_CONTROLER, UPDATE_USER_LOGGED_IN_STATUS } from '../../actions/action-types'
import { strings } from '../../config/localization'
import Utils from '../../services/utils-service'

const mapStateToProps = state => ({
    mainImageUrl: state.contentfulAPIcontent.mainImageUrl,
    isLoggedIn: state.user.isLoggedIn,
    termsAndConditions: state.config.TERMS_AND_CONDITIONS_URL,
    privacyPolicy: state.config.PRIVACY_POLICY_URL,
})

const mapDispatchToProps = dispatch => ({
    onSignUpSuccess: () => {
        dispatch({ type: UPDATE_USER_LOGGED_IN_STATUS, isLoggedIn: true })
        dispatch({ type: UPDATE_ROOT_CONTROLER, rootTypeController: 'tabBasedApp' })
    },
    onTermsAndConditionButtonPress: props => {
        props.navigator.push({
            screen: Routes.WEB,
            title: strings.navigation_titles.terms_of_service,
            passProps: {
                url: props.termsAndConditions+Utils.getDeviceLocale()
            }
        })
    },
    onPrivacyPolicyButtonPress: props => {
        props.navigator.push({
            screen: Routes.WEB,
            title: strings.navigation_titles.privacy_policy,
            passProps: {
                url: props.privacyPolicy+Utils.getDeviceLocale()
            }
        })
    },
    onLogin: props => {
        props.navigator.pop()
    }
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SignUpScreen)