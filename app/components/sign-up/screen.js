import React, { Component } from 'react'
import {
    StyleSheet,
    View,
    Dimensions,
    TextInput,
    TouchableWithoutFeedback,
    Platform,
    DatePickerAndroid,
    Picker,
    KeyboardAvoidingView,
    Keyboard,
    Alert,
    ActivityIndicator,
    AsyncStorage,
    Text
} from 'react-native'

import Colors from '../../config/colors'
import countries from '../../utils/countries.json'
import Utils from '../../services/utils-service'
import ActionButton from '../views/action-button'
import LogoImageView from '../common/main-image-view/logo-image-view'
import PickerView from '../views/native-picker'
import ReadButton from '../common/sign-up/read-button/ui'
import SpinnerView from '../common/spinner/ui'
import { signUp, createSession, validateEmail } from '../../services/authentication'

const FONT_COLOR = '#EEF0F2'
const Error_Color = '#fe0642'
const Error_Existing_User_Color = '#ffb400'
import {
    ScrollViewContainer,
    ScrollViewContentView,
    FormContainer,
    FormTitleContainer,
    FormTitleTextView,
    FormTextInputField,
    ActionButtonContainer,
    FormSubContainer,
    EmailDescriptionText,
    FooterView,
    BottomContainer,
    AlreadyHaveAcountTextView,
    LineFooter,
    ErrorContainerView,
    ErrorView,
    FormTextInputErrorField,
    ErrorText,
    ErrorCheckboxContainerView,
    ErrorContainerExistingUserView,
    ErrorTextExistinUser,
    ErrorExistinUserView
    
} from './styles'

import CheckBoxView from '../common/check-box/ui'
import { strings } from '../../config/localization'
import publicIP from 'react-native-public-ip'
import RequestService from '../../services/middleware/request-service'
export default class SignUpScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            simpleDate: new Date(2020, 4, 5),
            password: '',
            dateOfBirth: '',
            selectedDateOfBirth: new Date(),
            country: '',
            joinParadoxCommunity: false,
            acceptedTermsAndConditions: false,
            acceptedPrivacyPolicy: false,
            signUpButtonEnabled: true,
            pickerType: 'date',
            showPicker: false,
            showCountryPicker: false,
            selectedCountry: countries[0],
            mainImageUrl: this.props.mainImageUrl,
            isPerformingSignUp: false,
            //tomitest: false,
            tomierror: '',
            emailFieldError: false,
            emailFieldErrorMessage:'',
            passwordFieldError: false,
            passwordFieldErrorMessage: '',
            countryFieldError: false,
            countryFieldErrorMessage: '',
            dateOfBirthError: false,
            dateOfBirthErrorMessage:'',
            termsAndConditionsError: false,
            termsAndConditionsErrorMessage:'',
            privacyPolicyError: false,
            privacyPolicyErrorMessage: '',
            existingUserError: false,
            existingUserErrorMessage: ''
        }
    }

    _showError = errorMessage => {
        this.setState({ isPerformingSignUp: false })
        setTimeout(() => {
            Alert.alert(
                strings.signUp.failed,
                errorMessage,
                [
                    { text: 'OK' },
                ]
            )
        }, 100);
    }

    _onSuccess = () => {
        this.setState({ isPerformingSignUp: false })
        setTimeout(() => {
            Alert.alert(
                strings.signUp.success.title,
                strings.signUp.success.message,
                [
                    { text: strings.signUp.success.button, onPress: () => this.props.onSignUpSuccess() }
                ]
            )
        }, 100)
    }
    
    componentWillMount()
    {
        this._preselectCountry()
    }

    _setupNoErrors(){
        this.setState({emailFieldError: false, passwordFieldError: false, countryFieldError: false, dateOfBirthError: false,
            termsAndConditionsError: false, privacyPolicyError: false, existingUserError: false})  
    }

    _validateSignUPForm(params) {
        this._setupNoErrors()
        let error = validateEmail(params['email'])
        if(error != null)
        {
            this.setState({emailFieldError: true, emailFieldErrorMessage: error})
           // return error
        }

        if (params.password == '') {
            error = 'Password must not be empty'
            this.setState({passwordFieldError: true, passwordFieldErrorMessage: 'Password must not be empty'})
        }

        if (params.details.country == '' || params.details.country == null) {
            error = 'Country must not be empty'
            this.setState({countryFieldError: true, countryFieldErrorMessage: 'Country must not be empty'})
        }

        if (params.details.dateOfBirth == '') {
            error = 'Date of Birth must not be empty'
            this.setState({dateOfBirthError: true, dateOfBirthErrorMessage: 'Date of Birth must not be empty'})
        }

        if(Utils.validateDateOfBirth(params.details.dateOfBirth) == false) {
            error = 'You need to be over 16 years old'
            this.setState({dateOfBirthError: true, dateOfBirthErrorMessage: 'You need to be over 16 years old to create an account'})
        }

        if (this.state.acceptedTermsAndConditions == false) {
            error = 'You must accept our terms & conditions'
            this.setState({termsAndConditionsError: true, termsAndConditionsErrorMessage: 'Terms must be accepted'})
        }

        if (this.state.acceptedPrivacyPolicy == false) {
            error = 'You must accept our privacy policy'
            this.setState({privacyPolicyError: true, privacyPolicyErrorMessage: 'Policy must be accepted'})
        }
        
        
        if (!error) {
            // if (params.password == '') {
            //     error = 'Password must not be empty'
            // } else 
            // if (params.details.dateOfBirth == '') {
            //     error = 'Date of Birth must not be empty'
            // } else 
            // if (params.details.country == '') {
            //     error = 'Country must not be empty'
            // } else 
            // if (this.state.acceptedTermsAndConditions == false) {
            //     error = 'You must accept our terms & conditions'
            //     this._showError(error)
            // } else 
            // if (this.state.acceptedPrivacyPolicy == false) {
            //     error = 'You must accept our privacy policy'
            //     this._showError(error)
            // } 
            // else {
            //     error = null
            // }
        }

        return error
    }

    _onSignUp = () => {

        //this.setState({tomitest:true})

        //return

        const signUpParameters = {
            email: this.state.email,
            password: this.state.password,
            details: {
                dateOfBirth: this.state.dateOfBirth,
                country: this.state.country['code'],
            },
            options: {
                marketingPermission: this.state.joinParadoxCommunity
            },
            sourceService: 'pdx_mobile_app'
        }

        const error = this._validateSignUPForm(signUpParameters)
        if (!error) {
            this.setState({ isPerformingSignUp: true })
            this._performSignUp(signUpParameters)
        } else {
            this.setState({ isPerformingSignUp: false })
           // this.setState({emailFieldErrorMessage: error})
          //  this._showError(error)
        }
    }

    _performSignUp = async (parameters) => {
        const response = await signUp(parameters)
        const success = (String(response.result).toLowerCase() == 'ok')
        if (success) {

            // Create and store session for next use..
            const createSessionResponse = await createSession(parameters.email, parameters.password)
            const session = JSON.stringify(createSessionResponse.session)
            const email = this.state.email
            AsyncStorage.setItem('session', session)
            AsyncStorage.setItem('username', email)

            setTimeout(() => { this._onSuccess() }, 100)
            return
        }
        console.log("sign up error")
        console.log(response)
        console.log(response.errorCode)
        if(response.errorCode=="account-exists")
        {
            this.setState({existingUserError: true, existingUserErrorMessage: "Account already exists"}) 
        }
        if(response.errorCode=="bad-password-length")
        {
            this.setState({passwordFieldError: true, passwordFieldErrorMessage: "Minimum 6 characters"}) 
        }
        if(response.errorCode=="ua")
        {
            this.setState({dateOfBirthError: true, dateOfBirthErrorMessage: "You need to be over 16 years old to create an account"}) 
        }

        if(response.errorCode=="invalid-email")
        {
            this.setState({emailFieldError: true, emailFieldErrorMessage: "Supplied email or host is blacklisted"}) 
        }
       

        this.setState({ isPerformingSignUp: false })
       // this._showError(response.errorMessage)
    }

    _showDatePicker = () => {
        return (this.state.showPicker) ?

            <PickerView
                selectedDate={this.state.selectedDateOfBirth}
                type={this.state.pickerType}
                dataSource={countries}
                onDoneButtonPress={(selectedValue) => {
                    Keyboard.dismiss()
                   // if (this.state.pickerType == 'date') {
                        this.setState({ dateOfBirth: Utils.transformDate(selectedValue), selectedDateOfBirth: selectedValue, showPicker: false })
                    //} else {
                        //this.setState({ country: selectedValue, showPicker: false })
                    //}
                }} /> : null
    }
    _showCountryPicker = () => {
        return (this.state.showCountryPicker) ?
            <PickerView
                selectedValue={this.state.country}
                type={this.state.pickerType}
                dataSource={countries}
                onDoneButtonPress={(selectedValue) => {
                    Keyboard.dismiss()
                    // if (this.state.pickerType == 'date') {
                    //     this.setState({ dateOfBirth: Utils.transformDate(selectedValue), showPicker: false })
                    // } else {
                      //  this.setState({ country: selectedValue, showPicker: false })
                        this.setState({ country: selectedValue, showCountryPicker: false })
                        
                  //  }
                }} /> : null
    }
    showPicker = async () => {
        if (Platform.OS == 'android') {
            Keyboard.dismiss()
            var maximumDate = new Date()
            var minDate = new Date()
            maximumDate.setFullYear(maximumDate.getFullYear())
            try {
                var newState = {};
                const { action, year, month, day } = await DatePickerAndroid.open({
                    //date: maximumDate,
                    date: this.state.selectedDateOfBirth,
                    maxDate: maximumDate,
                    mode: 'default'
                });
                if (action === DatePickerAndroid.dismissedAction) {
                    if(trim(this.state.dateOfBirth) == ''){
                        this.setState({
                            dateOfBirth: ''
                        });
                    }
                }
                if (action !== DatePickerAndroid.dismissedAction) {
                    // Selected year, month (0-11), day
                    let date = new Date(year, month, day)
                    this.setState({
                        dateOfBirth: year + '-' + Number(month + 1) + "-" + day,
                        selectedDateOfBirth: date
                    });
                }

            } catch ({ code, message }) {
                console.warn(`Error in example`);
            }
        } else {

            this.setState({ pickerType: 'date', showPicker: true })
        }

    };

    _preselectCountry()
    {
        publicIP().then(ip => {
            let queryParameters = 'contentType=geolocation&ipaddress=' + ip
                RequestService.getCode(queryParameters, '').then( response => {
                if(response._bodyText != undefined) {
                    this.setState({ country: countries.find( country => country.code === response._bodyText ) })
                    this.setState({ selectedCountry: countries.find( country => country.code === response._bodyText ) })
                }
                return response._bodyText;
            })
        })
        .catch(error => {
            console.log(error);
        })
    }

    _renderCountryView(color='', margin, countryCode = '') {
       
        // publicIP().then(ip => {
        //     let queryParameters = 'contentType=geolocation&ipaddress=' + ip
        //         RequestService.getCode(queryParameters, '').then( response => {
        //         if(response._bodyText != undefined) {
        //             this.setState({ country: countries.find( country => country.code === response._bodyText ) })
        //             this.setState({ selectedCountry: countries.find( country => country.code === response._bodyText ) })
        //         }
        //         return response._bodyText;
        //     })
        // })
        // .catch(error => {
        //     console.log(error);
        // })

        
        if (Platform.OS == 'ios') {
            return (
                <TouchableWithoutFeedback onPress={() => { this.setState({ pickerType: 'country', showPicker: true }) }}>
                    <FormTextInputField 
                        marginTop={margin}
                        color={color}
                        editable={false}
                        style={styles.textInputContainer}
                        placeholder={strings.country}
                        value={this.state.country['name']}
                        underlineColorAndroid='white'
                        onTouchStart={() => {
                          //  this.setState({ pickerType: 'country', showPicker: true })
                            this.setState({ pickerType: 'country', showCountryPicker: true })
                            
                            
                        }}
                    />
                </TouchableWithoutFeedback>
            )
        }
        return (
            <View style={[styles.textInputContainer, { marginTop: margin, paddingLeft: 8}]}>
                <Picker
                    style={{ height: 40, flex: 1, marginRight: 8, }}
                    selectedValue={this.state.selectedCountry.name}
                    prompt='Choose country'
                    mode='dialog'
                    onValueChange={(value, position) => { 
                        this.setState({ selectedCountry: countries[position] }) 
                        this.setState({ country: countries[position] }) 

                        }} >
                    {
                        
                        countries.map(item => {
                            return <Picker.Item key={item.name} label={item.name} value={item.name} />
                        })
                    }
                </Picker>
            </View>
        )
    }
    _renderDateView(color='', margin) {
        if (Platform.OS == 'ios') {
            return (
                <TouchableWithoutFeedback onPress={() => { this.setState({ pickerType: 'date', showPicker: true }) }}>
                    <FormTextInputField
                        marginTop={margin}
                        color={color}
                        editable={false}
                        style={styles.textInputContainer}
                        placeholder={strings.date_of_birth}
                        value={this.state.dateOfBirth}
                        underlineColorAndroid='white'
                        onTouchStart={() => {
                            this.setState({ pickerType: 'date', showPicker: true })
                        }}
                    />
                </TouchableWithoutFeedback>
            )
        } else {
            return (
                <FormTextInputField
                    marginTop={margin}
                    color={color}
                    autoCorrect={false}
                    editable={!this.state.showPicker}
                    placeholder={strings.date_of_birth}
                    value={this.state.dateOfBirth}
                    onFocus={this.showPicker}
                    underlineColorAndroid='white' />
            )

        }
    }


    // Lifecycle
    componentWillReceiveProps(nextProps) {
        this.setState({ mainImageUrl: nextProps.mainImageUrl })
    }

    _renderEmailInput(color = '', errorMessage){ return( 
                <FormTextInputField
                    value={this.state.email}
                    marginTop={1}
                    color={color}
                    keyboardType='email-address'
                    autoCorrect={false}
                    autoCapitalize='none'
                    placeholder={strings.email_address_placeholder}
                    onChangeText={(text) => { 
                        this.setState({  email: text }) 
                    }}
                    underlineColorAndroid='white' />  )}

    _renderEmailErrorInput(error) { return(
        this._renderErrorView(error, this._renderEmailInput(Error_Color))
    )}
    _renderEmailErrorInputExistingUser(error) { return(
        this._renderErrorExistingUserView(error, this._renderEmailInput(Error_Existing_User_Color))
    )}
    _renderPasswordInput(color=''){ return(
        <FormTextInputField
            value={this.state.password}
            color={color}
            marginTop={1}
            autoCorrect={false}
            placeholder={strings.password}
            secureTextEntry={true}
            onChangeText={(text) => { this.setState({ password: text }) }}
            underlineColorAndroid='white' /> 
    )}
    
    _renderPasswordErrorInput(error){ return(
        this._renderErrorView(error, this._renderPasswordInput(Error_Color))
    )}

    _renderErrorView(error, view){ return (
        <ErrorContainerView> 
            {view}
            <ErrorView>
                <ErrorText numberOfLines={2}>{error}</ErrorText> 
            </ErrorView>
        </ErrorContainerView> 
    )}
    _renderErrorExistingUserView(error, view){ return (
        <ErrorContainerExistingUserView> 
            {view}
        
            <View style={{flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
      }}>
        <View style={{ marginLeft:5 }} >
            <Text>{error}</Text>
            </View>
            <View style={{ marginRight:5}} >
                             <ActionButton
                                title1='1'
                                title='login'
                                backgroundColor='white'
                                style={{ color: Colors.DARK_GRAY_COLOR }}
                                onPress={() => this.props.onLogin(this.props)} />
            </View>
        </View>  
        </ErrorContainerExistingUserView> 
    )}
    
    
    _renderCheckboxErrorView(error){ return( 
        <ErrorCheckboxContainerView>
            <ErrorView>
                <ErrorText>{error}</ErrorText> 
            </ErrorView>
        </ErrorCheckboxContainerView>
    )}

    _renderPickerErrorView(error, view){ return (
        this._renderErrorView(error, view)
    )}

    render() {
        const accessView = this.state.isPerformingSignUp ? false : true
        const pointerEvents = accessView ? 'auto' : 'none'
        return (
            <ScrollViewContainer
                keyboardShouldPersistTaps='handled'
                pointerEvents={pointerEvents}
                showsVerticalScrollIndicator={false}
            >
                <SpinnerView visible={this.state.isPerformingSignUp} />
                    <ScrollViewContentView>
                        <LogoImageView url={this.state.mainImageUrl} />
                        <FormContainer>
                            <FormTitleContainer>
                                <FormTitleTextView numberOfLines={1}>
                                    {strings.signUp.title.toUpperCase()}
                                </FormTitleTextView>
                            </FormTitleContainer>
                            
                            {this.state.emailFieldError? this._renderEmailErrorInput(this.state.emailFieldErrorMessage) 
                                
                                : 
                                
                                this.state.existingUserError?  this._renderEmailErrorInputExistingUser(this.state.existingUserErrorMessage) 
                                :  

                                this._renderEmailInput()  } 

                            {/* {this.state.existingUserError ? this._renderEmailErrorInputExistingUser(this.state.existingUserErrorMessage)  : this._renderEmailInput()  } */}
                             
                            <EmailDescriptionText numberOfLines={0}>
                                {strings.signUp.email_description}
                            </EmailDescriptionText>

                            {this.state.passwordFieldError? this._renderPasswordErrorInput(this.state.passwordFieldErrorMessage) : this._renderPasswordInput()    }

                            {/* {this.state.countryFieldError?   this._renderCountryViewError(this.state.countryFieldErrorMessage) : this._renderCountryView('',10)} */}
                            {this.state.countryFieldError?   this._renderPickerErrorView(this.state.countryFieldErrorMessage, this._renderCountryView(Error_Color,1)) : this._renderCountryView('',10)}


                            {this.state.dateOfBirthError?   this._renderPickerErrorView(this.state.dateOfBirthErrorMessage, this._renderDateView(Error_Color, 1)) : this._renderDateView('',10)}
                           
                            {/* {this._renderDateView()} */}
                            <FormSubContainer>
                                <CheckBoxView
                                    title={strings.join_paradox_comunity}
                                    selected={this.state.joinParadoxCommunity}
                                    onCheckBoxButtonPress={() => {
                                        this.setState({ joinParadoxCommunity: !this.state.joinParadoxCommunity })
                                    }}
                                />
                                <CheckBoxView
                                    title={strings.terms_of_service_checkbox_text}
                                    selected={this.state.acceptedTermsAndConditions}
                                    onCheckBoxButtonPress={() => {
                                        this.setState({ acceptedTermsAndConditions: !this.state.acceptedTermsAndConditions })
                                    }}
                                />
                                {this.state.termsAndConditionsError ? this._renderCheckboxErrorView(this.state.termsAndConditionsErrorMessage) : null}
                                <ReadButton
                                    title={strings.signUp.read_terms_of_service}
                                    onPress={() => this.props.onTermsAndConditionButtonPress(this.props)}
                                />
                                <CheckBoxView
                                    title={strings.privacy_policy_checkbox_text}
                                    selected={this.state.acceptedPrivacyPolicy}
                                    onCheckBoxButtonPress={() => {
                                        this.setState({ acceptedPrivacyPolicy: !this.state.acceptedPrivacyPolicy })
                                    }}
                                />
                                {this.state.privacyPolicyError ? this._renderCheckboxErrorView(this.state.privacyPolicyErrorMessage) : null}
                                <ReadButton
                                    title={strings.signUp.read_privacy_policy}
                                    onPress={() => this.props.onPrivacyPolicyButtonPress(this.props)}
                                />
                                <ActionButtonContainer>
                                    <ActionButton
                                        title={strings.login_button_create_account}
                                        backgroundColor={Colors.GREEN_COLOR}
                                        onPress={this._onSignUp}
                                    />
                                </ActionButtonContainer>
                            </FormSubContainer>
                        </FormContainer>
                        <LineFooter />
                        <BottomContainer>
                            <AlreadyHaveAcountTextView>
                                {strings.signUp.already_have_account}
                            </AlreadyHaveAcountTextView>
                            <ActionButton
                                title='login'
                                backgroundColor='white'
                                style={{ color: Colors.DARK_GRAY_COLOR }}
                                onPress={() => this.props.onLogin(this.props)} />
                        </BottomContainer>
                        <FooterView />
                    </ScrollViewContentView>
                    {this._showDatePicker()}
                    {this._showCountryPicker()}
                    
            </ScrollViewContainer>
        )
    }
}

// Styles
var styles = StyleSheet.create({
    textInputContainer: {
        height: 40,
        backgroundColor: 'white',
        borderRadius: 2,
        paddingLeft: 16,
        //borderColor:'red',
        //borderWidth:2,
    },
})