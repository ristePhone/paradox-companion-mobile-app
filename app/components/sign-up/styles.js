import glamorous from 'glamorous-native'
import { Dimensions } from 'react-native'
let { height, width } = Dimensions.get('window')
import Colors from '../../config/colors'

const FONT_COLOR = '#EEF0F2'
const Error_Color = '#fe0642'
const Error_Existing_User_Color = '#ffb400'

export const ScrollViewContainer = glamorous.scrollView({
    flex: 1,
    paddingTop: 50,
    paddingHorizontal: 10,
    backgroundColor: Colors.BACKGROUND_COLOR,
})

export const ScrollViewContentView = glamorous.view({
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 2
})

export const FormContainer = glamorous.view({
    marginHorizontal: 10,
    paddingHorizontal: 16,
    paddingVertical: 20,
    width: width - 2 * 10,
    backgroundColor: Colors.DARK_GRAY_COLOR,
    borderRadius: 4
})

export const FormTitleContainer = glamorous.view({
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 20,
})

export const FormTitleTextView = glamorous.text({
    fontFamily: 'SourceSansPro-Regular',
    color: 'white',
    textAlign: 'center',
    fontSize: 15,
})

export const EmailDescriptionText = glamorous.text({
    marginVertical: 6,
    fontFamily: 'Arial',
    fontSize: 11,
    textAlign: 'left',
    color: '#ababab'
})

export const FormInnerContainer = glamorous.view({
    flex: 1,
    justifyContent: 'center'
})

export const FormTextInputField = glamorous.textInput(props => ({
    height: 40,
    backgroundColor: 'white',
    borderRadius: 2,
    paddingLeft: 16,
    marginTop: props.marginTop ? props.marginTop : 10,
    borderColor: props.color ? props.color : '',
    borderWidth:  2,
    color:'black'    
}))

export const FormSubContainer = glamorous.view({
    flex: 1,
    marginTop: 10,
    marginHorizontal: 16,
}) 

export const CheckBoxContainer = glamorous.view({
    marginVertical: 10,
    flexDirection: 'row',
    alignItems: 'center'
})

export const ActionButtonContainer = glamorous.view({
    justifyContent: 'center',
    marginTop: 25,
    flexDirection: 'row'
})

export const FooterView = glamorous.view({
    width: 1,
    height: 100
})
export const LineFooter = glamorous.view({
    width: 1,
    height: 10
})

export const BottomContainer = glamorous.view({
    flex: 1,
    marginHorizontal: 30,
    marginVertical: 20,
    paddingHorizontal: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
})

export const AlreadyHaveAcountTextView = glamorous.text({
    flex: 1,
    fontFamily: 'Arial',
    color: 'white',
    fontSize: 13
})

export const ErrorContainerView = glamorous.view({
    marginTop:10,
    backgroundColor: Error_Color,
    height:75, 
    borderRadius:2,
    padding:2
})
export const ErrorContainerExistingUserView = glamorous.view({
    marginTop:10,
    backgroundColor: Error_Existing_User_Color,
    height:90, 
    borderRadius:2,
    padding:2
})
export const ErrorCheckboxContainerView = glamorous.view({
    backgroundColor: Error_Color,
    height:30, 
    borderRadius:15,
    marginBottom:5,
})
export const ErrorView = glamorous.view(props => ({
     backgroundColor: props.error ? props.color : '',
     height:25, 
     alignItems: 'center',
     justifyContent: 'center',
 }))
 export const ErrorExistinUserView = glamorous.view(props => ({
    backgroundColor: props.error ? props.color : '',
    height:55, 
    marginLeft:5,
    alignItems: 'center',
    justifyContent: 'center',
}))

export const ErrorText = glamorous.text({
    // flex: 1,
    fontFamily: 'Arial',
    color: 'white',
    fontSize: 13,
    marginTop:5,
    paddingVertical: 7,
    paddingHorizontal: 7,
    color:'white'
})

export const ErrorTextExistinUser = glamorous.text({
    flex: 1,
    fontFamily: 'Arial',
    color: 'black',
    fontSize: 13,
    marginTop:5,
})

 export const FormTextInputErrorField = glamorous.textInput(props => ({
    height: 40,
    backgroundColor: 'white',
    borderRadius: 2,
    paddingLeft: 16,
    borderColor:Error_Color,
    borderWidth:2,
    
}))
// export const ViewRow = glamorous.view({
//     flex: 1,
//     flexDirection: 'row',
//     justifyContent: 'space-between',
//     alignItems: 'center'
// })


 
