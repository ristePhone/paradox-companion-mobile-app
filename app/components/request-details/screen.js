import React, { Component } from 'react'
import { View, Text, Platform, FlatList, ScrollView, Keyboard, Image, TouchableOpacity, AppState, Alert } from 'react-native'

// services 
import RequestService from '../../services/request-service'
import Utils from '../../services/utils-service'

// constants
import Configuration from '../../config/environment/config'
import { strings } from '../../config/localization'
import Colors from '../../config/colors'
import { configurationSetup } from '../../config/config'
import RNFetchBlob from 'rn-fetch-blob'
import ActionButton from '../views/action-button'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import EntypoIcons from 'react-native-vector-icons/Entypo'
import logo from '../../resources/paradox-logo1.png'
import Entypo from 'react-native-vector-icons/Entypo'
import {
    FormTextInputField, ActionButtonContainer, RowContainer, RowTitleView, SeparatorContainer,
    SeparatorContainer1, RowTextBody, InputCommentContainer, RowInContainer, RowSeparatorContainer, RowInConatainerSeparator,
    RowDetailsView, RowInContainer1, RowContainer1, RowSeparatorContainer1, CommentSeparator, CommentSeparatorText, GrayRow, WhiteRow,
    RequestHeaderTextView, AttachmentContainer, AttachmentItem
} from './styles'
import ZendeskService from '../../services/zendesk-service'
import SpinnerView from '../common/spinner/ui'
import Ionicons from 'react-native-vector-icons/Ionicons'

export default class RequestDetailsScreen extends Component {
    constructor(props) {
        super(props)
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this))
        this.state = {
            id: '',
            comments: [],
            comment: '',
            requesterId: '',
            loading: true,
            apiIntiRendering: true,
            closeRequestAllowed: false,
            spinnerVisible: false,
            appState: AppState.currentState,
            attachmentList:[
               
            ],
            uploads:[]
        }
    }

    onNavigatorEvent(event) {
        switch (event.id) {
            case 'didDisappear':
                this._scrollToTop()
                break;
        }
    }

    _init() {
        this._showDetails(this.props.item.id, true)
        this.setState({
            id: this.props.item.id,
            requesterId: this.props.item.requester_id,
            closeRequestAllowed: this.props.item.can_be_solved_by_me
        })
    }

    componentWillMount() {

        this._init()
        //todo tomi: implement model map
    }

    componentDidMount() {
        AppState.addEventListener('change', this._handleAppStateChange);
    }

    componentWillUnmount() {
        AppState.removeEventListener('change', this._handleAppStateChange);
    }

    _handleAppStateChange = (nextAppState) => {
        if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
            //App has come to the foreground!
            this._init() //check for new comments
        }
        this.setState({ appState: nextAppState });
    }
    // ---------------------------------------------------------------------------------------
    // helpers start
    // ---------------------------------------------------------------------------------------
    _startLoading() {
        Keyboard.dismiss()
        this.setState({ loading: true, spinnerVisible: true })
    }

    _endLoading() {
        this.setState({ loading: false, spinnerVisible: false })
    }
    _checkCommentState() {
        if (this.state.comment.trim().length == 0) {
            Alert.alert(strings.request_details_validation_enter_comment)
            return true
        }
        return false
    }
    // ---------------------------------------------------------------------------------------
    // helpers end
    // ---------------------------------------------------------------------------------------
    async _showDetails(id, initState) {
        this._startLoading()
        let url = Configuration.common.ZENDESK_REQUEST_DETAILS(id)
        var headers = await ZendeskService.setHeaders()
        let result = await RequestService.get(url, headers)
        this.setState({ comments: result.comments, apiIntiRendering: false })
        this._endLoading()
        if (initState == false) {
            this.scrollToEnd()
        }
    }

    closeRequest = () => {
        this._closeRequest()
    }

    async _closeRequest(id) {
        if (!this.state.closeRequestAllowed) {
            Alert.alert("You don't have permissions to close this request.")
            return
        }

        if (this._checkCommentState())
            return

        this._startLoading()
        let url = Configuration.common.ZENDESK_REQUEST_CLOSE(this.state.id)
        var headers = await ZendeskService.setJsonHeaders()
        var body = JSON.stringify({
            request: { comment: { body: this.state.comment }, solved: true }
        })
        let result = await RequestService.put(url, headers, body)

        this.props._navigateToFeedback(this.props)
        this._endLoading()
    }

    async _createNewComment() {
        if (this._checkCommentState())
            return

        this._startLoading()
        let url = Configuration.common.ZENDESK_REQUEST_UPDATE_COMMENT(this.state.id)
        var headers = await ZendeskService.setJsonHeaders()
        var body = JSON.stringify({
            request: { comment: { body: this.state.comment, "uploads": this.state.uploads }  }
        })
        let result = await RequestService.put(url, headers, body)
        this._endLoading()
        this._showDetails(this.state.id, false)
        this.setState({ comment: '', uploads: []})
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ mainImageUrl: nextProps.mainImageUrl })
    }
    _onPressAttachment = async () =>  
    {
        var ImagePicker = require('react-native-image-picker');

        // More info on all the options is below in the README...just some common use cases shown here
        var options = {
          title: 'Select Avatar',
          customButtons: [
            {name: 'fb', title: 'Choose Photo from Facebook'},
          ],
          storageOptions: {
            skipBackup: true,
            path: 'images'
          }
        };
        
        ImagePicker.launchImageLibrary(options, async (response)  => {
            console.log("response is")
            console.log(response)
            console.log(response.didCancel)
            if(response.didCancel)
            return
            
                this._startLoading()
                // Same code as in above section!
                let source = { uri: response.uri };
                let source1 = { uri: 'data:image/jpeg;base64,' + response.data };
                console.log('after select attach response is ')
                console.log(response)
               // console.log(source1)
                //console.log(response.uri)
                //this.setState({
                   // url: source,
                    //url1: source1,
                    //response: response
                //})
    
                var attList  = this.state.attachmentList
                attList.push(response)
                this.setState({attachmentList: attList})
                
                var attach = null
                //const  attach  = await this.sendImageToZendesk(response.origURL)
                if(Platform.OS == 'android'){
                    let dirs = RNFetchBlob.fs.dirs
                    // const attach  = await this.sendImageToZendesk(dirs.DocumentDir +response.path)
                     attach  = await this.sendImageToZendesk(response.uri)
                    // const attach  = await this.sendImageToZendesk(response.uri)
                     console.log("attachis")
                     console.log(attach)
               }
               else{
                    attach  = await this.sendImageToZendesk(response.origURL)
               }
                console.log(attach)
                console.log(attach.upload.token)
    
                let up = this.state.uploads
                up.push(attach.upload.token)
                this.setState({uploads:up})
    
                console.log("attachment list")
                console.log(this.state.attachmentList)
                console.log("uploads list")
                console.log(this.state.uploads)
                this._endLoading()
            
          
        
          });

          //return





       


        // ImagePicker.launchImageLibrary(options, (response) => {
        //     console.log("result from picker is")
        //     console.log(response)
        //     console.log(response.fileName)
        //     console.log(response.origURL)
        //     //this._addAttach(response.origURL)
        //     //var attach  = this.state.attachmentList
        //     //attList.push(response.origURL)
        //     this.setState({attachmentList: response})
        // })
    }

    async sendImageToZendesk (origURL) {
       // if (imageURL) {
            const { ZENDESK_BASIC_URL } = await configurationSetup()
            const baseUrl = `${ZENDESK_BASIC_URL}/api/v2/uploads.json?filename=attachment.png`
            const headers = {
                'Autorization': 'Basic cmlzdGUuc3Bhc2Vza2lAZGF0YWpvYi5zZS8yVzVKRGkxbGFLYVpnU0JHSVZWdWN4cmh3YUVNQ0t0b1AzMXkwWnFCOjJXNUpEaTFsYUthWmdTQkdJVlZ1Y3hyaHdhRU1DS3RvUDMxeTBacUI=',
                'Accept': 'application/json'
            }
        
           // const wrapFileURL = RNFetchBlob.wrap(imageURL)
            const wrapFileURL = RNFetchBlob.wrap(origURL)
            return RNFetchBlob.fetch('POST',
                baseUrl,
                headers,
                wrapFileURL)
                .then(response => {
                    //console.log(response)
                    return response.json()
                })
                .then(responseData => {
                    //console.log(responseData)
                    return responseData
                })
                .catch(error => {
                    //console.log(responseData)
                    return { error: error }
                })
        //}
    
        return { error: 'No image url provided'}
    }

    _renderRow = ({ item, index }) => { return (
            <View style={{ flex:1}} >
                  <RowSeparatorContainer>
                        <RowInConatainerSeparator></RowInConatainerSeparator>
                </RowSeparatorContainer> 

               

                <RowContainer>
                
                    {
                        item.author_id == this.state.requesterId ?
                            <TouchableOpacity
                                style={{
                                    borderWidth: 1,
                                    borderColor: 'rgba(0,0,0,0.2)',
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    width: 30,
                                    height: 30,
                                    backgroundColor: '#fff',
                                    borderRadius: 30 }}>
                                <EntypoIcons size={20} color='black' name='user' />
                            </TouchableOpacity>
                            :
                            <TouchableOpacity
                                style={{
                                    borderWidth: 1,
                                    borderColor: 'rgba(0,0,0,0.2)',
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    width: 30,
                                    height: 30,
                                    backgroundColor: '#fff',
                                    borderRadius: 30,
                                }}>
                                <Image style={{ height: 20, width: 20, }} source={logo} resizeMode='cover' />
                            </TouchableOpacity>
                    }
                    <View style={{
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'space-between',
                        paddingLeft: 10
                    }}>
                        {
                            item.author_id == this.state.requesterId ?
                                <View>
                                    <View style={{ width: 150, height: 20, backgroundColor: Colors.DARK_GRAY_COLOR }}>
                                        <Text style={{ color: '#adadad' }}>You</Text>
                                    </View>
                                </View>
                                :
                                <View>
                                    <View style={{ width: 150, height: 20, backgroundColor: Colors.DARK_GRAY_COLOR }}>
                                        <Text style={{ color: '#adadad' }}>Paradox</Text>
                                    </View>
                                    <View style={{ width: 150, height: 20, backgroundColor: Colors.DARK_GRAY_COLOR }}>
                                        <Text style={{ color: '#adadad' }}>Support</Text>
                                    </View>
                                </View>
                        }
                    </View>
                    <RowTitleView numberOfLines={2}>{Utils.formatDateToGMTOffset(item.created_at)}</RowTitleView>
                </RowContainer>
                
                <RowTextBody numberOfLines={50}>{item.body}</RowTextBody>
               
                {/* <SeparatorContainer1 /> */}

               
            </View>
    )}

    _listHeaderComponent = () => { //todo tomi: implement new class for request details
        return (
            <View>
                <RowContainer1>
                    <RowInContainer>
                        <RowTitleView>
                            <GrayRow>{strings.request_list_label_subject}</GrayRow>
                            <WhiteRow>{this.props.item.subject}</WhiteRow>
                        </RowTitleView>
                    </RowInContainer>
                    
                    <RowInContainer>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                            <RowDetailsView>
                                <GrayRow>{strings.request_list_label_id} </GrayRow>
                                <WhiteRow>#{this.props.item.id}</WhiteRow>
                            </RowDetailsView>
                            <RowDetailsView>
                                {Utils.formatDateString(this.props.item.created_at)}, {Utils.formatHour(this.props.item.created_at)}h
                            </RowDetailsView>
                            <RowDetailsView>
                                <GrayRow>{strings.request_list_label_state} </GrayRow>
                                <WhiteRow>{this.props.item.status} </WhiteRow>
                            </RowDetailsView>
                        </View>
                    </RowInContainer>
                    
                </RowContainer1>
                <CommentSeparator>
                    <CommentSeparatorText>
                        Comments:
                    </CommentSeparatorText>
                </CommentSeparator> 

                {/* <RowSeparatorContainer>
                        <RowInConatainerSeparator></RowInConatainerSeparator>
                </RowSeparatorContainer> */}
            </View>

    )}

    _renderSeparatorComponent = () => {
        return (
            <View>
                <RowContainer><Text>test</Text></RowContainer>
            </View>
    )}
    _renderFooterComponent = () => { return (
            this.state.apiIntiRendering ? null :
                <View style={{ marginBottom: 70 }}>

             {/* <InputCommentContainer> */}
                {/* <AttachmentItem>test.png</AttachmentItem>
                <AttachmentItem>test.png</AttachmentItem>
                <AttachmentItem>test.png</AttachmentItem> */}
                {this.state.attachmentList.map(item => {       
                                    return  <AttachmentItem>{item.fileName}
                                    </AttachmentItem>
                                })}
                <TouchableOpacity onPress={(e)=>{this._onPressAttachment(e)}}>
                        <AttachmentContainer>
                            <Entypo size={30} color='white' name='attachment' />
                            <RequestHeaderTextView>Add file here</RequestHeaderTextView>
                        </AttachmentContainer>
                </TouchableOpacity>
                {/* </InputCommentContainer> */}

                    <InputCommentContainer>
                        <FormTextInputField
                            multiline={true}
                            value={this.state.comment}
                            autoCorrect={false}
                            onFocus={() => this.setSel()}
                            onBlur={() => this.setBack()}
                            innerRef={(node) => { this.inputNode = node; }}
                            onFocus={() => this.setSel('inputArea')}
                           // returnKeyType={'done'}
                            //onSubmitEditing={() => Keyboard.dismiss()}
                            placeholder={strings.request_details_label_comment}
                            onChangeText={(text) => { this.setState({ comment: text }) }}
                            underlineColorAndroid='white' />
                    </InputCommentContainer>
                    <ActionButtonContainer >
                        <ActionButton
                            disabled={this.state.loading}
                            title={strings.request_details_button_send}
                            backgroundColor={Colors.GREEN_COLOR}
                            onPress={() => this._createNewComment()} />
                    </ActionButtonContainer>
                 {this.state.closeRequestAllowed?  
                    <ActionButtonContainer >
                        <ActionButton
                            disabled={this.state.loading}
                            title={strings.request_details_button_close}
                            backgroundColor={Colors.GREEN_COLOR}
                            onPress={() => this.closeRequest()} />
                    </ActionButtonContainer>
                     :null}  

                </View>
            // {/* </View> */}
    )}
    setBack() {
        if (Platform.OS == 'ios') {
            setTimeout(() => {
                this.myFlatList.scrollToEnd({ animated: true });
            }, 100)
    }}

    scrollToEnd() {
        if (Platform.OS == 'android') {
            //try{
            setTimeout(() => {
                try {
                    this.refs._scrollView.scrollToEnd({ animated: true })
                }
                catch (err) {
                }

            }, 500)
            //}
            //catch(err){
            //  }
        } else {
            setTimeout(() => {
                //this.myFlatList.scrollToIndex({ index: 0, animated: true })
                this.myFlatList.scrollToEnd({ animated: true });
            }, 500);
        }
    }
    _scrollToTop() {
        if (Platform.OS == 'ios') {
            this.myFlatList.scrollToPosition(0, 0)
            return
        }
        this.refs._scrollView.scrollTo({ x: 0, y: 0, animated: true })
    }
    setSel() {
        this.scrollToEnd()
    }

    // Lifecycle
    render() {
        if (Platform.OS == 'android') {
            Keyboard.addListener('keyboardDidHide', () => {
                try { this.inputNode.blur() }
                catch (err) { }

                this.props.navigator.toggleTabs({
                    to: 'shown',
                    animated: false
                });
            });
            Keyboard.addListener('keyboardDidShow', () => {
                this.props.navigator.toggleTabs({
                    to: 'hidden',
                    animated: false
                });
                this.scrollToEnd()
            });

        }
        return (
            Platform.OS == 'ios' ?
                <KeyboardAwareScrollView
                    keyboardShouldPersistTaps='handled'
                    ref={(list) => this.myFlatList = list}
                    automaticallyAdjustContentInsets={false}
                    enableResetScrollToCoords={false} extraScrollHeight={50}>
                    <SpinnerView visible={this.state.spinnerVisible} />
                    <View style={{ flex: 1, marginTop: 20 }}>
                        <FlatList
                            ref={(ref) => { this.flatListRef = ref; }}
                            indicatorStyle='white'
                            style={{ flex: 1, paddingHorizontal: 10, backgroundColor: Colors.BACKGROUND_COLOR }}
                            data={this.state.comments}
                            keyExtractor={(item, index) => `${item}-${index}`}
                            renderItem={this._renderRow}
                            ListHeaderComponent={this._listHeaderComponent} />
                        <View>
                            {this._renderFooterComponent()}
                        </View>
                    </View>
                </KeyboardAwareScrollView>
                :
                <ScrollView ref="_scrollView" keyboardShouldPersistTaps='handled'>
                    <View style={{ flex: 1, marginTop: 20 }}>
                        <SpinnerView visible={this.state.spinnerVisible} />
                        <FlatList
                            indicatorStyle='white'
                            style={{ flex: 1, paddingHorizontal: 10, backgroundColor: Colors.BACKGROUND_COLOR }}
                            data={this.state.comments}
                            keyExtractor={(item, index) => `${item}-${index}`}
                            renderItem={this._renderRow}
                            ListHeaderComponent={this._listHeaderComponent}/>
                        <View>
                            {this._renderFooterComponent()}
                        </View>
                    </View>
                </ScrollView>
        )
    }
}
