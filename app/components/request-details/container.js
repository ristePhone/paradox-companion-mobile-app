import { connect } from 'react-redux'
//import CreateRequestScreen from '../create-request/screen'
import RequestDetailsScreen from '../request-details/screen'
import * as Constants from '../../utils/constants'
import { signUp, createSession, validateEmail } from '../../services/authentication'
import Routes from '../../config/routes'
import { store } from '../../reducers/index'
import { UPDATE_ROOT_CONTROLER, UPDATE_USER_LOGGED_IN_STATUS } from '../../actions/action-types'
import { strings } from '../../config/localization'

const mapStateToProps = state => ({
    mainImageUrl: state.contentfulAPIcontent.mainImageUrl
})

const mapDispatchToProps = dispatch => ({
    _navigateToFeedback: props =>{
        props.navigator.resetTo({
            screen: Routes.FEEDBACK,
            title: strings.request_navigator_title_open_requests,
            navigatorStyle: {
                navBarHidden: false,
            },
            passProps: {
                notify: true,
                notifyMessage: strings.request_details_notify_request_closed
            },
        })
    }
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RequestDetailsScreen)