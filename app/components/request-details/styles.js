import glamorous from 'glamorous-native'
import { Dimensions } from 'react-native'
let { height, width } = Dimensions.get('window')
import Colors from '../../config/colors'
const FONT_COLOR = '#EEF0F2'
import * as Animatable from 'react-native-animatable';

export const ImageWrapperContainer = glamorous.view({
    flex: 1,
    height: 83,
    marginVertical: 15,
    paddingHorizontal: 10,
    paddingVertical: 15
})
export const ImageContainer = glamorous.image({
    flex: 1,
})
export const ScrollViewContentView = glamorous.view({
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
})

export const RowContainer = glamorous.view({
    flexDirection: 'row',
    backgroundColor: Colors.DARK_GRAY_COLOR,
    alignItems: 'center',
    justifyContent: 'space-between',
    flex: 1,
    height: 50,
    borderRadius: 4,
    paddingLeft: 14
})

export const RowExpandedContainer = Animatable.createAnimatableComponent(glamorous.view({
    flex: 1, 
    height: 100,
    alignItems: 'center',
    justifyContent: 'center'
}))

export const RowTitleView = glamorous.text({
    color: '#adadad',
    textAlign: 'left',
    fontSize: 14,
    fontFamily: 'Arial',
    marginRight: 20
})
export const RowTextBody = glamorous.text({
    backgroundColor: Colors.DARK_GRAY_COLOR,
    color: '#eeeeee',
    textAlign: 'left',
    fontSize: 16,
    fontFamily: 'Arial',
    //marginRight: 20
    //padding:20
    paddingHorizontal:20,
    paddingBottom:20
})
export const ScrollViewContainer = glamorous.scrollView({
    flex: 1,
    paddingTop: 25,
    paddingHorizontal: 16,
})
export const SeparatorContainer = glamorous.view({
    height: 10,
    width: 1
})

export const SeparatorContainer1 = glamorous.view({
    height: 2,
    width: 1
})

export const ActionButtonContainer = glamorous.view({
   // justifyContent: 'space-between',
    justifyContent: 'center',
    marginTop: 25,
    marginTop: 25,
    flexDirection: 'row',
    //marginLeft:25,
    paddingHorizontal:30,
    flex:1
})

export const InputCommentContainer = glamorous.view({
    justifyContent: 'center',
    marginTop: 25,
    flexDirection: 'row'
})


export const FormTextInputField = glamorous.textInput(
    {
        textAlignVertical: 'top',
        height: 70,
        backgroundColor: 'white',
        borderRadius: 4,
        paddingLeft: 8,
        marginTop: 8,
        width: width - 2*30
    }
)

// header todo tomi implement new component (common for requst list and request details)
export const RowInContainer = glamorous.view({
    flexDirection: 'row',
    backgroundColor: Colors.DARK_GRAY_COLOR,
    alignItems: 'center',
    justifyContent: 'space-between',
    flex: 1,
    padding: 14,
    borderRadius:6
})

// export const RowSeparatorContainer = glamorous.view({
//     flexDirection: 'row',
//     justifyContent: 'center',
//     paddingHorizontal:15

// })
export const RowSeparatorContainer1 = glamorous.view({
    flexDirection: 'row',
    justifyContent: 'center',
    paddingHorizontal:15

})

// export const RowInConatainerSeparator = glamorous.view({
//     flexDirection: 'row',
//     backgroundColor: Colors.WHITE_GRAY,
//     justifyContent: 'center',
//     flex: 1,
//     height: 1 / 2,
// })

export const RowDetailsView = glamorous.text({
    color: '#ffffff',
    textAlign: 'left',
    fontSize: 11.5,
    fontFamily: 'Arial',
    //marginRight: 20
})
export const GrayRow = glamorous.text({
    color: '#adadad',
})
export const WhiteRow = glamorous.text({
    color: '#eeeeee',
})
// export const RowInContainer1 = glamorous.view({
//     flexDirection: 'column',
//     backgroundColor: Colors.DARK_GRAY_COLOR,
//     justifyContent: 'space-between',
//     flex: 1,
//     height: 101,
//     borderRadius:6
// })
export const RowContainer1 = glamorous.view({
    flexDirection: 'column',
    backgroundColor: Colors.DARK_GRAY_COLOR,
    justifyContent: 'space-between',
    flex: 1,
    height: 101,
   // borderRadius:6
})

export const CommentSeparator = glamorous.view({
    flexDirection: 'row',
    backgroundColor: Colors.DARK_GRAY_COLOR,
  // justifyContent: 'space-between',
    flex: 1,
    height: 30,
    paddingLeft:14
   //    backgroundColor: 'pink'
   // borderRadius:6
})

export const CommentSeparatorText = glamorous.text({
    color: '#cbced3',
    textAlign: 'left',
    fontSize: 16,
    fontFamily: 'Arial',
    marginRight: 20
})

export const RowSeparatorContainer = glamorous.view({
    flexDirection: 'row',
    justifyContent: 'center',
    paddingHorizontal:15, 
    //flex:1,

})
    
export const RowInConatainerSeparator = glamorous.view({
   // flexDirection: 'row',
    backgroundColor: Colors.REQUEST_LIST_DETAILS_WHITE, 
    backgroundColor: '#222222', //todo tomi: put colors in constans
    width:100,
    //justifyContent: 'center',
    //flex: 1,
    height: 2,
})

export const RequestHeaderTextView = glamorous.text({
    color: 'white',
    fontSize: 12,
   // marginTop:15
})

export const AttachmentContainer = glamorous.view({
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.BACKGROUND_COLOR,
   // width:200,
    height: 100,
    borderRadius: 2,
    marginTop:10
})

export const AttachmentItem = glamorous.text({
    color: 'white',
    fontSize: 12,
    marginTop:10,
    marginLeft:12
})
