// WIX Navigation
import { Navigation } from 'react-native-navigation'
import Routes from './config/routes'

// List of all screens that will be used in our app
import Login from './components/login/container'
import SignUp from './components/sign-up/container'
import Feedback from './components/feedback/container'
import Web from './components/webview/screen'
import NewsFeed from './components/news-feed/container'
import NewsDetails from './components/news-details/container'
import Home from './components/home/container'
import EventDetails from './components/event-details/container'
import More from './components/more/container'
import RetryData from './components/screens/retry-data/container'
// WIKI
import WikiPage from './components/WikiPage/container'
import WikiDetails from './components/wiki-details/container'
import WikiSection from './components/wiki-section/contatiner'
// requests
import CreateRequest from './components/request-create-category/container'
import CreateRequest3 from './components/request-create/container'
import RequestList from './components/requests-list/container'
import RequestDetails from './components/request-details/container'
import RequestFeedback from './components/request-feedback/container'
//import RequestFeedback from './components/attachment/container'
/**
 * 
 *  This function is called only once, so the app will know which screens should use
 *  Additional screen registrations need to be added in the function body
 * 
 * @param {redux_store} store 
 * @param {redux_provider_component} provider 
 */

const registerScreens = (store, provider) => {
    Navigation.registerComponent(Routes.LOGIN, () => Login, store, provider)
    Navigation.registerComponent(Routes.SIGN_UP, () => SignUp, store, provider)
    Navigation.registerComponent(Routes.FEEDBACK, () => Feedback, store, provider)
    Navigation.registerComponent(Routes.INFO, () => Info, store, provider)
    Navigation.registerComponent(Routes.WEB, () => Web, store, provider)
    Navigation.registerComponent(Routes.NEWS, () => NewsFeed, store, provider)
    Navigation.registerComponent(Routes.HOME, () => Home, store, provider)
    Navigation.registerComponent(Routes.NEWS_DETAILS, () => NewsDetails, store, provider)
    Navigation.registerComponent(Routes.EVENT_DETAILS, () => EventDetails, store, provider)
    Navigation.registerComponent(Routes.WIKI, () => WikiPage, store, provider)
    Navigation.registerComponent(Routes.MORE, () => More, store, provider)
    Navigation.registerComponent(Routes.RETRY, () => RetryData, store, provider)
    // WIKI
    Navigation.registerComponent(Routes.WIKI, () => WikiPage, store, provider)
    Navigation.registerComponent(Routes.WIKI_DETAILS, () => WikiDetails, store, provider)
    Navigation.registerComponent(Routes.WIKI_SECTION, () => WikiSection, store, provider)
    // REQUESTS ZENDESK
    Navigation.registerComponent(Routes.REQUEST_CREATE, () => CreateRequest, store, provider)
    Navigation.registerComponent(Routes.REQUEST_CREATE3, () => CreateRequest3, store, provider)
    Navigation.registerComponent(Routes.REQUESTS_LIST, () => RequestList, store, provider)
    Navigation.registerComponent(Routes.REQUEST_DETAILS, () => RequestDetails, store, provider)
    Navigation.registerComponent(Routes.REQUEST_FEEDBACK, () => RequestFeedback, store, provider)
}

export {
    registerScreens
}