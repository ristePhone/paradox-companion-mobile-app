# Project setup

### Mac OSX

Open terminal and do the following:

Install `Homebrew` package manager

(_Note: We will use this to install our other dependencies_)

    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" 

Install `node.js`

    brew install node

Install `npm` or `yarn`

    brew install yarn

Create `<destination_folder>`

    # This step can be done with/out terminal
    # Create folder
    mkdir <path_to_destination_folder>

    # Navigate to the folder
    cd <path_to_destination_folder>

Initialize `git`

    git init`

Clone project repo from bitbucket

    git clone git@bitbucket.org:paradox-tad/pdx-mobile-app.git

If you don't have access to the repository, create `ssh key`.
You can find the instructions in the following link:

https://help.github.com/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent/

Fetch project

    git fetch
    git checkout develop

Install `node_modules`

    # 1. Navitate inside the folder
    cd pdx-mobile-app 

    # or if you are not already in the destination folder
    cd <path_to_destionation_folder>/pdx-mobile-app 
  
    # 2. Install modules
    yarn  // prefered

    # or
    npm install

Link third party libraries

    react-native link


## Windows/Linux

Install `node.js`, `yarn or npm` without the `Homebrew` manager

Go through the procedure, but exclude **IOS** in the bellow section, because you need **MAC** to be able to be able to build on IOS platform. 

# Platforms

# IOS

_**Note: Only MAC users**_

Install `Xcode 9.x` or above

Navigate to `/ios` folder within your project

Install `Cocoapods` dependency manager for Swift 

    sudo gem install cocoapods

Run the following commands:

    pod install
    # or
    pod install --verbose // to get into more details


Open _`.xcworkspace`_ to run the project

_**Note**: There is a .xcproject file, this should be ignored, you only start the project with .xcworkspace_

If you get error during the build process, got to Xcode/Product/Clean and build


# Android

Install `Android Studio 3.x`

Install required `SKD` and `Build tools`

_(For example 23.x SDK with 23.x Build tools ect.)_

_(You can do this when run the application in Android studio, the studio itself will tell which stuff you need to install to be able to run the app)_

## Run project 

You can run the project in 2 ways:
1. Android Studio
2. Terminal

---
### Android Studio

Open simulator menu and create an emulator(if you don't use real device).

Go to start menu

![gradle_closed](resources/android/start_menu.png)

and select

![gradle_closed](resources/android/simulator_menu.png)

Create an emulator to run the applicaion on.

Next, open gradle menu

![gradle_closed](resources/android/gradle_close_menu.png)

From the tab 

![gradle_tab](resources/android/gradle_open_menu.png)

Synchronize the gradle

![gradle_sync](resources/android/gradle_refresh.png)

After the sync in finished, press `play` button on the top

![gradle_sync](resources/android/start_menu.png)

---

### Terminal

Make sure you have **_device/emulator_** up and running

Run the following command:

    react-native run-android --variant=productionDebug

_(**Note:** This is the same as pressing play button in Android studio)_


# More info

For more info, go to [Build_process](Build_process.md)