# Build process

Documentation about the build process and useful commands to be used to speed up development/release process.

Following documentation also for creating release builds.

- [Development](#Development)
    - [Android](#Android)
         - [Debug](#Debug)
         - [Release](#Release)
    - [IOS](#IOS)
- [Create Production build](#Create\ production\ build)
    - [Android](#For\ Android)
    - [IOS](#For\ IOS)
- [Troubleshooting](#Troubleshooting)

# Development

---

## Android

There are 2 things that we need to consider: `ProductFlavors` and `BuildType`

Combination of these two, gives us a `BUILD_VARIANT`

For example, combination of **_environment=dev_** and **_buildType=debug_**, gives us **_buildVariant=devDebug_**.

This means we build a app, with dev environment configuration, in debug mode.

---

### Debug

Run the following command `react-native run-android --variant=<flavour>Debug`

For example, one of the following

    # For dev
    react-native run-android --variant=devDebug 

    # For stage
    react-native run-android --variant=stageDebug

    # For production
    react-native run-android --variant=productionDebug


### Release

Run the following command `react-native run-android --variant=<flavour>Release`

For example, one of the following

    # For dev
    react-native run-android --variant=devRelease

    # For stage
    react-native run-android --variant=stageRelease

    # For production
    react-native run-android --variant=productionRelease

---

## IOS

For IOS, the difference from Android is that we use `SCHEMAS` instead of `BUILD_VARIANTS`

The two things here are: `Schema` and `Build Configuration`

The combination of the two gives us desired build configuration.

To select a schema, open **Xcode->Open schema menager**, and select one of the schemas

![schema_manager](resources/ios/schema_manager_simulator.png)

To change the default configuration

![build_configuration](resources/ios/build_configuration_edit.png)

To distribute Ad Hoc Build, got **Window -> Organizer -> Distribute App -> Ad Hoc**

# Create production build

# For Android

Go to `<project_folder>/android/app` and open `build.gradle`

Increment `versionCode` by 1

Update versionName `x.x.x`

Create bundle for our app with following `command`

    react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/res

Make sure you have the release-keysore.keystore file located in `app/android/` 

_(**Note:** You can get it from Shared folder on Google Drive)_

Create the `.apk` file with the command `cd android && ./gradlew assemble<environment>Release`

    # Choose one of the following commands:

    # For dev
    cd android && ./gradlew assembleDevRelease

    # For stage
    cd android && ./gradlew assembleStageRelease

    # For production
    cd android && ./gradlew assembleProductionRelease   

After everything is successful, you can find the `.apk file` in `android/app/build/outputs/apk/<environment>/<build_type>/`

# For IOS

Open **General** in Project navigation

![build_configuration](resources/ios/release_configuration.png)

Increase `Version`

Increase `Build` by one

![build_configuration](resources/ios/version_code.png)

Change the simultator from simulator menu to `Generic IOS DEVICE`

![build_configuration](resources/ios/archive_simulator.png)

Go to **Product -> Archive**, and wait for the archive to finish.

Next, **Window -> Organizer** will open, where you can locate your build

Click on **Distribute App -> IOS App Store** and finish the process

# Troubleshooting